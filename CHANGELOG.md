# Changelog

All notable changes to [ppr_prodige_admin](https://gitlab.adullact.net/prodige/ppr_prodige_admin) project will be documented in this file.

## [5.0.182](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.181...5.0.182) - 2025-03-04

## [5.0.181](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.180...5.0.181) - 2025-02-28

## [5.0.180](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.179...5.0.180) - 2025-02-27

## [5.0.179](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.178...5.0.179) - 2025-02-25

## [5.0.178](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.177...5.0.178) - 2025-02-25

## [5.0.177](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.176...5.0.177) - 2025-02-24

## [5.0.176](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.175...5.0.176) - 2025-02-21

## [5.0.175](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.174...5.0.175) - 2025-02-20

## [5.0.174](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.173...5.0.174) - 2025-02-20

## [5.0.173](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.172...5.0.173) - 2025-02-14

## [5.0.172](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.171...5.0.172) - 2025-02-13

## [5.0.171](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.170...5.0.171) - 2025-02-12

## [5.0.170](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.169...5.0.170) - 2025-02-12

## [5.0.169](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.168...5.0.169) - 2025-02-12

## [5.0.168](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.167...5.0.168) - 2025-02-11

## [5.0.167](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.166...5.0.167) - 2025-02-10

## [5.0.166](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.165...5.0.166) - 2025-02-06

## [5.0.165](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.164...5.0.165) - 2025-02-04

## [5.0.165](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.163...5.0.165) - 2025-02-04

## [5.0.165](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.163...5.0.165) - 2025-02-04

## [5.0.165](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.163...5.0.165) - 2025-01-13

## [5.0.164](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.163...5.0.164) - 2025-01-13

## [5.0.163](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.162...5.0.163) - 2025-01-10

## [5.0.162](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.161...5.0.162) - 2025-01-08

## [5.0.161](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.160...5.0.161) - 2024-12-31

## [5.0.160](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.159...5.0.160) - 2024-12-18

## [5.0.159](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.158...5.0.159) - 2024-12-18

## [5.0.158](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.157...5.0.158) - 2024-12-12

## [5.0.157](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.156...5.0.157) - 2024-12-12

## [5.0.156](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.155...5.0.156) - 2024-12-11

## [5.0.155](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.154...5.0.155) - 2024-12-11

## [5.0.154](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.153...5.0.154) - 2024-12-10

## [5.0.153](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.152...5.0.153) - 2024-12-10

## [5.0.152](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.151...5.0.152) - 2024-12-10

## [5.0.151](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.150...5.0.151) - 2024-12-05

## [5.0.150](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.149...5.0.150) - 2024-12-05

## [5.0.149](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.148...5.0.149) - 2024-12-04

## [5.0.148](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.147...5.0.148) - 2024-12-04

## [5.0.147](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.146...5.0.147) - 2024-12-04

## [5.0.146](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.145...5.0.146) - 2024-12-04

## [5.0.145](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.144...5.0.145) - 2024-12-04

## [5.0.144](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.143...5.0.144) - 2024-12-04

## [5.0.143](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.142...5.0.143) - 2024-12-02

## [5.0.142](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.141...5.0.142) - 2024-12-02

## [5.0.141](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.140...5.0.141) - 2024-11-29

## [5.0.140](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.139...5.0.140) - 2024-11-29

## [5.0.139](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.138...5.0.139) - 2024-11-26

## [5.0.138](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.137...5.0.138) - 2024-11-26

## [5.0.137](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.136...5.0.137) - 2024-11-26

## [5.0.136](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.135...5.0.136) - 2024-11-21

## [5.0.135](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.134...5.0.135) - 2024-11-21

## [5.0.134](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.133...5.0.134) - 2024-11-21

## [5.0.133](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.132...5.0.133) - 2024-11-20

## [5.0.132](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.131...5.0.132) - 2024-11-15

## [5.0.131](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.130...5.0.131) - 2024-11-15

## [5.0.130](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.129...5.0.130) - 2024-11-15

## [5.0.129](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.128...5.0.129) - 2024-11-15

## [5.0.128](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.127...5.0.128) - 2024-11-14

## [5.0.127](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.126...5.0.127) - 2024-10-25

## [5.0.126](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.125...5.0.126) - 2024-10-25

## [5.0.125](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.124...5.0.125) - 2024-10-21

## [5.0.124](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.123...5.0.124) - 2024-10-21

## [5.0.123](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.122...5.0.123) - 2024-10-18

## [5.0.122](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.121...5.0.122) - 2024-09-19

## [5.0.121](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.120...5.0.121) - 2024-09-10

## [5.0.120](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.119...5.0.120) - 2024-09-06

## [5.0.119](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.1.6...5.0.119) - 2024-08-27

## [5.0.1.6](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.1.5...5.0.1.6) - 2024-08-22

## [5.0.1.5](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.1.4...5.0.1.5) - 2024-08-22

## [5.0.1.4](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.1.3...5.0.1.4) - 2024-08-22

## [5.0.1.3](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.1.2...5.0.1.3) - 2024-08-21

## [5.0.1.2](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.1.1...5.0.1.2) - 2024-08-20

## [5.0.1.1](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.118...5.0.1.1) - 2024-08-05

## [5.0.118](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.117...5.0.118) - 2024-07-09

## [5.0.117](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.116...5.0.117) - 2024-06-21

## [5.0.116](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.115...5.0.116) - 2024-06-14

### Feat

- make ip filter info available through internal/me

## [5.0.115](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.114...5.0.115) - 2024-06-13

## [5.0.114](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.113...5.0.114) - 2024-06-12

## [5.0.113](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.112...5.0.113) - 2024-06-12

## [5.0.112](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.111...5.0.112) - 2024-06-12

## [5.0.111](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.110...5.0.111) - 2024-06-11

## [5.0.110](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.109...5.0.110) - 2024-06-11

### Add

- secure info in verify right internal output
- secure info in verify right internal output

## [5.0.109](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.108...5.0.109) - 2024-06-11

## [5.0.108](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.107...5.0.108) - 2024-06-10

## [5.0.107](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.106...5.0.107) - 2024-06-10

## [5.0.106](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.105...5.0.106) - 2024-06-10

## [5.0.105](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.104...5.0.105) - 2024-06-10

### Add

- support for secure wxs field in Layer entity
- support for secure wxs field in Layer entity

## [5.0.104](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.103...5.0.104) - 2024-06-05

## [5.0.103](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.102...5.0.103) - 2024-06-04

## [5.0.102](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.101...5.0.102) - 2024-06-04

## [5.0.101](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.100...5.0.101) - 2024-06-04

## [5.0.100](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.99...5.0.100) - 2024-06-03

## [5.0.99](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.98...5.0.99) - 2024-06-03

## [5.0.98](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.97...5.0.98) - 2024-05-30

## [5.0.97](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.96...5.0.97) - 2024-05-21

## [5.0.96](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.95...5.0.96) - 2024-05-07

## [5.0.95](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.94...5.0.95) - 2024-05-07

## [5.0.94](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.93...5.0.94) - 2024-05-07

## [5.0.93](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.92...5.0.93) - 2024-04-09

## [5.0.92](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.91...5.0.92) - 2024-04-09

## [5.0.91](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.90...5.0.91) - 2024-04-09

## [5.0.90](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.89...5.0.90) - 2024-04-08

## [5.0.89](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.88...5.0.89) - 2024-04-08

## [5.0.88](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.87...5.0.88) - 2024-04-08

## [5.0.87](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.86...5.0.87) - 2024-04-08

## [5.0.86](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.85...5.0.86) - 2024-04-05

## [5.0.85](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.84...5.0.85) - 2024-03-21

## [5.0.84](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.83...5.0.84) - 2024-03-21

## [5.0.83](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.82...5.0.83) - 2024-03-20

## [5.0.82](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.81...5.0.82) - 2024-03-20

## [5.0.81](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.80...5.0.81) - 2024-03-15

## [5.0.80](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.79...5.0.80) - 2024-03-08

## [5.0.79](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.78...5.0.79) - 2024-03-08

## [5.0.78](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.77...5.0.78) - 2024-03-08

## [5.0.77](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.76...5.0.77) - 2024-02-27

## [5.0.76](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.75...5.0.76) - 2024-02-20

## [5.0.75](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.74...5.0.75) - 2024-02-09

## [5.0.74](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.73...5.0.74) - 2024-02-01

## [5.0.73](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.72...5.0.73) - 2024-01-31

## [5.0.72](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.71...5.0.72) - 2024-01-25

## [5.0.71](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.70...5.0.71) - 2024-01-23

## [5.0.70](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.69...5.0.70) - 2024-01-19

## [5.0.69](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.68...5.0.69) - 2024-01-18

## [5.0.68](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.67...5.0.68) - 2024-01-18

## [5.0.67](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.66...5.0.67) - 2024-01-18

## [5.0.66](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.65...5.0.66) - 2024-01-18

## [5.0.65](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.64...5.0.65) - 2024-01-15

## [5.0.64](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.63...5.0.64) - 2024-01-09

## [5.0.63](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.62...5.0.63) - 2024-01-08

## [5.0.62](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.61...5.0.62) - 2024-01-03

## [5.0.61](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.60...5.0.61) - 2024-01-02

## [5.0.60](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.59...5.0.60) - 2023-12-21

## [5.0.59](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.58...5.0.59) - 2023-12-21

## [5.0.58](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.57...5.0.58) - 2023-12-20

## [5.0.57](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.56...5.0.57) - 2023-12-20

## [5.0.56](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.55...5.0.56) - 2023-12-20

## [5.0.55](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.54...5.0.55) - 2023-12-19

## [5.0.54](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.53...5.0.54) - 2023-12-18

## [5.0.53](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.52...5.0.53) - 2023-12-12

## [5.0.52](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.51...5.0.52) - 2023-12-06

## [5.0.51](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.50...5.0.51) - 2023-10-06

## [5.0.50](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.49...5.0.50) - 2023-09-29

## [5.0.49](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.48...5.0.49) - 2023-09-15

## [5.0.48](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.47...5.0.48) - 2023-08-30

## [5.0.47](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.46...5.0.47) - 2023-08-23

## [5.0.46](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.45...5.0.46) - 2023-08-23

## [5.0.45](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.44...5.0.45) - 2023-08-18

## [5.0.44](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.43...5.0.44) - 2023-07-21

## [5.0.43](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.42...5.0.43) - 2023-07-21

## [5.0.42](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.41...5.0.42) - 2023-07-19

## [5.0.41](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.40...5.0.41) - 2023-07-19

## [5.0.40](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.39...5.0.40) - 2023-07-19

## [5.0.39](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.38...5.0.39) - 2023-07-18

## [5.0.38](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.37...5.0.38) - 2023-07-18

## [5.0.37](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.36...5.0.37) - 2023-07-17

## [5.0.36](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.35...5.0.36) - 2023-07-17

## [5.0.35](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.34...5.0.35) - 2023-07-17

## [5.0.34](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.33...5.0.34) - 2023-07-13

## [5.0.33](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.32...5.0.33) - 2023-07-11

## [5.0.32](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.31...5.0.32) - 2023-07-11

## [5.0.31](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.30...5.0.31) - 2023-07-07

## [5.0.30](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.29...5.0.30) - 2023-06-16

## [5.0.29](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.28...5.0.29) - 2023-06-16

## [5.0.28](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.27...5.0.28) - 2023-06-06

## [5.0.27](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.26...5.0.27) - 2023-05-31

## [5.0.26](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.25...5.0.26) - 2023-05-31

## [5.0.25](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.24...5.0.25) - 2023-05-17

## [5.0.24](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.23...5.0.24) - 2023-05-15

## [5.0.23](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.22...5.0.23) - 2023-05-04

## [5.0.22](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.21...5.0.22) - 2023-04-11

## [5.0.21](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.20...5.0.21) - 2023-03-22

## [5.0.20](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.19...5.0.20) - 2023-03-21

## [5.0.19](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.18...5.0.19) - 2023-03-21

## [5.0.18](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.17...5.0.18) - 2023-03-20

## [5.0.17](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.16...5.0.17) - 2023-03-15

## [5.0.16](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.15...5.0.16) - 2023-03-07

## [5.0.15](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.14...5.0.15) - 2023-03-07

## [5.0.14](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.13...5.0.14) - 2023-03-06

## [5.0.13](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.12...5.0.13) - 2023-03-06

## [5.0.12](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.11...5.0.12) - 2023-03-06

## [5.0.11](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.10...5.0.11) - 2023-03-06

## [5.0.10](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.9...5.0.10) - 2023-03-06

## [5.0.9](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.8...5.0.9) - 2023-03-06

## [5.0.8](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.7...5.0.8) - 2023-03-06

## [5.0.7](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.6...5.0.7) - 2023-03-03

## [5.0.6](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.5...5.0.6) - 2023-03-03

## [5.0.5](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.4...5.0.5) - 2023-03-03

## [5.0.4](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.3...5.0.4) - 2023-03-02

## [5.0.3](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.2...5.0.3) - 2023-03-02

## [5.0.2](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.1...5.0.2) - 2023-03-01

## [5.0.1](https://gitlab.adullact.net/prodige/ppr_prodige_admin/compare/5.0.0...5.0.1) - 2023-03-01

