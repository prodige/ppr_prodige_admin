#!/bin/sh
set -e

# Source env file
SCRIPT=`readlink -f $0`
SCRIPTDIR=`dirname $SCRIPT`
SCRIPTNAME=`basename $SCRIPT`
. $SCRIPTDIR/env.sh

mv ${SCRIPTDIR}/../../../site/.env.docker-dev ${SCRIPTDIR}/../../../site/.env.local
