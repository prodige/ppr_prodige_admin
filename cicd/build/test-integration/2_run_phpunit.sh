#!/bin/sh
set -e

# Source env file
SCRIPT=`readlink -f $0`
SCRIPTDIR=`dirname $SCRIPT`/..
SCRIPTNAME=`basename $SCRIPT`
. $SCRIPTDIR/env.sh

echo -n "LOCAL_USER_ID=$(id -u)\nLOCAL_GROUP_ID=$(id -g)" > $SCRIPTDIR/docker-build/docker-compose.env;

# Create dockers
docker-compose --env-file $SCRIPTDIR/test-integration/env.sh -f $SCRIPTDIR/docker-build/docker-compose.yml -f $SCRIPTDIR/test-integration/docker-compose.network.yml build
docker-compose --env-file $SCRIPTDIR/test-integration/env.sh -f $SCRIPTDIR/docker-build/docker-compose.yml -f $SCRIPTDIR/test-integration/docker-compose.network.yml up -d

# Exec
sleep 1

docker exec ppr_prodige_admin_build_web /bin/bash -c " \
  set -e;
  apt update && apt install -y php8.1-xdebug"

# POC
#docker exec --user www-data -w /var/www/html/site ppr_prodige_admin_build_web /bin/bash -c " \
#  set -e;
#  XDEBUG_MODE=coverage php vendor/bin/phpunit \
#  --coverage-clover ../report/coverage.xml \
#  --coverage-filter src \
#  --log-junit ../report/tests.xml "

docker exec --user www-data -w /var/www/html/site ppr_prodige_admin_build_web /bin/bash -c " \
  set -e;
  php bin/console d:s:v --skip-sync "

# Lint
docker exec --user www-data -w /var/www/html/site ppr_prodige_admin_build_web /bin/bash -c " \
  set -e;
  php bin/console lint:yaml config/ --parse-tags "

# Stop dockers
docker-compose -f $SCRIPTDIR/docker-build/docker-compose.yml down
