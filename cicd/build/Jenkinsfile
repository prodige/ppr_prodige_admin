pipeline {
  agent any
  options {
    timestamps ()
    disableConcurrentBuilds()
    gitLabConnection('gitlab_api_jenkins')
    gitlabBuilds(builds: ['0 - Fetch dependencies','5 - Make release','6 - Make docker','7 - Test docker'])
  }
  stages {
    stage('0 - Fetch dependencies') {
      environment {
        NEXUS = credentials('Nexus')
      }
      agent any
      steps {
        updateGitlabCommitStatus name: '0 - Fetch dependencies', state: 'running'
        sh 'cicd/build/0_fetch-dependencies.sh'
        stash includes: 'site/vendor/', name: 'vendor', useDefaultExcludes: false, allowEmpty: true
        stash includes: 'site/public/bundles/', name: 'bundles', useDefaultExcludes: false, allowEmpty: true
      }
      post {
        success { updateGitlabCommitStatus name: '0 - Fetch dependencies', state: 'success'  }
        failure { updateGitlabCommitStatus name: '0 - Fetch dependencies', state: 'failed'   }
        aborted { updateGitlabCommitStatus name: '0 - Fetch dependencies', state: 'canceled' }
      }
    }
    stage('5 - Make release') {
      agent any
      steps {
        updateGitlabCommitStatus name: '5 - Make release', state: 'running'
        unstash 'vendor'
        unstash 'bundles'
        sh 'cicd/build/5_make_release.sh'
        stash includes: 'jenkins_release/', name: 'jenkins_release', useDefaultExcludes: false, allowEmpty: true
      }
      post {
        success { updateGitlabCommitStatus name: '5 - Make release', state: 'success'  }
        failure { updateGitlabCommitStatus name: '5 - Make release', state: 'failed'   }
        aborted { updateGitlabCommitStatus name: '5 - Make release', state: 'canceled' }
      }
    }
    stage('6 - Make docker') {
      agent any
      steps {
        updateGitlabCommitStatus name: '6 - Make docker', state: 'running'
        unstash 'jenkins_release'
        sh 'cicd/build/6_make_docker.sh'
        stash includes: 'jenkins_release/', name: 'jenkins_release', useDefaultExcludes: false, allowEmpty: true
      }
      post {
        success { updateGitlabCommitStatus name: '6 - Make docker', state: 'success'  }
        failure { updateGitlabCommitStatus name: '6 - Make docker', state: 'failed'   }
        aborted { updateGitlabCommitStatus name: '6 - Make docker', state: 'canceled' }
      }
    }
    stage('7 - Test docker') {
      agent any
      steps {
        updateGitlabCommitStatus name: '7 - Test docker', state: 'running'
        sh '/bin/bash cicd/build/7_test_docker.sh'
        stash includes: 'trivy/cve_report.html', name: 'cve_report'
      }
      post {
        success { updateGitlabCommitStatus name: '7 - Test docker', state: 'success'  }
        failure { updateGitlabCommitStatus name: '7 - Test docker', state: 'failed'   }
        aborted { updateGitlabCommitStatus name: '7 - Test docker', state: 'canceled' }
      }
    }
  }
  post {
    always { sh 'cicd/build/99_clean.sh' }
    success {
      unstash 'cve_report'
      publishHTML (target: [
        allowMissing: false,
        alwaysLinkToLastBuild: false,
        keepAll: true,
        reportDir: '.',
        reportFiles: 'trivy/cve_report.html',
        reportName: 'CVE Report'
      ])
    }
  }
}
