# README
<!-- TOC -->

- [README](#readme)
  - [Overview](#overview)
  - [Start development](#start-development)
    - [Change mode dev, prod](#change-mode-dev-prod)
    - [Run unit test](#run-unit-test)
  - [Stop development](#stop-development)
  - [General Docker commands](#general-docker-commands)
  - [Advanced Docker commands](#advanced-docker-commands)
    - [Clean commands](#clean-commands)
    - [Monitoring commands](#monitoring-commands)

<!-- /TOC -->

## Overview

This file give commands to start development with docker.

Require:
 - Docker installed.
 - Docker well configured, cf. **/etc/docker/deamon.json**
 - User in docker group to avoid sudo/su right

## Start development

Make these commands in this directory `cd ./cicd/dev`.

Run container
```bash
# Login on Alkante docker registry
docker login docker.alkante.com

# Put UID and GID in env file
echo -e "LOCAL_USER_ID=$(id -u)\nLOCAL_GROUP_ID=$(id -g)" > docker-compose.env;

# Build dev image
docker-compose build

# Run containers
docker-compose up
```

In an other terminal, connect as www-data to the dev container
```bash
# Connect to container composer install as www-data
docker exec --user www-data -w /var/www/html/site -it ppr_prodige_admin_dev_web bash
```

In container
```bash
# Install with composer
composer install

# Migrate database
php ./bin/console doctrine:migrations:migrate --no-interaction

#Clear cache
php ./bin/console cache:clear --env dev
php ./bin/console cache:clear --env prod

# Follow symfony logs
tail -f var/log/dev.log
```

The url web are: http://localhost:4200

### Override Apache config:
/!\\ Not required, there is already a functional config for each environment
Create "conf" folder in the home project folder, with one or both of the following files :
- "conf/apache_virtualhost.conf" : Is included at the root of the "Virtualhost"
- "conf/apache_directory.conf" : Is included in the "Directory" part (example for sf3 : /var/www/html/site/web)

Minimum configuration :
- "conf/apache_virtualhost.conf" :
```
DirectoryIndex index.php
```
- "conf/apache_directory.conf" :
```
Require all granted
```
In the apache files you can get the directory with the variable : $HOME_SITE
```
Define HOME_SITE "/var/www/html"
```

### Change mode dev, prod
Edit the `./site/.env`

```bash
# mode dev
APP_ENV=dev
# mode prod
APP_ENV=prod
```

### Run unit test

```bash
# Run unit test
php bin/phpunit
```

## Stop development

Make these commands in this directory `cd ./cicd/dev`.

```bash
# Shutdown containers
docker-compose down

# Remove dev image
docker rmi ppr_prodige_admin_dev_web
```

## General Docker commands

| Description                    | Commandes                                                                                    |
| ------------------------------ | -------------------------------------------------------------------------------------------- |
| Display images                 | `docker images`                                                                              |
| Remove image                   | `docker rmi myimages`                                                                        |
| ------------------------------ | -------------------------------------------------------------------------------------------- |
| Display all running containers | `docker ps`                                                                                  |
| Display all containers         | `docker ps -a`                                                                               |
| Remove container               | `docker rm mycontainer`                                                                      |
| Display info container         | `docker inpect mycontainer`                                                                  |
| Display container output       | `docker logs mycontainer`                                                                    |
| Stop container                 | `docker stop mycontainer`                                                                    |
| Kill container                 | `docker kill mycontainer`                                                                    |
| ------------------------------ | -------------------------------------------------------------------------------------------- |
| Connection as root             | `docker exec --user 0 -w / -it alkante-template_dev_web bash`                                |
| Connection as www-data         | `docker exec --user www-data -w /var/www/html -it alkante-template_dev_web bash`             |
| Run composer install           | `docker exec --user www-data -w /var/www/html -it alkante-template_dev_web composer install` |

## Advanced Docker commands


### Clean commands
```bash
# Stop all container
docker stop $(docker ps -a -q)

# Remove all container
docker rm $(docker ps -a -q)

# Delete all image with name or tag as "<none>"
docker rmi `docker images| egrep "<none>" |awk '{print $3}'`
```

### Monitoring commands

```bash
# Watch all container
watch -n 1 "docker ps -a"

# Watch all images
watch -n 1 "docker images"

# Watch all resources container
docker stats
```
