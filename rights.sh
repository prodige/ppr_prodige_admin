#!/bin/bash
set -e
SCRIPT=`readlink -f $0`
SCRIPTDIR=`dirname $(readlink -f $0)`
SCRIPTNAME=`basename $SCRIPT`
# Param
PARAM_USER="${1}"
PARAM_GROUP="${2}"

# Default value:
OWNER="ansible-dev"
APACHE="www-data"

# User :
if [[ -n "${PARAM_USER}" ]] && [[ $(getent passwd "${PARAM_USER}") ]]; then
    # cas en parametre du script
    OWNER="${PARAM_USER}"
else
  if [[ ! -z ${LOCAL_USER_ID} ]] && [[ $(getent passwd "${LOCAL_USER_ID}") ]]; then
    # cas docker (variable du docker-compose.env)
    OWNER=`id -un "${LOCAL_USER_ID}"`
  else
    if [[ ! $(getent passwd "${OWNER}") ]]; then
      # cas sans ansible-dev
      OWNER="ftpuser"
      if [[ ! $(getent passwd "${OWNER}") ]]; then
        # cas sans ftpuser
        OWNER="www-data"
      fi
    fi
  fi
fi
echo "${SCRIPTNAME}: User = ${OWNER}"
# Group
if [[ -n "${PARAM_GROUP}" ]] && [[ $(getent group "${PARAM_GROUP}") ]]; then
    # cas en parametre du script
    APACHE="${PARAM_GROUP}"
else
  if [[ ! -z ${LOCAL_GROUP_ID} ]] && [[ $(getent group "${LOCAL_GROUP_ID}") ]]; then
    # cas docker (variable du docker-compose.env)
    APACHE=`grep ${LOCAL_GROUP_ID} /etc/group |awk -F':' '{print $1}'`
  fi
fi
echo "${SCRIPTNAME}: Group = ${APACHE}"


rights()
{
    if [ -d "${1}" ]
    then
        echo "Chowning ${1}"
        find "${1}" -print0 | xargs -0 chown ${2}.${3}
        if [ "${4}" == "ro" ]; then
            echo "Chmoding ${1} directories ${4}"
            find "${1}" -type d -print0 | xargs -0 chmod 750
            echo "Chmoding ${1} files ${4}"
            find "${1}" -type f -print0 | xargs -0 chmod 640
        fi
        if [ "${4}" == "rx" ]; then
            echo "Chmoding ${1} directories ${4}"
            find "${1}" -type d -print0 | xargs -0 chmod 750
            echo "Chmoding ${1} files ${4}"
            find "${1}" -type f -print0 | xargs -0 chmod 750
        fi
        if [ "${4}" == "rw" ]; then
            echo "Chmoding ${1} directories ${4}"
            find "${1}" -type d -print0 | xargs -0 chmod 770
            echo "Chmoding ${1} files ${4}"
            find "${1}" -type f -print0 | xargs -0 chmod 660
        fi
        if [ "${4}" == "rwx" ]; then
            echo "Chmoding ${1} directories ${4}"
            find "${1}" -type d -print0 | xargs -0 chmod 770
            echo "Chmoding ${1} files ${4}"
            find "${1}" -type f -print0 | xargs -0 chmod 770
        fi
    fi
}

chmod 755 "${SCRIPTDIR}"
chown ${OWNER}.${APACHE} "${SCRIPTDIR}/site"
rights "${SCRIPTDIR}/site" "${OWNER}" "${APACHE}" "ro"
mkdir -p "${SCRIPTDIR}/site/var"
rights "${SCRIPTDIR}/site/var" "${OWNER}" "${APACHE}" "rw"
rights "${SCRIPTDIR}/site/public/upload/" "${OWNER}" "${APACHE}" "rw"

find "${SCRIPTDIR}/site" -maxdepth 1 -name "*.sh" -type f -exec chown "${OWNER}"."${ROOT_GRP}" {} \;
find "${SCRIPTDIR}/site" -maxdepth 1 -name "*.sh" -type f -exec chmod 750 {} \;

find "${SCRIPTDIR}/site/bin" -name "*.sh" -type f -exec chown "${OWNER}"."${ROOT_GRP}" {} \;
find "${SCRIPTDIR}/site/bin" -name "*.sh" -type f -exec chmod 750 {} \;
