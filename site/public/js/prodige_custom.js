/**
 * Retrieve url param from name (empty string if not set)
 * @param sParam
 * @returns string
 */
function getUrlParameter(sParam) {
    let sPageURL = window.location.search.substring(1),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? '' : decodeURIComponent(sParameterName[1]);
        }
    }
    return '';
}

/**
 * Get suffix language for localization (datatable)
 * @returns string
 */
function getLanguage() {
    let supportedLanguages = ['en', 'fr'],
        language = (navigator.languages !== undefined) ? navigator.languages[0] : navigator.language;
    return ($.inArray(language, supportedLanguages) !== -1) ? language : 'en';
}

/**
 * Update fieldId & fieldName select in Zoning form
 */
function updateLayerSelect(path, layer, prefix = '') {
    // Disable selects
    $(prefix + '_fieldId, ' + prefix + '_fieldName').attr('disabled', 'disabled').html('');

    if (layer.replace('/api/layers/', '') !== '') {
        // Reload field_id & field_label list
        $.ajax({
            type: "GET",
            url: path,
            headers: {'Authorization': 'Bearer internal', 'Accept': "application/ld+json"},
            success: function (e) {
                if (Object.keys(e.dbFields).length > 0) {
                    $(prefix + '_fieldId, ' + prefix + '_fieldName').removeAttr('disabled');
                    $.each(e.dbFields, function (key, value) {
                        $(prefix + "_fieldId").get(0).options.add(new Option(key));
                        $(prefix + "_fieldName").get(0).options.add(new Option(key));
                        if ($(prefix + "_joinField").length !== 0) {
                            $(prefix + "_joinField").get(0).options.add(new Option(key));
                        }
                    });
                }
            },
            error: function (e) {
                if (e.responseJSON.error !== undefined) {
                    toastr.error(e.responseJSON.error)
                }
            }
        });
    } else {
        $(prefix + "_fieldId, " + prefix + "_fieldName, " + prefix + "_joinField").find('option[value!=""]').remove().end();
        $(prefix + '_joinField').val(null);
    }
}

/**
 * Add subdomain on domain select
 */
function selectSubdomains(e) {
    $('[data-domain="' + e.params.data.element.value + '"]').each(function () {
        let subdomains = $(e.data.selector).val();
        if (!subdomains.includes($(this).val())) {
            subdomains = subdomains.concat($(this).val());
            $(e.data.selector).val(subdomains);
            $(e.data.selector).trigger('change');
        }
    });
}

/**
 * Delete subdomains on domain unselect
 */
function unselectSubdomains(e) {
    let subdomains = $(e.data.selector).val();
    $('[data-domain="' + e.params.data.element.value + '"]').each(function () {
        let index = subdomains.indexOf($(this).val());
        if (index > -1) {
            subdomains.splice(index, 1);
        }
    });
    $(e.data.selector).val(subdomains);
    $(e.data.selector).trigger('change');
}

/**
 * Trigger dynamic activation
 */
triggerActive = function () {
    let attr = $(this).attr('data-trigger-active');
    if ($(this)[0].nodeName.toLowerCase() === 'select') {
        let selectedValue = $(this).val();
        $("[" + attr + "]").each(function () {
            if (selectedValue === $(this).attr('data-trigger-value')) {
                $(this).removeAttr('disabled').attr('required', 'required');
            } else {
                $(this).attr('disabled', 'disabled').removeAttr('required').val('');
            }
        });
    } else {
        let checked = $(this).is(':checked');
        $("[" + attr + "]").each(function () {
            if (checked) {
                $(this).removeAttr('disabled');
            } else {
                $(this).attr('disabled', 'disabled');
            }
        });
    }
}
/**
 * Trigger dynamic display
 */
triggerDisplay = function () {
    let attr = $(this).attr('data-trigger-display');
    let checked = $(this).is(':checked');
    $("[" + attr + "]").each(function () {
        if (checked) {
            $(this).parent().show();
        } else {
            $(this).val('').parent().hide();
        }
    });
}

/**
 * Trigger duallist required select
 */
initDualListBox = function (duallistbox) {
    var instance = duallistbox.data('plugin_bootstrapDualListbox');
    var nonSelectedList = instance.elements.select1;
    var isDualListBoxValidated = !(instance.selectedElements > 0);
    nonSelectedList.prop('required', isDualListBoxValidated);
    instance.elements.originalSelect.prop('required', false);
}
