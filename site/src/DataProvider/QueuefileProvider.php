<?php

declare(strict_types=1);

namespace App\DataProvider;

use ApiPlatform\Core\DataProvider\ContextAwareCollectionDataProviderInterface;
use ApiPlatform\Core\DataProvider\ItemDataProviderInterface;
use ApiPlatform\Core\DataProvider\RestrictedDataProviderInterface;
use App\Entity\Administration\Directory\User\User;
use App\Entity\Configuration\Queuefile;
use App\Service\QueuefileService;
use Doctrine\DBAL\Connection;
use Doctrine\Persistence\ManagerRegistry;
use DateTime;

use function PHPUnit\Framework\isEmpty;


final class QueuefileProvider implements ItemDataProviderInterface, ContextAwareCollectionDataProviderInterface,
                                         RestrictedDataProviderInterface
{

    private Connection $catalogueConnect;

    public function __construct(private QueuefileService $queueService, private ManagerRegistry $doctrine)
    {
        $this->catalogueConnect = $doctrine->getConnection('catalogue');
    }

    public function supports(string $resourceClass, string $operationName = null, array $context = []): bool
    {
        return Queuefile::class === $resourceClass;
    }

    public function getItem(string $resourceClass, $id, string $operationName = null, array $context = []): ?Queuefile
    {
        return $this->queueService->get($id);
    }

    public function getCollection(string $resourceClass, string $operationName = null, array $context = []): array
    {
        $strSql = "select * from telechargement.download WHERE download.queue_name IS NOT NULL ";

        $downloads = $this->catalogueConnect->fetchAllAssociative($strSql);
        $objQueue = [];

        foreach ($downloads as $download) {
            $date = DateTime::createFromFormat('Y-m-d H:i:s', $download['created_at']);

            $queueFile = new Queuefile();
            $queueFile->setId($download['id']);
            $queueFile->setMetadata($download['metadata_uuid']);
            /** @var User $user */
            $user = $this->doctrine->getRepository(User::class)->find($download['user_id']);
            $queueFile->setCaller($user->getUsername());
            $queueFile->setFormat($download['format']);
            $queueFile->setProjection($download['srs']);
            $queueFile->setDate($date->format("F j, Y, g:i a"));
            $queueFile->setFile($download['file']);
            if ($download['direct'] === true) {
                $queueFile->setType('direct');
            } else {
                $queueFile->setType('differed');
            }

            if ($download['extract'] !== "[]") {
                $extract = json_decode($download['extract'], true);
                $queueFile->setExtractionType($extract['type']);
            } else {
                $queueFile->setExtractionType('globale');
            }
            $objQueue[] = $queueFile;
        }

        return $objQueue;
    }

}