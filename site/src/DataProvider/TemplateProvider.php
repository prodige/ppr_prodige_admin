<?php

declare(strict_types=1);

namespace App\DataProvider;

use ApiPlatform\Core\Bridge\Doctrine\Orm\Extension\QueryResultCollectionExtensionInterface;
use ApiPlatform\Core\DataProvider\ContextAwareCollectionDataProviderInterface;
use ApiPlatform\Core\DataProvider\ItemDataProviderInterface;
use ApiPlatform\Core\DataProvider\RestrictedDataProviderInterface;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Extension\QueryResultItemExtensionInterface;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Util\QueryNameGenerator;
use ApiPlatform\Core\DataProvider\SerializerAwareDataProviderInterface;
use ApiPlatform\Core\DataProvider\SerializerAwareDataProviderTrait;
use App\Entity\Configuration\Projection;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Paginator as ApiPlatformPaginator;
use App\Entity\Configuration\Template;
use App\Service\SpatialService;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;


final class TemplateProvider implements ItemDataProviderInterface,
    RestrictedDataProviderInterface,
    SerializerAwareDataProviderInterface,
    ContextAwareCollectionDataProviderInterface
{
    use SerializerAwareDataProviderTrait;

    public function __construct(
        private ManagerRegistry $managerRegistry,
        private iterable        $itemExtensions,
        private iterable        $collectionExtensions,
    )
    {
    }

    public function supports(string $resourceClass, string $operationName = null, array $context = []): bool
    {
        return Template::class === $resourceClass;
    }

    /**
     * @param string $resourceClass
     * @param $id
     * @param string|null $operationName
     * @param array $context
     * @return Template|null
     * @throws NonUniqueResultException
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     */
    public function getItem(string $resourceClass, $id, string $operationName = null, array $context = []): ?Template
    {
        $manager = $this->managerRegistry->getManagerForClass($resourceClass);
        $repository = $manager->getRepository($resourceClass);
        $queryBuilder = $repository->createQueryBuilder('l')
            ->where('l.id = :id')
            ->setParameter('id', $id);

        $template = $queryBuilder->getQuery()->getOneOrNullResult();
        if (!is_null($template)) {
            // Call Prodige.carmen.map to get uuid
            try {
                $mapfile = explode('.', $template->getMapfile());
                $query = $this->managerRegistry->getConnection('prodige')->executeQuery(
                    'SELECT map_wmsmetadata_uuid FROM carmen.map where map_file = ? group by map_wmsmetadata_uuid',
                    [$mapfile[0]]
                );
                $uuid = $query->fetchAssociative();
                if (is_array($uuid)) {
                    $template->setUuid($uuid['map_wmsmetadata_uuid']);
                }
            } catch (\Exception) {
                return null;
            }
        }
        return $template;
    }

    public function getCollection(string $resourceClass, string $operationName = null, array $context = []): iterable
    {
        $manager = $this->managerRegistry->getManagerForClass($resourceClass);
        $repository = $manager->getRepository($resourceClass);
        $queryBuilder = $repository->createQueryBuilder('e');
        $queryNameGenerator = new QueryNameGenerator();

        try {
            $query = $this->managerRegistry->getConnection('prodige')->executeQuery(
                'SELECT map_file, map_wmsmetadata_uuid FROM carmen.map where published_id is null and map_file like \'MODEL_%\' group by map_file, map_wmsmetadata_uuid '
            );
            $uuids = [];
            foreach ($query->fetchAllAssociative() as $map) {
                $uuids[$map['map_file']] = $map['map_wmsmetadata_uuid'];
            }
        } catch (\Exception) {
        }

        foreach ($this->collectionExtensions as $extension) {
            $extension->applyToCollection($queryBuilder, $queryNameGenerator, $resourceClass, $operationName, $context);

            if ($extension instanceof QueryResultCollectionExtensionInterface && $extension->supportsResult(
                    $resourceClass,
                    $operationName,
                    $context
                )) {
                $queryBuilder->setMaxResults(1000);
                $templateDB = $extension->getResult($queryBuilder, $resourceClass, $operationName, $context);
                $templates = [];
                foreach ($templateDB as $item) {
                    $mapfile = explode('.', $item->getMapfile());
                    if (array_key_exists($mapfile[0], $uuids)) {
                        $item->setUuid($uuids[$mapfile[0]]);
                    }
                    $templates[] = $item;
                }
                return $templates;
            }
        }
        return $queryBuilder->getQuery()->getResult();
    }
}