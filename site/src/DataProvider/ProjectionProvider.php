<?php

declare(strict_types=1);

namespace App\DataProvider;

use ApiPlatform\Core\Bridge\Doctrine\Orm\Extension\QueryResultCollectionExtensionInterface;
use ApiPlatform\Core\DataProvider\ContextAwareCollectionDataProviderInterface;
use ApiPlatform\Core\DataProvider\ItemDataProviderInterface;
use ApiPlatform\Core\DataProvider\RestrictedDataProviderInterface;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Extension\QueryResultItemExtensionInterface;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Util\QueryNameGenerator;
use ApiPlatform\Core\DataProvider\SerializerAwareDataProviderInterface;
use ApiPlatform\Core\DataProvider\SerializerAwareDataProviderTrait;
use App\Entity\Configuration\Projection;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Paginator as ApiPlatformPaginator;
use App\Service\SpatialService;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;


final class ProjectionProvider implements ItemDataProviderInterface,
    RestrictedDataProviderInterface,
    SerializerAwareDataProviderInterface,
    ContextAwareCollectionDataProviderInterface
{
    use SerializerAwareDataProviderTrait;

    public function __construct(
        private ManagerRegistry $managerRegistry,
        private iterable        $itemExtensions,
        private iterable        $collectionExtensions,
        private SpatialService  $spatialService,
    )
    {
    }

    public function supports(string $resourceClass, string $operationName = null, array $context = []): bool
    {
        return Projection::class === $resourceClass;
    }

    /**
     * @param string $resourceClass
     * @param $id
     * @param string|null $operationName
     * @param array $context
     * @return Projection|null
     * @throws ExceptionInterface
     * @throws NonUniqueResultException
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function getItem(string $resourceClass, $id, string $operationName = null, array $context = []): ?Projection
    {
        $manager = $this->managerRegistry->getManagerForClass($resourceClass);
        $repository = $manager->getRepository($resourceClass);
        $queryBuilder = $repository->createQueryBuilder('p')
            ->where('p.epsg = :epsg')
            ->setParameter('epsg', $id);

        $projection = $queryBuilder->getQuery()->getOneOrNullResult();
        if (is_null($projection)) {
            $projection = new Projection();
        }

        // Check in Spatial_ref_sys if projection exist
        $proj = $this->managerRegistry->getConnection('prodige')
            ->fetchAllAssociative(
                'SELECT srid, auth_name as "authName", auth_srid as "authSrid", srtext as "srText", proj4text as "proj4"  
                 FROM public.spatial_ref_sys where srid =:srid',
                ["srid" => $id]
            );

        // Get name for EPSG file
        $name = $this->spatialService->getNameFromEpsg($id);

        if (!is_null($name) && empty($projection->getName())) {
            $proj[0]['name'] = $name;
        }

        if (!empty($proj)) {
            $proj[0]['epsg'] = $id;
        } else {
            // Search par API
            if (!is_null($name)) {
                $projection->setName($name);
            }
            return $this->spatialService->getSpatialRefFromAPI($id, $projection);
        }

        return $this->getSerializer()->denormalize(
            $proj[0],
            Projection::class,
            "array",
            [AbstractNormalizer::OBJECT_TO_POPULATE => $projection]
        );
    }

    public function getCollection(string $resourceClass, string $operationName = null, array $context = []): iterable
    {
        if ($operationName === 'get_spatial_ref') {
            return $this->spatialService->intersectProjection();
        }

        $repository = $this->managerRegistry->getRepository($resourceClass);
        $queryBuilder = $repository->createQueryBuilder('p');
        $queryNameGenerator = new QueryNameGenerator();

        foreach ($this->collectionExtensions as $extension) {
            $extension->applyToCollection(
                $queryBuilder,
                $queryNameGenerator,
                $resourceClass,
                $operationName,
                $context
            );

            if ($extension instanceof QueryResultCollectionExtensionInterface && $extension->supportsResult(
                    $resourceClass,
                    $operationName,
                    $context
                )) {
                return $extension->getResult($queryBuilder, $resourceClass, $operationName, $context);
            }
        }
        return new ApiPlatformPaginator(new Paginator($queryBuilder));
    }

}