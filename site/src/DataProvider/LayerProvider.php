<?php

declare(strict_types=1);

namespace App\DataProvider;

use ApiPlatform\Core\Bridge\Doctrine\Orm\Extension\QueryResultCollectionExtensionInterface;
use ApiPlatform\Core\DataProvider\ContextAwareCollectionDataProviderInterface;
use ApiPlatform\Core\DataProvider\ItemDataProviderInterface;
use ApiPlatform\Core\DataProvider\RestrictedDataProviderInterface;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Extension\QueryResultItemExtensionInterface;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Util\QueryNameGenerator;
use ApiPlatform\Core\DataProvider\SerializerAwareDataProviderInterface;
use ApiPlatform\Core\DataProvider\SerializerAwareDataProviderTrait;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Paginator as ApiPlatformPaginator;
use App\Entity\Administration\Resource\Dataviz;
use App\Entity\Administration\Resource\Layer;
use App\Service\CatalogueService;
use Doctrine\DBAL\Exception;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;


final class LayerProvider implements ItemDataProviderInterface,
                                     RestrictedDataProviderInterface,
                                     SerializerAwareDataProviderInterface
{
    use SerializerAwareDataProviderTrait;

    public function __construct(
        private ManagerRegistry $managerRegistry,
        private iterable $itemExtensions,
        private CatalogueService $catalogueService
    ) {
    }

    public function supports(string $resourceClass, string $operationName = null, array $context = []): bool
    {
        return Layer::class === $resourceClass;
    }

    /**
     * @param string $resourceClass
     * @param array|int|object|string $id
     * @param string|null $operationName
     * @param array $context
     * @return Layer|null
     * @throws NonUniqueResultException|Exception
     */
    public function getItem(string $resourceClass, $id, string $operationName = null, array $context = []): ?Layer
    {
        $manager = $this->managerRegistry->getManagerForClass($resourceClass);
        $repository = $manager->getRepository($resourceClass);
        $queryBuilder = $repository->createQueryBuilder('l')
            ->where('l.id = :id')
            ->setParameter('id', $id);

        /** @var Layer $layer */
        $layer = $queryBuilder->getQuery()->getOneOrNullResult();

        if ($layer !== null) {
            $uuid = $this->catalogueService->getUuidFromMetadata($layer);
            if ($uuid !== false) {
                $layer->setMetadataUuid($uuid);
            }

            if ($operationName === 'get_fields') {
                $layer->setDbFields($this->catalogueService->getFields($layer->getStoragePath()));
            }
        }

        return $layer;
    }

    public function getCollection(string $resourceClass, string $operationName = null, array $context = []): array
    {
        return $this->managerRegistry->getRepository(Layer::class)->findAll();
    }

}