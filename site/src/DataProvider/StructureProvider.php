<?php

namespace App\DataProvider;


use ApiPlatform\Core\DataProvider\ItemDataProviderInterface;
use ApiPlatform\Core\DataProvider\RestrictedDataProviderInterface;
use ApiPlatform\Core\DataProvider\SerializerAwareDataProviderInterface;
use ApiPlatform\Core\DataProvider\SerializerAwareDataProviderTrait;
use App\Entity\Administration\Directory\Structure;
use App\Service\GeonetworkService;
use Doctrine\Persistence\ManagerRegistry;

final class StructureProvider implements ItemDataProviderInterface,
                                         RestrictedDataProviderInterface,
                                         SerializerAwareDataProviderInterface
{

    use SerializerAwareDataProviderTrait;

    public function __construct(
        private ManagerRegistry $managerRegistry,
        private iterable $itemExtensions,
        private GeonetworkService $structureService
    ) {
    }

    public function supports(string $resourceClass, string $operationName = null, array $context = []): bool
    {
        return Structure::class === $resourceClass;
    }

    public function getItem(string $resourceClass, $id, string $operationName = null, array $context = [])
    {
        // TODO: Implement getItem() method.
        $manager = $this->managerRegistry->getManagerForClass($resourceClass);
        $repository = $manager->getRepository($resourceClass);
        $queryBuilder = $repository->createQueryBuilder('d')
            ->where('d.id = :id')
            ->setParameter('id', $id);
        /** @var Structure $structure */
        $structure = $queryBuilder->getQuery()->getOneOrNullResult();

        if($structure !== null){
            if ($structure->getReferenceGn()) {
                $structure->setIsSynchronised(true);
            }
        }

        return $structure;
    }

}