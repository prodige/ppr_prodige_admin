<?php

declare(strict_types=1);

namespace App\DataProvider;

use ApiPlatform\Core\Bridge\Doctrine\Orm\Extension\QueryResultCollectionExtensionInterface;
use ApiPlatform\Core\DataProvider\ContextAwareCollectionDataProviderInterface;
use ApiPlatform\Core\DataProvider\ItemDataProviderInterface;
use ApiPlatform\Core\DataProvider\RestrictedDataProviderInterface;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Extension\QueryResultItemExtensionInterface;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Util\QueryNameGenerator;
use ApiPlatform\Core\DataProvider\SerializerAwareDataProviderInterface;
use ApiPlatform\Core\DataProvider\SerializerAwareDataProviderTrait;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Paginator as ApiPlatformPaginator;
use App\Entity\Administration\Resource\Dataviz;
use App\Entity\Administration\Resource\Layer;
use App\Entity\Configuration\Setting;
use App\Entity\OGCFeatures\OGCCollection;
use App\Service\CatalogueService;
use App\Service\GenerateOGCLinksService;
use App\Service\GeoDataService;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Exception;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Prodige\ProdigeBundle\Services\GeoDataService as ServicesGeoDataService;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;


final class OGCCollectionProvider implements ItemDataProviderInterface,
    RestrictedDataProviderInterface,
    SerializerAwareDataProviderInterface,
    ContextAwareCollectionDataProviderInterface
{
    use SerializerAwareDataProviderTrait;

    private Connection $prodigeCon;

    public function __construct(
        private ManagerRegistry         $managerRegistry,
        private iterable                $itemExtensions,
        private CatalogueService        $catalogueService,
        private GenerateOGCLinksService $ogcLinksService,
        private iterable                $collectionExtensions,
        private LoggerInterface         $logger,
        private RequestStack            $requestStack,
        private ServicesGeoDataService  $geoDataService
    ){
        $this->prodigeCon = $managerRegistry->getConnection('prodige');
        //$this->prodigeCon->executeStatement('set search_path to '.'public, postgis');
    }

    public function supports(string $resourceClass, string $operationName = null, array $context = []): bool
    {
        return OGCCollection::class === $resourceClass;
    }


    /**
     * @param string $resourceClass
     * @param array|int|object|string $id
     * @param string|null $operationName
     * @param array $context
     * @return OGCCollection|null
     * @throws NonUniqueResultException|Exception
     */
    public function getItem(string $resourceClass, $id, string $operationName = null, array $context = []): ?OGCCollection
    {
        $manager = $this->managerRegistry->getManagerForClass($resourceClass);
        $repository = $manager->getRepository($resourceClass);
        $queryBuilder = $repository->createQueryBuilder('l')
            ->where('l.id = :id')
            ->setParameter('id', $id);

        /** @var OGCCollection $collection */
        $collection = $queryBuilder->getQuery()->getOneOrNullResult();

        $this->completeCollectionElements($collection);

        return $collection;
    }

    public function getCollection(string $resourceClass, string $operationName = null, array $context = []): array
    {
        // Generate the OGC-compliant links using the service
        if ('get_items_by_collection' === $operationName) {
            // Add timestamp
            //dd($context);
            $request = $this->requestStack->getCurrentRequest();
            $table_name = $request->get("collectionId");
            $collection = $this->managerRegistry->getRepository(OGCCollection::class)->findBy(["id" => $table_name]);

            if(empty($collection)){
                return [];
            }

            $id = $collection[0]->getSheetMetadata()->getPublicMetadataId();
            $page = intval($request->get('page',1));
            $offset = 0;
            $limit = intval($request->get('itemsPage',30));

            if($page & $limit){
                $offset = ($page-1) * $limit;
            }

            //construct filter
            $tabFilterData = [];
            $dataTable = 'public.' . $table_name;
            $enum = $this->geoDataService->_getAllDataType($dataTable);
            foreach($enum as $enum_=> $enumInfos){
                if($request->get($enumInfos["column_name"], "")!==''){
                    $tabFilterData[$enumInfos["column_name"]] = "'".$request->get($enumInfos["column_name"])."'";
                }
            }

            $filters_key_val = [];
            if($request->get('filter')){
                $filters = explode(";",$request->get('filter'));
                foreach($filters as $filter){
                    $fkv = explode("=", $filter);
                    $filters_key_val[$fkv[0]] = $fkv[1];
                }
            }

            //Check CRS compatibility
            $allowed_crs = $this->ogcLinksService->generateAllowedCrsLinks();
            $crs = $request->get("crs", '');
            if(in_array($crs, $allowed_crs)){
                //Correct crs
                $crs_correct_array = explode('/',$crs);
                $crs = end($crs_correct_array);
            }elseif($crs){
                //Wrong crs
                return ['Error' => "Incorrect parameter crs, must be one of " . implode(',',$allowed_crs)];
            }
            //default force to EPSG:4326
            if($crs===''){
                $crs = '4326';
            }

            $bbox_crs = $request->get("bbox_crs", '');
            if(in_array($bbox_crs, $allowed_crs)){
                //Correct crs
                $crs_correct_array = explode('/',$bbox_crs);
                $bbox_crs = end($crs_correct_array);
            }elseif($bbox_crs){
                //Wrong crs
                return ['Error' => "Incorrect parameter bbox_crs, must be one of " . implode(',',$allowed_crs)];
            }

            $bbox_array = null;
            if($request->get('bbox')){
                $bbox_array = explode(',',$request->get('bbox'));
                if(count($bbox_array) != 4){
                    return ['Error' => "Incorrect parameter bbox, must be of length 4 "];
                }
            }
            

            $items = $this->geoDataService->getData(
                id:$id, 
                bbox:$bbox_array, 
                bbox_crs:$bbox_crs, 
                limit:$limit, 
                offset:$offset,
                spatialfilter:$request->get("spatialfilter"),
                crs:$crs,
                filters:$filters_key_val
            );
            $items["timeStamp"] = date(\DateTime::RFC3339);
            unset($items["name"]);
            unset($items["crs"]);
            $this->addPaginationLinks($offset, $limit, $items);
            $items['links'] = array_merge($items['links'], $this->ogcLinksService->generateItemLinks($table_name));
            unset($items["headers"]);

            foreach($items["features"] as &$feature){
                $feature['id'] = $feature['properties']['gid'];
            }

            return $items;
        }

        // Generate the OGC-compliant links using the service
        if ('get_item_by_collection' === $operationName) {
            // Add timestamp
            //dd($context);
            $request = $this->requestStack->getCurrentRequest();
            $table_name = $request->get("collectionId");
            $collection = $this->managerRegistry->getRepository(OGCCollection::class)->findBy(["id" => $table_name]);

            if(empty($collection)){
                return [];
            }

            $collectionId = $collection[0]->getSheetMetadata()->getPublicMetadataId();

            $filters = ["gid" => $request->get('featureId')];

            $items = $this->geoDataService->getData(
                id:$collectionId, 
                bbox:$request->get('bbox'), 
                bbox_crs:$request->get('bbox_crs'),
                filters:$filters,
                crs:$request->get("crs", "")
            );
            $items["timeStamp"] = date(\DateTime::RFC3339);
            unset($items["name"]);
            unset($items["crs"]);
            $items['links'] = $this->ogcLinksService->generateItemLinks($table_name);
            unset($items["headers"]);

            return $items;
        }

        // Generate the OGC-compliant links using the service
        if ('landingpage' === $operationName) {
            $type = 'json';
            $settings = $this->managerRegistry->getRepository(Setting::class)->findBy(["name" => ["OGC_API_TITLE", "OGC_API_ABSTRACT"]]);
            $title = 'titre aa';
            $abstract = 'description aa';
            if (!empty($settings)) {
                $title = $settings[0]->getTitle();
                $abstract = $settings[1]->getTitle();
            }
            
            if(str_ends_with($context['request_uri'], ".html")){
                $type = 'html';
            }

            return [
                'title' => $title,
                'description' => $abstract,
                'links' => $this->ogcLinksService->generateLandingPageLinks($type),
            ];
        }

        if ('conformance' === $operationName) {
            return [
                'conformsTo' => [
                    "http://www.opengis.net/spec/ogcapi-common-1/1.0/req/core",
                    "http://www.opengis.net/spec/ogcapi-common-1/1.0/req/landing-page",
                    "http://www.opengis.net/spec/ogcapi-common-1/1.0/req/oas30",
                    "http://www.opengis.net/spec/ogcapi-common-1/1.0/req/html",
                    "http://www.opengis.net/spec/ogcapi-common-1/1.0/req/json"
                ]
            ];
        }

        // Process collections for return
        $ogc_collections = $this->managerRegistry->getRepository(OGCCollection::class)->findBy(["api_layer" => true]);

        foreach($ogc_collections as $key => &$collection){
            try{
                $this->completeCollectionElements($collection);
            }catch(Exception $e){
                $this->logger->error("".$e->getMessage());
                unset($ogc_collections[$key]); //remove if not able to complete
            }
        }

        return ["collections" => array_values($ogc_collections), "links" => $this->ogcLinksService->generateCollectionsLinks()];
    }

    private function completeCollectionElements(OGCCollection &$collection){
        //Process for return
        $collection->setCrs($this->ogcLinksService->generateAllowedCrsLinks());
        $table = $collection->getId();
        $srid = $this->prodigeCon->fetchAllAssociative(
            "SELECT find_srid('public', '$table', 'the_geom') as srid"
        )[0]['srid'];
        $extent = $this->prodigeCon->fetchAllAssociative(
            "SELECT ST_XMin(ST_Transform(st_setsrid(ST_EstimatedExtent('$table', 'the_geom'), $srid), 4326)::geometry) as st_xmin, 
            ST_XMax(ST_Transform(st_setsrid(ST_EstimatedExtent('$table', 'the_geom'), $srid), 4326)::geometry) as st_xmax, 
            ST_yMin(ST_Transform(st_setsrid(ST_EstimatedExtent('$table', 'the_geom'), $srid), 4326)::geometry) as st_ymin, 
            ST_yMax(ST_Transform(st_setsrid(ST_EstimatedExtent('$table', 'the_geom'), $srid), 4326)::geometry) as st_ymax;"
        );
        try{
            $collection->setExtent(
                [
                    "spatial" => [
                        "bbox" => [
                            floatval($extent[0]["st_xmin"]),
                            floatval($extent[0]["st_xmax"]),
                            floatval($extent[0]["st_ymin"]),
                            floatval($extent[0]["st_ymax"]),
                        ]
                    ]
                ]
            );
            $collection->setItemType("Feature");

            $id = $collection->getId();
            $links = $this->ogcLinksService->generateCollectionLinks($id, $collection->getSheetMetadata()->getPublicMetadataId());
            $collection->setLinks($links);
        }catch(Exception $error){
            //TODO get rid of collection
            $this->logger->error($error->getMessage());
            // Error getting geom
            $collection->setExtent([]);
            $collection->setItemType("");
        }
        
    }

    private function addPaginationLinks($offset, $limit, &$items){
        $total_amount = $items["numberMatched"];
        //Pagination Links
        $page = $offset/$limit + 1; //Set page
        $self = "https://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] ;
        if(!str_contains($self,'page')){
            if(str_contains($self,'?')){
                $self = $self."&page=1";
            }else{
                $self = $self."?page=1";
            }
        }
        if($this->str_contains($self, "page=") && ($limit != null) ){
            $next = $offset + $limit < $total_amount ? ['href' => preg_replace("/page=[0-9]+/","page=".strval($page + 1),$self), 'rel' => 'next'] : [];
            $first = ['href' => preg_replace("/page=[0-9]+/","page=".strval(1),$self), 'rel' => 'first'];
            //$last = preg_replace("/offset=[0-9]+/","offset=".strval($total_amount - ($total_amount%$limit)),$self) . ">; rel=\"last\"";
            $last = ['href' => preg_replace("/page=[0-9]+/","page=".strval(floor($total_amount/$limit)),$self), 'rel' => 'last'];
            //$prev = $offset > 0 ? preg_replace("/offset=[0-9]+/","offset=".strval($offset - $limit),$self) . ">; rel=\"prev\"" : "" ;
            $prev = $page > 1 ? ['href' => preg_replace("/page=[0-9]+/","page=".strval($page-1),$self), 'rel' => 'prev']: [] ;
        }else if($limit != null){
            //$next = $offset + $limit < $total_amount ? $self."&offset=".strval($offset + $limit) . ">; rel=\"next\"" : "";
            $next = $offset + $limit < $total_amount ? ['href' => preg_replace("/page=[0-9]+/","page=".strval($page + 1),$self), 'rel' => 'next'] : [];
            //$first = $self."&offset=0". ">; rel=\"first\"";
            $first = ['href' => $self."page=".strval(1), 'rel' => 'first'];
            //$last = $self."&offset=".strval($total_amount - ($total_amount%$limit)) . ">; rel=\"last\"";
            $last = ['href' => $self."page=".strval(floor($total_amount/$limit)), 'rel' => 'last'];
            //$prev = $offset > 0 ? $self."&offset=".strval($offset - $limit) . ">; rel=\"prev\"" : "" ;
            $prev = $page > 1 ? ['href' => $self."page=".strval($page-1), 'rel' => 'prev']: [] ;
        }else{//sans pagination
            $next = [];
            $first = [];
            $last = [];
            $prev = [];
        }
        $self = ['href' => $self, 'rel' => 'self'];

        $items['links'] = array_filter([$self,$next,$first,$last,$prev]);
    }

    protected function str_contains($haystack, $needle) {
        return $needle !== '' && mb_strpos($haystack, $needle) !== false;
    }
}