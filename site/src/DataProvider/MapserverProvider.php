<?php

declare(strict_types=1);

namespace App\DataProvider;

use ApiPlatform\Core\DataProvider\ContextAwareCollectionDataProviderInterface;
use ApiPlatform\Core\DataProvider\ItemDataProviderInterface;
use ApiPlatform\Core\DataProvider\RestrictedDataProviderInterface;
use App\Entity\Configuration\Mapserver;
use App\Exception\NotFoundException;
use App\Service\MapserverApiService;
use Doctrine\Persistence\ManagerRegistry;


/**
 * MapcacheProvider
 */
final class MapserverProvider implements ItemDataProviderInterface, RestrictedDataProviderInterface
{
    public function __construct(private Mapserver $mapserver, private MapserverApiService $mapserverApiService)
    {
    }

    public function supports(string $resourceClass, string $operationName = null, array $context = []): bool
    {
        return Mapserver::class === $resourceClass;
    }

    /**
     * @throws NotFoundException
     */
    public function getItem(string $resourceClass, $id, string $operationName = null, array $context = []): ?Mapserver
    {
        switch ($operationName) {
            case 'get_map' :
                $this->mapserver->setFile($id);
                $map = $this->mapserverApiService->getMap(
                    $id,
                    ['directory' => $this->mapserver->getDirectory()]
                );
                unset($map['status_code']);
                return $this->mapserver->setMap($map);
            default:
                return null;
        }
    }

}
