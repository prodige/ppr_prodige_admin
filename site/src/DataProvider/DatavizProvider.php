<?php

declare(strict_types=1);

namespace App\DataProvider;

use ApiPlatform\Core\DataProvider\CollectionDataProviderInterface;
use ApiPlatform\Core\DataProvider\ItemDataProviderInterface;
use ApiPlatform\Core\DataProvider\RestrictedDataProviderInterface;
use ApiPlatform\Core\DataProvider\SerializerAwareDataProviderInterface;
use ApiPlatform\Core\DataProvider\SerializerAwareDataProviderTrait;
use ApiPlatform\Core\Exception\ResourceClassNotSupportedException;
use App\Entity\Administration\Resource\Dataviz;
use App\Service\CatalogueService;
use Doctrine\Persistence\ManagerRegistry;

final class DatavizProvider implements ItemDataProviderInterface, CollectionDataProviderInterface,
                                       RestrictedDataProviderInterface,
                                       SerializerAwareDataProviderInterface
{
    use SerializerAwareDataProviderTrait;

    public function __construct(
        private ManagerRegistry $managerRegistry,
        private CatalogueService $catalogueService
    ) {
    }

    public function supports(string $resourceClass, string $operationName = null, array $context = []): bool
    {
        return Dataviz::class === $resourceClass;
    }

    public function getItem(string $resourceClass, $id, string $operationName = null, array $context = []): ?Dataviz
    {
        $manager = $this->managerRegistry->getManagerForClass($resourceClass);
        $repository = $manager->getRepository($resourceClass);
        $queryBuilder = $repository->createQueryBuilder('d')
            ->where('d.id = :id')
            ->setParameter('id', $id);
        $dataviz = $queryBuilder->getQuery()->getOneOrNullResult();
        if ($dataviz !== null) {
            $layer = $dataviz->getLayer();
            $uuid = $this->catalogueService->getUuidFromMetadata($layer);
            $layer->setMetadataUuid($uuid);
            $dataviz->setLayer($layer);
        }

        return $dataviz;
    }

    public function getCollection(string $resourceClass, string $operationName = null, array $context = []): iterable
    {
        $return = [];
        if (Dataviz::class !== $resourceClass) {
            throw new ResourceClassNotSupportedException();
        }
        $manager = $this->managerRegistry->getManagerForClass($resourceClass);
        $repository = $manager->getRepository($resourceClass);
        $datavizs = $repository->findAll();
        foreach ($datavizs as $dataviz) {
            $layer = $dataviz->getLayer();
            $uuid = $this->catalogueService->getUuidFromMetadata($layer);
            $layer->setMetadataUuid($uuid);
            $dataviz->setLayer($layer);
            $return[] = $dataviz;
        }

        return $return;
    }
}
