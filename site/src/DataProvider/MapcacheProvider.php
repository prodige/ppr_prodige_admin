<?php

declare(strict_types=1);

namespace App\DataProvider;

use ApiPlatform\Core\DataProvider\ContextAwareCollectionDataProviderInterface;
use ApiPlatform\Core\DataProvider\ItemDataProviderInterface;
use ApiPlatform\Core\DataProvider\RestrictedDataProviderInterface;
use ApiPlatform\Core\Exception\ResourceClassNotSupportedException;
use App\Entity\Configuration\Mapcache;
use App\Exception\NotFoundException;
use Doctrine\Persistence\ManagerRegistry;


/**
 * MapcacheProvider
 */
final class MapcacheProvider implements ItemDataProviderInterface,
                                        ContextAwareCollectionDataProviderInterface,
                                        RestrictedDataProviderInterface
{
    public function __construct(private Mapcache $mapcache)
    {
    }

    public function supports(string $resourceClass, string $operationName = null, array $context = []): bool
    {
        return Mapcache::class === $resourceClass;
    }

    /**
     * @throws NotFoundException|ResourceClassNotSupportedException
     */
    public function getItem(
        string $resourceClass,
        $id,
        string $operationName = null,
        array $context = []
    ): Mapcache|array|null {
        $this->mapcache->setName($id);
        $count = match ($operationName) {
            'get_grid' => count($this->mapcache->getGridByName($id)),
            'get_tile' => count($this->mapcache->getTileByName($id)),
            'get_grid_tiles' => count(["1"]),
        };
        if ($operationName === 'get_grid_tiles') {
            return $this->getCollection(Mapcache::class, 'get_grid_tiles', ['name' => $id]);
        }
        if ($count == 0) {
            throw new NotFoundException('Resource not found');
        }
        return $this->mapcache;
    }

    public function getCollection(string $resourceClass, string $operationName = null, array $context = []): array
    {
        $search = match ($operationName) {
            'get_grids' => $this->mapcache->generateGrids(),
            'get_tiles' => $this->mapcache->generateTiles(),
            'get_grid_tiles' => $this->mapcache->getTilesByGrid($context['name'])
        };
        $tmp = [];
        foreach ($search as $key => $tile) {
            $tile['name'] = $key;
            $tmp[] = $tile;
        }
        return $tmp;
    }
}