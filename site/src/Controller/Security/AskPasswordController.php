<?php

declare(strict_types=1);

namespace App\Controller\Security;

use ApiPlatform\Core\Validator\ValidatorInterface;
use App\Entity\Administration\Directory\User\User;
use App\Service\AuthenticationService;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use ApiPlatform\Core\Annotation as OA;

class AskPasswordController extends AbstractController
{

    public function __construct(private ValidatorInterface $validator, private AuthenticationService $authService)
    {
    }

    /**
     * @throws \Exception
     */
    public function __invoke(User $data, Request $request): JsonResponse
    {
        $this->validator->validate($data, ['groups' => 'changePasswordValidation']);

        // Send mail to change password
        $this->authService->sendMailForPassword($data,true);

        return new JsonResponse($data, 200);
    }

}
