<?php

declare(strict_types=1);

namespace App\Controller\Security;

use ApiPlatform\Core\Validator\ValidatorInterface;
use App\Entity\Administration\Directory\User\User;
use App\Entity\Administration\Resource\MetadataSheet;
use App\Response\JsonLDResponse;
use App\Service\AlkRequestService;
use App\Service\AuthenticationService;
use App\Service\MapserverApiService;
use App\Service\UserService;
use Doctrine\Persistence\ManagerRegistry;
use Gregwar\CaptchaBundle\Type\CaptchaType;
use Prodige\ProdigeBundle\Services\CacheService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Regex;
use Nelmio\ApiDocBundle\Annotation as Nelmio;
use OpenApi\Attributes as OA;
use Psr\Log\LoggerInterface;

class SecurityController extends AbstractController
{

    public function __construct(
        private ValidatorInterface          $validator,
        private AuthenticationService       $authService,
        private UserPasswordHasherInterface $passwordHasher,
        private ManagerRegistry             $managerRegistry,
        private SerializerInterface         $serializer,
        private UserService                 $userService,
        private MapserverApiService         $mapserverApiService,
        private LoggerInterface             $logger,
        private AlkRequestService $alkRequestService
    )
    {
        $this->mapserverApiService = $mapserverApiService;
    }

    /**
     * @throws \Exception
     */
    public function __invoke(User $data, Request $request): JsonResponse
    {
        $this->validator->validate($data, ['groups' => 'loginValidation']);

        // Get token from cas
        $data = $this->authService->login($data->getLogin(), $data->getPassword());

        return new JsonResponse($data, 200);
    }

    #[Route('/logout', name: 'logout', methods: ['GET', 'POST'])]
    public function logout(): void
    {
    }

    #[Route('/login/forgot', name: 'login_forgot', methods: ['GET', 'POST'])]
    public function forgotPassword(Request $request)
    {
        $user = new User();
        $form = $this->createFormBuilder($user, ['validation_groups' => ['changePasswordValidation']])
            ->add('login', TextType::class, [
                'attr' => ['placeholder' => 'login'],
            ])
            ->add('email', EmailType::class, [
                'attr' => ['placeholder' => 'email'],
            ])
            ->add('captcha', CaptchaType::class, [
                'width' => 150,
                'height' => 38,
                'length' => 6,
            ])
            ->getForm();

        $form->handleRequest($request);
        $render['form'] = $form;
        $render['status'] = false;

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $user = $this->managerRegistry->getRepository(User::class)->findOneBy(
                [
                    'login' => $data->getLogin(),
                    'email' => $data->getEmail(),
                ]
            );
            $render['status'] = true;
            $render['email'] = $data->getEmail();
            if (!is_null($user)) {
                $this->authService->sendMailForPassword($data);
            }
        }

        return $this->renderForm('common/reset_password.html.twig', $render);
    }

    #[Route('/password/change/{token}', name: 'password_change', methods: ['GET', 'POST'])]
    public function changePassword(string $token, Request $request)
    {
        $render['status'] = false;
        $tab = explode('&', $this->alkRequestService->getDecodeParam($token));
        if (3 === count($tab)) {
            $expiredTime = substr($tab[2], 2);
            // Check Token valid
            if (time() <= $expiredTime) {
                $render['status'] = true;
            } else {
                return $this->renderForm('common/change_password.html.twig', $render);
            }
        }

        $login = substr($tab[0], 2);
        $form = $this->createFormBuilder()
            ->add(
                'password',
                RepeatedType::class,
                [
                    'type' => PasswordType::class,
                    'invalid_message' => 'The password fields must match.',
                    'options' => ['attr' => ['class' => 'password-field']],
                    'required' => true,
                    'constraints' => [
                        new NotBlank(),
                        new Regex('/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{12,}$/'),
                    ],
                    'first_options' => ['label' => 'Password'],
                    'second_options' => ['label' => 'Repeat Password'],
                ]
            )
            ->add('login', HiddenType::class, ['data' => $login])
            ->getForm();
        $form->handleRequest($request);
        $render['form'] = $form;

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $user = $this->managerRegistry->getRepository(User::class)->findOneBy(['login' => $data['login']]);

            $hashedPassword = $this->passwordHasher->hashPassword(
                $user,
                $data['password']
            );
            $user->setPassword($hashedPassword);
            $user->setIsExpired(false);
            $expiredTime = new \DateTime();
            $expiredTime->modify('+6 month');
            $user->setPasswordExpire($expiredTime);
            $this->managerRegistry->getManager()->persist($user);
            $this->managerRegistry->getManager()->flush();

            return $this->redirect($this->container->get('parameter_bag')->get('PRODIGE_URL_CATALOGUE'));
        }

        return $this->renderForm('common/change_password.html.twig', $render);
    }

    #[Route('/api/users/test/traitement', name: 'get_traitements', methods: ['GET'])]
    public function getTraitement(Request $request)
    {
        $b = $this->userService->hasTraitement('NAVIGATION', 67, null, null);
        return new JsonLDResponse(["asTraitement" => $b], 200, [], false, "Traitements");
    }

    #[Route('/api/users/test/getMapRights', name: 'get_map_rights', methods: ['GET'])]
    public function getMapRightsAction(Request $request)
    {
        $b = $this->userService->getMapRights(657401);
        return new JsonLDResponse(["map" => $b], 200, [], false, "Maps");
    }

    #[Route('/api/users/test/getChartRights', name: 'get_chart_rights', methods: ['GET'])]
    public function getChartRightsAction(Request $request)
    {
        $b = $this->userService->getChartRights(3121693);
        return new JsonLDResponse(["charts" => $b], 200, [], false, "Maps");
    }

    #[Route('/api/users/test/getServiceRights', name: 'get_service_rights', methods: ['GET'])]
    public function getServiceRightsAction(Request $request)
    {
        $b = $this->userService->getServiceRights(3121693);
        return new JsonLDResponse(["service" => $b], 200, [], false, "Service");
    }

    #[Route('/api/users/test/getLayerRights', name: 'get_layer_rights', methods: ['GET'])]
    public function getLayerRightsAction(Request $request)
    {
        $b = $this->userService->getLayerRights(3121693, ['NAVIGATION']);
        return new JsonLDResponse(["layer" => $b], 200, [], false, "Layer");
    }

    #[Route('/api/users/test/getTraitementTerritoire', name: 'get_traitement_layer_ter', methods: ['GET'])]
    public function getTraitementTerritoire(Request $request)
    {
        $b = $this->userService->getTraitementTerritoire('TELECHARGEMENT', 2160);
        return new JsonLDResponse(["layer" => $b], 200, [], false, "Layer");
    }

    #[Route('/api/users/test/getFilterAttribut', name: 'get_FilterAttribut', methods: ['GET'])]
    public function getFilterAttributAction(Request $request)
    {
        $b = $this->userService->getFilterAttribut('NAVIGATION', 2160);
        return new JsonLDResponse(["layer" => $b], 200, [], false, "Layer");
    }

    #[Route('/api/users/test/getGeneralRights', name: 'get_GeneralRights', methods: ['GET'])]
    public function getGeneralRightsAction(Request $request)
    {
        $b = $this->userService->getGeneralRights(
            ['NAVIGATION', 'EDITION', 'CMS', 'GLOBAL_RIGHTS'],
            'dataset',
            5098619
        );
        return new JsonLDResponse(["layer" => $b], 200, [], false, "Layer");
    }

    #[Nelmio\Areas(['internal'])]
    #[OA\Tag(name: 'Authorization')]
    #[OA\Parameter(name: 'TRAITEMENTS', in: 'query', example: 'NAVIGATION|EDITION|CMS')]
    #[OA\Parameter(name: 'ID', in: 'query', example: 5098619)]
    #[OA\Parameter(name: 'OBJET_TYPE', in: 'query', example: 'dataset')]
    #[OA\Parameter(name: 'OBJET_STYPE', in: 'query', example: 'invoke')]
    #[OA\Parameter(name: 'uuid', in: 'query', example: '55718e4e-9629-4978-98f4-2a70d34ba281')]
    #[OA\Parameter(name: 'callback', in: 'query', example: 'stcCallback1001')]
    #[OA\Parameter(name: 'DATASET_NAME', in: 'query', example: 'departements')]
    #[OA\Response(response: 200, description: 'All Right for Prodige')]
    #[OA\Response(response: 401, description: 'Authentication mandatory')]
    #[Route('/api/internal/verify_right', name: 'prodige_verify_rights_url_admin', methods: ['GET'])]
    public function verifyRightsAction(Request $request, CacheService $cacheService)
    {
        //1 Init
        defined("PRO_TRT_TYPE_EDITION") || define("PRO_TRT_TYPE_EDITION", "1");
        defined("PRO_OBJET_TYPE_COUCHE") || define("PRO_OBJET_TYPE_COUCHE", "1");
        $callback = $request->get("callback", "");
        $tabRes = array();

        //1 get params
        $traitements = $request->get("TRAITEMENTS", "");
        $objectId = (int)$request->get("ID", "");
        $objectType = $request->get("OBJET_TYPE", "");
        $objectSType = $request->get("OBJET_STYPE", "");

        $tabTraitement = explode("|", $traitements);
//        // Append GLOBBAL_RIGHTS
//        $tabTraitement[] = "GLOBAL_RIGHTS";
        $tabTraitement = array_unique($tabTraitement);
        $tabTraitement = array_diff($tabTraitement, array(""));
        sort($tabTraitement, SORT_STRING);

        if ($objectId === 0) {
            //affect objectId when service working with uuid
            $uuid = $request->get("uuid", "");
            if ($uuid != "") {
                $metadata = $this->managerRegistry->getRepository(MetadataSheet::class)->findByUuid($uuid);
                if ($metadata != 0) {
                    $objectId = $metadata;
                }
            }
            $layerName = $request->get("MAPFILE_LAYER_NAME", "");
            $mapName = $request->get("MAPFILE_NAME", "");
            if ($mapName !== '' && $layerName !== '' && $objectType === 'dataset') {
                try {
                    $layer = $this->mapserverApiService->getLayer($mapName, $layerName, ['directory' => $_ENV['ROOT_DATA'] . $_ENV['MAPSERVER_ACCOUNT_DIR'] . $_ENV['MAPSERVER_MAPFILE_DIR']]);
                    $datasetName = $layer['data'];

                    //determine layer dataset name from data expression
                    preg_match("!from\s+\(*([\w\s\.\*]*)\)*\s+as!", $datasetName, $matches);
                    if (isset($matches[1])) {
                        $layerTable = $matches[1];
                    }
                    if (isset($layerTable)) {
                        preg_match("!from\s+([\w\.]*)\s*(?:order)?!", $layerTable, $matches);
                        if (isset($matches[1])) {
                            $layerTable = $matches [1];
                        }
                    }
                    if (isset($layerTable)) {
                        $layerTable = str_replace("public.", "", $layerTable);
                    }
                    $metadata = $this->managerRegistry->getRepository(MetadataSheet::class)->findByDatasetname($layerTable);
                    if ($metadata != 0) {
                        $objectId = $metadata;
                    }
                } catch (\Exception) {
                    $this->logger->error('can\'t detect layer' . $layerName . ' in map ' . $mapName);
                }
            }
        }

        //2 get Rights according to datatype and resource
        if ($objectType == "service") {
            if ($objectSType == "invoke") {
                $tabRes = $this->userService->getMapRights($objectId);
            } elseif ($objectSType == "chart") {
                $tabRes = $this->userService->getChartRights($objectId, $tabTraitement);
            } else {
                $tabRes = $this->userService->getServiceRights($objectId, $tabTraitement);
            }
        } elseif ($objectType == "dataset" || $objectType == "nonGeographicDataset") {
            $tabRes = $this->userService->getLayerRights($objectId, $tabTraitement);
        } elseif ($objectType == "series" && array_search("TELECHARGEMENT", $tabTraitement) !== false) {
            $tabRes["TELECHARGEMENT"] = true;
        } elseif ($objectType == "dataviz") {
            $tabRes = $this->userService->getDatavizRights($objectId, $tabTraitement);
        }

        //3 get general rights
        $tabRes = array_merge($tabRes, $this->userService->getGeneralRights($tabTraitement, $objectType, $objectId));

        //for cross domain case
        if ($callback != "") {
            return new Response($callback . "(" . json_encode($tabRes) . ")");
        } else {
            return new JsonLDResponse(["right" => $tabRes], 200, [], false, "Right");
        }
    }


}
