<?php

declare(strict_types=1);

namespace App\Controller;

use ApiPlatform\Core\Bridge\Symfony\Bundle\SwaggerUi\SwaggerUiAction;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class OpenApiController extends AbstractController
{
    public function __construct(private SwaggerUiAction $swaggerUiAction)
    {
    }

    #[Route('/api/ogc-features/docs', name: 'openapi')]
    public function __invoke(Request $request): Response
    {
        return $this->swaggerUiAction->__invoke($request);
    }

    #[Route('/api/ogc-features/', name: 'openapi_landing')]
    public function landingPage(Request $request): Response
    {
        return $this->redirect($this->generateUrl('api_collections_landingpage_collection') . '.html');
    }

    //#[Route('/api/ogc-features/collections', name: 'openapi_collections')]
    //public function collections(Request $request): Response
    //{
    //    return $this->redirect($this->generateUrl('api_collections_get_collection') . '.html');
    //}
//
    //#[Route('/api/ogc-features/conformance', name: 'openapi_conformance')]
    //public function conformance(Request $request): Response
    //{
    //    return $this->redirect($this->generateUrl('api_collections_conformance_collection') . '.html');
    //}



}
