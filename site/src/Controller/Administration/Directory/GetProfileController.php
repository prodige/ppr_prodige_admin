<?php

namespace App\Controller\Administration\Directory;

use App\Entity\Administration\Directory\Profile\Profile;
use App\Service\ProfileGeonetworkService;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Attribute\AsController;

#[AsController]
class GetProfileController extends AbstractController
{
    /**
     * @throws Exception
     */
    public function __invoke(Profile $data, Request $request, ProfileGeonetworkService $profileGeonetworkService): JsonResponse
    {
        $dataResponse = $profileGeonetworkService->getPrivilegeForGeonetwork($data);

        if ($dataResponse) {
            return new JsonResponse($dataResponse, 200);
        }
        throw new Exception('Parameters are incorrect or missing', 400);
    }
}