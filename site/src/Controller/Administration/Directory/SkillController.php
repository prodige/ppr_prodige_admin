<?php

namespace App\Controller\Administration\Directory;

use App\Entity\Administration\Directory\Skill;
use App\Form\Administration\Directory\SkillType;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SkillController extends AbstractController
{
    #[Route('/skill', name: 'skill_list', methods: ['GET'])]
    public function index(): Response
    {
        $form = $this->createForm(SkillType::class, new Skill(), ['attr' => ['id' => 'add']]);
        $render = [
            'form' => $form,
            'api_filter' => 'properties[]=id&properties[]=name&properties[]=layers&properties[]=maps',
        ];

        return $this->renderForm('administration/directory/skill/list.html.twig', $render);
    }

    #[Route('/skill/edit/{id}', name: 'skill_edit', methods: ['GET'])]
    public function edit(int $id, ManagerRegistry $managerRegistry): Response
    {
        $entity = $managerRegistry->getRepository(Skill::class)->findOneBy(['id' => $id]);
        $form = $this->createForm(SkillType::class, $entity);
        $render = ['form' => $form];

        return $this->renderForm('common/entity_edit_form.html.twig', $render);
    }
}
