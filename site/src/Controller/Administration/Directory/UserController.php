<?php

namespace App\Controller\Administration\Directory;

use App\Entity\Administration\Directory\Profile\ProfileUser;
use App\Entity\Administration\Directory\Structure;
use App\Entity\Administration\Directory\User\User;
use App\Entity\Administration\Directory\User\UserAreaAccess;
use App\Entity\Administration\Directory\User\UserAreaAlert;
use App\Entity\Administration\Directory\User\UserAreaEdition;
use App\Form\Administration\Directory\ProfileUserType;
use App\Form\Administration\Directory\UserAreaAccessType;
use App\Form\Administration\Directory\UserAreaAlertType;
use App\Form\Administration\Directory\UserAreaEditionType;
use App\Form\Administration\Directory\UserType;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class UserController extends AbstractController
{
    #[Route('/user', name: 'user_list', methods: ['GET'])]
    public function index(): Response
    {
        $form = $this->createForm(UserType::class, new User(), ['attr' => ['id' => 'add']]);
        if (file_exists('../../VERSION.txt')) {
            $version = file_get_contents('../../VERSION.txt');
        } else {
            $version = "not found";
        }

        $render = [
            'form' => $form,
            'api_filter' => 'properties[]=id&properties[]=name&properties[]=firstName&properties[]=login&properties[]=email&properties[profiles]=profile&properties[]=countAlert&properties[]=structures.id&properties[]=canDelete',
            'version' => $version,
            'reloadPage' => true
        ];

        return $this->renderForm('administration/directory/user/list.html.twig', $render);
    }

    #[Route('/user/edit/{id}', name: 'user_edit', methods: ['GET'])]
    public function edit(int $id, ManagerRegistry $managerRegistry): Response
    {
        $entity = $managerRegistry->getRepository(User::class)->findOneBy(['id' => $id]);
        $form = $this->createForm(UserType::class, $entity);
        $render = ['form' => $form];

        return $this->renderForm('common/entity_edit_form.html.twig', $render);
    }

    #[Route('/user/alerts/{id}', name: 'user_alerts', methods: ['GET'])]
    public function alerts(int $id, ManagerRegistry $managerRegistry): Response
    {
        $user = $managerRegistry->getRepository(User::class)->find($id);
        $userAreaAlert = new UserAreaAlert();
        $userAreaAlert->setUser($user);
        $form = $this->createForm(UserAreaAlertType::class, $userAreaAlert);
        $render = [
            'form' => $form,
            'api_filter' => 'properties[]=user&properties[]=layer&properties[]=area&user=' . $id,
            'user' => $user,
        ];

        return $this->renderForm('administration/directory/user/alert_list.html.twig', $render);
    }


    #[Route('/user/profiles/{id}', name: 'user_profiles', methods: ['GET'])]
    public function profiles(int $id, ManagerRegistry $managerRegistry): Response
    {
        $user = $managerRegistry->getRepository(User::class)->find($id);
        $userAreaAlert = new ProfileUser();
        $userAreaAlert->setUser($user);
        $form = $this->createForm(ProfileUserType::class, $userAreaAlert);
        $render = [
            'form' => $form,
            'api_filter' => 'properties[]=user&properties[]=layer&properties[]=area&user=' . $id,
            'user' => $user,
        ];

        return $this->renderForm('administration/directory/user/profile_list.html.twig', $render);
    }

    #[Route('/user/viewable_areas/{id}', name: 'user_viewable_areas', methods: ['GET'])]
    public function viewable_areas(int $id, ManagerRegistry $managerRegistry): Response
    {
        $user = $managerRegistry->getRepository(User::class)->find($id);
        $userAreaAlert = new UserAreaAccess();
        $userAreaAlert->setUser($user);
        $form = $this->createForm(UserAreaAccessType::class, $userAreaAlert);
        $render = [
            'form' => $form,
            'api_filter' => 'properties[]=user&properties[]=area&user=' . $id,
            'user' => $user
        ];

        return $this->renderForm('administration/directory/user/area_access_list.html.twig', $render);
    }

    #[Route('/user/editable_areas/{id}', name: 'user_editable_areas', methods: ['GET'])]
    public function editable_areas(int $id, ManagerRegistry $managerRegistry): Response
    {
        $user = $managerRegistry->getRepository(User::class)->find($id);
        $userAreaAlert = new UserAreaEdition();
        $userAreaAlert->setUser($user);
        $form = $this->createForm(UserAreaEditionType::class, $userAreaAlert);
        $render = [
            'form' => $form,
            'api_filter' => 'properties[]=user&properties[]=area&user=' . $id,
            'user' => $user,
        ];

        return $this->renderForm('administration/directory/user/area_edition_list.html.twig', $render);
    }
    #[Route('/user/structures/{id}', name: 'user_structures', methods: ['GET'])]
    public function structures(int $id, ManagerRegistry $managerRegistry): Response
    {
        $form = $this->createForm(UserType::class, new User(), ['attr' => ['id' => 'add']]);

        $structure = $managerRegistry->getRepository(Structure::class)->findOneBy(['id' => $id]);
        $render = [
            'form' => $form,
            'api_filter' => 'structures.id='. $id . '&properties[]=id&properties[]=name&properties[]=firstName&properties[]=login&properties[]=email&properties[profiles]=profile&properties[]=countAlert',
            'title' => 'Liste des utilisateurs pour la structure : ' . $structure->getName(),
            'addBtn' => false
        ];

        return $this->renderForm('administration/directory/user/list.html.twig', $render);
    }

}
