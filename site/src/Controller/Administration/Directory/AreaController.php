<?php

namespace App\Controller\Administration\Directory;

use App\Entity\Administration\Directory\Area;
use App\Form\Administration\Directory\AreaType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AreaController extends AbstractController
{
    #[Route('/area', name: 'area_list', methods: ['GET'])]
    public function index(): Response
    {
        $form = $this->createForm(AreaType::class, new Area());
        $render = [
            'form' => $form,
            'api_filter' => 'properties[]=id&properties[]=name&properties[]=code&properties[zoning]=name',
        ];

        return $this->renderForm('administration/directory/territory/list.html.twig', $render);
    }
}
