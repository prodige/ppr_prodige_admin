<?php

namespace App\Controller\Administration\Directory;

use App\DataProvider\StructureProvider;
use App\Entity\Administration\Directory\Structure;
use App\Form\Administration\Directory\StructureType;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class StructureController extends AbstractController
{
    #[Route('/structure/{id}/children', name: 'substructure_list', methods: ['GET'])]
    public function subStructureList(ManagerRegistry $manager, int $id): Response
    {
        $data = $manager->getRepository(Structure::class)->findOneBy(['id' => $id]);
        $sub = new Structure();
        $sub->setParent($data);

        $form = $this->createForm(StructureType::class, $sub, ['attr' => ['parentId' => $id]]);

        $niveau = $data->getLevel() + 1;
        $down = $niveau < 4;
        $add = $niveau < 5;

        $parentId = $data->getParent() !== null ? $data->getParent()->getId() : true;

        $render = [
            'add' => $add,
            'down' => $down,
            'up' => true,
            'isTop' => $parentId === true ? true : false,
            'parentId' => $parentId,
            'form' => $form,
            'title' => 'Liste des sous-structure de ' . $data->getName() . ' ( Niveau : ' . $niveau . ' )',
            'api_filter' => 'parent.id=' . $id . '&properties[]=id&properties[]=name&properties[]=parent&properties[]=canDelete&properties[]=countUser&properties[]=parentReferral&properties[]=bottomLevel&properties[]=User&properties[]=referenceGn',
        ];


        return $this->renderForm('administration/directory/structure/substructure_list.html.twig', $render);
    }

    #[Route('/structure', name: 'structure_list', methods: ['GET'])]
    public function index(): Response
    {
        $form = $this->createForm(StructureType::class, new Structure(), ['attr' => ['parentId' => null]]);
        $render = [
            'form' => $form,
            'down' => true,
            'up' => false,
            'title' => 'Liste des structures ( Niveau : 1 )',
            'api_filter' => 'exists[parent]=false&properties[]=id&properties[]=name&properties[]=parent&properties[]=bottomLevel&properties[]=canDelete&properties[]=countUser&properties[]=parentReferral&properties[]=referenceGn',
        ];

        return $this->renderForm('administration/directory/structure/list.html.twig', $render);
    }

    #[Route('/structure/edit/{id}', name: 'structure_edit', methods: ['GET'])]
    public function edit(int $id, StructureProvider $provider): Response
    {
        $entity = $provider->getItem(Structure::class, $id);
        $form = $this->createForm(StructureType::class, $entity, ['attr' => ['parentId' => $entity->getParent() !== null ? $entity->getParent()->getId() : null ]]);
        $render = ['form' => $form];

        return $this->renderForm('administration/directory/structure/structure_edit_form.html.twig', $render);
    }

}
