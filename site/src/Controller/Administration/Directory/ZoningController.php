<?php

namespace App\Controller\Administration\Directory;

use App\Entity\Administration\Directory\Zoning;
use App\Form\Administration\Directory\ZoningType;
use App\Service\AreaService;
use Doctrine\DBAL\Exception;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

class ZoningController extends AbstractController
{
    public function __construct(private AreaService $areaService, private TranslatorInterface $translator)
    {
    }

    #[Route('/zoning', name: 'zoning_list', methods: ['GET'])]
    public function index(ManagerRegistry $managerRegistry): Response
    {
        $entities = $managerRegistry->getRepository(Zoning::class)->findAll();
        $zoning = new Zoning();
        if ((count($entities) > 0)) {
            $zoning->setLayer($entities[0]->getLayer());
            $addBtn = 'custom';
        } else {
            $addBtn = $reload_page = true;
        }
        $form = $this->createForm(ZoningType::class, $zoning);
        $render = [
            'addBtn' => $addBtn,
            'reloadDelete' => true,
            'form' => $form,
            'api_filter' => 'properties[]=id&properties[]=name&properties[layer]=name&properties[]=fieldId&properties[]=fieldName',
        ];

        if (isset($reload_page)) {
            $render['reloadPage'] = true;
        }

        return $this->renderForm('administration/directory/zoning/list.html.twig', $render);
    }

    #[Route('/zoning/edit/{id}', name: 'zoning_edit', methods: ['GET'])]
    public function edit(int $id, ManagerRegistry $managerRegistry): Response
    {
        $entity = $managerRegistry->getRepository(Zoning::class)->findOneBy(['id' => $id]);
        $form = $this->createForm(ZoningType::class, $entity);
        $render = ['form' => $form];

        return $this->renderForm('common/entity_edit_form.html.twig', $render);
    }

    #[Route('/zoning/territories/refresh', name: 'zoning_refresh_territories', methods: ['POST'])]
    public function refreshTerritories(Request $request, EntityManagerInterface $entityManager): Response
    {
        if ($id = $request->get('id')) {
            $zonings = $entityManager->getRepository(Zoning::class)->findAll();
            if ($zonings) {
                try {
                    $this->areaService->updateTerritories($zonings,$id);

                    return new JsonResponse(['message' => $this->translator->trans('Territories updated')]);
                } catch (Exception $e) {
                    return new JsonResponse(['error' => $e->getMessage()]);
                }
            }

            return new JsonResponse(['error' => $this->translator->trans('Zoning not found')], 404);
        }

        return new JsonResponse(['error' => $this->translator->trans('Missing zoning id')], 400);
    }

}
