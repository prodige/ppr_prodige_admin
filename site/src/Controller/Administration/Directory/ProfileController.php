<?php

namespace App\Controller\Administration\Directory;

use App\Entity\Administration\Directory\Profile\Profile;
use App\Entity\Administration\Directory\User\User;
use App\Form\Administration\Directory\ProfileType;
use App\Form\Administration\Directory\UserType;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ProfileController extends AbstractController
{
    #[Route('/profile', name: 'profile_list', methods: ['GET'])]
    public function index(): Response
    {
        $form = $this->createForm(ProfileType::class, new Profile(), ['attr' => ['id' => 'add']]);
        $render = [
            'form' => $form,
            'api_filter' => 'properties[]=id&properties[]=name&properties[privileges]=&properties[]=canDelete&properties[]=countUser'
        ];

        return $this->renderForm('administration/directory/profile/list.html.twig', $render);
    }

    #[Route('/profile/edit/{id}', name: 'profile_edit', methods: ['GET'])]
    public function edit(int $id, ManagerRegistry $managerRegistry): Response
    {
        $entity = $managerRegistry->getRepository(Profile::class)->findOneBy(['id' => $id]);
        $form = $this->createForm(ProfileType::class, $entity);
        $render = ['form' => $form];

        return $this->renderForm('administration/directory/profile/profile_edit_form.html.twig', $render);
    }

    #[Route('/profiles/{id}/users', name: 'profile_users', methods: ['GET'])]
    public function profileUsers(int $id, ManagerRegistry $managerRegistry): Response
    {
        $form = $this->createForm(UserType::class, new User(), ['attr' => ['id' => 'add']]);
        $profile = $managerRegistry->getRepository(Profile::class)->findOneBy(['id' => $id]);
        $render = [
            'form' => $form,
            'api_filter' => 'profiles.profile.id=' . $id . '&properties[]=id&properties[]=name&properties[]=firstName&properties[]=login&properties[]=email&properties[profiles]=profile&properties[]=countAlert',
            'title' => 'Liste des utilisateurs pour le profil : ' . $profile->getName(),
            'addBtn' => false
        ];

        return $this->renderForm('administration/directory/user/list.html.twig', $render);
    }
}
