<?php

namespace App\Controller\Administration\Resource;

use App\Entity\Administration\Resource\Map;
use App\Form\Administration\Resource\MapType;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MapController extends AbstractController
{
    #[Route('/map', name: 'map_list', methods: ['GET'])]
    public function index(): Response
    {
        $form = $this->createForm(MapType::class, new Map());
        $render = [
            'form' => $form,
            'api_filter' => 'properties[]=id&properties[]=name&properties[]=description&properties[lexMapFormat]=name&properties[]=path&properties[]=state',
        ];

        return $this->renderForm('administration/resources/map/list.html.twig', $render);
    }

    #[Route('/map/edit/{id}', name: 'map_edit', methods: ['GET'])]
    public function edit(int $id, ManagerRegistry $managerRegistry): Response
    {
        $entity = $managerRegistry->getRepository(Map::class)->findOneBy(['id' => $id]);
        $form = $this->createForm(MapType::class, $entity);
        $render = ['form' => $form];

        return $this->renderForm('common/entity_edit_form.html.twig', $render);
    }
}
