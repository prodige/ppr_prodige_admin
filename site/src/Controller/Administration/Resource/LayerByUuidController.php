<?php

declare(strict_types=1);

namespace App\Controller\Administration\Resource;

use App\DataProvider\LayerProvider;
use App\Entity\Administration\Resource\Layer;
use App\Entity\Administration\Resource\MetadataSheet;
use App\Response\JsonLDResponse;
use Doctrine\DBAL\Connection;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Uid\Uuid;

class LayerByUuidController extends AbstractController
{
    private Connection $prodigeCon;

    public function __construct(
        private ManagerRegistry $managerRegistry,
        private LayerProvider $layerProvider
    ) {
        $this->prodigeCon = $managerRegistry->getConnection('prodige');
    }

    public function __invoke(Request $request): JsonLDResponse|Layer
    {
        $uuid = $request->get('uuid');

        if(Uuid::isValid($uuid)){
            $layer = $this->layerByUuid($uuid);
            if ($layer !== null) {
                return $layer;
            }
        }

        return new JsonLDResponse([
            "hydra:title" => "An error occurred",
            "hydra:description" => "Not Found"
        ],
            404, [], false, 'layer');
    }

    private function layerByUuid(string $uuid): ?Layer
    {
        $sql = 'SELECT id FROM public.metadata WHERE uuid = :uuid';
        $stmt = $this->managerRegistry->getConnection('catalogue')->prepare($sql);
        $publicMetadataId = $stmt->executeQuery(['uuid' => $uuid])->fetchOne();
        $layer = null;
        if ($publicMetadataId !== false) {
            $metadataSheet = $this->managerRegistry->getRepository(MetadataSheet::class)->findOneBy(
                ['publicMetadataId' => $publicMetadataId]
            );
            if ($metadataSheet !== null) {
                $layer = $this->managerRegistry->getRepository(Layer::class)->findOneBy(
                    ['sheetMetadata' => $metadataSheet]
                );

                $layer = $this->layerProvider->getItem(Layer::class, $layer->getId());
            }
        }
        return $layer;
    }

}