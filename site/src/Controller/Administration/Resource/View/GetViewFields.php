<?php

namespace App\Controller\Administration\Resource\View;

use App\Response\JsonLDResponse;
use App\Service\UserService;
use Doctrine\DBAL\Connection;
use Doctrine\Persistence\ManagerRegistry;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Attribute\AsController;

#[AsController]
class GetViewFields extends AbstractController
{
    protected Connection $prodigeCon;
    protected Connection $catalogueCon;

    public function __construct(private UserService $userService, ManagerRegistry $doctrine)
    {
        $this->prodigeCon = $doctrine->getConnection('prodige');
        $this->catalogueCon = $doctrine->getConnection('catalogue');
    }

    /**
     * @throws Exception
     */
    public function __invoke(string $layer_id, Request $request): JsonLDResponse
    {
        try {
            $results = [];
            $strSql = "SELECT storage_path, public_metadata_id FROM admin.layer l
                       JOIN admin.metadata_sheet ms ON l.metadata_sheet_id = ms.id 
                       WHERE l.id = {$layer_id}";
            $layer = $this->catalogueCon->executeQuery($strSql)->fetchAssociative();

            $verifyRights = $this->userService->getLayerRights($layer['public_metadata_id'], ['NAVIGATION'] );
            if ($verifyRights['NAVIGATION']) {
                $strSql = "SELECT * FROM information_schema.columns WHERE table_schema = 'public' AND table_name = '{$layer['storage_path']}'";
                $columns = $this->prodigeCon->executeQuery($strSql)->fetchAllAssociative();
                foreach ($columns as $column) {
                    $results[] = [
                        'column_name' => $column['column_name'],
                        'column_default' => $column['column_default'],
                        'is_nullable' => $column['is_nullable'] == 'YES',
                        'data_type' => $column['data_type'],
                    ];
                }
                return new JsonLDResponse(['fields' => $results]);
            } else {
                return new JsonLDResponse(['error' => 'You\'re not authorized to list field of this layer']);
            }
        } catch (Exception $exception) {
            return new JsonLDResponse(['error' => $exception->getMessage()], 400);
        }
    }
}
