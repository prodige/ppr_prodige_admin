<?php

namespace App\Controller\Administration\Resource\View;

use App\Response\JsonLDResponse;
use App\Service\UserService;
use Doctrine\DBAL\Connection;
use Doctrine\Persistence\ManagerRegistry;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Attribute\AsController;

#[AsController]
class GetViewDataset extends AbstractController
{
    protected Connection $prodigeCon;
    protected Connection $catalogueCon;

    public function __construct(private UserService $userService, ManagerRegistry $doctrine)
    {
        $this->prodigeCon = $doctrine->getConnection('prodige');
        $this->catalogueCon = $doctrine->getConnection('catalogue');
    }

    /**
     * @throws Exception
     */
    public function __invoke(string $layer_type, Request $request): JsonLDResponse
    {
        try {
            $results = [];
            $strSql = "SELECT l.id, name, storage_path, public_metadata_id FROM admin.layer l
                       JOIN admin.metadata_sheet ms ON l.metadata_sheet_id = ms.id 
                       WHERE lex_layer_type_id = {$layer_type}";
            $layers = $this->catalogueCon->executeQuery($strSql)->fetchAllAssociative();
            foreach ($layers as $layer) {
                $result = [
                    'layer_id' => $layer['id'],
                    'name' => $layer['name'],
                    'storage_path' => $layer['storage_path'],
                ];

                $verifyRights = $this->userService->getLayerRights($layer['public_metadata_id'], ['NAVIGATION']);

                $strSql = "SELECT uuid FROM public.metadata WHERE id = {$layer['public_metadata_id']}";
                $uuid = $this->catalogueCon->executeQuery($strSql)->fetchOne();
                if (empty($uuid)) {
                    return new JsonLDResponse(['error' => 'Uuid not found in public.metadata table'], 404);
                }
                $result['uuid'] = $uuid;

                if ($verifyRights['NAVIGATION']) {
                    $results[] = $result;
                }
            }

            return new JsonLDResponse(['layers' => $results]);
        } catch (Exception $exception) {
            return new JsonLDResponse(['error' => $exception->getMessage()], 400);
        }
    }
}
