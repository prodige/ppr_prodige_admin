<?php

namespace App\Controller\Administration\Resource;

use App\Entity\Administration\Directory\Profile\ProfilePrivilegeLayer;
use App\Entity\Administration\Directory\Profile\ProfilePrivilegeObject;
use App\Entity\Administration\Resource\Layer;
use App\Form\Administration\Directory\ProfilePrivilegeLayerType;
use App\Form\Administration\Directory\ProfilePrivilegeObjectType;
use App\Form\Administration\Resource\LayerType;
use App\Service\CatalogueService;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DataController extends AbstractController
{
    #[Route('/data', name: 'data_list', methods: ['GET'])]
    public function index(): Response
    {
        $form = $this->createForm(LayerType::class, new Layer());
        $render = [
            'form' => $form,
            'api_filter' => 'properties[]=id&properties[]=name&properties[]=wms&properties[]=wfs&properties[]=wms&properties[]=isAwardeeManagement&properties[]=isObjectManagement&properties[lexLayerType]=name&properties[]=lastImport&properties[]=restriction&properties[]=viewable&properties[]=download',
        ];

        return $this->renderForm('administration/resources/data/list.html.twig', $render);
    }

    #[Route('/layer/edit/{id}', name: 'data_edit', methods: ['GET'])]
    public function edit(int $id, ManagerRegistry $managerRegistry): Response
    {
        $entity = $managerRegistry->getRepository(Layer::class)->findOneBy(['id' => $id]);
        $form = $this->createForm(LayerType::class, $entity);
        $render = ['form' => $form];

        return $this->renderForm('common/entity_edit_form.html.twig', $render);
    }

    #[Route('/data/object', name: 'data_object_list', methods: ['GET', 'POST'])]
    public function objects(): Response
    {
        $form = $this->createForm(ProfilePrivilegeObjectType::class, new ProfilePrivilegeObject());
        $render = [
            'form' => $form,
            'api_filter' => 'properties[]=id&properties[profile]=name&properties[lexPrivilege]=name&properties[]=metadataSheet',
        ];

        return $this->renderForm('administration/resources/data/object_list.html.twig', $render);
    }

    #[Route('/data/attribute', name: 'data_attribute_list', methods: ['GET', 'POST'])]
    public function layers(): Response
    {
        $form = $this->createForm(ProfilePrivilegeLayerType::class, new ProfilePrivilegeLayer());
        $render = [
            'form' => $form,
            'api_filter' => json_encode('properties[]=id'),
        ];

        return $this->renderForm('administration/resources/data/attribute_list.html.twig', $render);
    }

}
