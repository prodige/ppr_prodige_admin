<?php

declare(strict_types=1);

namespace App\Controller\Administration\Resource;

use App\Entity\Administration\Resource\Map;
use App\Entity\Administration\Resource\MetadataSheet;
use App\Response\JsonLDResponse;
use Doctrine\DBAL\Connection;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Uid\Uuid;

class MapByUuidController extends AbstractController
{

    private Connection $prodigeCon;

    public function __construct(
        private ManagerRegistry $managerRegistry,
    ) {
        $this->prodigeCon = $managerRegistry->getConnection('prodige');
    }

    public function __invoke(Request $request): JsonLDResponse|Map
    {
        $uuid = $request->get('uuid');

        if(Uuid::isValid($uuid)){
            $map = $this->mapByUuid($uuid);
            if ($map !== null) {
                return $map;
            }
        }

        return new JsonLDResponse([
            "hydra:title" => "An error occurred",
            "hydra:description" => "Not Found"
        ],
            404, [], false, 'map');
    }

    private function mapByUuid(string $uuid): ?Map
    {
        $sql = 'SELECT id FROM public.metadata WHERE uuid = :uuid';
        $stmt = $this->managerRegistry->getConnection('catalogue')->prepare($sql);
        $publicMetadataId = $stmt->executeQuery(['uuid' => $uuid])->fetchOne();

        $map = null;
        if ($publicMetadataId !== false) {
            $metadataSheet = $this->managerRegistry->getRepository(MetadataSheet::class)->findOneBy(
                ['publicMetadataId' => $publicMetadataId]
            );

            if ($metadataSheet !== null) {
                $map = $this->managerRegistry->getRepository(Map::class)->findOneBy(
                    ['sheetMetadata' => $metadataSheet]
                );

            }
        }
        return $map;
    }

}
