<?php

declare(strict_types=1);

namespace App\Controller\Administration\Organisation;

use App\Entity\Administration\Organisation\Rubric;
use App\Form\Administration\Organisation\RubricType;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class RubricController extends AbstractController
{
    #[Route('/rubric', name: 'rubric_list', methods: ['GET'])]
    public function index(): Response
    {
        $form = $this->createForm(RubricType::class, new Rubric());
        $render = [
            'form' => $form,
            'api_filter' => 'properties[]=id&properties[]=name&properties[]=countDomain&properties[]=canDelete',
        ];

        return $this->renderForm('administration/organisation/rubric/list.html.twig', $render);
    }

    #[Route('/rubric/edit/{id}', name: 'rubric_edit', methods: ['GET'])]
    public function edit(int $id, ManagerRegistry $managerRegistry): Response
    {
        $entity = $managerRegistry->getRepository(Rubric::class)->findOneBy(['id' => $id]);
        $form = $this->createForm(RubricType::class, $entity);
        $render = ['form' => $form];

        return $this->renderForm('common/entity_edit_form.html.twig', $render);
    }
}
