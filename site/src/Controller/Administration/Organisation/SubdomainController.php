<?php

declare(strict_types=1);

namespace App\Controller\Administration\Organisation;

use App\Entity\Administration\Organisation\Subdomain;
use App\Form\Administration\Organisation\SubdomainType;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SubdomainController extends AbstractController
{
    #[Route('/subdomain', name: 'subdomain_list', methods: ['GET'])]
    public function index(): Response
    {
        $form = $this->createForm(SubdomainType::class, new Subdomain());
        $render = [
            'form' => $form,
            'api_filter' => 'properties[]=name&properties[]=id&properties[]=domain',
        ];

        return $this->renderForm('administration/organisation/subdomain/list.html.twig', $render);
    }

    #[Route('/subdomain/edit/{id}', name: 'subdomain_edit', methods: ['GET'])]
    public function edit(int $id, ManagerRegistry $managerRegistry): Response
    {
        $entity = $managerRegistry->getRepository(Subdomain::class)->findOneBy(['id' => $id]);
        $form = $this->createForm(SubdomainType::class, $entity);
        $render = ['form' => $form];

        return $this->renderForm('common/entity_edit_form.html.twig', $render);
    }
}
