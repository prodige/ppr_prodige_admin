<?php

declare(strict_types=1);

namespace App\Controller\Administration\Organisation;

use App\Entity\Administration\Organisation\Domain;
use App\Form\Administration\Organisation\DomainType;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DomainController extends AbstractController
{
    #[Route('/domain', name: 'domain_list', methods: ['GET'])]
    public function index(): Response
    {
        $form = $this->createForm(DomainType::class, new Domain());
        $render = [
            'form' => $form,
            'api_filter' => 'properties[]=id&properties[]=name&properties[rubric]=name&properties[]=countSubdomain&properties[]=canDelete',
        ];

        return $this->renderForm('administration/organisation/domain/list.html.twig', $render);
    }

    #[Route('/domain/edit/{id}', name: 'domain_edit', methods: ['GET'])]
    public function edit(int $id, ManagerRegistry $managerRegistry): Response
    {
        $entity = $managerRegistry->getRepository(Domain::class)->findOneBy(['id' => $id]);
        $form = $this->createForm(DomainType::class, $entity);
        $render = ['form' => $form];

        return $this->renderForm('common/entity_edit_form.html.twig', $render);
    }
}

