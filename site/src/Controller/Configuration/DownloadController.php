<?php

declare(strict_types=1);

namespace App\Controller\Configuration;

use App\Entity\Configuration\Engine;
use App\Entity\Configuration\Setting;
use App\Entity\Lex\LexEngineType;
use App\Form\Configuration\EngineType;
use App\Form\Configuration\ViewerSettingsType;
use Doctrine\Persistence\ManagerRegistry;
use Prodige\ProdigeBundle\Services\AdminClientService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

#[Route('/download')]
class DownloadController extends AbstractController
{
    private ParameterBagInterface $bagParameter;
    private AdminClientService $adminClientService;

    public function __construct(ParameterBagInterface $bag, AdminClientService $adminClientService){
        $this->bagParameter = $bag;
        $this->adminClientService = $adminClientService;
    }
    #[Route('/config', name: 'download_config', methods: ['GET'])]
    public function config(ManagerRegistry $managerRegistry): Response
    {
        $settings = $managerRegistry->getRepository(Setting::class)->findBy(['lexSettingCategory' => 8], ['id' => 'ASC']
        );
        $form = $this->createForm(ViewerSettingsType::class, null, ['settings' => $settings]);
        $render = [
            'form' => $form,
            'title' => 'viewer.config',
        ];

        return $this->renderForm('common/setting_list.html.twig', $render);
    }

    #[Route('/list', name: 'download_list', methods: ['GET'])]
    public function index(Security $security): Response
    {
        $url = $this->bagParameter->get('PRODIGE_URL_TELECHARGEMENT') . '/api/downloads?status.id%5B%5D=1&status.id%5B%5D=2&status.id%5B%5D=4';
        $downloads = $this->adminClientService->curlAdminWithAdmincli($url,'GET');

        $render = [
            'title' => 'download',
            'downloads'=> $downloads['hydra:member']
        ];

        return $this->renderForm('configuration/download/list.html.twig', $render);
    }

    #[Route('/delete/{id}', name: 'download_delete', methods: ['DELETE'])]
    public function delete($id): Response
    {
        $url = $this->bagParameter->get('PRODIGE_URL_TELECHARGEMENT') . '/api/downloads/' . $id;
        $delete = $this->adminClientService->curlAdminWithAdmincli($url,'DELETE');

        if($delete === true){
            return new Response('success', 204);
        }

        throw new BadRequestHttpException('failed',null, 404);
    }


    #[Route('/engine', name: 'download_engine', methods: ['GET'])]
    public function search(ManagerRegistry $managerRegistry): Response
    {
        $lexEngineTypeRepo = $managerRegistry->getRepository(LexEngineType::class);
        $lexEngineType = $lexEngineTypeRepo->findOneBy(['name' => 'download']);
        $form = $this->createForm(
            EngineType::class,
            new Engine(),
            ['attr' => ['id' => 'add'], 'lexEngineType' => $lexEngineType->getId()]
        );
        $render = [
            'form' => $form,
            'title_suffix' => 'download.engine',
            'api_filter' => 'properties[]=id&properties[]=name&properties[]=rowOrder&properties[]=layer&properties[]=fieldId&properties[]=fieldName&properties[joinId]=name&properties[]=canDelete&properties[]=joinField&lexEngineType='.$lexEngineType->getId(),
        ];

        return $this->renderForm('configuration/download/extract.html.twig', $render);
    }


    #[Route('/engine/edit/{id}', name: 'download_engine_edit', methods: ['GET'])]
    public function engine_edit(int $id, ManagerRegistry $managerRegistry): Response
    {
        $entity = $managerRegistry->getRepository(Engine::class)->findOneBy(['id' => $id]);
        $lexEngineTypeRepo = $managerRegistry->getRepository(LexEngineType::class);
        $lexEngineType = $lexEngineTypeRepo->findOneBy(['name' => 'download']);
        $form = $this->createForm(EngineType::class, $entity, ['lexEngineType' => $lexEngineType->getId()]);
        $render = ['form' => $form];

        return $this->renderForm('configuration/engine/engine_edit_form.html.twig', $render);
    }
}
