<?php

namespace App\Controller\Configuration\Mapserver;

use App\Entity\Configuration\Mapserver;
use App\Entity\Configuration\Setting;
use App\Service\MapserverApiService;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Attribute\AsController;
use Exception;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

#[AsController]
class MapserverController extends AbstractController
{
    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws ClientExceptionInterface
     */
    public function __invoke(
        Mapserver $data,
        Request $request,
        MapserverApiService $mapserverApiService,
        ManagerRegistry $manager
    ): Mapserver {
        if (!empty($data->getMap())) {
            $map = $data->getMap();
            unset($map['reference']);
            $map['metadata'] = json_encode($map['metadata']);
            $map['directory'] = $data->getDirectory();
            $file = $request->get('file');
            $data->setFile($file);
            $postMap = $mapserverApiService->postMap($file, $map);

            if ((int)$postMap['status_code'] === 200) {
                if ($file === 'wms' || $file === 'wfs') {
                    $settings = $manager->getRepository(Setting::class)->findBy(
                        [
                            'name' => [
                                $file . '_server_title',
                                $file . '_server_abstract',
                            ]
                        ]
                    );
                    foreach ($settings as $setting) {
                        switch ($setting->getTitle()) {
                            case $file . '_server_title' :
                                if (isset($data->getMap()['metadata'][$file . '_title'])) {
                                    $setting->setValue(
                                        $data->getMap()['metadata'][$file . '_title']
                                    );
                                }
                                break;
                            case $file . '_server_abstract' :
                                if (isset($data->getMap()['metadata'][$file . '_abstract'])) {
                                    $setting->setValue(
                                        $data->getMap()['metadata'][$file . '_abstract']
                                    );
                                }
                                break;
                        }
                        $manager->getManager()->persist($setting);
                    }
                    $manager->getManager()->flush();
                }
            }

            return $data;
        }
        throw new Exception('parameters are incorrect or missing', 400);
    }
}