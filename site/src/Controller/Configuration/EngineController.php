<?php

namespace App\Controller\Configuration;

use App\DataProvider\MapserverProvider;
use App\Entity\Configuration\Engine;
use App\Entity\Configuration\Mapserver;
use App\Entity\Configuration\Setting;
use App\Exception\NotFoundException;
use App\Service\MapserverApiService;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Attribute\AsController;
use Exception;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;

#[AsController]
class EngineController extends AbstractController
{

    /**
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ClientExceptionInterface
     * @throws NotFoundException
     */
    public function __invoke(
        Engine $data,
        Request $request,
        MapserverProvider $mapserverProvider,
        ManagerRegistry $managerRegistry,
        MapserverApiService $mapserverApiService,
        ParameterBagInterface $bag,
    ): JsonResponse {
        $file = 'locator';
        // Call MapServer API
        // Get Map (list layers)
        $mapserver = $mapserverProvider->getItem(Mapserver::class, $file, 'get_map');
        $layers = $mapserver->getMap()['layers'];

        // Delete all layer
        foreach ($layers as $layer) {
            $mapserverApiService->deleteLayer(
                $file,
                $layer['name'],
                ['directory' => $mapserver->getDirectory()]
            );
        }

        // Get layers in DB (engines)
        $engines = $managerRegistry->getRepository(Engine::class)->findBy(["lexEngineType" => 2]);

        // Get default projection
        $setting = $managerRegistry->getRepository(Setting::class)->findOneBy(['name' => 'PRO_IMPORT_EPSG']);
        $epsg = $setting->getProjections()->first()->getEpsg();
        $projection = 'init=epsg:' . $epsg;

        // Add all layers
        foreach ($engines as $engine) {
            $joinId = $engine->getJoinId()?->getName() ? ", " . $engine->getJoinId()->getName() : "";
            // query
            $data = "the_geom from (select ST_Envelope(the_geom) as the_geom, gid" . $joinId .
                " from " . $engine->getLayer()->getStoragePath() . " order by " .
                $engine->getFieldName() . ") as foo using unique gid using srid=" . $epsg;
            $name = "layer" . $engine->getId() . "_locator";

            $mapserverApiService->addLayerToFile(
                $file,
                $name,
                [
                    'directory' => $mapserver->getDirectory(),
                    'name' => $name,
                    'type' => 'POLYGON',
                    'projection' => $projection,
                    'status' => 'ON',
                    'tileitem' => 'location',
                    'connectionType' => 'POSTGIS',
                    'connection' => "user=" . $bag->get('prodige_user') . " password=" . $bag->get('prodige_password') .
                        " dbname=" . $bag->get('prodige_name') .
                        " host=" . $bag->get('prodige_host') . " port=" . $bag->get('prodige_port'),
                    'data' => $data,
                    'metadata' => [
                        "ows_include_items" => "all",
                        "gml_include_items" => "all",
                        "gml_types" => "auto"
                    ]
                ]
            );
        }

        return new JsonResponse(null, 201);
    }
}