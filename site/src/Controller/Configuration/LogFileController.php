<?php

declare(strict_types=1);

namespace App\Controller\Configuration;

use App\Response\JsonLDResponse;
use App\Service\LogService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class LogFileController extends AbstractController
{
    public function __construct(private LogService $logService)
    {
    }

    public function __invoke(int $id)
    {
        $log = $this->logService->getOneLogs($id);
        if ($log === null) {
            return new JsonLDResponse(['error' => "Le fichier n'existe pas"],
                404,
                [],
                false,
                "Download",
                $id);
        }
        $response = $this->logService->getFile($log);

        if (is_string($response)) {
            return new JsonLDResponse(['error' => $response],
                404,
                [],
                false,
                "Download",
                $id
            );
        }

        return $response;
    }
}
