<?php

declare(strict_types=1);

namespace App\Controller\Configuration;

use App\Entity\Configuration\Setting;
use App\Form\Configuration\CatalogueType;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CatalogueController extends AbstractController
{
    #[Route('/catalogue', name: 'catalogue_config', methods: ['GET'])]
    public function index(ManagerRegistry $managerRegistry): Response
    {
        $settings = $managerRegistry->getRepository(Setting::class)
            ->findBy(['lexSettingCategory' => 2], ['settingOrder' => 'ASC', 'id' => 'ASC']);
        $form = $this->createForm(CatalogueType::class, null, ['settings' => $settings]);
        $render = ['form' => $form];

        return $this->renderForm('common/setting_list.html.twig', $render);
    }
}
