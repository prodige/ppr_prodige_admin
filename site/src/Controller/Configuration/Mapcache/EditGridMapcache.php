<?php

namespace App\Controller\Configuration\Mapcache;

use App\Entity\Configuration\Mapcache;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Attribute\AsController;

#[AsController]
class EditGridMapcache extends AbstractController
{
    /**
     * @throws \Exception
     */
    public function __invoke(Mapcache $data, Request $request): Mapcache
    {
        $body = json_decode($request->getContent(), true);
        if (is_array($body)) {
            $result = $data->editGrid($request->get('name'), $body);
            if ($result['code']) {
                $data->generateTiles();
                $data->generateGrids();
                return $data;
            }
            throw new \Exception($result['message'], 400);
        }
        throw new \Exception('parameters are incorrect or missing', 400);
    }
}