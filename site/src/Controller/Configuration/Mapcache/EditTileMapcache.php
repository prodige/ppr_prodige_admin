<?php

namespace App\Controller\Configuration\Mapcache;

use App\Entity\Configuration\Mapcache;
use App\Service\AdminCartoService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Attribute\AsController;

#[AsController]
class EditTileMapcache extends AbstractController
{
    public function __construct(private AdminCartoService $adminCartoService)
    {
    }

    /**
     * @throws \Exception
     */
    public function __invoke(Mapcache $data, Request $request): Mapcache
    {
        $data->setAdminCartoService($this->adminCartoService);
        $body = json_decode($request->getContent(), true);
        if (is_array($body)) {
            $result = $data->editTile($request->get('name'), $body);
            if ($result['code']) {
                $data->generateTiles();
                $data->generateGrids();
                return $data;
            }
            throw new \Exception($result['message'], 400);
        }
        throw new \Exception('parameters are incorrect or missing', 400);
    }
}
