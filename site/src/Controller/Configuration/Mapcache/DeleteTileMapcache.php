<?php

namespace App\Controller\Configuration\Mapcache;

use App\Entity\Configuration\Mapcache;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Attribute\AsController;

#[AsController]
class DeleteTileMapcache extends AbstractController
{
    /**
     * @throws \Exception
     */
    public function __invoke(Mapcache $data, Request $request): Mapcache
    {
        $result = $data->removeTile($request->get('name'));
        if ($result['code']) {
            $data->generateTiles();
            $data->generateGrids();
            return $data;
        }
        throw new \Exception($result['message'], 400);
    }
}