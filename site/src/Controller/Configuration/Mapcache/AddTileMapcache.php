<?php

namespace App\Controller\Configuration\Mapcache;

use App\Entity\Administration\Resource\Layer;
use App\Entity\Configuration\Mapcache;
use App\Service\AdminCartoService;
use App\Service\CatalogueService;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Attribute\AsController;

#[AsController]
class AddTileMapcache extends AbstractController
{
    public function __construct(private AdminCartoService $adminCartoService)
    {
    }

    /**
     * @throws \Exception
     */
    public function __invoke(
        Mapcache $data,
        Request $request,
        ManagerRegistry $manager,
        ParameterBagInterface $parameterBag,
        CatalogueService $catalogueService
    ): Mapcache {
        $data->setAdminCartoService($this->adminCartoService);
        $body = json_decode($request->getContent(), true);
        if (is_array($body)) {
            $layer = $manager->getRepository(Layer::class)->findOneBy(['storagePath' => $body['name']]);
            if ($layer) {
                $body['tiles']['layer']['path'] = $parameterBag->get('PRODIGE_PATH_DATA').'/cartes/Publication/';
                $body['tiles']['layer']['uuid'] = $catalogueService->getUuidFromMetadata($layer);
                $result = $data->addNewTile($body['name'], $body['tiles']);
                if ($result['code']) {
                    $data->generateTiles();
                    $data->generateGrids();

                    return $data;
                }
                throw new \Exception($result['message'], 400);
            }
            throw new \Exception('no layer found', 400);
        }
        throw new \Exception('parameters are incorrect or missing', 400);
    }
}
