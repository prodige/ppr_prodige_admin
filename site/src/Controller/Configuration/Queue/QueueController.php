<?php

namespace App\Controller\Configuration\Queue;

use App\Entity\Configuration\Queuefile;
use App\Exception\NotFoundException;
use App\Service\QueuefileService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Attribute\AsController;
use Exception;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;

#[AsController]
class QueueController extends AbstractController
{

    /**
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ClientExceptionInterface
     * @throws NotFoundException
     */
    public function __invoke(
        Queuefile $data,
        Request $request,
        QueuefileService $queueService
    ): Queuefile|NotFoundException {
        switch ($request->get('_api_item_operation_name')) {
            case 'remove':
                if (!$queueService->removeFile($request->get('file'))) {
                    throw new NotFoundException("File ".$request->get('file')." not found");
                }
                break;
            case 'flush':
                $queueService->removeAllFile();
                break;
        }

        return new NotFoundException("File ".$request->get('file')." not found");
    }
}