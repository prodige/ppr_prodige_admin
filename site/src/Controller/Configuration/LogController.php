<?php

declare(strict_types=1);

namespace App\Controller\Configuration;

use App\Entity\Configuration\Log;
use App\Response\JsonLDResponse;
use App\Service\LogService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class LogController extends AbstractController
{
    public function __construct(private SerializerInterface $serializer, private LogService $logService)
    {
    }

    #[Route('/log', name: 'log_list', methods: ['GET'])]
    public function index(Request $request)
    {
        $render = [
            'downloadBtn' => true
        ];

        if ($request->query->get('error')) {
            $render['error'] = $request->query->get('error');
        }

        return $this->render('configuration/log/list.html.twig', $render);
    }


    #[Route('/logs', name: 'get_logs_collection', methods: ['GET'])]
    public function getListLogs()
    {
        $values = $this->logService->getAllLogs();
        $logs = [];

        foreach ($values as $id => $name) {
            $log = new Log();
            $log->setId($id);
            $log->setName($name);
            $log = $this->serializer->serialize($log, 'json');
            $logs[] = json_decode($log);
        }

        $response = ['hydra:totalItems' => count($values), 'hydra:member' => $logs];

        return new JsonLDResponse($response, 200, [], false, 'Logs');
    }

    #[Route('/log/{id}', name: 'get_log_file', methods: ['GET'])]
    public function getFileLog(int $id): BinaryFileResponse|RedirectResponse
    {
        $log = $this->logService->getOneLogs($id);
        if ($log === null) {
            return $this->redirectToRoute('log_list', ['error' => 'Fichier invalide']);
        }

        $response = $this->logService->getFile($log);

        if (is_string($response)) {
            return $this->redirectToRoute('log_list', ['error' => $response]);
        }

        return $response;
    }
}
