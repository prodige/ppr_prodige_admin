<?php

namespace App\Controller\Configuration;

use App\Entity\Configuration\Engine;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Exception;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Attribute\AsController;

#[AsController]
class EngineDataController extends AbstractController
{
    protected Connection $prodigeCon;

    public function __construct(ManagerRegistry $doctrine)
    {
        $this->prodigeCon = $doctrine->getConnection('prodige');
    }

    /**
     * @throws Exception
     */
    public function __invoke(Engine $data, Request $request): JsonResponse
    {
        $storagePath = $data->getLayer()->getStoragePath();
        $fieldId = $data->getFieldId();
        $fieldName = $data->getFieldName();
        $strSql = "SELECT engine.$fieldId as value, engine.$fieldName as label  FROM public.$storagePath engine";

        if ($data->getJoinId()) {
            $joinOn = $data->getJoinField();
            $joinStorage = $data->getJoinId()->getLayer()->getStoragePath();
            $strSql .= " JOIN public.$joinStorage joined ON joined.$joinOn = engine.$fieldId";
        }

        $results = $this->prodigeCon->executeQuery($strSql)->fetchAllAssociative();

        return new JsonResponse($results);
    }
}
