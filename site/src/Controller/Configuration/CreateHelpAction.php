<?php
namespace App\Controller\Configuration;

use App\Entity\Administration\Directory\Profile\Profile;
use App\Entity\Configuration\Help;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Attribute\AsController;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

#[AsController]
final class CreateHelpAction extends AbstractController
{
    public function __invoke(Request $request, ManagerRegistry $managerRegistry): Help
    {
        $uploadedFile = $request->files->get('file');
        if (!$uploadedFile) {
            throw new BadRequestHttpException('"file" is required');
        }

        $help = new Help();
        $help->file = $uploadedFile;
        $help->setTitle($request->get('title'));
        $help->setDescription($request->get('description'));

        $profiles = explode(',', $request->get('profiles'));
        $profileRepository = $managerRegistry->getRepository(Profile::class);
        foreach($profiles as $profile) {
            if (preg_match('#\/api\/profiles\/([0-9]+)#', $profile, $matches)) {
                $entity = $profileRepository->find($matches[1]);
                if ($entity) {
                    $help->addProfile($entity);
                }
            }
        }

        return $help;
    }
}
