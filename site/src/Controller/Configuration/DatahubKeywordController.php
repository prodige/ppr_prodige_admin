<?php

namespace App\Controller\Configuration;

use App\Entity\Configuration\DatahubKeyword;
use App\Form\Datahub\DatahubKeywordType;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class DatahubKeywordController extends AbstractController
{
    #[Route('/datahub_keyword', name: 'datahub_keyword_list', methods: ['GET'])]
    public function index(): Response
    {
        $form = $this->createForm(DatahubKeywordType::class, new DatahubKeyword());
        $render = [
            'form' => $form,
            'api_filter' => 'properties[]=id&properties[]=name&properties[]=filter&properties[]=canDelete',
        ];

        return $this->renderForm('datahub/datahub_keyword_list.html.twig', $render);
    }

    #[Route('/datahub_keyword/edit/{id}', name: 'datahub_keyword_edit', methods: ['GET'])]
    public function edit(int $id, ManagerRegistry $managerRegistry): Response
    {
        $entity = $managerRegistry->getRepository(DatahubKeyword::class)->findOneBy(['id' => $id]);
        $form = $this->createForm(DatahubKeywordType::class, $entity);
        $render = ['form' => $form];

        return $this->renderForm('common/entity_edit_form.html.twig', $render);
    }
}