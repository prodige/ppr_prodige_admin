<?php

declare(strict_types=1);

namespace App\Controller\Configuration;

use App\Entity\Configuration\Engine;
use App\Entity\Configuration\Setting;
use App\Entity\Configuration\Template;
use App\Entity\Lex\LexEngineType;
use App\Form\Configuration\EngineType;
use App\Form\Configuration\TemplateType;
use App\Form\Configuration\CommonSettingsType;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/viewer')]
class ViewerController extends AbstractController
{
    #[Route('/template', name: 'viewer_list', methods: ['GET'])]
    public function index(): Response
    {
        $form = $this->createForm(TemplateType::class, new Template());
        $render = [
            'form' => $form,
            'api_filter' => 'properties[]=id&properties[]=name&properties[]=mapfile&properties[lexTemplateType]=name&properties[]=uuid',
            'admincarto_url' => $this->container->get('parameter_bag')->get('PRODIGE_URL_ADMINCARTO').'/edit_map/',
        ];

        return $this->renderForm('configuration/viewer/template.html.twig', $render);
    }

    #[Route('/edit/{id}', name: 'viewer_edit', defaults: ['id' => 0], methods: ['GET'])]
    public function edit(int $id, ManagerRegistry $managerRegistry): Response
    {
        $entity = $managerRegistry->getRepository(Template::class)->findOneBy(['id' => $id]);
        $form = $this->createForm(TemplateType::class, $entity);
        $render = ['form' => $form];

        return $this->renderForm('common/entity_edit_form.html.twig', $render);
    }

    #[Route('/search', name: 'viewer_engine', methods: ['GET'])]
    public function search(ManagerRegistry $managerRegistry): Response
    {
        $lexEngineTypeRepo = $managerRegistry->getRepository(LexEngineType::class);
        $lexEngineType = $lexEngineTypeRepo->findOneBy(['name' => 'search']);
        $form = $this->createForm(
            EngineType::class,
            new Engine(),
            ['attr' => ['id' => 'add'], 'lexEngineType' => $lexEngineType->getId()]
        );
        $render = [
            'form' => $form,
            'title_suffix' => 'viewer.engine',
            'api_filter' => 'properties[]=id&properties[]=name&properties[]=rowOrder&properties[]=layer&properties[]=fieldId&properties[]=fieldName&properties[joinId]=name&properties[]=canDelete&properties[]=joinField&lexEngineType='.$lexEngineType->getId(),
        ];

        return $this->renderForm('configuration/viewer/search.html.twig', $render);
    }

    #[Route('/engine/edit/{id}', name: 'viewer_engine_edit', methods: ['GET'])]
    public function engine_edit(int $id, ManagerRegistry $managerRegistry): Response
    {
        $entity = $managerRegistry->getRepository(Engine::class)->findOneBy(['id' => $id]);
        $lexEngineTypeRepo = $managerRegistry->getRepository(LexEngineType::class);
        $lexEngineType = $lexEngineTypeRepo->findOneBy(['name' => 'search']);
        $form = $this->createForm(EngineType::class, $entity, ['lexEngineType' => $lexEngineType->getId()]);
        $render = ['form' => $form];

        return $this->renderForm('configuration/engine/engine_edit_form.html.twig', $render);
    }

    #[Route('/config', name: 'viewer_config', methods: ['GET'])]
    public function config(ManagerRegistry $managerRegistry): Response
    {
        $settings = $managerRegistry->getRepository(Setting::class)
            ->findBy(['lexSettingCategory' => 7], ['id' => 'ASC']);
        $form = $this->createForm(CommonSettingsType::class, null, ['settings' => $settings]);
        $render = [
            'form' => $form,
            'title' => 'viewer.config',
        ];

        return $this->renderForm('common/setting_list.html.twig', $render);
    }
}
