<?php

declare(strict_types=1);

namespace App\Controller\Configuration;

use App\DataProvider\ProjectionProvider;
use App\Entity\Configuration\Projection;
use App\Entity\Configuration\Setting;
use App\Form\Configuration\ProjectionSettingsType;
use App\Form\Configuration\ProjectionType;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ProjectionController extends AbstractController
{
    #[Route('/projection', name: 'projection_list', methods: ['GET'])]
    public function index(): Response
    {
        $form = $this->createForm(ProjectionType::class, new Projection());
        $render = [
            'form' => $form,
            'api_filter' => 'properties[]=id&properties[]=name',
        ];

        return $this->renderForm('configuration/projection/list.html.twig', $render);
    }

    #[Route('/projection/edit/{id}', name: 'projection_edit', methods: ['GET'])]
    public function edit(int $id, ManagerRegistry $managerRegistry, ProjectionProvider $projectionProvider): Response
    {
        $entity = $managerRegistry->getRepository(Projection::class)->findOneBy(['id' => $id]);
        $projectionProvider->getItem(Projection::class, $entity->getEpsg());

        $form = $this->createForm(ProjectionType::class, $entity);
        $render = ['form' => $form, 'id' => 'epsg'];

        return $this->renderForm('common/entity_edit_form.html.twig', $render);
    }

    #[Route('/projection/config', name: 'projection_config', methods: ['GET'])]
    public function config(ManagerRegistry $managerRegistry): Response
    {
        $settings = $managerRegistry->getRepository(Setting::class)
            ->findBy(['lexSettingCategory' => 3], ['settingOrder' => 'ASC', 'id' => 'ASC']);
        $form = $this->createForm(ProjectionSettingsType::class, null, ['settings' => $settings]);
        $render = ['form' => $form];

        return $this->renderForm('configuration/projection/config.html.twig', $render);
    }
}
