<?php

declare(strict_types=1);

namespace App\Controller\Configuration;

use App\Entity\Configuration\Help;
use App\Form\Configuration\HelpType;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HelpController extends AbstractController
{
    #[Route('/helps', name: 'help_list', methods: ['GET'])]
    public function index(): Response
    {
        $form = $this->createForm(HelpType::class, new Help(), ['attr' => ['id' => 'add']]);
        $render = [
            'form' => $form,
            'api_filter' => 'properties[]=id&properties[]=title&properties[]=description&properties[]=contentUrl',
        ];

        return $this->renderForm('configuration/help/list.html.twig', $render);
    }

    #[Route('/help/edit/{id}', name: 'help_edit', methods: ['GET'])]
    public function edit(int $id, ManagerRegistry $managerRegistry): Response
    {
        $entity = $managerRegistry->getRepository(Help::class)->findOneBy(['id' => $id]);
        $form = $this->createForm(HelpType::class, $entity);
        $render = ['form' => $form];

        return $this->renderForm('common/entity_edit_form.html.twig', $render);
    }
}
