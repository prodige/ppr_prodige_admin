<?php

declare(strict_types=1);

namespace App\Controller\Configuration;

use App\Entity\Configuration\DatahubSetting;
use App\Form\Datahub\DatahubType;
use App\Service\DatahubService;
use Doctrine\Persistence\ManagerRegistry;
use Prodige\ProdigeBundle\Services\AdminClientService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class DatahubController extends AbstractController
{
    public function __construct(
        private DatahubService $datahubService,
        private ParameterBagInterface $parameterBag,
        private AdminClientService $adminClientService
    ) {
    }

    #[Route('/datahub', name: 'datahub_config', methods: ['GET'])]
    public function index(ManagerRegistry $managerRegistry): Response
    {
        $datahub = $managerRegistry->getRepository(DatahubSetting::class)
            ->findby([], ['settingOrder' => 'ASC', 'id' => 'ASC']);

        $form = $this->createForm(DatahubType::class, null, ['settings' => $datahub]);
        $render = ['form' => $form];

        return $this->renderForm('datahub/datahub_setting_list.html.twig', $render);
    }


    #[Route('/datahub/default.toml', name: 'toml_get', methods: ['GET'])]
    public function getToml(): Response
    {
        $file = $this->datahubService->generateToml();

        $response = new Response();

        $response->headers->set('Content-Type', 'application/octet-stream');
        $response->headers->set('Content-Description', 'File Transfer');
        $response->headers->set('Content-Transfer-Encoding', 'binary');
        $response->headers->set('Content-Length', strlen($file->getTomlString()));
        $response->setContent($file->getTomlString());

        return $response;
    }

}