<?php

declare(strict_types=1);

namespace App\Controller\Configuration;

use App\DataProvider\MapcacheProvider;
use App\Entity\Configuration\Mapcache;
use App\Entity\Configuration\Setting;
use App\Form\Configuration\AtomSettingsType;
use App\Form\Configuration\CommonSettingsType;
use App\Form\Configuration\GridType;
use App\Form\Configuration\TileType;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class OGCServicesController extends AbstractController
{
    #[Route('/wms', name: 'wms_config', methods: ['GET'])]
    public function wms(ManagerRegistry $managerRegistry): Response
    {
        $settings = $managerRegistry->getRepository(Setting::class)
            ->findBy(['lexSettingCategory' => 4], ['settingOrder' => 'ASC', 'id' => 'ASC']);
        $form = $this->createForm(CommonSettingsType::class, null, ['settings' => $settings]);
        $render = [
            'form' => $form,
            'title' => 'wms.config',
        ];

        return $this->renderForm('common/setting_list.html.twig', $render);
    }

    #[Route('/wfs', name: 'wfs_config', methods: ['GET'])]
    public function wfs(ManagerRegistry $managerRegistry): Response
    {
        $settings = $managerRegistry->getRepository(Setting::class)
            ->findBy(['lexSettingCategory' => 5], ['settingOrder' => 'ASC', 'id' => 'ASC']);
        $form = $this->createForm(CommonSettingsType::class, null, ['settings' => $settings]);
        $render = [
            'form' => $form,
            'title' => 'wfs.config',
        ];

        return $this->renderForm('common/setting_list.html.twig', $render);
    }

    #[Route('/wmts/grid', name: 'wmts_grid_list', methods: ['GET'])]
    public function grids(): Response
    {
        $form = $this->createForm(GridType::class, new Mapcache());
        $render = [
            'title' => 'grid',
            'form' => $form,
            'api_path' => 'api_mapcaches_get_grids_collection',
        ];

        return $this->renderForm('configuration/ogc_services/grid_list.html.twig', $render);
    }


    #[Route('/wmts/grid/edit/{name}', name: 'wmts_grid_edit', methods: ['GET'])]
    public function grid_edit(string $name): Response
    {
        $mapcache = new Mapcache();
        $mapcache->getGridByName($name);
        $form = $this->createForm(GridType::class, $mapcache);
        $render = [
            'form' => $form,
            'api_path' => 'api_mapcaches_get_grids_collection'
        ];

        return $this->renderForm('configuration/ogc_services/mapcache_edit_form.html.twig', $render);
    }


    #[Route('/wmts/grid/{name}/tile', name: 'wmts_tile_list', methods: ['GET'])]
    public function tiles(string $name): Response
    {
        $grid = new Mapcache();
        $grid = $grid->getGridByName($name);
        $form = $this->createForm(TileType::class, new Mapcache(), ['grid' => $name]);
        $render = [
            'title' => 'tile',
            'form' => $form,
            'grid' => $grid,
            'get_tiles_api_path' => '/api/mapcache/grids/'.$name.'/tiles',
            'api_path' => 'api_mapcaches_get_tiles_collection',
        ];

        return $this->renderForm('configuration/ogc_services/tile_list.html.twig', $render);
    }


    #[Route('/wmts/grid/{name}/tile/edit/{tile}', name: 'wmts_tile_edit', methods: ['GET'])]
    public function tile_edit(string $name, string $tile): Response
    {
        $mapcache = new Mapcache();
        $mapcache->getTileByName($tile);
        $form = $this->createForm(TileType::class, $mapcache);
        $render = [
            'form' => $form,
            'api_path' => 'api_mapcaches_get_tiles_collection'
        ];

        return $this->renderForm('configuration/ogc_services/mapcache_edit_form.html.twig', $render);
    }


    #[Route('/atom', name: 'atom_config', methods: ['GET'])]
    public function atom(ManagerRegistry $managerRegistry): Response
    {
        $settings = $managerRegistry->getRepository(Setting::class)
        ->findBy(['lexSettingCategory' => 6], ['settingOrder' => 'ASC', 'id' => 'ASC']);
        $form = $this->createForm(AtomSettingsType::class, null, ['settings' => $settings]);
        $render = ['form' => $form];

        return $this->renderForm('configuration/ogc_services/config.html.twig', $render);
    }

    #[Route('/ogcapi', name: 'ogcapi_config', methods: ['GET'])]
    public function ogcapi(ManagerRegistry $managerRegistry): Response
    {
        $settings = $managerRegistry->getRepository(Setting::class)
            ->findBy(['lexSettingCategory' => 9], ['settingOrder' => 'ASC', 'id' => 'ASC']);
        $form = $this->createForm(CommonSettingsType::class, null, ['settings' => $settings]);
        $render = [
            'form' => $form,
            'title' => 'ogcapi_settings.config',
        ];

        return $this->renderForm('common/setting_list.html.twig', $render);
    }
}
