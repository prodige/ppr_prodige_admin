<?php

declare(strict_types=1);

namespace App\Controller\Configuration;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ModuleController extends AbstractController
{
    #[Route('/module', name: 'module_list', methods: ['GET'])]
    public function index(): Response
    {
        $render = [
            'title' => 'Module list',
            'api_filter' => 'properties[]=id&properties[]=title&properties[]=value&properties[]=lexSettingCategory&lexSettingCategory.id=1',
        ];

        return $this->render('configuration/module/list.html.twig', $render);
    }
}
