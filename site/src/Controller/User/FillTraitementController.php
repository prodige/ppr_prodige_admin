<?php

declare(strict_types=1);

namespace App\Controller\User;

use App\Entity\Administration\Directory\User\User;
use App\Response\JsonLDResponse;
use App\Service\UserService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;

class FillTraitementController extends AbstractController
{
    public function __construct(
        private UserService $userService,
        private Security $security,
        private EntityManagerInterface $entityManager
    ) {
    }

    /**
     * @throws \Exception
     */
    public function __invoke(Request $request): JsonLDResponse
    {
        $user = $this->entityManager->getRepository(User::class)->findOneBy(
            ['login' => $this->security->getUser()->getUsername()]
        );
        $t = $this->userService->fillTraitements($user);
        dd($t);
    }

}
