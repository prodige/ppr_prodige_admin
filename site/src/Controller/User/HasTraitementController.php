<?php

declare(strict_types=1);

namespace App\Controller\User;

use App\Response\JsonLDResponse;
use App\Service\UserService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

class HasTraitementController extends AbstractController
{

    public function __construct(private UserService $userService)
    {
    }

    /**
     * @throws \Exception
     */
    public function __invoke(Request $request): JsonLDResponse
    {
        $domain_id = is_null($request->get('domain_id')) ? null : (int)$request->get('domain_id');
        $subdomain_id = is_null($request->get('subdomain_id')) ? null : (int)$request->get('subdomain_id');
        $metadata_id = is_null($request->get('metadata_id')) ? null : (int)$request->get('metadata_id');

        $hasTraitement = $this->userService->hasTraitement(
            $request->get('traitements'),
            $domain_id,
            $subdomain_id,
            $metadata_id
        );
        return new JsonLDResponse(["asTraitement" => $hasTraitement], 200, [], false, "Traitements");
    }

}
