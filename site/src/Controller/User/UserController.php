<?php

declare(strict_types=1);

namespace App\Controller\User;

use App\Entity\Administration\Directory\User\User;
use App\Response\JsonLDResponse;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Common\Annotations\AnnotationReader;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactory;
use Symfony\Component\Serializer\Mapping\Loader\AnnotationLoader;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Serializer\Serializer;
use Nelmio\ApiDocBundle\Annotation as Nelmio;
use OpenApi\Attributes as OA;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class UserController extends AbstractController
{

    public function __construct(
        private ManagerRegistry $managerRegistry,
        private Security        $security
    )
    {
    }

    #[Nelmio\Areas(['internal'])]
    #[OA\Tag(name: 'Authorization')]
    #[OA\Response(response: 200, description: 'All Right for Prodige')]
    #[OA\Response(response: 401, description: 'Authentication mandatory')]
    #[Route('/api/internal/me', name: 'prodige_me', methods: ['GET'])]
    public function userMeAction(Request $request): JsonLDResponse
    {
        $user = $this->security->getUser();
        if (is_null($user)) {
            throw new AccessDeniedException();
        }
        $user = $this->managerRegistry->getRepository(User::class)->findOneBy(['login' => $user->getUsername()]);
        $classMetadataFactory = new ClassMetadataFactory(new AnnotationLoader(new AnnotationReader()));
        $normalizer = new ObjectNormalizer($classMetadataFactory);
        $serializer = new Serializer([$normalizer]);
        $data = $serializer->normalize($user, null, ['groups' => 'GetUserMe']);

        return new JsonLDResponse($data, 200, [], false, "User", $user->getId());
    }


}
