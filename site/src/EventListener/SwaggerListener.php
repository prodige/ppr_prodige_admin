<?php

declare(strict_types=1);

namespace App\EventListener;

use ApiPlatform\Core\Bridge\Symfony\Bundle\EventListener\SwaggerUiListener;
use App\Service\OGCApiService;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Twig\Environment;
use Symfony\Component\HttpFoundation\Response;

final class SwaggerListener
{
    public const DISABLE_OPENAPI = '_api_openapi_doc';

    public function __construct(
        private Environment $twig, private OGCApiService $apiService
    )
    {
    }

    public function onKernelRequest(RequestEvent $event): void
    {
        $request = $event->getRequest();
        if (
            'html' !== $request->getRequestFormat('') ||
            !($request->attributes->has('_api_resource_class') || $request->attributes->getBoolean('_api_respond', false))
        ) {
            return;
        }

        if ($request->attributes->get(self::DISABLE_OPENAPI) === false) {
            // Peut-être un service pour generer le bon template ?
            $response = $this->apiService->findTemplate(
                $event->getRequest()->attributes->get('_api_ressource_template'),
                $event->getRequest()->attributes->get('data'),
                $request
            );

//            $response = new Response(
//                $this->twig->render('ogc/template.html.twig',
//                    [
//                        'request' => $event->getRequest(),
//                        'data' => $event->getRequest()->attributes->get('data'),
//                        'route' => $event->getRequest()->attributes->get('_route'),
//                        'identifiers' => $event->getRequest()->attributes->get('_api_identifiers'),
//                        'ressource' => $event->getRequest()->attributes->get('_api_ressource_title'),
//                    ]
//                )
//            );
            $event->setResponse($response);
        } else {
            $request->attributes->set('_controller', 'api_platform.swagger.action.ui');
        }
    }
}