<?php

declare(strict_types=1);

namespace App\EventListener;

use App\Entity\Administration\Directory\Area;
use App\Entity\Administration\Directory\Profile\Profile;
use App\Entity\Administration\Directory\Skill;
use App\Entity\Administration\Directory\User\User;
use App\Entity\Administration\Directory\User\UserAreaAccess;
use App\Entity\Administration\Directory\User\UserAreaAlert;
use App\Entity\Administration\Directory\User\UserAreaEdition;
use App\Entity\Administration\Organisation\Domain;
use App\Entity\Administration\Organisation\Rubric;
use App\Entity\Administration\Organisation\Subdomain;
use App\Entity\Administration\Resource\Layer;
use App\Entity\Administration\Resource\Map;
use App\Entity\Administration\Resource\MetadataSheet;
use App\Entity\Administration\Directory\Structure;
use App\Entity\Configuration\Engine;
use Doctrine\Bundle\DoctrineBundle\Attribute\AsDoctrineListener;
use Prodige\ProdigeBundle\Services\CacheService;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Doctrine\ORM\Events;
use Doctrine\Persistence\Event\LifecycleEventArgs;

use Memcached;

#[AsDoctrineListener(event: Events::preUpdate, priority: 500, connection: 'default')]
#[AsDoctrineListener(event: Events::prePersist, priority: 500, connection: 'default')]
#[AsDoctrineListener(event: Events::preRemove, priority: 500, connection: 'default')]
class CacheSubscriber
{
    private ParameterBagInterface $bag;
    private array $entityToCache = [
        Structure::class, Profile::class, Skill::class, User::class, Area::class, Layer::class, Engine::class,
        MetadataSheet::class, Map::class, Domain::class, Subdomain::class, Rubric::class, UserAreaAccess::class,
        UserAreaEdition::class, UserAreaAlert::class
    ];

    public function __construct(private CacheService $cacheService, ParameterBagInterface $bag)
    {
        $this->bag = $bag;
    }

    public function preUpdate(LifecycleEventArgs $args): void
    {
        $entity = $args->getObject();
        $this->cleanCache($entity);
    }

    public function prePersist(LifecycleEventArgs $args): void
    {
        $entity = $args->getObject();
        $this->cleanCache($entity);
    }

    public function preRemove(LifecycleEventArgs $args): void
    {
        $entity = $args->getObject();
        $this->cleanCache($entity);
    }

    private function cleanCache($entity)
    {
        /*
        foreach ($this->entityToCache as $item) {
            if ($entity instanceof $item) {
                $cache = $this->cacheService->getRightsCache();
                $cache->clear();
                return;
            }
        }
        */
        $memCacheClient = new Memcached();
        $memCacheClient->addServer($this->bag->get("MEMCACHE_URL"), intval($this->bag->get("MEMCACHE_PORT")));
        $memCacheClient->flush();
    }
}
