<?php

declare(strict_types=1);

namespace App\Form\Datahub;

use App\Entity\Administration\Directory\Profile\Profile;
use App\Entity\Configuration\DatahubSetting;
use App\Entity\Configuration\Template;
use App\Service\DatahubService;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DatahubType extends AbstractType
{
    public function __construct(private DatahubService $datahubService, private ManagerRegistry $managerRegistry)
    {
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        foreach ($options['settings'] as $setting) {
            $type = $setting->getLexSettingType()->getName();
            $options = [
                'label' => $setting->getTitle(),
                'mapped' => false,
                'data' => $setting->getValue(),
                'attr' => ['data-id' => $setting->getId()],
            ];

            if ($type == 'CheckboxType') {
                $options['data'] = $setting->getValue() == '1';
            }

            if ($type == 'ChoiceType') {
                $options['choices'] = $this->datahubService->getMapModele();
                $options['choice_label'] = 'name';
                $options['choice_value'] = function ($template) {
                    if(is_string($template)){
                        $template = $this->managerRegistry->getRepository(Template::class)->find((int)$template);
                    }
                    return $template ? $template->getMapfile() : '';
                };
                $options['attr']['class'] = "form-control select2";
                $options['required'] = true;
            }

            $builder->add($setting->getName(), "Symfony\Component\Form\Extension\Core\Type\\" . $type, $options);
        }
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => DatahubSetting::class,
            'settings' => [],
        ]);
    }
}