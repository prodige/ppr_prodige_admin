<?php

declare(strict_types=1);

namespace App\Form\Administration\Directory;

use App\Entity\Administration\Directory\Structure;
use App\Entity\Administration\Directory\User\User;
use App\Entity\Lex\LexAccountStatus;
use ReflectionProperty;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('login', TextType::class )
            ->add('email', EmailType::class, ['attr' => ['data-type' => ""]]);
        $rp = new ReflectionProperty(User::class, 'id');

        if (!$rp->isInitialized($options['data'])) {
            $builder
                ->add('dontSendEmail', CheckboxType::class, [
                    'label' => "Don't send password initialization e-mail",
                    'mapped' => false,
                    'required' => false,
                    'attr' => ['data-trigger-display' => 'data-display-on'],
                ])
                ->add('password', PasswordType::class, [
                    'label' => "Password",
                    'required' => false,
                    'mapped' => true,
                    'attr' => ['data-display-on' => 'useradd_dontSendEmail'],
                ]);
        }
        $builder->add('name')
            ->add('firstName', null, ['label' => 'First Name'])
            ->add('phoneNumber', TelType::class, [
                'required' => false,
                'attr' => ['data-inputmask' => "'mask': '99 99 99 99 99'", 'data-mask' => ''],
            ])
            ->add('phoneNumber2', TelType::class, [
                'required' => false,
                'attr' => ['data-inputmask' => "'mask': '99 99 99 99 99'", 'data-mask' => ''],
            ])
            ->add('accountExpire', DateType::class, [
                'required' => false,
                'widget' => 'single_text',
            ])
            ->add('structures', EntityType::class, [
                'required' => false,
                'class' => Structure::class,
                'choice_label' => 'name',
                'choice_value' =>
                    function (?Structure $entity) {
                        return $entity ? '/api/structures/' . $entity->getId() : '';
                    },
                'attr' => [
                    'class' => 'form-control select2',
                    'data-max-selection' => 1,
                ],
                'multiple' => true,
            ])
            ->add('service')
            ->add('ipv4Address', TextType::class, ['label' => 'Adresse IPv4', 'required' => false])
            ->add('ipv6Address', TextType::class, ['label' => 'Adresse IPv6', 'required' => false])
            ->add('description', TextareaType::class, ['required' => false])
            ->add('lexAccountStatus', EntityType::class, [
                'class' => LexAccountStatus::class,
                'choice_label' => 'name',
                'choice_value' =>
                    function (?LexAccountStatus $entity) {
                        return $entity ? '/api/lex_account_statuses/' . $entity->getId() : '';
                    },
                'attr' => [
                    'class' => 'form-control select2'
                ]
            ]);
    }

    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        // Fix for select 2
        parent::buildView($view, $form, $options);
        if (isset($options['attr']['id'])) {
            $id = $options['attr']['id'];
            $view->vars["id"] .= $id;
        }
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => User::class
        ]);
    }

}
