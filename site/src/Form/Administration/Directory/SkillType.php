<?php

declare(strict_types=1);

namespace App\Form\Administration\Directory;

use App\Entity\Administration\Resource\Layer;
use App\Entity\Administration\Resource\Map;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;

class SkillType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name')
            ->add('layers', EntityType::class, [
                'class' => Layer::class,
                'choice_label' => 'name',
                'choice_value' =>
                    function (?Layer $entity) {
                        return $entity ? '/api/layers/'.$entity->getId() : '';
                    },
                'attr' => ['class' => 'form-control select2', 'data-dropdown-css-class' => 'select2-blue'],
                'multiple' => true,
                'required' => false,
                'query_builder' => function (EntityRepository $entityRepository) {
                    return $entityRepository->createQueryBuilder('layer')
                        ->orderBy('layer.name', 'ASC');
                }
            ])
            ->add('maps', EntityType::class, [
                'class' => Map::class,
                'choice_label' => 'name',
                'choice_value' =>
                    function (?Map $entity) {
                        return $entity ? '/api/maps/'.$entity->getId() : '';
                    },
                'attr' => ['class' => 'form-control select2', 'data-dropdown-css-class' => 'select2-blue'],
                'multiple' => true,
                'required' => false,
                'query_builder' => function (EntityRepository $entityRepository) {
                    return $entityRepository->createQueryBuilder('map')
                        ->orderBy('map.name', 'ASC');
                }
            ]);
    }

    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        // Fix for select 2
        parent::buildView($view, $form, $options);
        if (isset($options['attr']['id'])){
            $id = $options['attr']['id'];
            $view->vars["id"] .= $id;
        }
    }
}