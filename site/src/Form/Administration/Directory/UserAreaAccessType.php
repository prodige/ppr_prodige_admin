<?php

declare(strict_types=1);

namespace App\Form\Administration\Directory;

use App\Entity\Administration\Directory\Area;
use App\Entity\Administration\Directory\User\User;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class UserAreaAccessType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('user', EntityType::class, [
                'placeholder' => 'Choose an user',
                'class' => User::class,
                'choice_label' => 'name',
                'choice_value' =>
                    function (?User $entity) {
                        return $entity ? '/api/users/' . $entity->getId() : '';
                    },
                'attr' => ['class' => 'form-control', 'disabled' => 'disabled'],
            ])
            ->add('area', EntityType::class, [
                'placeholder' => 'Choose a territory',
                'class' => Area::class,
                'choice_label' => function (?Area $entity) {
                    return $entity ? $entity->getNameAndCode() : '';
                },
                'choice_value' =>
                    function (?Area $entity) {
                        return $entity ? '/api/areas/' . $entity->getId() : '';
                    },
                'attr' => ['class' => 'form-control'],
                'query_builder' => function (EntityRepository $entityRepository) {
                    return $entityRepository->createQueryBuilder('area')
                        ->orderBy('area.name', 'ASC');
                }
            ]);
    }
}
