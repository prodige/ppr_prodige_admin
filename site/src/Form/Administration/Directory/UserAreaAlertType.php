<?php

declare(strict_types=1);

namespace App\Form\Administration\Directory;

use App\Entity\Administration\Directory\Area;
use App\Entity\Administration\Directory\User\User;
use App\Entity\Administration\Resource\Layer;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class UserAreaAlertType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('user', EntityType::class, [
                'placeholder' => 'Choose an user',
                'class' => User::class,
                'choice_label' =>
                    function (?User $entity) {
                        return $entity ? $entity->getFirstName() . ' ' . $entity->getName() .  ' (' . $entity->getEmail() .')': '';
                    },
                'choice_value' =>
                    function (?User $entity) {
                        return $entity ? '/api/users/'.$entity->getId() : '';
                    },
                'attr' => ['class' => 'form-control', 'disabled' => 'disabled'],
            ])
            ->add('layer', EntityType::class, [
                'placeholder' => 'Choose a layer',
                'class' => Layer::class,
                'choice_label' => 'name',
                'choice_value' =>
                    function (?Layer $entity) {
                        return $entity ? '/api/layers/'.$entity->getId() : '';
                    },
                'attr' => ['class' => 'form-control'],
                'query_builder' => function (EntityRepository $entityRepository) {
                    return $entityRepository->createQueryBuilder('layer')
                        ->orderBy('layer.name', 'ASC');
                }
            ])
            ->add('area', EntityType::class, [
                'placeholder' => 'Choose an area',
                'class' => Area::class,
                'choice_label' => function (?Area $entity) {
                    return $entity ? $entity->getNameAndCode() : '';
                },
                'choice_value' =>
                    function (?Area $entity) {
                        return $entity ? '/api/areas/'.$entity->getId() : '';
                    },
                'attr' => ['class' => 'form-control'],
                'query_builder' => function (EntityRepository $entityRepository) {
                    return $entityRepository->createQueryBuilder('area')
                        ->orderBy('area.name', 'ASC');
                }
            ]);
    }
}
