<?php

declare(strict_types=1);

namespace App\Form\Administration\Directory;

use App\Entity\Administration\Directory\Profile\Profile;
use App\Entity\Administration\Directory\Profile\ProfileUser;
use App\Entity\Administration\Directory\User\User;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use ReflectionProperty;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;

class ProfileUserType extends AbstractType
{
    public function __construct(private EntityManagerInterface $entityManager)
    {
    }


    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $rp = new ReflectionProperty(ProfileUser::class, 'user');
        if ($rp->isInitialized($options['data'])) {
            $userId = $options['data']->getUser()->getId();
            $user = $this->entityManager->getRepository(User::class)->find($userId);
            $profiles = [];
            foreach ($user->getProfiles() as $profile) {
                $profiles[] = $profile->getProfile()->getId();
            }
            if (count($profiles) > 0) {
                $options['profiles'] = $profiles;
            }
        }


        $builder
            ->add('user', EntityType::class, [
                'placeholder' => 'Choose an user',
                'class' => User::class,
                'choice_label' => 'name',
                'choice_value' =>
                    function (?User $entity) {
                        return $entity ? '/api/users/' . $entity->getId() : '';
                    },
                'attr' => ['class' => 'form-control', 'disabled' => 'disabled'],
            ])
            ->add('profile', EntityType::class, [
                'placeholder' => 'Choose a profile',
                'class' => Profile::class,
                'choice_label' => 'name',
                'choice_value' =>
                    function (?Profile $entity) {
                        return $entity ? '/api/profiles/' . $entity->getId() : '';
                    },
                'choice_attr' =>
                    function ($val, $key, $index) use ($options) {
                        return (array_key_exists('profiles', $options) && in_array(
                                $val->getId(),
                                $options['profiles']
                            )) ? ['disabled' => 'disabled'] : [];
                    },
                'attr' => ['class' => 'form-control'],
                'query_builder' => function (EntityRepository $entityRepository) {
                    return $entityRepository->createQueryBuilder('profiles')
                        ->orderBy('profiles.name', 'ASC');
                }
            ])
            ->add('privilege', ChoiceType::class, [
                'disabled' => true,
                'choices' => [
                    'Select a profile' => null
                ],
                'attr' => ['class' => 'form-control'],
            ]);
    }
}
