<?php

declare(strict_types=1);

namespace App\Form\Administration\Directory;

use App\Entity\Administration\Directory\Profile\Profile;
use App\Entity\Administration\Resource\Layer;
use App\Entity\Lex\LexPrivilege;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;

class ProfilePrivilegeObjectType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('profile', EntityType::class, [
                'class' => Profile::class,
                'choice_label' => 'name',
                'choice_value' =>
                    function (?Profile $entity) {
                        return $entity ? '/api/profiles/' . $entity->getId() : '';
                    },
                'attr' => ['class' => 'form-control'],
                'query_builder' => function (EntityRepository $entityRepository) {
                    return $entityRepository->createQueryBuilder('profile')
                        ->orderBy('profile.name', 'ASC');
                }
            ])
            ->add('metadataSheet', EntityType::class, [
                'label' => 'Data',
                'class' => Layer::class,
                'choice_label' => 'name',
                'choice_value' =>
                    function (?Layer $entity) {
                        return $entity ? '/api/metadata_sheets/' . $entity->getSheetMetadata()->getId() : '';
                    },
                'attr' => ['class' => 'form-control'],
                'query_builder' => function (EntityRepository $entityRepository) {
                    return $entityRepository->createQueryBuilder('layer')
                        ->orderBy('layer.name', 'ASC');
                }
            ])
            ->add('lexPrivilege', EntityType::class, [
                'label' => 'Privilege',
                'class' => LexPrivilege::class,
                'choice_label' => 'name',
                'choice_value' =>
                    function (?LexPrivilege $entity) {
                        return $entity ? '/api/lex_privileges/' . $entity->getId() : '';
                    },
                'attr' => ['class' => 'form-control'],
                'query_builder' => function (EntityRepository $entityRepository) {
                    return $entityRepository->createQueryBuilder('lexPrivilege')
                        ->orderBy('lexPrivilege.name', 'ASC');
                }
            ]);
    }

    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        // Fix for select 2
        parent::buildView($view, $form, $options);
        if (isset($options['attr']['id'])){
            $id = $options['attr']['id'];
            $view->vars["id"] .= $id;
        }
    }
}
