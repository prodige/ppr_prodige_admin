<?php

declare(strict_types=1);

namespace App\Form\Administration\Directory;

use App\Entity\Administration\Directory\Skill;
use App\Entity\Administration\Organisation\Domain;
use App\Entity\Administration\Organisation\Subdomain;
use App\Entity\Lex\LexPrivilege;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;

class ProfileType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name')
            ->add('domains', EntityType::class, [
                'class' => Domain::class,
                'required' => false,
                'choice_label' => 'name',
                'choice_value' =>
                    function (?Domain $entity) {
                        return $entity ? '/api/domains/' . $entity->getId() : '';
                    },
                'attr' => ['class' => 'form-control select2', 'data-dropdown-css-class' => 'select2-blue'],
                'multiple' => true,
            ])
            ->add('subdomains', EntityType::class, [
                'class' => Subdomain::class,
                'required' => false,
                'choice_label' => 'name',
                'choice_value' =>
                    function (?Subdomain $entity) {
                        return $entity ? '/api/subdomains/' . $entity->getId() : '';
                    },
                'choice_attr' =>
                    function (?Subdomain $entity) {
                        return $entity ? ['data-domain' => '/api/domains/' . $entity->getDomain()->getId()] : [];
                    },
                'attr' => ['class' => 'form-control select2', 'data-dropdown-css-class' => 'select2-blue'],
                'multiple' => true,
                'query_builder' => function (EntityRepository $entityRepository) {
                    return $entityRepository->createQueryBuilder('subdomain')
                        ->orderBy('subdomain.name', 'ASC');
                }
            ])
            ->add('privileges', EntityType::class, [
                'class' => LexPrivilege::class,
                'required' => false,
                'choice_label' => 'name',
                'choice_value' =>
                    function (?LexPrivilege $entity) {
                        return $entity ? '/api/lex_privileges/' . $entity->getId() : '';
                    },
                'attr' => ['class' => 'form-control select2', 'data-dropdown-css-class' => 'select2-blue'],
                'multiple' => true,
                'query_builder' => function (EntityRepository $entityRepository) {
                    return $entityRepository->createQueryBuilder('privilege')
                        ->orderBy('privilege.name', 'ASC');
                }
            ])
            ->add('skills', EntityType::class, [
                'class' => Skill::class,
                'required' => false,
                'choice_label' => 'name',
                'choice_value' =>
                    function (?Skill $entity) {
                        return $entity ? '/api/skills/' . $entity->getId() : '';
                    },
                'attr' => ['class' => 'form-control select2', 'data-dropdown-css-class' => 'select2-blue'],
                'multiple' => true,
                'query_builder' => function (EntityRepository $entityRepository) {
                    return $entityRepository->createQueryBuilder('skill')
                        ->orderBy('skill.name', 'ASC');
                }
            ])
            ->add('isSynchronised', CheckboxType::class, [
                'label' => 'Synchronized with Geonetwork',
                'required' => false,
            ]);
    }

    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        // Fix for select 2
        parent::buildView($view, $form, $options);
        if (isset($options['attr']['id'])) {
            $id = $options['attr']['id'];
            $view->vars["id"] .= $id;
        }
    }
}
