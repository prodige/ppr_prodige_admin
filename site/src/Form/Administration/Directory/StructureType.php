<?php

declare(strict_types=1);

namespace App\Form\Administration\Directory;

use App\Entity\Administration\Directory\Structure;
use phpDocumentor\Reflection\Types\Boolean;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

class StructureType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name')
            ->add('sigle')
            ->add('siren', null, [
                'label' => 'SIREN',
                'required' => false,
                'attr' => ['data-inputmask' => "'mask': '9{3} 9{3} 9{3}'", 'data-mask' => ''],
            ])
            ->add('siret', null, [
                'label' => 'SIRET',
                'required' => false,
                'attr' => ['data-inputmask' => "'mask': '9{3} 9{3} 9{3} 9{5}'", 'data-mask' => ''],
            ])
            ->add('phoneNumber', TelType::class, [
                'required' => false,
                'attr' => ['data-inputmask' => "'mask': '99 99 99 99 99'", 'data-mask' => ''],
            ])
            ->add(
                'parent',
                HiddenType::class,
                ['data' => $options['attr']['parentId'] !== null ? 'api/structures/' . $options['attr']['parentId'] : null]
            )
            ->add('email', EmailType::class, ['required' => false])
            ->add('address', TextType::class, ['required' => false])
            ->add('zipCode', TextType::class, ['required' => false])
            ->add('city', TextType::class, ['required' => false])
            ->add('country', TextType::class, ['required' => false])
            ->add('isSynchronised', CheckboxType::class, [
                'label' => 'Synchronized as a contact in geonetwork',
                'required' => false
            ]);

    }
}
