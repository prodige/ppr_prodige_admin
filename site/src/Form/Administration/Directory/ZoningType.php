<?php

declare(strict_types=1);

namespace App\Form\Administration\Directory;

use App\Entity\Administration\Directory\Zoning;
use App\Entity\Administration\Resource\Layer;
use App\Service\CatalogueService;
use Doctrine\ORM\EntityRepository;
use ReflectionProperty;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;

class ZoningType extends AbstractType
{
    public function __construct(private CatalogueService $catalogueService)
    {
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $attr = ['class' => 'form-control'];
        $layer_options = [
            'class' => Layer::class,
            'choice_label' => 'name',
            'choice_value' =>
                function (?Layer $entity) {
                    return $entity ? '/api/layers/'.$entity->getId() : '';
                },
            'attr' => $attr,
            'query_builder' => function (EntityRepository $entityRepository) {
                    return $entityRepository->createQueryBuilder('layer')
                        ->orderBy('layer.name', 'ASC');
            }
        ];
        $rp = new ReflectionProperty(Zoning::class, 'layer');
        if ($rp->isInitialized($options['data'])) {
            $fields = $this->catalogueService->getFields($options['data']->getLayer()->getStoragePath(), true);
            $layer_options['attr'] = array_merge($layer_options['attr'], ['disabled' => 'disabled']);
        } else {
            $fields = [];
            $layer_options['placeholder'] = 'Choose a layer';
            $attr = array_merge($attr, ['disabled' => 'disabled']);
        }

        $builder
            ->add('name')
            ->add('layer', EntityType::class, $layer_options)
            ->add('fieldId', ChoiceType::class, [
                'choices' => $fields,
                'attr' => $attr,
            ])
            ->add('fieldName', ChoiceType::class, [
                'choices' => $fields,
                'attr' => $attr,
            ]);
    }
}
