<?php

declare(strict_types=1);

namespace App\Form\Administration\Resource;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class MapType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', null, ['disabled' => true])
            ->add('description', null, ['disabled' => true])
            ->add('state', null, ['required' => false, 'value' => false])
            ->add('user_email');
    }
}
