<?php

declare(strict_types=1);

namespace App\Form\Administration\Resource;

use App\Entity\Administration\Directory\Zoning;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;

class LayerType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', null, ['disabled' => true])
            ->add('description', null, ['disabled' => true])
            ->add('storage_path', null, ['disabled' => true])
            ->add('restriction', null, [
                'label' => 'Territory management',
                'attr' => ['data-trigger-active' => 'data-active-on']
            ])
            ->add('exclusiveRestriction', null, [
                'label' => 'Exclusive restriction',
                'attr'=> ['data-active-on' => 'layer_restriction']
            ])
            ->add('zoning', EntityType::class, [
                'label' => 'Zoning',
                'class' => Zoning::class,
                'placeholder' => 'Choose a layer',
                'choice_label' => 'name',
                'choice_value' =>
                    function (?Zoning $entity) {
                        return $entity ? '/api/zonings/'.$entity->getId() : '';
                    },
                'attr' => ['class' => 'form-control', 'data-active-on' => 'layer_restriction'],
            ])
            ->add('bufferRestriction', NumberType::class, [
                'html5' => true,
                'label' => 'Buffer restriction',
                'attr'=> ['data-active-on' => 'layer_restriction']
            ]);
    }
}
