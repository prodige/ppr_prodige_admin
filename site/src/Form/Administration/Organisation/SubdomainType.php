<?php

declare(strict_types=1);

namespace App\Form\Administration\Organisation;

use App\Entity\Administration\Directory\Profile\Profile;
use App\Entity\Administration\Organisation\Domain;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class SubdomainType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name')
            ->add('description')
            ->add('domain', EntityType::class, [
                'class' => Domain::class,
                'choice_label' => 'name',
                'choice_value' =>
                    function (?Domain $entity) {
                        return $entity ? '/api/domains/'.$entity->getId() : '';
                    },
                'attr' => ['class' => 'form-control'],
                'query_builder' => function (EntityRepository $entityRepository) {
                    return $entityRepository->createQueryBuilder('domain')
                        ->orderBy('domain.name', 'ASC');
                }
            ])
            ->add('profileAdmin', EntityType::class, [
                'class' => Profile::class,
                'choice_label' => 'name',
                'choice_value' =>
                    function (?Profile $entity) {
                        return $entity ? '/api/profiles/'.$entity->getId() : '';
                    },
                'required' => false,
                'placeholder' => 'None',
                'attr' => ['class' => 'form-control'],
                'query_builder' => function (EntityRepository $entityRepository) {
                    return $entityRepository->createQueryBuilder('profile')
                        ->orderBy('profile.name', 'ASC');
                }
            ])
            ->add('profileEditor', EntityType::class, [
                'class' => Profile::class,
                'choice_label' => 'name',
                'choice_value' =>
                    function (?Profile $entity) {
                        return $entity ? '/api/profiles/'.$entity->getId() : '';
                    },
                'required' => false,
                'placeholder' => 'None',
                'attr' => ['class' => 'form-control'],
                'query_builder' => function (EntityRepository $entityRepository) {
                    return $entityRepository->createQueryBuilder('profile')
                        ->orderBy('profile.name', 'ASC');
                }
            ])
            ->add('wmsService', null, ['required' => false, 'value' => false]);
    }
}
