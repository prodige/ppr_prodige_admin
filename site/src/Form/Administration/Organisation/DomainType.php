<?php

declare(strict_types=1);

namespace App\Form\Administration\Organisation;

use App\Entity\Administration\Organisation\Rubric;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;

class DomainType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name')
            ->add('description', TextareaType::class)
            ->add('rubric', EntityType::class, [
                'class' => Rubric::class,
                'choice_label' => 'name',
                'choice_value' =>
                    function (?Rubric $entity) {
                        return $entity ? '/api/rubrics/'.$entity->getId() : '';
                    },
                'attr' => ['class' => 'form-control'],
            ]);
    }
}