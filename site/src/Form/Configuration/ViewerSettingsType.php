<?php

declare(strict_types=1);

namespace App\Form\Configuration;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ViewerSettingsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        foreach ($options['settings'] as $setting) {
            $options = [
                'label' => $setting->getTitle(),
                'mapped' => false,
                'attr' => ['class' => 'form-control', 'data-id' => $setting->getId()],
                'data' => $setting->getValue(),
            ];

            if ($setting->getLexSettingType()->getName() == 'NumberType') {
                $options['html5'] = true;
                $builder->add($setting->getName(), NumberType::class, $options);
            } else {
                if ($setting->getName() == 'PRO_CATALOGUE_TELECHARGEMENT_LICENCE') {
                    $builder->add($setting->getName(), CheckboxType::class, [
                        'label' => $setting->getTitle(),
                        'mapped' => false,
                        'attr' => ['data-id' => $setting->getId(), 'data-unchecked' => 'off'],
                        'data' => ($setting->getValue() == 'on'),
                        'value' => 'on',
                    ]);
                } else {
                    $builder->add($setting->getName(), null, $options);
                }
            }
        }
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'settings' => [],
        ]);
    }
}
