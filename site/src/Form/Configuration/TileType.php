<?php

declare(strict_types=1);

namespace App\Form\Configuration;

use App\Entity\Administration\Resource\Layer;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ButtonType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TileType extends AbstractType
{
    public function __construct(private EntityManagerInterface $entityManager)
    {
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $layer = null;
        if (is_array($options['data']->getTiles()) && count($options['data']->getTiles()) > 0) {
            $layer = $this->entityManager->getRepository(Layer::class)->findOneBy(
                ['storagePath' => $options['data']->getTiles()['layer']]
            );
        }

        $builder
            ->add('layer', EntityType::class, [
                'mapped' => false,
                'label' => 'Layer',
                'class' => Layer::class,
                'choice_label' =>
                    function (?Layer $entity) {
                        return $entity ? $entity->getName() : '';
                    },
                'choice_value' =>
                    function (?Layer $entity) {
                        return $entity ? $entity->getStoragePath() : '';
                    },
                'data' => $layer,
                'attr' => ['class' => 'form-control'],
            ]);
        $builder->add('format', ChoiceType::class, [
            'mapped' => false,
            'choices' => ['PNG (image/png)' => 'PNG', 'JPEG (image/jpeg)' => 'JPEG'],
            'data' => (isset($tiles['format'])) ? $tiles['format'] : null,
            'attr' => ['class' => 'form-control'],
        ])
            ->add('mode', ChoiceType::class, [
                'mapped' => false,
                'choices' => [
                    'A la volée : Au fur et à mesure de la consultation' => 'direct',
                    'Mise en cache : Génération totale en différé' => 'defer',
                ],
                'data' => (isset($tiles['mode'])) ? $tiles['mode'] : null,
                'attr' => ['class' => 'form-control', 'data-trigger-active' => 'data-active-on'],
            ])
            ->add('time', TimeType::class, [
                'mapped' => false,
                'input' => 'string',
                'input_format' => 'H:i',
                'required' => false,
                'label' => 'Generation time',
                'widget' => 'single_text',
                'data' => (isset($tile['time'])) ? $tile['time'] : null,
                'attr' => ['data-active-on' => 'tile_mode', 'data-trigger-value' => 'defer'],
            ])
            ->add('grid', HiddenType::class, [
                'mapped' => false,
                'data' => $options['grid'],
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'grid' => null,
        ]);
    }

    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        // Fix for select 2
        parent::buildView($view, $form, $options);
        if (isset($options['attr']['id'])) {
            $id = $options['attr']['id'];
            $view->vars["id"] .= $id;
        }
    }
}
