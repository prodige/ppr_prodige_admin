<?php

declare(strict_types=1);

namespace App\Form\Configuration;

use App\Entity\Administration\Directory\Profile\Profile;
use App\Entity\Configuration\Help;
use Doctrine\DBAL\Types\TextType;
use Phalcon\Forms\Element\TextArea;
use ReflectionProperty;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;

class HelpType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('title')
            ->add('description', TextareaType::class, ['label' => 'Description']);

        $rp = new ReflectionProperty(Help::class, 'id');
        if (!$rp->isInitialized($options['data']) || is_null($options['data']->filePath)) {
            $builder->add('file', FileType::class);
        } else {
            $builder->add('file_disabled', null, [
                'mapped' => false,
                'disabled' => true,
                'label' => 'File',
                'data' => $options['data']->filePath,
            ]);
        }

        $builder->add('profiles', EntityType::class, [
            'class' => Profile::class,
            'choice_label' => 'name',
            'choice_value' =>
                function (?Profile $entity) {
                    return $entity ? '/api/profiles/'.$entity->getId() : '';
                },
            'required' => false,
            'placeholder' => 'None',
            'attr' => ['class' => 'form-control select2', 'data-dropdown-css-class' => 'select2-blue'],
            'multiple' => true,
        ]);
    }

    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        // Fix for select 2
        parent::buildView($view, $form, $options);
        if (isset($options['attr']['id'])) {
            $id = $options['attr']['id'];
            $view->vars["id"] .= $id;
        }
    }
}
