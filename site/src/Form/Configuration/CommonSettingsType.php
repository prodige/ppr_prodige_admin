<?php

declare(strict_types=1);

namespace App\Form\Configuration;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CommonSettingsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        foreach ($options['settings'] as $setting) {
            $options = [
                'label' => $setting->getTitle(),
                'mapped' => false,
                'attr' => ['class' => 'form-control', 'data-id' => $setting->getId()],
                'data' => $setting->getValue(),
            ];
            $builder->add($setting->getName(), null, $options);
        }
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'settings' => [],
        ]);
    }
}
