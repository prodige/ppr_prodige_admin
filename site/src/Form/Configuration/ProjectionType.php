<?php

declare(strict_types=1);

namespace App\Form\Configuration;

use App\Entity\Configuration\Projection;
use App\Service\SpatialService;
use ReflectionProperty;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;

class ProjectionType extends AbstractType
{
    public function __construct(private SpatialService $spatialService)
    {
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $rp = new ReflectionProperty(Projection::class, 'id');
        if (!$rp->isInitialized($options['data'])) {
            $entities = $this->spatialService->intersectProjection();
            $choices = [];
            foreach ($entities as $entity) {
                $choices['EPSG:'.$entity->getEpsg().' - '.$entity->getName()] = $entity->getEpsg();
            }
            asort($choices);
            $builder->add(
                'projections',
                ChoiceType::class,
                [
                    'mapped' => false,
                    'label' => 'Available projections',
                    'attr' => ['class' => 'form-control select2'],
                    'choices' => $choices,
                ]
            );
            $epsg_options = ['label' => 'EPSG'];
        } else {
            $epsg_options = ['label' => 'EPSG', 'disabled' => 'disabled'];
        }
        $builder
            ->add('name')
            ->add('epsg', null, $epsg_options)
            ->add('proj4', null, ['required' => true])
            ->add('srid', NumberType::class, ['required' => true, 'html5' => true])
            ->add('authName', null, ['required' => true])
            ->add('authSrid', NumberType::class, ['required' => true, 'html5' => true])
            ->add('srText', TextareaType::class, ['required' => true]);
    }
}
