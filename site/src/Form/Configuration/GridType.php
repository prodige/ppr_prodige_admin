<?php

declare(strict_types=1);

namespace App\Form\Configuration;

use App\Entity\Configuration\Mapcache;
use App\Entity\Configuration\Projection;
use Doctrine\ORM\EntityManagerInterface;
use ReflectionProperty;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;

class GridType extends AbstractType
{
    public function __construct(private EntityManagerInterface $entityManager)
    {
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $grids = $options['data']->getGrids();
        $resolutions = [
            '591658711' => '156543.03395208332',
            '295829355' => '78271.51684375',
            '147914678' => '39135.75855416666',
            '73957339' => '19567.87927708333',
            '36978669' => '9783.939506249999',
            '18489335' => '4891.969885416666',
            '9244667' => '2445.9848104166663',
            '4622334' => '1222.9925374999998',
            '2311167' => '611.4962687499999',
            '1155583' => '305.7480020833333',
            '577792' => '152.8741333333333',
            '288896' => '76.43706666666665',
            '144448' => '38.218533333333326',
            '72224' => '19.109266666666663',
            '36112' => '9.554633333333332',
            '18056' => '4.777316666666666',
            '9028' => '2.388658333333333',
            '4514' => '1.1943291666666664',
            '2257' => '0.5971645833333332',
            '1128' => '0.29845',
            '564' => '0.149225',
            '282' => '0.0746125',
        ];

        $projection = null;
        if (isset($grids['srs'])) {
            $projectionRepo = $this->entityManager->getRepository(Projection::class);
            $projection = $projectionRepo->findOneBy(['epsg' => str_replace('EPSG:', '', $grids['srs'])]);
        }
        if (isset($grids['size'])) {
            $size = explode(' ', $grids['size']);
        }
        if (isset($grids['resolutions'])) {
            foreach($grids['resolutions'] as $resolution) {
                $selected_resolutions[] = array_search($resolution, $resolutions);
            }
        }

        $builder
            ->add('metadata', null, [
                'mapped' => false,
                'label' => 'Grid name',
                'data' => (isset($grids['metadata'])) ? $grids['metadata'] : '',
            ])
            ->add('srs', EntityType::class, [
                'mapped' => false,
                'label' => 'Projection',
                'class' => Projection::class,
                'choice_label' =>
                    function (?Projection $entity) {
                        return $entity ? 'EPSG:'.$entity->getEpsg().' - '.$entity->getName() : '';
                    },
                'choice_value' =>
                    function (?Projection $entity) {
                        return $entity ? 'EPSG:'.$entity->getEpsg() : null;
                    },
                'data' => $projection,
                'attr' => ['class' => 'form-control'],
            ])
            ->add('xmin', NumberType::class, [
                'mapped' => false,
                'label' => 'Extent (xMin)',
                'html5' => true,
                'data' => $grids['extent'][0] ?? null,
            ])
            ->add('ymin', NumberType::class, [
                'mapped' => false,
                'label' => 'Extent (yMin)',
                'html5' => true,
                'data' => $grids['extent'][1] ?? null,
            ])
            ->add('xmax', NumberType::class, [
                'mapped' => false,
                'label' => 'Extent (xMax)',
                'html5' => true,
                'data' => $grids['extent'][2] ?? null,
            ])
            ->add('ymax', NumberType::class, [
                'mapped' => false,
                'label' => 'Extent (yMax)',
                'html5' => true,
                'data' => (isset($grids['extent'][3])) ? $grids['extent'][3] : null,
            ])
            ->add('width', NumberType::class, [
                'mapped' => false,
                'label' => 'Tiles dimension (width)',
                'html5' => true,
                'data' => (isset($size)) ? $size[0] : 256,
            ])
            ->add('height', NumberType::class, [
                'mapped' => false,
                'label' => 'Tiles dimension (height)',
                'html5' => true,
                'data' => (isset($size)) ? $size[1] : 256,
            ])
            ->add('resolutions', ChoiceType::class, [
                'mapped' => false,
                'choices' => $resolutions,
                'attr' => ['class' => 'form-control duallistbox'],
                'multiple' => true,
                'data' => $grids['resolutions'] ?? null,
            ])
            ->add('units', HiddenType::class, ['mapped' => false, 'data' => 'm']);
    }
}
