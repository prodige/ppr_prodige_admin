<?php

declare(strict_types=1);

namespace App\Form\Configuration;

use App\Entity\Administration\Resource\Layer;
use App\Entity\Configuration\Engine;
use App\Service\CatalogueService;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use ReflectionProperty;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EngineType extends AbstractType
{
    public function __construct(
        private CatalogueService $catalogueService,
        private EntityManagerInterface $entityManager
    ) {
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $attr = $joinFieldAttr = ['class' => 'form-control'];
        $layer_options = [
            'class' => Layer::class,
            'choice_label' => 'name',
            'choice_value' =>
                function (?Layer $entity) {
                    return $entity ? '/api/layers/'.$entity->getId() : '';
                },
            'attr' => $attr,
            'query_builder' => function (EntityRepository $er): QueryBuilder {
                return $er->createQueryBuilder('l')
                    ->orderBy('l.name', 'ASC');
            },
        ];

        $rp = new ReflectionProperty(Engine::class, 'id');
        if ($rp->isInitialized($options['data'])) {
            $fields = $this->catalogueService->getFields($options['data']->getLayer()->getStoragePath(), true);
            if (is_null($options['data']->getJoinId())) {
                $joinFieldAttr = array_merge($attr, ['disabled' => 'disabled']);
            }
            $options['engineId'] = $options['data']->getId();
        } else {
            $fields = [];
            $layer_options['placeholder'] = 'Choose a layer';
            $attr = $joinFieldAttr = array_merge($attr, ['disabled' => 'disabled']);

            // Add rowOrder value only on new Engine
            $conn = $this->entityManager->getConnection();
            $sql = "SELECT COALESCE(MAX(row_order), 0) as max FROM admin.engine WHERE lex_engine_type_id = '".$options['lexEngineType']."'";
            $maxOrder = $conn->prepare($sql)->executeQuery()->fetchAssociative()['max'] + 1;
            $builder->add('rowOrder', HiddenType::class, [
                'data' => $maxOrder,
                'attr' => ['class' => 'form-control', 'data-type' => 'number'],
            ]);
        }
        $builder
            ->add('name')
            ->add('layer', EntityType::class, $layer_options)
            ->add('fieldId', ChoiceType::class, [
                'choices' => $fields,
                'attr' => $attr,
            ])
            ->add('fieldName', ChoiceType::class, [
                'choices' => $fields,
                'attr' => $attr,
            ])
            ->add('joinId', EntityType::class, [
                'class' => Engine::class,
                'query_builder' => function (EntityRepository $er) use ($options) {
                    $qb = $er->createQueryBuilder('e')
                        ->where('e.lexEngineType = '.$options['lexEngineType']);
                    if (array_key_exists('engineId', $options)) {
                        $qb->andWhere('e.id != '.$options['engineId']);
                    }

                    return $qb;
                },
                'choice_label' => 'name',
                'choice_value' =>
                    function (?Engine $entity) {
                        return $entity ? '/api/engines/'.$entity->getId() : '';
                    },
                'required' => false,
                'placeholder' => 'None',
                'attr' => ['class' => 'form-control'],
            ])
            ->add('joinField', ChoiceType::class, [
                'choices' => $fields,
                'placeholder' => 'Choose a field',
                'required' => false,
                'attr' => $joinFieldAttr,
            ]);

        if ($options['lexEngineType']) {
            $builder->add('lexEngineType', HiddenType::class, [
                'data' => '/api/lex_engine_types/'.$options['lexEngineType'],
            ]);
        }
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'lexEngineType' => null,
        ]);
    }

    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        // Fix for select 2
        parent::buildView($view, $form, $options);
        if (isset($options['attr']['id'])) {
            $id = $options['attr']['id'];
            $view->vars["id"] .= $id;
        }
    }
}
