<?php

declare(strict_types=1);

namespace App\Form\Configuration;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CatalogueType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        foreach ($options['settings'] as $setting) {
            $type = $setting->getLexSettingType()->getName();
            $options = [
                'label' => $setting->getTitle(),
                'mapped' => false,
                'data' => $setting->getValue(),
                'attr' => ['data-id' => $setting->getId()],
            ];

            if ($type == 'CheckboxType') {
                $options['data'] = $setting->getValue() == '1';
            }

            $builder->add($setting->getName(), "Symfony\Component\Form\Extension\Core\Type\\".$type, $options);
        }
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'settings' => [],
        ]);
    }
}
