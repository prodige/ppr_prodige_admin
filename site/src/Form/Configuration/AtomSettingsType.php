<?php

declare(strict_types=1);

namespace App\Form\Configuration;

use App\Entity\Configuration\Format;
use App\Entity\Configuration\Projection;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AtomSettingsType extends AbstractType
{
    public function __construct(private EntityManagerInterface $entityManager)
    {
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        foreach ($options['settings'] as $setting) {
            $values = $choices = $entities = [];
            if ($setting->getName() == 'PRO_FLUXATOM_FORMAT') {
                $entities = $this->entityManager->getRepository(Format::class)->findAll();
                foreach ($setting->getFormats() as $value) {
                    $values[] = '/api/formats/'.$value->getId();
                }
                foreach ($entities as $entity) {
                    $choices[$entity->getName()] = '/api/formats/'.$entity->getId();
                }
            } elseif ($setting->getName() == 'PRO_FLUXATOM_PROJECTION') {
                $entities = $this->entityManager->getRepository(Projection::class)->findAll();
                foreach ($setting->getProjections() as $value) {
                    $values[] = '/api/projections/'.$value->getEpsg();
                }
                foreach ($entities as $entity) {
                    $choices['EPSG:'.$entity->getEpsg().' '.$entity->getName()] = '/api/projections/'.$entity->getEpsg();
                }
            }

            $options = [
                'label' => $setting->getTitle(),
                'mapped' => false,
                'choices' => $choices,
                'multiple' => true,
                'attr' => [
                    'class' => 'form-control select2',
                    'data-id' => $setting->getId(),
                    'data-dropdown-css-class' => 'select2-blue',
                ],
                'data' => $values,
            ];
            $builder->add($setting->getName(), ChoiceType::class, $options);
        }
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'settings' => [],
        ]);
    }
}
