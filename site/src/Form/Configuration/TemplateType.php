<?php

declare(strict_types=1);

namespace App\Form\Configuration;

use App\Entity\Lex\LexTemplateType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class TemplateType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name')
            ->add('lexTemplateType', EntityType::class,[
                'label' => 'Type',
                'class' => LexTemplateType::class,
                'choice_label' => 'name',
                'choice_value' =>
                    function (?LexTemplateType $entity) {
                        return $entity ? '/api/lex_template_types/' . $entity->getId() : '';
                    },
                'attr' => ['class' => 'form-control']
            ]);
    }
}
