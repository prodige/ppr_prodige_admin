<?php

declare(strict_types=1);

namespace App\Form\Configuration;

use App\Entity\Configuration\Projection;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProjectionSettingsType extends AbstractType
{
    public function __construct(private EntityManagerInterface $entityManager)
    {
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        // Get projection list
        $projectionsEntities = $this->entityManager->getRepository(Projection::class)->findAll();
        $projections = [];
        foreach($projectionsEntities as $projectionsEntity) {
            $projections['EPSG:'.$projectionsEntity->getEpsg().' '.$projectionsEntity->getName()] = '/api/projections/'.$projectionsEntity->getEpsg();
        }

        foreach ($options['settings'] as $setting) {
            $values = [];

            foreach($setting->getProjections() as $value) {
                $values[] = '/api/projections/'.$value->getEpsg();
            }
            $options = [
                'label' => $setting->getTitle(),
                'mapped' => false,
                'choices' => $projections,
                'attr' => ['class' => 'form-control select2', 'data-id' => $setting->getId()],
                'data' => implode(',', $values),
            ];

            if (in_array($setting->getName(), ['PRO_PROJ_WMS', 'PRO_PROJ_WFS','PRO_PROJ_OGCAPI'])) {
                $options['multiple'] = true;
                $options['attr'] = [
                    'class' => 'form-control select2',
                    'data-id' => $setting->getId(),
                    'data-dropdown-css-class' => 'select2-blue',
                ];
                $options['data'] = $values;
            }

            $builder->add($setting->getName(), ChoiceType::class, $options);
        }
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'settings' => [],
        ]);
    }
}
