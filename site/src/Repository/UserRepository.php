<?php

namespace App\Repository;


use App\Entity\Administration\Directory\User\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class UserRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    public function findByPasswordExpired(int $limit)
    {
        $date = new \DateTime();
        $date->modify('+' . $limit . ' month');
        return $this->createQueryBuilder('u')
            ->andWhere('u.passwordExpire < :date')
            ->andWhere('u.isExpired = false')
            ->setParameter('date', $date->format('Y-m-d H:i:s'))
            ->getQuery()
            ->getResult();
    }

    public function findByAccountWillExpired(int $limit)
    {
        $date = new \DateTime();
        $date->modify('+' . $limit . ' month');
        $query = $this->createQueryBuilder('u')
            ->andWhere('DATE(u.accountExpire) = :date')
            ->setParameter('date', $date->format('Y-m-d'))
            ->getQuery();
        return $query->getResult();
    }

}
