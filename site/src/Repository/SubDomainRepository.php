<?php

namespace App\Repository;


use App\Entity\Administration\Directory\User\User;
use App\Entity\Administration\Organisation\Subdomain;
use App\Entity\Administration\Resource\MetadataSheet;
use App\Entity\Lex\LexPrivilege;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Exception;
use Doctrine\Persistence\ManagerRegistry;

class SubDomainRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Subdomain::class);
    }

    /**
     * @param int $id Subdomain id
     * @param User $user User
     * @return bool
     * @throws Exception
     */
    public function findByIdWhenAccess(int $id, User $user): bool
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = 'select s.id 
                from admin.subdomain s
                inner join admin.profile_subdomain_access psa on psa.subdomain_id = s.id
                inner join admin.profile p on p.id = psa.profile_id 
                inner join admin.profile_user pu on pu.profile_id = p.id
                where pu.user_id = :user_id and s.id = :subdomain_id;
            ';
        $stmt = $conn->prepare($sql);
        $stmt->bindValue(':subdomain_id', $id);
        $stmt->bindValue(':user_id', $user->getId());
        $resultSet = $stmt->executeQuery();

        return !empty($resultSet->fetchAllAssociative());
    }

    /**
     * @param int $id Subdomain id
     * @param User $user User
     * @return bool
     * @throws Exception
     */
    public function findByIdWhenEditor(int $id, User $user): bool
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = 'select s.id
                from admin.subdomain s 
                inner join admin.profile p on p.id = s.profile_editor_id
                inner join admin.profile_user pu on pu.profile_id = p.id
                where pu.user_id = :user_id and s.id = :subdomain_id;
            ';
        $stmt = $conn->prepare($sql);
        $stmt->bindValue(':subdomain_id', $id);
        $stmt->bindValue(':user_id', $user->getId());
        $resultSet = $stmt->executeQuery();

        return !empty($resultSet->fetchAllAssociative());
    }

    /**
     * @param int $id Subdomain id
     * @param User $user User
     * @return bool
     * @throws Exception
     */
    public function findByIdWhenManage(int $id, User $user): bool
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = 'select s.id
                from admin.subdomain s 
                inner join admin.profile p on p.id = s.profile_admin_id
                inner join admin.profile_user pu on pu.profile_id = p.id
                where pu.user_id = :user_id and s.id = :subdomain_id;
            ';
        $stmt = $conn->prepare($sql);
        $stmt->bindValue(':subdomain_id', $id);
        $stmt->bindValue(':user_id', $user->getId());
        $resultSet = $stmt->executeQuery();

        return !empty($resultSet->fetchAllAssociative());
    }

    /**
     * @param User $user User
     * @return array
     * @throws Exception
     */
    public function findWhenManage(User $user): array
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = 'select s.id, s.name
                from admin.subdomain s 
                inner join admin.profile p on p.id = s.profile_admin_id
                inner join admin.profile_user pu on pu.profile_id = p.id
                where pu.user_id = :user_id;
            ';
        $stmt = $conn->prepare($sql);
        $stmt->bindValue(':user_id', $user->getId());
        $resultSet = $stmt->executeQuery();

        return $resultSet->fetchAllAssociative();
    }

}
