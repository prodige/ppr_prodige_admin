<?php

namespace App\Repository;


use App\Entity\Administration\Directory\User\User;
use App\Entity\OGCFeatures\OGCItem;
use App\Entity\Lex\LexPrivilege;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Exception;
use Doctrine\Persistence\ManagerRegistry;

class OGCItemRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, OGCItem::class);
    }

    /**
     * @param int $id metadata_id
     * @return array
     * @throws Exception
     */
    public function findByRightsAndId(int $id): array
    {//TODO ADAPT
        $conn = $this->getEntityManager()->getConnection();

        $sql = 'select d."name" as DOM_NOM, s."name" as SSDOM_NOM, m.id as PK_CARTE_PROJET, ms.public_metadata_id as FMETA_ID,
            CASE
                WHEN o.operationid = 0 THEN 4
                ELSE 1
            END AS statut,
            m."name" as CARTP_NOM, 
             CASE
                WHEN m.lex_map_format_id = 1 THEN 0
                WHEN m.lex_map_format_id = 2 THEN 1
                WHEN m.lex_map_format_id = 3 THEN 2
                ELSE NULL::integer
            END AS CARTP_FORMAT, m."path"  as STKCARD_PATH, 
            ms.id as PK_FICHE_METADONNEES, m.public_metadata_wms as CARTP_WMS, d.id as domain_id, s.id as subdomain_id
            from admin.metadata ms 
            left join public.operationallowed o  ON ms.public_metadata_id = o.metadataid AND o.operationid = 0 AND o.groupid = 1
            left join admin."map" m on m.metadata_sheet_id = ms.id 
            left join admin.subdomain_metadata_sheet sms on sms.metadata_sheet_id = ms.id
            left join admin.subdomain s on s.id = sms.subdomain_id 
            left join admin."domain" d on d.id = s.domain_id 
            WHERE ms.public_metadata_id = :metadata_id;
            ';
        $stmt = $conn->prepare($sql);
        $stmt->bindValue(':metadata_id', $id);
        $resultSet = $stmt->executeQuery();

        return $resultSet->fetchAllAssociative();
    }

    /**
     * @param int $id metadata_id
     * @return array
     * @throws Exception
     */
    public function findOperationStatusBId(int $id): array
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = 'select ms.id, d."name" as DOM_NOM, s."name" as SSDOM_NOM, 
            CASE WHEN o.operationid = 0 THEN 4 ELSE 1 END AS statut
            from admin.metadata_sheet ms 
            inner join public.operationallowed o  ON ms.public_metadata_id = o.metadataid AND o.operationid = 0 AND o.groupid = 1
            inner join admin.subdomain_metadata_sheet sms on sms.metadata_sheet_id = ms.id
            inner join admin.subdomain s on s.id = sms.subdomain_id 
            inner join admin."domain" d on d.id = s.domain_id 
            WHERE ms.public_metadata_id = :metadata_id;
            ';
        $stmt = $conn->prepare($sql);
        $stmt->bindValue(':metadata_id', $id);
        $resultSet = $stmt->executeQuery();

        return $resultSet->fetchAllAssociative();
    }

    /**
     * @param int $id publicMetadataId
     * @return array
     * @throws Exception
     */
    public function findLayerInfo(int $publicMetadataId): array
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = 'select d."name" as DOM_NOM, s."name" as SSDOM_NOM, l.id as  PK_COUCHE_DONNEES, ms.public_metadata_id as FMETA_ID, 
                CASE WHEN o.operationid = 0 THEN 4 ELSE 1 END AS statut, l."name" as COUCHE_NOM, l.storage_path as COUCHE_TABLE,
                l.lex_layer_type_id as COUCHE_TYPE, l.download as COUCHD_DOWNLOAD, l.wms as COUCHD_WMS, l.wfs as COUCHD_WFS,
                l.viewable as COUCHD_VISUALISABLE, ms.id as PK_FICHE_METADONNEES, d.id as domain_id, s.id as subdomain_id,
                wms_secure, wfs_secure
                from admin.metadata_sheet ms 
                left join public.operationallowed o  ON ms.public_metadata_id = o.metadataid AND o.operationid = 0 AND o.groupid = 1
                left join admin.layer l on l.metadata_sheet_id = ms.id 
                left join admin.subdomain_metadata_sheet sms on sms.metadata_sheet_id = ms.id
                left join admin.subdomain s on s.id = sms.subdomain_id 
                left join admin."domain" d on d.id = s.domain_id
                WHERE ms.public_metadata_id = :metadata_id;
            ';
        $stmt = $conn->prepare($sql);
        $stmt->bindValue(':metadata_id', $publicMetadataId);
        return $stmt->executeQuery()->fetchAllAssociative();
    }

    /**
     * @param int $public_metadata_id
     * @return array
     */
    public function findAllDomainAndSubDomainById(int $public_metadata_id): array
    {
        $qb = $this->createQueryBuilder('ms')
            ->select('d.name as DOM_NOM, sd.name as SSDOM_NOM, d.id as domain_id, sd.id as subdomain_id')
            ->join('ms.subdomains', 'sd')
            ->join('sd.domain', 'd')
            ->where('ms.publicMetadataId = :public_metadata_id')
            ->setParameter('public_metadata_id', $public_metadata_id);

        return $qb->getQuery()->execute();
    }

    /**
     * @throws Exception
     */
    public function findImportedTaskById(int $id): array
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = 'SELECT pk_tache_import_donnees	
                FROM catalogue.tache_import_donnees
                WHERE uuid = (select uuid from public.metadata where id= :metadata_id)
            ';
        $stmt = $conn->prepare($sql);
        $stmt->bindValue(':metadata_id', $id);
        return $stmt->executeQuery()->fetchAllAssociative();
    }

    /**
     * Find template iso19110
     *
     * @return int
     * @throws Exception
     */
    public function findTemplateIso(): int
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = 'SELECT id FROM public.metadata WHERE istemplate=\'y\' AND schemaid=\'iso19110\'';
        $stmt = $conn->prepare($sql);
        return $stmt->executeQuery()->fetchOne();
    }

    /**
     * Find By uuid
     *
     * @param string $uuid
     * @return int
     * @throws Exception
     */
    public function findByUuid(string $uuid): int
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = 'select id from public.metadata where uuid=:uuid';
        $stmt = $conn->prepare($sql);
        $stmt->bindValue(':uuid', $uuid);
        return $stmt->executeQuery()->fetchOne();
    }

    /**
     * Find By datasetname
     *
     * @param string $datasetname
     * @return int
     * @throws Exception
     */
    public function findByDatasetname(string $datasetname): int
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = 'select public_metadata_id from admin.metadata_sheet '.
               ' inner join admin.layer on metadata_sheet.id = layer.metadata_sheet_id'.
               ' where storage_path=:datasetname';
        $stmt = $conn->prepare($sql);
        $stmt->bindValue(':datasetname', $datasetname);
        return $stmt->executeQuery()->fetchOne();
    }

}
