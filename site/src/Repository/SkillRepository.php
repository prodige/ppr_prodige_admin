<?php

namespace App\Repository;


use App\Entity\Administration\Directory\Profile\Profile;
use App\Entity\Administration\Directory\Skill;
use App\Entity\Administration\Directory\User\User;
use App\Entity\Administration\Resource\Layer;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\DBAL\Exception;
use Doctrine\Persistence\ManagerRegistry;

class SkillRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Skill::class);
    }

    /**
     * @param int $layer_id
     * @return array
     */
    public function findByLayer(int $layer_id): array
    {
        $qb = $this->createQueryBuilder('s')
            ->join('s.layers', 'l')
            ->Where('l.id = :layer_id')
            ->setParameter('layer_id', $layer_id);

        return $qb->getQuery()->execute();
    }

    /**
     * @param int $map_id
     * @return array
     */
    public function findByMap(int $map_id): array
    {
        $qb = $this->createQueryBuilder('s')
            ->join('s.maps', 'm')
            ->andWhere('m.id = :map_id')
            ->setParameter('map_id', $map_id);

        return $qb->getQuery()->execute();
    }

    /**
     * @param int $layer_id
     * @param User $user User
     * @return array
     */
    public function findByLayerAndUser(int $layer_id, User $user): array
    {
        $qb = $this->createQueryBuilder('s')
            ->join('s.profiles', 'p')
            ->join('p.users', 'pu')
            ->join('pu.user', 'u')
            ->join('s.layers', 'l')
            ->where('u.id = :user_id')
            ->andWhere('l.id = :layer_id')
            ->setParameter('layer_id', $layer_id)
            ->setParameter('user_id', $user->getId());

        return $qb->getQuery()->execute();
    }

    /**
     * @param int $map_id
     * @param User $user User
     * @return array
     */
    public function findByMapAndUser(int $map_id, User $user): array
    {
        $qb = $this->createQueryBuilder('s')
            ->join('s.profiles', 'p')
            ->join('p.users', 'pu')
            ->join('pu.user', 'u')
            ->join('s.maps', 'm')
            ->where('u.id = :user_id')
            ->andWhere('m.id = :map_id')
            ->setParameter('map_id', $map_id)
            ->setParameter('user_id', $user->getId());

        return $qb->getQuery()->execute();
    }

}
