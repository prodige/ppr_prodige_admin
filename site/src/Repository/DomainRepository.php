<?php

namespace App\Repository;


use App\Entity\Administration\Directory\User\User;
use App\Entity\Administration\Organisation\Domain;
use App\Entity\Administration\Resource\MetadataSheet;
use App\Entity\Lex\LexPrivilege;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Exception;
use Doctrine\Persistence\ManagerRegistry;

class DomainRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Domain::class);
    }

    /**
     * @param int $id Subdomain id
     * @param User $user User
     * @return bool
     * @throws Exception
     */
    public function findByIdWhenAccess(int $id, User $user): bool
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = 'select * 
                from admin.domain d
                inner join admin.profile_domain_access pda on pda.domain_id = d.id
                inner join admin.profile p on p.id = pda.profile_id
                inner join admin.profile_user pu on pu.profile_id = p.id
                where pu.user_id = :user_id and d.id = :domain_id;
            ';
        $stmt = $conn->prepare($sql);
        $stmt->bindValue(':domain_id', $id);
        $stmt->bindValue(':user_id', $user->getId());
        $resultSet = $stmt->executeQuery();

        return !empty($resultSet->fetchAllAssociative());
    }

    /**
     * @param int $id Subdomain id
     * @param User $user User
     * @return bool
     * @throws Exception
     */
    public function findByIdAndUserProfiles(int $id, User $user): bool
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = 'select * 
                from admin.domain d 
                inner join admin.profile p on p.id = d.profile_editor_id
                inner join admin.profile_user pu on pu.profile_id = p.id
                where pu.user_id = :user_id and s.id = :subdomain_id;
            ';
        $stmt = $conn->prepare($sql);
        $stmt->bindValue(':subdomain_id', $id);
        $stmt->bindValue(':user_id', $user->getId());
        $resultSet = $stmt->executeQuery();

        return !empty($resultSet->fetchAllAssociative());
    }


}
