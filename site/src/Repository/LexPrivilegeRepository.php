<?php

namespace App\Repository;


use App\Entity\Administration\Directory\User\User;
use App\Entity\Administration\Resource\MetadataSheet;
use App\Entity\Lex\LexPrivilege;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\DBAL\Connection;
use Doctrine\Persistence\ManagerRegistry;

class LexPrivilegeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, LexPrivilege::class);
    }

    public function findByUser(User $user)
    {
        return $this->createQueryBuilder('lp')
            ->innerJoin('lp.profiles', 'p')
            ->innerJoin('p.users', 'pp')
            ->innerJoin('pp.user', 'u')
            ->Where('u.id = :id')
            ->groupBy('lp.id')
            ->setParameter('id', $user->getId())
            ->getQuery()
            ->getArrayResult();
    }

    /**
     * @throws \Doctrine\DBAL\Exception
     */
    public function findByUserAndMetadataSheet(User $user, MetadataSheet $metadataSheet, LexPrivilege $traitement): array
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = 'select status
                from admin.profile_privilege_object ppo
                inner join admin.profile p on p.id = ppo.profile_id
                inner join admin.profile_user pu on pu.profile_id = p.id
                inner join admin.lex_privilege lp on lp.id = ppo.lex_privilege_id
                where ppo.metadata_sheet_id = :metadata_id and pu.user_id = :user_id 
                  and lp."name" = :traitement;
            ';
        $stmt = $conn->prepare($sql);
        $stmt->bindValue(':metadata_id', $metadataSheet->getId());
        $stmt->bindValue(':user_id', $user->getId());
        $stmt->bindValue(':traitement', $traitement->getName());
        $resultSet = $stmt->executeQuery();

        return $resultSet->fetchFirstColumn();
    }

}
