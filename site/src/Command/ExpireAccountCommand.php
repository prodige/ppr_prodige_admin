<?php

namespace App\Command;

use App\Entity\Administration\Directory\User\User;
use App\Service\AuthenticationService;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(
    name: 'admin:expire-account',
    description: 'Check & modify expired account',
    hidden: false
)]
class ExpireAccountCommand extends Command
{
    public function __construct(
        private ManagerRegistry $registry,
        private AuthenticationService $authService
    ) {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->writeln([
            'Checking expired account',
            '-----------------------------------------',
        ]);

        try {
            $users = $this->registry->getRepository(User::class)->findByAccountWillExpired(1);

            foreach ($users as $user) {
                $this->authService->sendMailForAccount($user);
                $output->writeln('Mail send to ' . $user->getEmail());
            }
            $output->writeln(count($users) . ' account(s) with account will expire in 1 month');

            return Command::SUCCESS;
        } catch (\Exception $e) {
            $output->writeln($e->getMessage());

            return Command::FAILURE;
        }
    }
}
