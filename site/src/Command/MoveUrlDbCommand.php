<?php

namespace App\Command;

use Doctrine\DBAL\Result;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;

#[AsCommand(
    name: 'admin:move_url_db',
    description: 'Adapte les URL en DB en fonction du domaine',
)]
class MoveUrlDbCommand extends Command
{
    public function __construct(private KernelInterface       $kernel,
                                private ManagerRegistry       $managerRegistry,
                                private ParameterBagInterface $bag)
    {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this->addOption('domain', '-d', InputOption::VALUE_OPTIONAL, 'Domain of prodige. (ex: .alkante.com or -test.alkante.com)', '.prodige.internal');
        $this->addOption('admin', null, InputOption::VALUE_OPTIONAL, 'Subdomain of admin', 'admin');
        $this->addOption('admincarto', null, InputOption::VALUE_OPTIONAL, 'Subdomain of admincarto', 'admincarto');
        $this->addOption('carto', null, InputOption::VALUE_OPTIONAL, 'Subdomain of carto', 'carto');
        $this->addOption('cas', null, InputOption::VALUE_OPTIONAL, 'Subdomain of cas', 'cas');
        $this->addOption('catalogue', null, InputOption::VALUE_OPTIONAL, 'Subdomain of catalogue', 'catalogue');
        $this->addOption('contribution', null, InputOption::VALUE_OPTIONAL, 'Subdomain of contribution', 'contribution');
        $this->addOption('datacarto', null, InputOption::VALUE_OPTIONAL, 'Subdomain of datacarto', 'datacarto');
        $this->addOption('dataviz', null, InputOption::VALUE_OPTIONAL, 'Subdomain of dataviz', 'dataviz');
        $this->addOption('mapserv', null, InputOption::VALUE_OPTIONAL, 'Subdomain of mapserv', 'mapserv');
        $this->addOption('print', null, InputOption::VALUE_OPTIONAL, 'Subdomain of print', 'print');
        $this->addOption('table', null, InputOption::VALUE_OPTIONAL, 'Subdomain of table', 'table');
        $this->addOption('traitement', null, InputOption::VALUE_OPTIONAL, 'Subdomain of traitement', 'traitement');
        $this->addOption('telechargement', null, InputOption::VALUE_OPTIONAL, 'Subdomain of telechargement', 'telechargement');
        $this->addOption('vues', null, InputOption::VALUE_OPTIONAL, 'Subdomain of vues', 'vues');

        // Old variable
        $this->addOption('olddomain', '-D', InputOption::VALUE_OPTIONAL, 'Old Domain of prodige. (ex: .alkante.com or -test.alkante.com)', '.prodige.internal');
        $this->addOption('oldadmin', null, InputOption::VALUE_OPTIONAL, 'Old Subdomain of admin', 'admin');
        $this->addOption('oldadmincarto', null, InputOption::VALUE_OPTIONAL, 'Old Subdomain of admincarto', 'admincarto');
        $this->addOption('oldcarto', null, InputOption::VALUE_OPTIONAL, 'Old Subdomain of carto', 'carto');
        $this->addOption('oldcas', null, InputOption::VALUE_OPTIONAL, 'Old Subdomain of cas', 'cas');
        $this->addOption('oldcatalogue', null, InputOption::VALUE_OPTIONAL, 'Old Subdomain of catalogue', 'catalogue');
        $this->addOption('oldcontribution', null, InputOption::VALUE_OPTIONAL, 'Old Subdomain of contribution', 'contribution');
        $this->addOption('olddatacarto', null, InputOption::VALUE_OPTIONAL, 'Old Subdomain of datacarto', 'datacarto');
        $this->addOption('olddataviz', null, InputOption::VALUE_OPTIONAL, 'Old Subdomain of dataviz', 'dataviz');
        $this->addOption('oldmapserv', null, InputOption::VALUE_OPTIONAL, 'Old Subdomain of mapserv', 'mapserv');
        $this->addOption('oldprint', null, InputOption::VALUE_OPTIONAL, 'Old Subdomain of print', 'print');
        $this->addOption('oldtable', null, InputOption::VALUE_OPTIONAL, 'Old Subdomain of table', 'table');
        $this->addOption('oldtraitement', null, InputOption::VALUE_OPTIONAL, 'Old Subdomain of traitement', 'traitement');
        $this->addOption('oldtelechargement', null, InputOption::VALUE_OPTIONAL, 'Old Subdomain of telechargement', 'telechargement');
        $this->addOption('oldvues', null, InputOption::VALUE_OPTIONAL, 'Old Subdomain of vues', 'vues');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $io->title("<info>Lancement des modifications</info>");

        $application = new Application($this->kernel);
        $application->setAutoExit(false);
        $allOptions = $input->getOptions();

        // Check domain valid
        if (!str_starts_with($allOptions['domain'], '.')) {
            $allOptions['domain'] = '.' . $allOptions['domain'];
        }
        if (!str_starts_with($allOptions['olddomain'], '.')) {
            $allOptions['olddomain'] = '.' . $allOptions['olddomain'];
        }

        $oldOptions = [];
        $toDisplay = [];
        $toUnset = ['help', 'quiet', 'verbose', 'ansi', 'no-interaction', 'env', 'no-debug', 'version'];
        foreach ($allOptions as $key => $value) {
            if (in_array($key, $toUnset)) {
                unset($allOptions[$key]);
            } else {
                if (preg_match("/^old.*/", $key) && ($key !== 'olddomain')) {
                    $oldOptions[$key] = ['value' => $value, 'new' => $allOptions[substr($key, 3, strlen($key))]];
                    $toDisplay[] = [$key, $value, $allOptions[substr($key, 3, strlen($key))]];
                }
            }
        }

        $output->writeln('Old Domain : <info>' . $allOptions['olddomain'] . '</info> -> New Domain : <info>' . $allOptions['domain'] . '</info>');
        $io->table(
            ['Key', 'Old', 'New'],
            $toDisplay
        );

        $helper = $this->getHelper('question');
        $question = new ConfirmationQuestion('<question>Are variables valid ? (y/n) </question>', false, '/^(y|yes)/i');
        if (!$helper->ask($input, $output, $question)) {
            $output->writeln('<error>Process stopped. Check your params.</error>');
            return Command::FAILURE;
        }

        try {
            $io->section("Set database values");
            $catalogue = $this->managerRegistry->getConnection('catalogue');
            $output->writeln('<info>(' . $catalogue->getDatabase() . ') Setting system/server/host in public.settings</info>');
            $host = $catalogue->executeQuery("select value from public.settings where name='system/server/host'");
            $catalogue->executeQuery("update public.settings set value='" . $allOptions['catalogue'] . $allOptions['domain'] . "' where name='system/server/host'");
            $host2 = $catalogue->executeQuery("select value from public.settings where name='system/server/host'");
            $output->writeln($host->fetchOne() . ' replaced by ' . $host2->fetchOne());

            $output->writeln("");
            $output->writeln('<info>(' . $catalogue->getDatabase() . ') Setting metadata/resourceIdentifierPrefix in public.settings</info>');
            $resourceIdentifierPrefix = $catalogue->executeQuery("select value from public.settings where name='metadata/resourceIdentifierPrefix'");
            $catalogue->executeQuery("update public.settings set value='https://" . $allOptions['catalogue'] . $allOptions['domain'] . "/geonetwork/srv/' where name='metadata/resourceIdentifierPrefix'");
            $resourceIdentifierPrefix2 = $catalogue->executeQuery("select value from public.settings where name='metadata/resourceIdentifierPrefix'");
            $output->writeln($resourceIdentifierPrefix->fetchOne() . ' replaced by ' . $resourceIdentifierPrefix2->fetchOne());

            $output->writeln("");
            $prodige = $this->managerRegistry->getConnection('prodige');
            $output->writeln('<info>(' . $prodige->getDatabase() . ') Setting account_hostpostgres, account_passwordpostgres, account_userpostgres, account_dbpostgres in carmen.account</info>');
            $account = $prodige->executeQuery("select account_hostpostgres, account_passwordpostgres, account_userpostgres, account_dbpostgres from carmen.account");
            $prodige->executeQuery("update carmen.account set account_hostpostgres = '" . $this->bag->get('prodige_host') . "', account_passwordpostgres = '" . $this->bag->get('prodige_password') . "',  account_userpostgres = '" . $this->bag->get('prodige_user') . "' where account_dbpostgres = '" . $prodige->getDatabase() . "';");
            $account2 = $prodige->executeQuery("select account_hostpostgres, account_passwordpostgres, account_userpostgres, account_dbpostgres from carmen.account");
            $output->writeln($account->fetchOne() . ' replaced by ' . $account2->fetchOne());

            $output->writeln("");
            $output->writeln("");

            $io->section("Replace database values");
            $layerMetadataFile = $prodige->executeQuery("select layer_metadata_file from carmen.layer where layer_metadata_file is not null");
            $wxsUrl = $prodige->executeQuery("select wxs_url from carmen.wxs");

            foreach ($oldOptions as $oldOption) {
                $newValue = $oldOption['new'] . $allOptions['domain'];
                $oldValue = $oldOption['value'] . $allOptions['olddomain'];
                $catalogue->executeQuery("update public.metadata set data = replace(data, '" . $oldValue . "', '" . $newValue . "')");
                $output->writeln("<info>Replacing " . $oldValue . " by " . $newValue . "</info> in public.metadata");

                $prodige->executeQuery("update carmen.layer set layer_metadata_file = replace(layer_metadata_file, '" . $oldValue . "', '" . $newValue . "')");
                $output->writeln("<info>Replacing " . $oldValue . " by " . $newValue . "</info> in layer_metadata_file from carmen.layer");

                $prodige->executeQuery("update carmen.wxs set wxs_url = replace(wxs_url, '" . $oldValue . "', '" . $newValue . "')");
                $output->writeln("<info>Replacing " . $oldValue . " by " . $newValue . "</info> in wxs_url from carmen.wxs");

                $output->writeln("");
            }

            /** @var Result $newWxsUrl */
            $newWxsUrl = $prodige->executeQuery("select wxs_url from carmen.wxs");
            $newWxsUrl = $newWxsUrl->fetchAllAssociative();
            $wxsUrl = $wxsUrl->fetchAllAssociative();
            $toDisplay = [];
            for ($i = 0; $i < count($newWxsUrl); $i++) {
                $toDisplay[] = [$wxsUrl[$i]['wxs_url'], $newWxsUrl[$i]['wxs_url']];
            }

            $output->writeln("Replacing <info>wxs_url from carmen.wxs</info>");
            $io->table(
                ['Old', 'New'],
                $toDisplay
            );

            $output->writeln("");
            $newLayerMetadataFile = $prodige->executeQuery("select layer_metadata_file from carmen.layer where layer_metadata_file is not null");
            $newLayerMetadataFile = $newLayerMetadataFile->fetchAllAssociative();
            $layerMetadataFile = $layerMetadataFile->fetchAllAssociative();
            $toDisplay = [];
            for ($i = 0; $i < count($newLayerMetadataFile); $i++) {
                $toDisplay[] = [$layerMetadataFile[$i]['layer_metadata_file'], $newLayerMetadataFile[$i]['layer_metadata_file']];
            }
            $output->writeln("Replacing <info>layer_metadata_file from carmen.layer</info>");
            $io->table(
                ['Old', 'New'],
                $toDisplay
            );

        } catch (\Exception $e) {
            $output->writeln("-------------------------");
            $output->writeln('<error> ' . $e->getMessage() . '</error> ');
            return Command::FAILURE;
        }

        $output->writeln("<info>Process OK</info>");
        return Command::SUCCESS;
    }
}
