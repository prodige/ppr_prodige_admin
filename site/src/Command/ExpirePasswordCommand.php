<?php

namespace App\Command;

use App\Entity\Administration\Directory\User\User;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(
    name: 'admin:expire-password',
    description: 'Check & modify expired password',
    hidden: false
)]
class ExpirePasswordCommand extends Command
{
    public function __construct(private ManagerRegistry $registry)
    {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->writeln([
            'Checking expired password',
            '-----------------------------------------'
        ]);

        try {
            $users = $this->registry->getRepository(User::class)->findByPasswordExpired(6);

            foreach ($users as $user) {
                $user->setIsExpired(true);
                $this->registry->getManager()->persist($user);
            }
            $this->registry->getManager()->flush();
            $output->writeln(count($users) . ' account(s) with password expired');
            return Command::SUCCESS;
        } catch (\Exception $e) {
            $output->writeln($e->getMessage());
            return Command::FAILURE;
        }
    }
}