<?php

namespace App\Command;

use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\HttpKernel\KernelInterface;


#[AsCommand(
    name: 'admin:migration-from-catalogue-44',
    description: 'Migrate data from Catalogue pour Admin v5.0.X',
)]
class MigrationFromCatalogue44Command extends Command
{

    public function __construct(private KernelInterface $kernel)
    {
        parent::__construct();
    }

    protected function configure(): void
    {
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $io->writeln("Lancement migration des données du catalogue");

        $application = new Application($this->kernel);
        $application->setAutoExit(false);

        $migrate = new ArrayInput([
            'command' => 'doctrine:migrations:migrate',
            '--configuration' => 'migrations/migrate_44/doctrine_migrations.yaml',
            '--no-interaction' => true
        ]);
        return $application->run($migrate, $output);


    }
}
