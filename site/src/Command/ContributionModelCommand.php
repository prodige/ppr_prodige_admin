<?php

namespace App\Command;

use App\DataPersister\TemplatePersister;
use App\Entity\Configuration\Template;
use App\Entity\Lex\LexTemplateType;
use App\Service\AdminCartoService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\HttpKernel\KernelInterface;

#[AsCommand(
    name: 'admin:modele-contribution',
    description: 'Création du modèle par défaut pour la contribution',
)]
class ContributionModelCommand extends Command
{
    public function __construct(private KernelInterface        $kernel,
                                private EntityManagerInterface $entityManager,
                                private AdminCartoService      $adminCartoService)
    {
        parent::__construct();
    }

    protected function configure(): void
    {
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $io->writeln("Création du modèle");

        $application = new Application($this->kernel);
        $application->setAutoExit(false);

        // Check if model already exists as 'Modèle contribution'
        try {
            $template = $this->entityManager->getRepository(Template::class)->findOneBy(['name' => 'Modèle contribution']);
            if (is_null($template)) {
                $lexTemplate = $this->entityManager->getRepository(LexTemplateType::class)->findOneBy(['name' => 'Contribution']);
                $templatePersister = new TemplatePersister($this->entityManager, $this->adminCartoService);
                $template = new Template();
                $template->setName('Modèle contribution');
                $template->setLexTemplateType($lexTemplate);
                $templatePersister->persist($template);
                $output->writeln('Template \'Modèle contribution\' created');
            } else {
                $output->writeln('Template \'Modèle contribution\' already exists');
            }
            return Command::SUCCESS;
        } catch (\Exception $e) {
            $output->writeln($e->getMessage());
            return Command::FAILURE;
        }

    }
}
