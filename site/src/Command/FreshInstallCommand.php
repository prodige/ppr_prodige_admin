<?php

namespace App\Command;

use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\HttpKernel\KernelInterface;

#[AsCommand(
    name: 'admin:fresh-install',
    description: 'Import des données minimal pour Admin v5',
)]
class FreshInstallCommand extends Command
{
    public function __construct(private KernelInterface $kernel)
    {
        parent::__construct();
    }

    protected function configure(): void
    {
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $io->writeln("Lancement import des données minimal");

        $application = new Application($this->kernel);
        $application->setAutoExit(false);

        try {
            $migrate = new ArrayInput([
                'command' => 'doctrine:migrations:migrate',
                '--configuration' => 'migrations/freshInstall/doctrine_migrations.yaml',
                '--no-interaction' => true
            ]);
            $migrate = $application->run($migrate, $output);
            if ($migrate === 0) {
                // Create Modèle Contribution
                $createModel = new ArrayInput([
                    'command' => 'admin:modele-contribution'
                ]);
                return $application->run($createModel, $output);
            }
            return Command::FAILURE;
        } catch (\Exception $e) {
            $output->writeln($e->getMessage());
            return Command::FAILURE;
        }
    }
}
