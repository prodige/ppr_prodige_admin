<?php

namespace App\Command;

use App\Encoder\GeonetworkPasswordEncoder;
use App\Entity\Administration\Directory\User\User;
use App\Service\AuthenticationService;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

#[AsCommand(
    name: 'admin:phpcli_update_password',
    description: 'Update phpcli password',
    hidden: false
)]
class UpdateCliPasswordCommand extends Command
{
    public function __construct(
        private ManagerRegistry             $registry,
        private UserPasswordHasherInterface $passwordHasher,
        private ParameterBagInterface       $parameterBag,
        private GeonetworkPasswordEncoder   $geonetworkEncoder
    )
    {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this->addArgument('phpcli_default_password', InputArgument::REQUIRED, 'new phpcli password');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->writeln([
            'Checking phpcli user',
            '-----------------------------------------',
        ]);

        try {
            $password = $input->getArgument('phpcli_default_password');

            $user = $this->registry->getRepository(User::class)->findOneBy(array("login" => $this->parameterBag->get('phpcli_default_login')));
            if ($user) {
                $hashedPassword = $this->passwordHasher->hashPassword($user, $password);
                $user->setPassword($hashedPassword);

                // Set password in admin DB
                $this->registry->getManager()->persist($user);
                $this->registry->getManager()->flush();
                $output->writeln('phpcli password updated in DB');

                // Set password in Geonetwork DB
                $digestedPassword = $this->geonetworkEncoder->encode($password);
                $this->geonetworkEncoder->persist($digestedPassword);
                $output->writeln('phpcli password updated in GeoNetwork DB');
            } else {
                $output->writeln('phpcli user not found in database');
                return Command::FAILURE;
            }

            return Command::SUCCESS;
        } catch (\Exception $e) {
            $output->writeln($e->getMessage());

            return Command::FAILURE;
        }
    }
}
