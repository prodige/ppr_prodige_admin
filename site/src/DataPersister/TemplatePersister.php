<?php

declare(strict_types=1);

namespace App\DataPersister;

use ApiPlatform\Core\DataPersister\ContextAwareDataPersisterInterface;
use App\Entity\Lex\LexTemplateType;
use App\Exception\BadRequestException;
use App\Exception\CatalogueException;
use App\Service\AdminCartoService;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Configuration\Template;

final class TemplatePersister implements ContextAwareDataPersisterInterface
{
    public function __construct(
        private EntityManagerInterface $entityManager,
        private AdminCartoService      $adminCartoService,
    )
    {
    }

    public function supports($data, array $context = []): bool
    {
        return ($data instanceof Template);
    }

    /**
     * @throws BadRequestException
     */
    public function persist($data, array $context = [])
    {
        $mapInfo = $this->adminCartoService->addMapCarmen("test");
        if (!is_numeric($mapInfo)) {
            $data->setMapfile($mapInfo['map']['mapFile'] . '.map');
            $this->entityManager->persist($data);
            $this->entityManager->flush();
            $this->entityManager->refresh($data); // Initialize Collection values

            return $data;
        }
        throw new BadRequestException();

    }

    /**
     * @throws CatalogueException
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface
     * @throws \Doctrine\DBAL\Exception
     */
    public function remove($data, array $context = [])
    {
        $mapfile = explode('.', $data->getMapfile());
        $query = $this->adminCartoService->removeMapCarmen($mapfile[0]);
        if ($query === true) {
            $this->entityManager->remove($data);
            $this->entityManager->flush();
            return true;
        }
        throw new CatalogueException();
    }
}
