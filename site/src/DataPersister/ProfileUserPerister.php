<?php

declare(strict_types=1);

namespace App\DataPersister;

use ApiPlatform\Core\DataPersister\ContextAwareDataPersisterInterface;
use App\Entity\Administration\Directory\Profile\ProfileUser;
use App\Service\ProfileGeonetworkService;
use App\Service\ProfileUserService;
use Doctrine\ORM\EntityManagerInterface;

class ProfileUserPerister implements ContextAwareDataPersisterInterface
{
    public function __construct(
        private EntityManagerInterface $entityManager,
        private ProfileUserService $profileUserService,
        private ProfileGeonetworkService $profileGeonetworkService
    ) {
    }

    /**
     * @inheritDoc
     */
    public function supports($data, array $context = []): bool
    {
        return $data instanceof ProfileUser;
    }

    /**
     * @param ProfileUser $data
     * @param array $context
     * @return object
     * @throws \JsonException
     */
    public function persist($data, array $context = []): object
    {
        if (in_array($data->getPrivilege(), ["RegisteredUser", "Reviewer"])) {
            $this->profileUserService->postPrivilegeUserInGroupGeonetwork($data);
        } elseif ($data->getPrivilege() === "Administrator") {
            $this->profileGeonetworkService->postProfileAdministrator($data->getUser());
        }
        $data->getUser()->setUpdatedAt(new \Datetime);

        $this->entityManager->persist($data);
        $this->entityManager->flush();
        $this->entityManager->refresh($data);

        return $data;
    }

    /**
     * @inheritDoc
     * @throws \JsonException
     */
    public function remove($data, array $context = [])
    {
        if ($data->getProfile()->isSynchronised()) {
            $this->profileGeonetworkService->userInGeonteworkGroupAction($data,'remove');
        }
        $data->getUser()->setUpdatedAt(new \Datetime);

        $this->entityManager->remove($data);
        $this->entityManager->flush();
    }
}