<?php

namespace App\DataPersister;

use ApiPlatform\Core\DataPersister\DataPersisterInterface;
use App\Entity\Configuration\DatahubSetting;
use App\Service\DatahubService;
use Doctrine\ORM\EntityManagerInterface;

class DatahubSettingPersister implements DataPersisterInterface
{
    public function __construct(
        private EntityManagerInterface $entityManager,
        private DatahubService $datahubService
        ) {
    }

    /**
     * @inheritDoc
     */
    public function supports($data, array $context = []): bool
    {
        return ($data instanceof DatahubSetting);
    }

    /**
     * @inheritDoc
     */
    public function persist($data, array $context = [])
    {
        $this->entityManager->persist($data);
        $this->entityManager->flush();
        $this->entityManager->refresh($data); // Initialize Collection values

        return $data;
    }

    /**
     * @inheritDoc
     */
    public function remove($data, array $context = [])
    {
        $this->entityManager->remove($data);
        $this->entityManager->flush();
    }
}