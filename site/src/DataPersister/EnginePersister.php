<?php

declare(strict_types=1);

namespace App\DataPersister;

use App\Entity\Configuration\Engine;
use ApiPlatform\Core\DataPersister\ContextAwareDataPersisterInterface;
use Doctrine\ORM\EntityManagerInterface;

final class EnginePersister implements ContextAwareDataPersisterInterface
{
    public function __construct(
        private EntityManagerInterface $entityManager
    ) {
    }

    public function supports($data, array $context = []): bool
    {
        return ($data instanceof Engine);
    }

    public function persist($data, array $context = [])
    {
        if (is_null($data->getJoinId())) {
            $data->setJoinField(null);
        }
        $this->entityManager->persist($data);
        $this->entityManager->flush();
        $this->entityManager->refresh($data); // Initialize Collection values

        return $data;
    }

    public function remove($data, array $context = [])
    {
        $this->entityManager->remove($data);
        $this->entityManager->flush();
    }
}
