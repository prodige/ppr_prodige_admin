<?PHP

declare(strict_types=1);

namespace App\DataPersister;

use ApiPlatform\Core\DataPersister\ContextAwareDataPersisterInterface;
use App\Entity\Administration\Directory\User\User;
use App\Service\UserGeonetworkService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;


final class UserPersister implements ContextAwareDataPersisterInterface
{
    public function __construct(
        private EntityManagerInterface $entityManager,
        private UserGeonetworkService $userGeonetworkService,
        private UserPasswordHasherInterface $passwordHasher
    ) {
    }

    public function supports($data, array $context = []): bool
    {
        return ($data instanceof User);
    }

    public function persist($data, array $context = [])
    {
        if (isset($context['collection_operation_name']) && $context['collection_operation_name'] === "post") {
            $this->userGeonetworkService->postUser($data);
            $data->setReferenceGn($this->getUserGnId($data->getLogin()));
            if ($data->getPassword()) {
                $hashPassword = $this->passwordHasher->hashPassword($data, $data->getPassword());
                $data->setPassword($hashPassword);
            }
        } else {
            $this->userGeonetworkService->putUser($data, $data->getReferenceGn());
        }


        $this->entityManager->persist($data);
        $this->entityManager->flush();
        $this->entityManager->refresh($data); // Initialize Collection values

        return $data;
    }

    /**
     * @param string $username
     * @return int
     * @throws \JsonException
     */
    private function getUserGnId(string $username): int
    {
        $users = $this->userGeonetworkService->getUsers();
        $user = array_search($username, array_column($users, 'username'), true);

        return (int)$users[$user]["id"];
    }

    public function remove($data, array $context = []): void
    {
        $this->userGeonetworkService->deleteUser($data->getReferenceGn());
        $this->entityManager->remove($data);
        $this->entityManager->flush();
    }

}
