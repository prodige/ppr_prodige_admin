<?php

namespace App\DataPersister;

use ApiPlatform\Core\DataPersister\ContextAwareDataPersisterInterface;
use App\Entity\Administration\Directory\Structure;
use App\Exception\StructureDeleteException;
use App\Response\JsonLDResponse;
use App\Service\GeonetworkService;
use App\Service\StructureService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

class StructurePersister implements ContextAwareDataPersisterInterface
{
    public function __construct(
        private EntityManagerInterface $entityManager,
        private StructureService $service,
        private GeonetworkService $gnService
    ) {
    }

    public function supports($data, array $context = []): bool
    {
        return ($data instanceof Structure);
    }


    public function persist($data, array $context = [])
    {
        if ($data->isSynchronised() === true) {
            /** @var Structure $data */
            if (isset($context['collection_operation_name']) && strtolower(
                    $context['collection_operation_name']
                ) === 'post') {
                $createAndPublishOrga = $this->service->createOrganisationAndContact($data);
                $data->setReferenceGn($createAndPublishOrga['uuid']);
            }

            if(isset($context['item_operation_name']) && $data->getReferenceGn() === null) {
                $createAndPublishOrga = $this->service->createOrganisationAndContact($data);
                $data->setReferenceGn($createAndPublishOrga['uuid']);
            }elseif(isset($context['item_operation_name']) && $data->getReferenceGn() !== null){
                $this->service->editOrganisationAndContact($data);
            }
        }else{
            if(isset($context['item_operation_name'])){
                $this->gnService->deleteXmlStructure($data->getReferenceGn());
                $data->setReferenceGn(null);
            }
        }
        $this->entityManager->persist($data);
        $this->entityManager->flush();

        return $data;
    }

    public function remove($data, array $context = [])
    {
        if ($data->getCanDelete() === true) {
            // Appel Geonetwork pour suppression d'un contact ?
            // « Attention, vous êtes sur le point de supprimer une structure. Cela impactera potentiellement les métadonnées qui ont cette structure en contact associé. Veillez à modifier ces métadonnées au préalable.»
            /** @var Structure $data */
            $this->gnService->deleteXmlStructure($data->getReferenceGn());
            $this->entityManager->remove($data);
            $this->entityManager->flush();

        } else {
            throw new StructureDeleteException();
        }
    }
}