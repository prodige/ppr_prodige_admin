<?PHP

declare(strict_types=1);

namespace App\DataPersister;

use ApiPlatform\Core\DataPersister\ContextAwareDataPersisterInterface;
use App\Entity\Configuration\Projection;
use App\Service\SpatialService;
use Doctrine\ORM\EntityManagerInterface;

final class ProjectionPersister implements ContextAwareDataPersisterInterface
{
    public function __construct(
        private EntityManagerInterface $entityManager,
        private SpatialService $spatialService
    ) {
    }

    public function supports($data, array $context = []): bool
    {
        return ($data instanceof Projection);
    }

    public function persist($data, array $context = [])
    {
        // Manage in DB
        $this->entityManager->persist($data);
        $this->entityManager->flush();

        // Manage prodige.public.spatial_ref_sys
        $this->spatialService->addOrUpdateSpatialRef($data);

        // Manage epsg file
        $this->spatialService->addOrUpdateEPSG($data);

        // Manage CARMEN LEX_PROJECTION
        $this->spatialService->addOrUpdateCamenLexProjection($data);

        return $data;
    }

    public function remove($data, array $context = [])
    {
        $this->entityManager->remove($data);
        $this->entityManager->flush();
    }
}
