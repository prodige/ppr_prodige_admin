<?php

declare(strict_types=1);

namespace App\DataPersister;

use ApiPlatform\Core\DataPersister\ContextAwareDataPersisterInterface;
use App\Entity\Administration\Directory\Profile\Profile;
use App\Entity\Administration\Organisation\Subdomain;
use App\Exception\GeonetworkException;
use App\Exception\MetadataException;
use App\Service\ProfileGeonetworkService;
use App\Service\ProfileService;
use Doctrine\ORM\EntityManagerInterface;

final class ProfilePersister implements ContextAwareDataPersisterInterface
{

    public function __construct(
        private EntityManagerInterface $entityManager,
        private ProfileService $profileService,
        private ProfileGeonetworkService $profileGeonetworkService
    ) {
    }

    /**
     * @inheritDoc
     */
    public function supports($data, array $context = []): bool
    {
        return $data instanceof Profile;
    }

    /**
     * @inheritDoc
     * @throws \JsonException
     */
    public function persist($data, array $context = [])
    {
        $profileName = $data->getName();
        // To manage name changes
        $profile = $this->entityManager->getUnitOfWork()->getOriginalEntityData($data);

        // Retrieving groups from geonetwork
        $groups = $this->profileGeonetworkService->getGroups();
        if ($groups) {
            $inGroup = $this->ifExistInGeonetwork($groups, $profileName);
            // If the data already exists, check with the last known name
            if (!empty($profile)) {
                $profileOldName = $profile["name"];
                $inGroup = $this->ifExistInGeonetwork($groups, $profileOldName);
            }

            if ($data->isSynchronised()) {
                // le group n'existe pas dans geonetwork
                if (!$inGroup) {
                    // Creating a new profile synchronised with a group GN
                    $this->profileService->postGroup($data);
                    // pour un patch si le groupe a déjà des utilisateurs ils sont mis à jours
                    $this->profileService->privilegeAction($data, 'add');
                } else {
                    //pour un patch, en cas de changement de nom de group
                    if (!empty($profile) && $profileName !== $profileOldName) {
                        $inGroup = $this->ifExistInGeonetwork($groups, $profileOldName);
                        $groupId = $this->getGeonetworkId($groups, $inGroup);
                        $this->profileService->putGroup($groupId, $profileName);
                    }
                }

                // in case isSynchronized has been checked
                if (!empty($profile) && $profile["isSynchronised"] === true) {
                    //pour un patch où l'on modifie les privileges
                    $this->profileService->privilegeAction($data, 'add');
                }
            } else {
                // on desynchronise un groupe existant
                if ($inGroup !== false) {
                    // in case isSynchronized has been unchecked
                    // on modifie les droits de tous les utilisateurs presents dans le groupes
                    $this->profileService->privilegeAction($data, 'remove');
                    // on supprime le groupe de geonetwork
                    $inGroup = $this->ifExistInGeonetwork($groups, $profileName);

                    if ($inGroup !== false) {
                        $groupId = $this->getGeonetworkId($groups, $inGroup);
                        $this->profileService->deleteGroup($groupId);
                    }
                }
            }
        }
        $this->entityManager->persist($data);
        $this->entityManager->flush();
        $this->entityManager->refresh($data);

        return $data;
    }

    /**
     * Checks the presence of the profile in the GN groups
     * @param array $groups
     * @param string $profileName
     * returns the position in the group array
     * @return bool|int
     */
    private function ifExistInGeonetwork(array $groups, string $profileName): bool|int
    {
        return array_search($profileName, array_column($groups, 'name'), true);
    }

    /**
     * The group id is retrieved from GN
     * @param array $groups
     * @param int $inGroup
     * returns the id GN
     * @return int
     */
    private function getGeonetworkId(array $groups, int $inGroup): int
    {
        return array_values($groups[$inGroup])[6];
    }

    /**
     * @inheritDoc
     * supprime un profil
     */
    public function remove($data, array $context = []): void
    {
        if ($data->isSynchronised()) {
            $this->profileService->privilegeAction($data, 'remove');

            $profileName = $data->getName();
            $groups = $this->profileGeonetworkService->getGroups();

            if ($groups) {
                $inGroup = $this->ifExistInGeonetwork($groups, $profileName);

                if ($inGroup !== false) {
                    $groupId = $this->getGeonetworkId($groups, $inGroup);
                    $response = $this->profileService->deleteGroup($groupId);

                    if (is_array($response) && $response["message"] !== null) {
                        throw new MetadataException($response["message"], 403);
                    } else {
                        throw new GeonetworkException();
                    }
                }
            }
        }
        $subdomainAdmin = $this->entityManager->getRepository(Subdomain::class)->findByprofileAdmin($data);

        if (!empty($subdomainAdmin)) {
            foreach ($subdomainAdmin as $subdomain) {
                $subdomain->setProfileAdmin(null);
            }
        }

        $subdomainEditor = $this->entityManager->getRepository(Subdomain::class)->findByprofileEditor($data);

        if (!empty($subdomainEditor)) {
            foreach ($subdomainEditor as $subdomain) {
                $subdomain->setProfileEditor(null);
            }
        }

        $this->entityManager->remove($data);
        $this->entityManager->flush();
    }


}