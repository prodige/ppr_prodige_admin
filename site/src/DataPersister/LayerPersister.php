<?php

namespace App\DataPersister;

use ApiPlatform\Core\DataPersister\ContextAwareDataPersisterInterface;
use App\DataProvider\LayerProvider;
use App\Entity\Administration\Resource\Layer;
use Doctrine\ORM\EntityManagerInterface;

class LayerPersister implements ContextAwareDataPersisterInterface
{
    public function __construct(private EntityManagerInterface $entityManager,private LayerProvider $layerProvider)
    {

    }

    public function supports($data, array $context = []): bool
    {
        return ($data instanceof Layer);
    }

    public function persist($data, array $context = [])
    {
        /** @var Layer $data */
        if($data->isApiLayer() === false){
            $data->setApiFields(null);
        }
        $this->entityManager->persist($data);
        $this->entityManager->flush();
        $this->entityManager->refresh($data); // Initialize Collection values
        return $this->layerProvider->getItem(Layer::class, $data->getId());
    }

    public function remove($data, array $context = []): void
    {

        $this->entityManager->remove($data);
        $this->entityManager->flush();
    }
}