<?php

namespace App\DataPersister;

use ApiPlatform\Core\DataPersister\ContextAwareDataPersisterInterface;
use App\DataProvider\LayerProvider;
use App\Entity\Administration\Directory\Zoning;
use Doctrine\ORM\EntityManagerInterface;

class ZoningPersister implements ContextAwareDataPersisterInterface
{
    public function __construct(private EntityManagerInterface $entityManager, private LayerProvider $layerProvider)
    {

    }

    public function supports($data, array $context = []): bool
    {
        return ($data instanceof Zoning);
    }

    public function persist($data, array $context = [])
    {
        $this->entityManager->persist($data);
        $this->entityManager->flush();
        $this->entityManager->refresh($data);
        return $data;
    }

    public function remove($data, array $context = [])
    {
        $this->entityManager->remove($data);
        $this->entityManager->flush();
    }
}