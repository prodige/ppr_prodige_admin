<?php

declare(strict_types=1);

namespace App\DataPersister;

use ApiPlatform\Core\DataPersister\ContextAwareDataPersisterInterface;
use App\DataProvider\MapserverProvider;
use App\Entity\Configuration\Mapserver;
use App\Entity\Configuration\Projection;
use App\Entity\Configuration\Setting;
use App\Service\MapserverApiService;
use App\Service\SpatialService;
use Doctrine\ORM\EntityManagerInterface;
use Prodige\ProdigeBundle\Common\UpdateArbo;
use Prodige\ProdigeBundle\Services\CacheService;

final class SettingPersister implements ContextAwareDataPersisterInterface
{
    public function __construct(
        private EntityManagerInterface $entityManager,
        private MapserverProvider      $mapserverProvider,
        private MapserverApiService    $mapserverApiService,
        private CacheService           $cacheService
    )
    {
    }

    public function supports($data, array $context = []): bool
    {
        return ($data instanceof Setting);
    }

    /**
     * @param Setting $data
     * @param array $context
     * @return object|void
     */
    public function persist($data, array $context = [])
    {
        // Check if WFS / WMS setting
        if (preg_match('/(wfs|wms)_server_(title|abstract)/i', $data->getName(), $matches)) {
            // Send to Mapserv API
            $mapserv = $this->mapserverProvider->getItem(Mapserver::class, $matches[1], 'get_map');
            $file = $matches[1];
            $map = $mapserv->getMap();
            unset($map['reference']);
            switch ($data->getName()) {
                case 'wfs_server_title' :
                case 'wms_server_title' :
                    if (isset($map['metadata'][$file . '_title'])) {
                        $map['metadata'][$file . '_title'] = $data->getValue();
                        $mapserv->setMap($map);
                    }
                    break;
                case 'wfs_server_abstract' :
                case 'wms_server_abstract' :
                    if (isset($map['metadata'][$file . '_abstract'])) {
                        $map['metadata'][$file . '_abstract'] = $data->getValue();
                        $mapserv->setMap($map);
                    }
                    break;
            }
            $map['metadata'] = json_encode($map['metadata']);
            $map['directory'] = $mapserv->getDirectory();
            $this->mapserverApiService->postMap($file, $map, true);
        } elseif ($data->getName() === "PRO_PROJ_WFS") {
            // Get file
            $mapserv = $this->mapserverProvider->getItem(Mapserver::class, 'wfs', 'get_map');

            // projection par default
            $epsg = '2154';
            $wfs_srs = 'EPSG:2154';

            if (count($data->getProjections()) > 0) {
                $epsg = $data->getProjections()->first()->getEpsg();
                $wfs_srs = "";
            }
            $map['projection'] = "epsg:" . $epsg;

            /** @var Projection $proj */
            foreach ($data->getProjections() as $proj) {
                $wfs_srs .= " EPSG:" . $proj->getEpsg();
            }

            $map = $mapserv->getMap();
            $map['metadata']['wfs_srs'] = trim($wfs_srs);
            $map['projection'] = '+init=epsg:' . $epsg;
            unset($map['reference']);

            $this->mapserverApiService->postMap('wfs', $map, true);
            $data->setValue(trim($wfs_srs));
        } elseif ($data->getName() === "PRO_PROJ_WMS") {
            // Get file
            $mapserv = $this->mapserverProvider->getItem(Mapserver::class, 'wms', 'get_map');

            // projection par default
            $wms_srs = (count($data->getProjections()) > 0) ? '' : 'EPSG:2154';

            /** @var Projection $proj */
            foreach ($data->getProjections() as $proj) {
                $wms_srs .= " EPSG:" . $proj->getEpsg();
            }

            $map = $mapserv->getMap();
            $map['metadata']['wms_srs'] = trim($wms_srs);
            unset($map['reference']);

            $this->mapserverApiService->postMap('wms', $map, true);
            $data->setValue(trim($wms_srs));
        }elseif($data->getName() === "PRO_PROJ_OGCAPI"){
            //TODO
        } elseif ($data->getLexSettingCategory()->getId() === 6) {
            UpdateArbo::updateArbo($this->entityManager->getConnection());
        }elseif ($data->getName() === "PRO_IMPORT_EPSG"){
            $adminProj = "";
            foreach ($data->getProjections() as $proj) {
                $adminProj .=  " EPSG:" . $proj->getEpsg();
            }
            $data->setValue(ltrim($adminProj));
        }

        // Manage in DB
        $this->entityManager->persist($data);
        $this->entityManager->flush();

        // Clear Cache
        $this->cacheService->getSettingsCache()->clear();

        return $data;
    }

    public function remove($data, array $context = [])
    {
    }
}
