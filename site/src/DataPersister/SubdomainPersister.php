<?php

declare(strict_types=1);

namespace App\DataPersister;

use ApiPlatform\Core\DataPersister\ContextAwareDataPersisterInterface;
use App\Entity\Administration\Organisation\Subdomain;
use App\Response\JsonLDResponse;
use App\Service\ProfileService;
use App\Service\SubdomainService;
use App\Service\ThesaurusSerializer;
use Doctrine\ORM\EntityManagerInterface;

class SubdomainPersister implements ContextAwareDataPersisterInterface
{

    public function __construct(
        private EntityManagerInterface $entityManager,
        private SubdomainService $service,
        private ProfileService $profileService
    ) {
    }

    public function supports($data, array $context = []): bool
    {
        return ($data instanceof Subdomain);
    }

    public function persist($data, array $context = [])
    {
        if (isset($context['item_operation_name'])) {
            $method = $context['item_operation_name'];
        } else {
            $method = 'post';
        }
        /** @var Subdomain $data */
        switch ($method) {
            case 'post':
                if ($data->getWmsService() === true) {
                    $MapFile = $this->service->createMapFile($data);
                    if (array_key_exists('error', $MapFile)) {
                        return ['error' => $MapFile['error'], 'status' => $MapFile['status']];
                    }
                    $createXml = $this->service->createServiceWMSFiche($MapFile['subdomain']);
                    if (array_key_exists('error', $createXml)) {
                        return new JsonLDResponse(
                            ["error" => $createXml['error']],
                            $createXml['status'],
                            [],
                            false,
                            "Data"
                        );
                    }

                    $data->setWmsServiceUuid($createXml['uuid']);
                }
                break;
            case 'patch':
                if ($data->getWmsService() === true && $this->entityManager->getUnitOfWork()->getOriginalEntityData(
                        $data
                    )['wmsService'] === false) {
                    $MapFile = $this->service->createMapFile($data);
                    if (array_key_exists('error', $MapFile)) {
                        return ['error' => $MapFile['error'], 'status' => $MapFile['status']];
                    }
                    $createXml = $this->service->createServiceWMSFiche($MapFile['subdomain']);
                    if (array_key_exists('error', $createXml)) {
                        return ['error' => $createXml['error'], 'status' => $createXml['status']];
                    }

                    $data->setWmsServiceUuid($createXml['uuid']);
                }
                if ($data->getWmsService() === false && $this->entityManager->getUnitOfWork()->getOriginalEntityData(
                        $data
                    )['wmsService'] === true) {
                    $deleteMapFile = $this->service->removeMapFile($data);
                    if (array_key_exists('error', $deleteMapFile)) {
                        return new JsonLDResponse(
                            ["error" => $deleteMapFile['error']],
                            $deleteMapFile['status'],
                            [],
                            false,
                            "Data"
                        );
                    }
                    if ($data->getWmsServiceUuid() !== null) {
                        $deleteXml = $this->service->removeServiceWMSFiche($data->getWmsServiceUuid());
                        if (array_key_exists('error', $deleteXml)) {
                            return new JsonLDResponse(
                                ["error" => $deleteXml['error']],
                                $deleteXml['status'],
                                [],
                                false,
                                "Data"
                            );
                        }
                    }
                    $data->setWmsServiceUuid(null);
                }
                break;
        }

        if ($method !== 'post') {
            //si il y a un changement de profilAdmin, on mémorise l'ancien profil
            $oldProfileAdmin = $this->entityManager->getUnitOfWork()->getOriginalEntityData(
                $data
            )['profileAdmin'];
        }

        $this->entityManager->persist($data);
        $this->entityManager->flush();

        $this->entityManager->refresh($data); // Initialize Collection values

        //Après enregistrement on met à jour les profils
        if (isset($oldProfileAdmin)) {
            $this->profileService->privilegeAction($oldProfileAdmin, 'add');
        }

        //si le ssdom a un profileAdmin
        if ($data->getProfileAdmin() !== null) {
            $this->profileService->privilegeAction($data->getProfileAdmin(), 'add');
        }

        // Send data to geonetwork Thesaurus for Rubric/Domain & Subdomain entities
        ThesaurusSerializer::getInstance($this->entityManager, THESAURUS_NAME)->generateDocument();

        return $data;
    }

    public function remove($data, array $context = [])
    {
        /** @var Subdomain $data */
        if ($data->getWmsService() === true && $data->getWmsServiceUuid() !== null) {
            $deleteMapFile = $this->service->removeMapFile($data);
            if (array_key_exists('error', $deleteMapFile)) {
                return new JsonLDResponse(
                    ["error" => $deleteMapFile['error']],
                    $deleteMapFile['status'],
                    [],
                    false,
                    "Data"
                );
            }
            $deleteXml = $this->service->removeServiceWMSFiche($data->getWmsServiceUuid());
            if (array_key_exists('error', $deleteXml)) {
                return new JsonLDResponse(
                    ["error" => $deleteXml['error']],
                    $deleteXml['status'],
                    [],
                    false,
                    "Data"
                );
            }
        }
        //si le ssdom a un profileAdmin
        if ($data->getProfileAdmin() !== null) {
            $profileBeDeleted = $data->getProfileAdmin();
        }

        $this->entityManager->remove($data);
        $this->entityManager->flush();

        if (isset($profileBeDeleted)) {
            $this->profileService->privilegeAction($profileBeDeleted, 'add');
        }

        ThesaurusSerializer::getInstance($this->entityManager, THESAURUS_NAME)->generateDocument();
    }
}
