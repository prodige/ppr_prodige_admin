<?PHP

declare(strict_types=1);

namespace App\DataPersister;

use ApiPlatform\Core\DataPersister\ContextAwareDataPersisterInterface;
use App\Entity\Administration\Organisation\Domain;
use App\Entity\Administration\Organisation\Rubric;
use App\Service\ThesaurusSerializer;
use Doctrine\ORM\EntityManagerInterface;

final class DomainPersister implements ContextAwareDataPersisterInterface
{
    public function __construct(private EntityManagerInterface $entityManager)
    {
    }

    public function supports($data, array $context = []): bool
    {
        return ($data instanceof Rubric || $data instanceof Domain);
    }

    public function persist($data, array $context = [])
    {
        $this->entityManager->persist($data);
        $this->entityManager->flush();
        $this->entityManager->refresh($data); // Initialize Collection values

        // Send data to geonetwork Thesaurus for Rubric/Domain & Subdomain entities
        ThesaurusSerializer::getInstance($this->entityManager, THESAURUS_NAME)->generateDocument();

        return $data;
    }

    public function remove($data, array $context = [])
    {
        $this->entityManager->remove($data);
        $this->entityManager->flush();

        ThesaurusSerializer::getInstance($this->entityManager, THESAURUS_NAME)->generateDocument();
    }
}
