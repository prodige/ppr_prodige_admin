<?PHP

declare(strict_types=1);

namespace App\DataPersister;

use ApiPlatform\Core\DataPersister\ContextAwareDataPersisterInterface;
use App\Entity\Administration\Resource\Dataviz;
use Doctrine\ORM\EntityManagerInterface;


final class DatavizPersister implements ContextAwareDataPersisterInterface
{
    public function __construct(
        private EntityManagerInterface $entityManager,
    )
    {
    }

    public function supports($data, array $context = []): bool
    {
        return ($data instanceof Dataviz);
    }

    public function persist($data, array $context = [])
    {
        $this->entityManager->persist($data);
        $this->entityManager->flush();
        $this->entityManager->refresh($data); // Initialize Collection values

        return $data;
    }


    public function remove($data, array $context = []): void
    {
        $this->entityManager->remove($data);
        $this->entityManager->flush();
    }

}
