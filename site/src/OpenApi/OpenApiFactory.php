<?php

namespace App\OpenApi;

//use ApiPlatform\OpenApi\Factory\OpenApiFactoryInterface;
use ApiPlatform\Core\OpenApi\Factory\OpenApiFactoryInterface;
use ApiPlatform\Core\OpenApi\OpenApi;
use ApiPlatform\Core\OpenApi\Model;
use ApiPlatform\Core\OpenApi\Model\PathItem;
use Symfony\Component\HttpFoundation\RequestStack;

class OpenApiFactory implements OpenApiFactoryInterface
{
    public function __construct(private OpenApiFactoryInterface $decorated, private RequestStack $requestStack)
    {
    }

    public function __invoke(array $context = []): OpenApi
    {
        $openApi = $this->decorated->__invoke($context);
        $paths = $openApi->getPaths()->getPaths();

        // Check if OGC doc
        if (!str_starts_with($this->requestStack->getCurrentRequest()->getRequestUri(), '/api/ogc-features/')) {
            $filteredPaths = new Model\Paths();
            /** @var PathItem $pathItem */
            foreach ($paths as $path => $pathItem) {
                if (!str_starts_with($path, '/api/ogc-features')) {
                    $filteredPaths->addPath($path, $pathItem);
                }
            }
            $tab = [];
            foreach ($openApi->getComponents()->getSchemas() as $name => $schema) {
                if (!str_starts_with($name, 'Ogc')) {
                    $tab[$name] = $schema;
                }
            }

            $openApi->getComponents()->getSchemas()->exchangeArray($tab);
            return $openApi->withPaths($filteredPaths);
        }

        // Cas OGC Features
        $filteredPaths = new Model\Paths();
        /** @var PathItem $pathItem */
        foreach ($paths as $path => $pathItem) {
            if (str_starts_with($path, '/api/ogc-features/')) {
                $filteredPaths->addPath($path, $pathItem);
            }
        }

        $tab = [];
        // TODO add other routes (items etc)
        foreach ($openApi->getComponents()->getSchemas() as $name => $schema) {
            if (str_starts_with($name, 'collection')) {
                $tab[$name] = $schema;
            }
        }

        // TODO : Change title, description, etc en fonction des specs (les valeurs devraient être en BDD dans parameters)
        $openApi = $openApi->withInfo((new Model\Info('API Features', 'v0.0.1', 'PRODIGE OGC API Features')));
        $openApi->getComponents()->getSchemas()->exchangeArray($tab);
        return $openApi->withPaths($filteredPaths);
    }


}