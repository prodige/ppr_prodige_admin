<?php

namespace App\Security;


use App\Entity\User\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Migrations\Configuration\EntityManager\ManagerRegistryEntityManager;
use Doctrine\ORM\EntityManager;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

class CasUserProvider implements UserProviderInterface
{

    public function __construct(private ManagerRegistry $managerRegistry)
    {
    }

    public function loadUserByUsername($username)
    {
        return $this->managerRegistry->getRepository(User::class)->findOneBy(["login" => $username]);
    }

    public function refreshUser(UserInterface $userInterface)
    {
        return $userInterface;
    }

    public function supportsClass($class)
    {
        return $class === 'Alk\Common\CasBundle\Security\User\User';
    }

}