<?php

declare(strict_types=1);

namespace App\Entity\Configuration;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Serializer\Filter\PropertyFilter;
use App\Entity\Lex\LexSettingType;
use App\Entity\Lex\LexSettingCategory;
use App\Filter\FullTextSearchFilter;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Admin.Setting
 */
#[ApiResource(
    itemOperations: ['get', 'patch'],
    denormalizationContext: ['groups' => ['PatchSetting']],
    normalizationContext: ['groups' => ['GetSetting']]
)]
#[ApiFilter(SearchFilter::class, properties: ['lexSettingCategory.id' => 'exact', 'name' => 'exact'])]
#[ApiFilter(FullTextSearchFilter::class, properties: ['name' => 'ipartial', 'title' => 'ipartial'])]
#[ApiFilter(OrderFilter::class, properties: ['name', 'title', 'value'])]
#[ApiFilter(PropertyFilter::class)]
#[ORM\Table(name: 'setting', schema: 'admin')]
#[ORM\Entity]
#[UniqueEntity(fields: 'name')]
class Setting
{
    #[ORM\Column(name: 'id', type: 'integer', nullable: false)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    #[Groups(['GetSetting'])]
    private int $id;

    #[ORM\Column(name: 'name', type: 'string', length: 128, unique: true, nullable: false)]
    #[Groups(['GetSetting'])]
    private string $name;

    #[ORM\Column(name: 'title', type: 'string', nullable: false)]
    #[Groups(['GetSetting'])]
    private string $title;

    #[ORM\Column(name: 'description', type: 'text', nullable: false)]
    #[Groups(['GetSetting'])]
    private string $description;

    #[ORM\Column(name: 'value', type: 'string', nullable: false)]
    #[Groups(['GetSetting', 'PatchSetting'])]
    private string $value;

    #[ORM\Column(name: 'setting_order', type: 'integer', nullable: false, options: ['default' => 0])]
    #[Groups(['GetSetting', 'PatchSetting'])]
    private int $settingOrder;

    #[ORM\ManyToOne(targetEntity: LexSettingType::class)]
    #[ORM\JoinColumn(name: 'lex_setting_type_id', referencedColumnName: 'id', nullable: false)]
    #[Groups(['GetSetting'])]
    private LexSettingType $lexSettingType;

    #[ORM\ManyToOne(targetEntity: LexSettingCategory::class)]
    #[ORM\JoinColumn(name: 'lex_setting_category_id', referencedColumnName: 'id', nullable: false)]
    #[Groups(['GetSetting'])]
    private LexSettingCategory $lexSettingCategory;

    #[ORM\ManyToOne(targetEntity: Module::class)]
    #[ORM\JoinColumn(name: 'module_id', referencedColumnName: 'id', nullable: true)]
    private Module $module;

    #[ORM\ManyToMany(targetEntity: Projection::class, inversedBy: 'settings')]
    #[ORM\JoinTable(name: 'settings_projections', schema: 'admin')]
    #[Groups(['GetSetting', 'PatchSetting'])]
    private Collection $projections;

    #[ORM\ManyToMany(targetEntity: Format::class, inversedBy: 'settings')]
    #[ORM\JoinTable(name: 'settings_formats', schema: 'admin')]
    #[Groups(['PatchSetting'])]
    private Collection $formats;

    #[ORM\ManyToMany(targetEntity: Template::class, inversedBy: 'settings')]
    #[ORM\JoinTable(name: 'settings_templates', schema: 'admin')]
    #[Groups(['PatchSetting'])]
    private Collection $templates;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getValue(): string
    {
        return $this->value;
    }

    /**
     * @param string $value
     */
    public function setValue(string $value): void
    {
        $this->value = $value;
    }


    /**
     * @return int
     */
    public function getSettingOrder(): int
    {
        return $this->settingOrder;
    }

    /**
     * @param int $settingOrder
     */
    public function setSettingOrder(int $settingOrder): void
    {
        $this->settingOrder = $settingOrder;
    }

    /**
     * @return LexSettingType
     */
    public function getLexSettingType(): LexSettingType
    {
        return $this->lexSettingType;
    }

    /**
     * @param LexSettingType $lexSettingType
     */
    public function setLexSettingType(LexSettingType $lexSettingType): void
    {
        $this->lexSettingType = $lexSettingType;
    }

    /**
     * @return LexSettingCategory
     */
    public function getLexSettingCategory(): LexSettingCategory
    {
        return $this->lexSettingCategory;
    }

    /**
     * @param LexSettingCategory $lexSettingCategory
     */
    public function setLexSettingCategory(LexSettingCategory $lexSettingCategory): void
    {
        $this->lexSettingCategory = $lexSettingCategory;
    }

    /**
     * @return Module
     */
    public function getModule(): Module
    {
        return $this->module;
    }

    /**
     * @param Module $module
     */
    public function setModule(Module $module): void
    {
        $this->module = $module;
    }

    /**
     * @return Collection
     */
    public function getProjections(): Collection
    {
        return $this->projections;
    }

    public function addProjection(Projection $projection): self
    {
        if (!$this->projections->contains($projection)) {
            $this->projections[] = $projection;
        }

        return $this;
    }

    public function removeProjection(Projection $projection): self
    {
        $this->projections->removeElement($projection);

        return $this;
    }

    /**
     * @return Collection
     */
    public function getFormats(): Collection
    {
        return $this->formats;
    }

    public function addFormat(Format $format): self
    {
        if (!$this->formats->contains($format)) {
            $this->formats[] = $format;
        }

        return $this;
    }

    public function removeFormat(Format $format): self
    {
        $this->formats->removeElement($format);

        return $this;
    }

    /**
     * @return Collection
     */
    public function getTemplates(): Collection
    {
        return $this->templates;
    }

    public function addTemplate(Template $template): self
    {
        if (!$this->templates->contains($template)) {
            $this->templates[] = $template;
        }

        return $this;
    }

    public function removeTemplate(Template $template): self
    {
        $this->templates->removeElement($template);

        return $this;
    }

}
