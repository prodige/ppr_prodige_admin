<?php

declare(strict_types=1);

namespace App\Entity\Configuration;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Serializer\Filter\PropertyFilter;
use App\Entity\Lex\LexTemplateType;
use App\Filter\FullTextSearchFilter;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Admin.Template
 */
#[ApiResource(
    itemOperations: ['get', 'patch', 'delete'],
    denormalizationContext: ['groups' => ['PatchTemplate']],
    normalizationContext: ['groups' => ['GetTemplate']]
)]
#[ApiFilter(FullTextSearchFilter::class, properties: [
    'name' => 'ipartial',
    'mapfile' => 'ipartial',
    'lexTemplateType.name' => 'ipartial',
])]
#[ApiFilter(SearchFilter::class, properties: ['lexTemplateType.name' => 'exact'])]
#[ApiFilter(OrderFilter::class, properties: ['name', 'mapfile', 'lexTemplateType.name'])]
#[ApiFilter(PropertyFilter::class)]
#[ORM\Table(name: 'template', schema: 'admin')]
#[UniqueEntity(fields: 'name')]
#[ORM\Entity]
class Template
{
    #[ORM\Column(name: 'id', type: 'integer', nullable: false)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    #[Groups(['GetTemplate'])]
    private int $id;

    #[ORM\Column(name: 'name', type: 'string', length: 128, unique: true, nullable: false)]
    #[Groups(['GetTemplate', 'PatchTemplate'])]
    private string $name;

    #[Assert\Regex('/[a-zA-Z0-9-_]*.map/')]
    #[ORM\Column(name: 'mapfile', type: 'string', nullable: false)]
    #[Groups(['GetTemplate', 'PatchTemplate'])]
    private string $mapfile;

    #[ORM\ManyToOne(targetEntity: LexTemplateType::class)]
    #[ORM\JoinColumn(name: 'lex_template_type_id', referencedColumnName: 'id')]
    #[Groups(['GetTemplate', 'PatchTemplate'])]
    private LexTemplateType $lexTemplateType;

    #[Groups(['GetTemplate'])]
    private string $uuid;

    #[ORM\ManyToMany(targetEntity: Setting::class, mappedBy: 'templates')]
    #[ORM\JoinTable(name: 'settings_templates', schema: 'admin')]
    private Collection $settings;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getMapfile(): string
    {
        return $this->mapfile;
    }

    /**
     * @param string $mapfile
     */
    public function setMapfile(string $mapfile): void
    {
        $this->mapfile = $mapfile;
    }

    /**
     * @return LexTemplateType
     */
    public function getLexTemplateType(): LexTemplateType
    {
        return $this->lexTemplateType;
    }

    /**
     * @param LexTemplateType $lexTemplateType
     */
    public function setLexTemplateType(LexTemplateType $lexTemplateType): void
    {
        $this->lexTemplateType = $lexTemplateType;
    }

    /**
     * @return string
     */
    public function getUuid(): string
    {
        return $this->uuid;
    }

    /**
     * @param string $uuid
     * @return Template
     */
    public function setUuid(string $uuid): Template
    {
        $this->uuid = $uuid;
        return $this;
    }

}
