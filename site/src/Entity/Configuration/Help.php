<?php

declare(strict_types=1);

namespace App\Entity\Configuration;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Serializer\Filter\PropertyFilter;
use App\Entity\Administration\Directory\Profile\Profile;
use App\Entity\Trait\TimestampsTrait;
use App\Filter\FullTextSearchFilter;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Serializer\Annotation\Groups;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @Vich\Uploadable
 */
#[ApiResource(
    collectionOperations: [
        'get',
        'post' => [
            'input_formats' => [
                'multipart' => ['multipart/form-data'],
                'json' => ['application/ld+json'],
            ],
        ],
    ],
    itemOperations: ['get', 'patch', 'delete'],
    denormalizationContext: ['groups' => ['InputHelp']],
    normalizationContext: ['groups' => ['GetHelp']],
)]
#[ApiFilter(FullTextSearchFilter::class, properties: ['name' => 'ipartial'])]
#[ApiFilter(OrderFilter::class, properties: ['name'])]
#[ApiFilter(PropertyFilter::class)]
#[ORM\Table(name: 'admin.help')]
#[ORM\Entity]
#[ORM\HasLifecycleCallbacks]
class Help
{
    use TimestampsTrait;

    #[ORM\Column(name: 'id', type: 'integer', nullable: false)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    #[Groups(['InputHelp', 'GetHelp'])]
    private int $id;

    #[ORM\Column(name: 'title', type: 'string', nullable: false)]
    #[Groups(['InputHelp', 'GetHelp'])]
    private string $title;

    #[ORM\Column(name: 'description', type: 'text', nullable: true)]
    #[Groups(['InputHelp', 'GetHelp'])]
    private ?string $description;

    #[Groups(['GetHelp'])]
    public ?string $contentUrl = null;

    /**
     * @Vich\UploadableField(mapping="help_file", fileNameProperty="filePath")
     */
    #[Groups(['InputHelp'])]
    public ?File $file = null;

    #[ORM\Column(nullable: true)]
    public ?string $filePath = null;

    #[ORM\ManyToMany(targetEntity: Profile::class, mappedBy: 'helps')]
    #[Groups(['InputHelp', 'GetHelp'])]
    private Collection $profiles;

    public function __construct()
    {
        $this->profiles = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return Collection
     */
    public function getProfiles(): Collection
    {
        return $this->profiles;
    }

    public function addProfile(Profile $profile): self
    {
        if (!$this->profiles->contains($profile)) {
            $this->profiles[] = $profile;
            $profile->addHelp($this);
        }

        return $this;
    }

    public function removeProfile(Profile $profile): self
    {
        if ($this->profiles->removeElement($profile)) {
            $profile->removeHelp($this);
        }

        return $this;
    }
}
