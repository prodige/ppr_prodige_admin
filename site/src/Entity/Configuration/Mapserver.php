<?php

declare(strict_types=1);

namespace App\Entity\Configuration;

use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Controller\Configuration\Mapserver\MapserverController;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Class to manage mapserver api.
 */
#[ApiResource(
    collectionOperations: [],
    itemOperations: [
        'get_map' => [
            'method' => 'GET',
            'path' => '/mapserver/map/{file}',
            'openapi_context' => [
                'summary' => 'Get a map from mapserver',
                'description' => 'Get a name from mapserver',
            ],
            'normalization_context' => ['groups' => 'GetMapServer'],
        ],
        'put_map' => [
            'method' => 'PUT',
            'path' => '/mapserver/map/{file}',
            'pagination_enabled' => false,
            'openapi_context' => [
                'summary' => 'Add or edit a map to mapserver',
                'description' => 'Add or edit a map to mapserver',
            ],
            'controller' => MapserverController::class,
            'read' => false,
            '_api_receive' => false,
            'denormalization_context' => ['groups' => 'PutMapserver'],
        ],
    ],
    normalizationContext: ['groups' => ['GetMapServer']]
)]
class Mapserver
{
    // TODO : Define all map attributes when API will be open
    #[Groups(['GetMapServer', 'PutMapserver'])]
    private array $map = [];

    #[ApiProperty(identifier: true,
        attributes: [
            'openapi_context' => ['example' => 'wms'],
        ]
    )]
    #[Groups(['PostMapServer', 'GetMapServer'])]
    private string $file;

    private string $directory;

    public function __construct()
    {
        $this->directory = $_ENV['ROOT_DATA'].$_ENV['MAPSERVER_ACCOUNT_DIR'].$_ENV['MAPSERVER_MAPFILE_DIR'];
    }

    /**
     * @return array
     */
    public function getMap(): array
    {
        return $this->map;
    }

    /**
     * @param array $map
     * @return Mapserver
     */
    public function setMap(array $map): Mapserver
    {
        $this->map = $map;
        return $this;
    }

    /**
     * @return string
     */
    public function getFile(): string
    {
        return $this->file;
    }

    /**
     * @param string $file
     * @return Mapserver
     */
    public function setFile(string $file): Mapserver
    {
        $this->file = $file;
        return $this;
    }

    /**
     * @return string
     */
    public function getDirectory(): string
    {
        return $this->directory;
    }

}
