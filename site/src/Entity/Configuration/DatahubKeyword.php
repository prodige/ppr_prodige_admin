<?php

namespace App\Entity\Configuration;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Serializer\Filter\PropertyFilter;
use App\Entity\Trait\TimestampsTrait;
use App\Filter\FullTextSearchFilter;
use Symfony\Component\Serializer\Annotation\Groups;
use Doctrine\ORM\Mapping as ORM;

#[ApiResource(
    itemOperations: ['get', 'patch', 'delete']
)]
#[ApiFilter(FullTextSearchFilter::class, properties: ['name' => 'ipartial'])]
#[ApiFilter(OrderFilter::class, properties: ['name'])]
#[ApiFilter(PropertyFilter::class)]
#[ORM\Table(name: 'datahub_keyword', schema: 'admin')]
#[ORM\Entity]
#[ORM\HasLifecycleCallbacks]
class DatahubKeyword
{
    use TimestampsTrait;

    /**
     * @var int
     */
    #[ORM\Column(name: 'id', type: 'integer', nullable: false)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    private int $id;

    /**
     * @var string
     */
    #[ORM\Column(name: 'name', type: 'string', nullable: false)]
    #[Assert\Type('string')]
    #[Assert\NotNull]
    private string $name;

    /**
     * @var string
     */
    #[ORM\Column(name: 'filter', type: 'string', nullable: false)]
    #[Assert\Type('string')]
    #[Assert\NotNull]
    private string $filter;

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): DatahubKeyword
    {
        $this->id = $id;
        return $this;
    }

    public function getName(): string
    {
        return strip_tags($this->name);
    }

    public function setName(string $name): DatahubKeyword
    {
        $this->name = strip_tags($name);
        return $this;
    }

    public function getFilter(): string
    {
        return strip_tags($this->filter);
    }

    public function setFilter(string $filter): DatahubKeyword
    {
        $this->filter = strip_tags($filter);
        return $this;
    }


}