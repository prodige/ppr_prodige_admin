<?php

declare(strict_types=1);

namespace App\Entity\Configuration;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Admin.Module
 */
#[ApiResource(
    itemOperations: ['get', 'patch']
)]
#[ORM\Table(name: 'module', schema: 'admin')]
#[ORM\Entity]
#[UniqueEntity(fields: 'name')]
class Module
{
    #[ORM\Column(name: 'id', type: 'integer', nullable: false)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    private int $id;

    #[ORM\Column(name: 'name', type: 'string', length: 128, unique: true, nullable: false)]
    private string $name;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }
}
