<?php

declare(strict_types=1);

namespace App\Entity\Configuration;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use App\Filter\FullTextSearchFilter;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Admin.Projection
 */
#[ApiResource(
    collectionOperations: [
        'post',
        'get',
        'get_spatial_ref' => [
            'method' => 'GET',
            'path' => '/projections/ref',
            'pagination_enabled' => false,
            'openapi_context' => [
                'summary' => 'Get all spatial rel projections',
                'description' => 'Get all spatial rel projections',
            ],
        ],
    ],
    itemOperations: [
        'put',
        'delete',
        'patch',
        'get' => ['normalization_context' => ['groups' => ['GetProjection']]],
    ],
    denormalizationContext: ['groups' => ['PostProjections']],
    normalizationContext: ['groups' => ['GetProjections']],
)]
#[ApiFilter(FullTextSearchFilter::class, properties: ['name' => 'ipartial', 'epsg' => 'exact'])]
#[ApiFilter(OrderFilter::class, properties: ['name', 'epsg'])]
#[ORM\Table(name: 'projection', schema: 'admin')]
#[ORM\Entity]
#[UniqueEntity(fields: 'name')]
#[UniqueEntity(fields: 'epsg')]
class Projection
{
    #[ORM\Column(name: 'id', type: 'integer', nullable: false)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    #[ApiProperty(identifier: false)]
    #[Groups(['GetProjections'])]
    private int $id;

    #[ORM\Column(name: 'name', type: 'string', length: 128, unique: true, nullable: false)]
    #[Groups(['GetProjection', 'GetProjections', 'PostProjections'])]
    private ?string $name = null;

    #[ORM\Column(name: 'epsg', type: 'integer', unique: true, nullable: false)]
    #[Groups(['GetProjection', 'GetProjections', 'PostProjections'])]
    #[ApiProperty(identifier: true)]
    private int $epsg;

    #[ORM\ManyToMany(targetEntity: Setting::class, mappedBy: 'projections')]
    #[ORM\JoinTable(name: 'settings_projections', schema: 'admin')]
    private Collection $settings;

    #[Groups(['GetProjection', 'PostProjections'])]
    private ?string $proj4;

    #[Groups(['GetProjection', 'PostProjections'])]
    private ?int $srid;

    #[Groups(['GetProjection', 'PostProjections'])]
    private ?string $authName;

    #[Groups(['GetProjection', 'PostProjections'])]
    private ?int $authSrid;

    #[Groups(['GetProjection', 'PostProjections'])]
    private ?string $srText;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Projection
     */
    public function setId(int $id): Projection
    {
        $this->id = $id;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): Projection
    {
        $this->name = $name;
        return $this;
    }


    /**
     * @return int
     */
    public function getEpsg(): int
    {
        return $this->epsg;
    }

    /**
     * @param int $epsg
     * @return Projection
     */
    public function setEpsg(int $epsg): Projection
    {
        $this->epsg = $epsg;

        return $this;
    }

    /**
     * @return Collection
     */
    public function getSettings(): Collection
    {
        return $this->settings;
    }

    /**
     * @param Collection $settings
     * @return Projection
     */
    public function setSettings(Collection $settings): Projection
    {
        $this->settings = $settings;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getProj4(): ?string
    {
        return $this->proj4;
    }

    /**
     * @param string|null $proj4
     * @return Projection
     */
    public function setProj4(?string $proj4): Projection
    {
        $this->proj4 = $proj4;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getSrid(): ?int
    {
        return $this->srid;
    }

    /**
     * @param int|null $srid
     * @return Projection
     */
    public function setSrid(?int $srid): Projection
    {
        $this->srid = $srid;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getAuthName(): ?string
    {
        return $this->authName;
    }

    /**
     * @param string|null $authName
     * @return Projection
     */
    public function setAuthName(?string $authName): Projection
    {
        $this->authName = $authName;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getAuthSrid(): ?int
    {
        return $this->authSrid;
    }

    /**
     * @param int|null $authSrid
     * @return Projection
     */
    public function setAuthSrid(?int $authSrid): Projection
    {
        $this->authSrid = $authSrid;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getSrText(): ?string
    {
        return $this->srText;
    }

    /**
     * @param string|null $srText
     * @return Projection
     */
    public function setSrText(?string $srText): Projection
    {
        $this->srText = $srText;

        return $this;
    }

}
