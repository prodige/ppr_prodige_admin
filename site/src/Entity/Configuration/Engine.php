<?php

declare(strict_types=1);

namespace App\Entity\Configuration;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Serializer\Filter\PropertyFilter;
use App\Controller\Configuration\EngineController;
use App\Controller\Configuration\EngineDataController;
use App\Entity\Administration\Resource\Layer;
use App\Entity\Lex\LexEngineType;
use App\Filter\FullTextSearchFilter;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Admin.Engine
 */
#[ApiResource(
    collectionOperations: [
        'get', 'post',
        'sync' => [
            'method' => 'POST',
            'path' => '/engines/sync',
            'openapi_context' => [
                'summary' => 'Synchronize engines in locator',
                'description' => 'Synchronize engines in locator',
            ],
            'controller' => EngineController::class,
            'read' => false,
            '_api_receive' => false,
        ],
    ],
    itemOperations: [
        'get', 'patch', 'delete',
        'data' => [
            'method' => 'GET',
            'path' => '/engines/{id}/data',
            'openapi_context' => [
                'summary' => 'Get engine data by id',
                'description' => 'Get engine data by id',
            ],
            'controller' => EngineDataController::class,
            'read' => false,
            '_api_receive' => false,
        ]
    ],
    denormalizationContext: ['groups' => ['InputEngine']],
    normalizationContext: ['groups' => ['GetEngine']]
)]
#[ApiFilter(FullTextSearchFilter::class, properties: [
    'name' => 'ipartial',
    'order' => 'ipartial',
    'table' => 'ipartial',
])]
#[ApiFilter(SearchFilter::class, properties: ['lexEngineType' => 'exact', 'lexEngineType.name' => 'exact'])]
#[ApiFilter(OrderFilter::class, properties: ['rowOrder'])]
#[ApiFilter(PropertyFilter::class)]
#[ORM\Table(name: 'engine', schema: 'admin')]
#[ORM\UniqueConstraint(name: 'engine_unique', columns: ['name', 'lex_engine_type_id'])]
#[ORM\Entity]
#[UniqueEntity(fields: ['name', 'lexEngineType'])]
class Engine
{
    #[ORM\Column(name: 'id', type: 'integer', nullable: false)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    #[Groups(['GetEngine'])]
    private int $id;

    #[ORM\Column(name: 'name', type: 'string', length: 128, nullable: false)]
    #[Groups(['GetEngine', 'InputEngine'])]
    private string $name;

    #[ORM\Column(name: 'row_order', type: 'integer', nullable: false)]
    #[Groups(['GetEngine', 'InputEngine'])]
    private int $rowOrder;

    #[ORM\ManyToOne(targetEntity: Layer::class)]
    #[Groups(['GetEngine', 'InputEngine'])]
    private Layer $layer;

    #[ORM\Column(name: 'field_id', type: 'string', nullable: false)]
    #[Groups(['GetEngine', 'InputEngine'])]
    private string $fieldId;

    #[ORM\Column(name: 'field_name', type: 'string', length: 128, nullable: false)]
    #[Groups(['GetEngine', 'InputEngine'])]
    private string $fieldName;

    #[ORM\ManyToOne(targetEntity: Engine::class, inversedBy: "engines")]
    #[ORM\JoinColumn(name: 'join_id', referencedColumnName: 'id', nullable: true)]
    #[Groups(['GetEngine', 'InputEngine'])]
    private ?Engine $joinId;

    #[ORM\OneToMany(mappedBy: 'joinId', targetEntity: Engine::class, orphanRemoval: false)]
    private Collection $engines;

    #[ORM\Column(name: 'join_field', type: 'string', length: 128, nullable: true)]
    #[Groups(['GetEngine', 'InputEngine'])]
    private ?string $joinField;

    #[ORM\ManyToOne(targetEntity: LexEngineType::class)]
    #[ORM\JoinColumn(name: 'lex_engine_type_id', referencedColumnName: 'id')]
    #[Groups(['GetEngine', 'InputEngine'])]
    private LexEngineType $lexEngineType;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return int
     */
    public function getRowOrder(): int
    {
        return $this->rowOrder;
    }

    /**
     * @param int $rowOrder
     */
    public function setRowOrder(int $rowOrder): void
    {
        $this->rowOrder = $rowOrder;
    }

    /**
     * @return Layer
     */
    public function getLayer(): Layer
    {
        return $this->layer;
    }

    /**
     * @param Layer $layer
     */
    public function setLayer(Layer $layer): void
    {
        $this->layer = $layer;
    }

    /**
     * @return string
     */
    public function getFieldId(): string
    {
        return $this->fieldId;
    }

    /**
     * @param string $fieldId
     */
    public function setFieldId(string $fieldId): void
    {
        $this->fieldId = $fieldId;
    }

    /**
     * @return string
     */
    public function getFieldName(): string
    {
        return $this->fieldName;
    }

    /**
     * @param string $fieldName
     */
    public function setFieldName(string $fieldName): void
    {
        $this->fieldName = $fieldName;
    }

    /**
     * @return Engine|null
     */
    public function getJoinId(): ?Engine
    {
        return $this->joinId;
    }

    /**
     * @param Engine|null $joinId
     */
    public function setJoinId(?Engine $joinId): void
    {
        $this->joinId = $joinId;
    }

    /**
     * @return string|null
     */
    public function getJoinField(): ?string
    {
        return $this->joinField;
    }

    /**
     * @param string|null $joinField
     */
    public function setJoinField(?string $joinField): void
    {
        $this->joinField = $joinField;
    }

    /**
     * @return LexEngineType
     */
    public function getLexEngineType(): LexEngineType
    {
        return $this->lexEngineType;
    }

    /**
     * @param LexEngineType $lexEngineType
     */
    public function setLexEngineType(LexEngineType $lexEngineType): void
    {
        $this->lexEngineType = $lexEngineType;
    }


    public function getEngines(): Collection
    {
        return $this->engines;
    }

    #[Groups(['GetEngine'])]
    public function getCanDelete(): bool
    {
        return (count($this->getEngines()) == 0);
    }

}
