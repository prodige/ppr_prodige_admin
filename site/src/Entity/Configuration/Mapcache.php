<?php

declare(strict_types=1);

namespace App\Entity\Configuration;

use Alk\Common\CasBundle\Security\Authentication\Provider\CasProvider;
use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Controller\Configuration\Mapcache\AddGridMapcache;
use App\Controller\Configuration\Mapcache\AddTileMapcache;
use App\Controller\Configuration\Mapcache\DeleteGridMapcache;
use App\Controller\Configuration\Mapcache\DeleteTileMapcache;
use App\Controller\Configuration\Mapcache\EditGridMapcache;
use App\Controller\Configuration\Mapcache\EditTileMapcache;
use App\Service\AdminCartoService;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Exception;
use Doctrine\Persistence\ManagerRegistry;
use Prodige\ProdigeBundle\Services\CurlUtilities;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\Service\Attribute\Required;

/**
 * Class to manage mapcache.xml.
 */
#[ApiResource(
    collectionOperations: [
        'get_tiles' => [
            'method' => 'GET',
            'path' => '/mapcache/tiles',
            'pagination_enabled' => false,
            'openapi_context' => [
                'summary' => 'Get all tiles',
                'description' => 'Get all tiles',
            ],
        ],
        'get_grids' => [
            'method' => 'GET',
            'path' => '/mapcache/grids',
            'pagination_enabled' => false,
            'openapi_context' => [
                'summary' => 'Get all grids',
                'description' => 'Get all grids',
            ],
        ],
        'post_grid' => [
            'method' => 'POST',
            'path' => '/mapcache/grids',
            'controller' => AddGridMapcache::class,
            'denormalization_context' => ['groups' => 'InputGridMapcache'],
            'read' => false,
            '_api_receive' => false,
            'openapi_context' => [
                'summary' => 'Add a grid',
                'description' => 'Add a grid',
            ],
        ],
        'post_tile' => [
            'method' => 'POST',
            'path' => '/mapcache/tiles',
            'controller' => AddTileMapcache::class,
            'denormalization_context' => ['groups' => 'InputTileMapcache'],
            'read' => false,
            '_api_receive' => false,
            'openapi_context' => [
                'summary' => 'Add a tile',
                'description' => 'Add a tile',
            ],
        ],
    ],
    itemOperations: [
        'get_tile' => [
            'method' => 'GET',
            'path' => '/mapcache/tiles/{name}',
            'openapi_context' => [
                'summary' => 'Get a tile by name',
                'description' => 'Get a tile by name',
            ],
        ],
        'get_grid' => [
            'method' => 'GET',
            'path' => '/mapcache/grids/{name}',
            'openapi_context' => [
                'summary' => 'Get a grid by name',
                'description' => 'Get a grid by name',
            ],
        ],
        'get_grid_tiles' => [
            'method' => 'GET',
            'path' => '/mapcache/grids/{name}/tiles',
            'openapi_context' => [
                'summary' => 'Get tiles by grid name',
                'description' => 'Get tiles by grid name',
            ],
        ],
        'put_grid' => [
            'method' => 'PUT',
            'path' => '/mapcache/grids/{name}',
            'controller' => EditGridMapcache::class,
            'denormalization_context' => ['groups' => 'InputEditGridMapcache'],
            'read' => false,
            '_api_receive' => false,
            'openapi_context' => [
                'summary' => 'Edit a grid',
                'description' => 'Edit a grid',
            ],
        ],
        'put_tile' => [
            'method' => 'PUT',
            'path' => '/mapcache/tiles/{name}',
            'controller' => EditTileMapcache::class,
            'denormalization_context' => ['groups' => 'InputEditTileMapcache'],
            'read' => false,
            '_api_receive' => false,
            'openapi_context' => [
                'summary' => 'Edit a tile',
                'description' => 'Edit a tile',
            ],
        ],
        'delete_tile' => [
            'method' => 'DELETE',
            'path' => '/mapcache/tiles/{name}',
            'controller' => DeleteTileMapcache::class,
            'read' => false,
            '_api_receive' => false,
            'openapi_context' => [
                'summary' => 'Delete a tile',
                'description' => 'Delete a tile',
            ],
        ],
        'delete_grid' => [
            'method' => 'DELETE',
            'path' => '/mapcache/grids/{name}',
            'controller' => DeleteGridMapcache::class,
            'read' => false,
            '_api_receive' => false,
            'openapi_context' => [
                'summary' => 'Delete a grid',
                'description' => 'Delete a grid',
            ],
        ],
    ],
    normalizationContext: ['groups' => ['OutputMapcache']]
)]
class Mapcache
{
    #[ApiProperty(
        attributes: [
            'openapi_context' => [
                'type' => 'array',
                'items' => [
                    'type' => 'object',
                    'properties' => [
                        'metadata' => [
                            'type' => 'object',
                            'properties' => [
                                'title' => ['type' => 'string', 'example' => 'Cantons de France (Geofla)'],
                                'generation' => [
                                    'type' => 'object',
                                    'properties' => [
                                        'mode' => ['type' => 'string', 'example' => 'direct'],
                                        'time' => ['type' => 'string', 'example' => 'time'],
                                        'status' => ['type' => 'string', 'example' => 'todo'],
                                    ],
                                ],
                            ],
                        ],
                        'format' => ['type' => 'string', 'example' => 'JPEG'],
                        'grid' => ['type' => 'string', 'example' => 'aGrid'],
                        'layer' => [
                            'type' => 'object',
                            'properties' => [
                                'wmts' => ['type' => 'string', 'example' => '/layers/wmts_.map'],
                                'msname' => ['type' => 'string', 'example' => 'aName'],
                                'format' => ['type' => 'string', 'example' => 'image/jpeg'],
                            ],
                        ],
                    ],
                ],
            ],
        ]
    )]
    #[Groups(['OutputMapcache', 'InputEditTileMapcache', 'InputTileMapcache'])]
    private array $tiles = [];

    #[ApiProperty(
        attributes: [
            'openapi_context' => [
                'type' => 'array',
                'items' => [
                    'type' => 'object',
                    'properties' => [
                        'metadata' => ['type' => 'string', 'example' => 'Cantons de France (Geofla) grid'],
                        'extent' => [
                            'type' => 'array',
                            'items' => ['type' => 'integer'],
                            'example' => [111111, 6000000, 1000000, 7123092],
                        ],
                        'srs' => ['type' => 'string', 'example' => 'EPSG:2154'],
                        'units' => ['type' => 'string', 'example' => 'm'],
                        'size' => ['type' => 'string', 'example' => '256 256'],
                        'resolutions' => [
                            'type' => 'array',
                            'items' => ['type' => 'float'],
                            'example' => [
                                78271.516843749,
                                39135.758554166176,
                                19567.879277083088,
                                9783.939506249877,
                                4891.969885416605,
                                2445.984810416636,
                                1222.9925374999846,
                            ],
                        ],
                    ],
                ],
            ],
        ]
    )]
    #[Groups(['OutputMapcache', 'InputEditGridMapcache', 'InputGridMapcache'])]
    private array $grids = [];

    private string $path;

    private Crawler $crawler;

    #[ApiProperty(identifier: true,
        attributes: [
            'openapi_context' => ['example' => 'gridName'],
        ]
    )]
    #[Groups(['InputTileMapcache'])]
    private string $name = '';

    private AdminCartoService $adminCartoService;

    #[Required]
    public function setAdminCartoService(AdminCartoService $adminCartoService): void
    {
        $this->adminCartoService = $adminCartoService;
    }

    public function __construct()
    {
        $this->path = $_ENV['ROOT_DATA'].$_ENV['MAPCACHE_PATH'];
    }

    public function getPath(): string
    {
        return $this->path;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function generateTiles(): array
    {
        $this->crawler = new Crawler();
        $this->crawler->addXmlContent(file_get_contents($this->path));
        $tiles = $this->crawler->filterXPath('//tileset')->each(
        /**
         * @throws Exception
         * @throws TransportExceptionInterface
         */ function (Crawler $nodes, $i) {
            $value = $nodes->children()->each(function (Crawler $n, $i) {
                return [$n->nodeName() => $n->text()];
            });
            $tmp = [];
            foreach ($value as $key => $val) {
                $tmp = array_merge($tmp, $val);
            }
            $tmp['mode'] = $nodes->filterXPath('//metadata/generation')->extract(['mode'])[0];
            $tmp['time'] = $nodes->filterXPath('//metadata/generation')->extract(['time'])[0];
            $tmp['status'] = $nodes->filterXPath('//metadata/generation')->extract(['status'])[0];
            $tmp['name'] = $nodes->attr('name');
            $searchSource = $this->crawler->filterXPath('//source[@name="'.$tmp['source'].'"]');
            $tmp['layer'] = $searchSource->filterXPath('//getmap/params/LAYERS')->text();
            $tmp['mapFile'] = $searchSource->filterXPath('//getmap/params/MAP')->text();
            $tmp['mapFileExists'] = file_exists($tmp['mapFile']);
            $tmp['uuid'] = $nodes->filterXPath('//metadata/uuid')->text();
            $tmp['mapUuid'] = $nodes->filterXPath('//mapUuid')->text();

            if ($tmp['mapFileExists']) {
                $tmp['mapFilePath'] = $_ENV['PRODIGE_URL_ADMINCARTO'].'/edit_map/'.$tmp['mapUuid'];
            } else {
                $tmp['mapFilePath'] = $this->adminCartoService->openMapCarmen($tmp);
                $tmp['mapFileExists'] = file_exists($tmp['mapFile']);
            }

            return [$nodes->attr('name') => $tmp];
        });

        $tmp = [];
        foreach ($tiles as $t) {
            $tmp = array_merge($tmp, $t);
        }
        $this->tiles = $tmp;

        return $tmp;
    }

    public function getTileByName(string $name): array
    {
        $this->tiles = $this->generateTiles()[$name] ?? [];

        return $this->tiles;
    }

    public function getTiles(): array
    {
        return $this->tiles;
    }

    public function getGrids(): array
    {
        return $this->grids;
    }

    public function getGridByName(string $name): array
    {
        $this->grids = $this->generateGrids()[$name] ?? [];

        return $this->grids;
    }

    public function generateGrids(): array
    {
        $this->crawler = new Crawler();
        $this->crawler->addXmlContent(file_get_contents($this->path));
        $tiles = $this->crawler->filterXPath('//grid[@name]')->each(function (Crawler $nodes, $i) {
            $value = $nodes->children()->each(function (Crawler $n, $i) {
                if ('resolutions' == $n->nodeName() || 'extent' == $n->nodeName()) {
                    return [$n->nodeName() => explode(' ', $n->text())];
                }

                return [$n->nodeName() => $n->text()];
            });
            $tmp = [];
            foreach ($value as $key => $val) {
                $tmp = array_merge($tmp, $val);
            }

            return [$nodes->attr('name') => $tmp];
        });

        $tmp = [];
        foreach ($tiles as $t) {
            $tmp = array_merge($tmp, $t);
        }
        $this->grids = $tmp;

        return $tmp;
    }

    public function removeGrid(string $name): array
    {
        $xml = new \SimpleXMLElement(file_get_contents($this->path));
        $search = $xml->xpath('//grid[@name="'.$name.'"]');

        if (empty($search)) {
            return ['code' => false, 'message' => 'Grid with name : '.$name.' not found'];
        }
        $this->name = $name;

        // Delete
        unset($search[0][0]);
        file_put_contents($this->path, $this->formatXML($xml));

        return ['code' => true, 'message' => 'Grid with name : '.$name.' removed'];
    }

    public function editGrid(string $name, array $params): array
    {
        $this->name = $name;
        if (!$this->removeGrid($name)['code']) {
            return ['code' => false, 'message' => 'Grid with name : '.$name.' not deleted'];
        }

        // Create
        return $this->addNewGrid($params, $name);
    }

    public function addNewGrid(array $params, string $name = null): array
    {
        $xml = new \SimpleXMLElement(file_get_contents($this->path));

        $ids = [];
        $grids = $xml->xpath("//mapcache/grid");
        foreach ($grids as $gridid) {
            $ids[] = (string)$gridid->attributes()->name;
        }
        if ($name == '') {
            $cpt = (count($grids) + 1);
            $id = "grid_".$cpt++;
            while (in_array($id, $ids)) {
                $id = "grid_".$cpt++;
            }
            $this->name = $id;
        } else {
            $id = $name;
        }

        $newXml = new \SimpleXMLElement('<grid name="'.$id.'"></grid>');

        if (!empty($xml->xpath('//grid[@name="'.$id.'"]'))) {
            return ['code' => false, 'message' => 'Grid with name : '.$id.' is already used'];
        }

        foreach ($params as $key => $param) {
            $value = is_array($param) ? implode(' ', $param) : $param;
            if ('metadata' == $key) {
                $metadata = $newXml->addChild('metadata');
                $metadata->addChild('title', $value);
            } else {
                $newXml->addChild($key, $value);
            }
        }
        $this->simplexml_insert_after($newXml, current($xml->xpath('//lock_dir[1]')));
        $xmlFormatted = $this->formatXML($xml);

        // TODO : Try to validate xml format
        file_put_contents($this->path, $xmlFormatted);

        return ['code' => true, 'message' => $xmlFormatted];
    }

    public function removeTile(string $name): array
    {
        $xml = new \SimpleXMLElement(file_get_contents($this->path));
        $search = $xml->xpath('//tileset[@name="'.$name.'"]');
        $searchSource = $xml->xpath('//source[@name="source_'.$name.'"]');

        if (empty($search)) {
            return ['code' => false, 'message' => 'Tile with name : '.$name.' not found'];
        }
        $this->name = $name;

        // Delete
        unset($search[0][0]);
        unset($searchSource[0][0]);
        file_put_contents($this->path, $this->formatXML($xml));

        return ['code' => true, 'message' => 'Tile with name : '.$name.' removed'];
    }

    public function editTile(string $name, array $params): array
    {
        $this->name = $name;
        if (!$this->removeTile($name)['code']) {
            return ['code' => false, 'message' => 'Tile with name : '.$name.' not deleted'];
        }

        // Create
        return $this->addNewTile($name, $params['tiles'][0]);
    }

    public function addNewTile(string $name, array $params): array
    {
        $xml = new \SimpleXMLElement(file_get_contents($this->path));

        $ids = [];
        $tiles = $xml->xpath("//mapcache/tileset");
        foreach ($tiles as $tileid) {
            $ids[] = (string)$tileid->attributes()->name;
        }
        $cpt = (count($tiles) + 1);
        $id = $name."_".$cpt++;
        while (in_array($id, $ids)) {
            $id = $name."_".$cpt++;
        }
        $newXml = new \SimpleXMLElement('<tileset name="'.$id.'"></tileset>');

        if (!empty($xml->xpath('//tileset[@name="'.$id.'"]'))) {
            return ['code' => false, 'message' => 'Tile with name : '.$id.' is already used'];
        }

        if (empty($xml->xpath('//grid[@name="'.$params['grid'].'"]'))) {
            return ['code' => false, 'message' => 'Grid with name : '.$params['grid'].' not found'];
        }

        $currentGrid = $params['grid'];

        $cache = 'disk';
        $metatile = '5 5';
        $metabuffer = '10';
        $expires = '3600';
        $layerParam = $params['layer'];
        
        $initalParam = [
            'source' => 'source_'.$id,
            'cache' => $cache,
            'format' => $layerParam['format'],
            'metatile' => $metatile,
            'metabuffer' => $metabuffer,
            'expires' => $expires,
            'mapUuid' => uniqid(),
        ];

        
        unset($params['layer']);
        $initalSource = [
            'getmap' => [
                'params' => [
                    'FORMAT' => 'image/'.strtolower($layerParam['format']),
                    'LAYERS' => $layerParam['name'],
                    'TRANSPARENT' => 'true',
                    'MAP' => $layerParam['path'].'wmts_'.$id.'_'.$layerParam['uuid'].'.map',
                ],
            ],
            'http' => [
                'url' => $_ENV['PRODIGE_URL_DATACARTO'].'/cgi-bin/mapserv?',
            ],
        ];

        $params = array_merge($params, $initalParam);

        // Add tile params
        foreach ($params as $key => $param) {
            if ('metadata' == $key) {
                $metadata = $newXml->addChild('metadata');
                $metadata->addChild('title', $layerParam['name']);
                $generation = $metadata->addChild('generation');
                $generation->addAttribute('mode', $param['generation']['mode']);
                $generation->addAttribute('time', $param['generation']['time']);
                $generation->addAttribute('status', 'todo');
                $metadata->addChild('uuid', $layerParam['uuid']);
            } else {
                $value = is_array($param) ? implode(' ', $param) : $param;
                $newXml->addChild($key, $value);
            }
        }
        $this->simplexml_insert_after($newXml, current($xml->xpath('//grid[@name="'.$currentGrid.'"]')));

        // Add source
        $newXmlSource = new \SimpleXMLElement('<source name="source_'.$id.'" type="wms"></source>');
        $getMap = $newXmlSource->addChild('getmap');
        $getMapParams = $getMap->addChild('params');
        foreach ($initalSource['getmap']['params'] as $key => $value) {
            $getMapParams->addChild($key, $value);
        }
        $http = $newXmlSource->addChild('http');
        foreach ($initalSource['http'] as $key => $value) {
            $http->addChild($key, $value);
        }

        $this->simplexml_insert_after($newXmlSource, current($xml->xpath('//tileset[@name="'.$id.'"]')));
        $xmlFormatted = $this->formatXML($xml);

        // TODO : Try to validate xml format
        file_put_contents($this->path, $xmlFormatted);

        return ['code' => true, 'message' => $xmlFormatted];
    }

    public function getTilesByGrid(string $name): array
    {
        $tiles = $this->generateTiles();

        $tmpTiles = [];
        foreach ($tiles as $key => $tile) {
            if ($tile['grid'] == $name) {
                $tmpTiles[$key] = $tile;
            }
        }

        $this->tiles = $tmpTiles;

        return $this->tiles;
    }

    private function simplexml_insert_after(\SimpleXMLElement $insert, \SimpleXMLElement $target): void
    {
        $target_dom = \dom_import_simplexml($target);
        $insert_dom = $target_dom->ownerDocument->importNode(\dom_import_simplexml($insert), true);
        if ($target_dom->nextSibling) {
            $target_dom->parentNode->insertBefore($insert_dom, $target_dom->nextSibling);
        } else {
            $target_dom->parentNode->appendChild($insert_dom);
        }
        $target_dom->formatOutput = true;
    }

    private function formatXML(\SimpleXMLElement $xml): string
    {
        $dom = new \DOMDocument('1.0');
        $dom->preserveWhiteSpace = false;
        $dom->formatOutput = true;
        $dom->loadXML($xml->asXML());

        return $dom->saveXML();
    }
}
