<?php

declare(strict_types=1);

namespace App\Entity\Configuration;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Entity\Lex\LexLayerType;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Admin.Format
 */
#[ApiResource]
#[ApiFilter(SearchFilter::class, properties: [
    'lexLayerType.id' => 'exact',
])]
#[ORM\Table(name: 'format', schema: 'admin')]
#[ORM\Entity]
#[ORM\UniqueConstraint(name: 'format_unique', columns: ['name', 'code'])]
#[UniqueEntity(fields: ['name', 'code'])]
class Format
{
    #[ORM\Column(name: 'id', type: 'integer', nullable: false)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    private int $id;

    #[ORM\Column(name: 'name', type: 'string', length: 128, nullable: false)]
    private string $name;

    #[ORM\Column(name: 'code', type: 'string', length: 128, nullable: false)]
    private string $code;

    #[ORM\ManyToMany(targetEntity: Setting::class, mappedBy: 'formats')]
    #[ORM\JoinTable(name: 'settings_formats', schema: 'admin')]
    private Collection $settings;

    #[ORM\ManyToOne(targetEntity: LexLayerType::class)]
    #[ORM\JoinColumn(name: 'lex_layer_type_id', referencedColumnName: 'id', nullable: false, options: ['default' => 1])]
    private LexLayerType $lexLayerType;

    public function __construct()
    {
        $this->settings = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }

    /**
     * @param string $code
     */
    public function setCode(string $code): void
    {
        $this->code = $code;
    }

    public function addSettings(Setting $setting): self
    {
        if (!$this->settings->contains($setting)) {
            $this->settings->add($setting);
        }

        return $this;
    }

    public function removeSettings(Setting $setting): self
    {
        $this->settings->removeElement($setting);

        return $this;
    }

    public function getLexLayerType(): LexLayerType
    {
        return $this->lexLayerType;
    }

    public function setLexLayerType(LexLayerType $lexLayerType): Format
    {
        $this->lexLayerType = $lexLayerType;
        return $this;
    }


}
