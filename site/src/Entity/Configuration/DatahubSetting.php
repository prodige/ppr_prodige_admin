<?php

declare(strict_types=1);

namespace App\Entity\Configuration;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Serializer\Filter\PropertyFilter;
use App\Entity\Lex\LexSettingType;
use App\Filter\FullTextSearchFilter;
use Doctrine\ORM\Mapping as ORM;
use OpenApi\Attributes\Patch;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Admin.Setting
 */
#[ApiResource(
    itemOperations: ['get', 'patch'],
    denormalizationContext: ['groups' => ['PatchDatahubSetting']],
    normalizationContext: ['groups' => ['GetDatahubSetting']]
)]
#[ApiFilter(SearchFilter::class, properties: [
    'name' => 'exact'
])]
#[ORM\Table(name: 'DatahubSetting', schema: 'admin')]
#[ORM\Entity]
#[UniqueEntity(fields: 'name')]
class DatahubSetting
{
    #[ORM\Column(name: 'id', type: 'integer', nullable: false)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    #[Groups(['GetDatahubSetting'])]
    private int $id;

    #[ORM\Column(name: 'name', type: 'string', length: 128, unique: true, nullable: false)]
    #[Groups(['GetDatahubSetting'])]
    private string $name;

    #[ORM\Column(name: 'title', type: 'string', nullable: false)]
    #[Groups(['GetDatahubSetting'])]
    private string $title;

    #[ORM\Column(name: 'value', type: 'string', nullable: false)]
    #[Groups(['GetDatahubSetting', 'PatchDatahubSetting'])]
    private string $value;

    #[ORM\Column(name: 'setting_order', type: 'integer', nullable: false, options: ['default' => 0])]
    #[Groups(['GetDatahubSetting', 'PatchDatahubSetting'])]
    private int $settingOrder;

    #[ORM\ManyToOne(targetEntity: LexSettingType::class)]
    #[ORM\JoinColumn(name: 'lex_setting_type_id', referencedColumnName: 'id', nullable: false)]
    #[Groups(['GetDatahubSetting'])]
    private LexSettingType $lexSettingType;

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function getName(): string
    {
        return strip_tags($this->name);
    }

    public function setName(string $name): void
    {
        $this->name = strip_tags($name);
    }

    public function getTitle(): string
    {
        return strip_tags($this->title);
    }

    public function setTitle(string $title): void
    {
        $this->title = strip_tags($title);
    }

    public function getValue(): string
    {
        return strip_tags($this->value);
    }

    public function setValue(string $value): void
    {
        $this->value = strip_tags($value);
    }

    public function getSettingOrder(): int
    {
        return $this->settingOrder;
    }

    public function setSettingOrder(int $settingOrder): void
    {
        $this->settingOrder = $settingOrder;
    }

    public function getLexSettingType(): LexSettingType
    {
        return $this->lexSettingType;
    }

    public function setLexSettingType(LexSettingType $lexSettingType): void
    {
        $this->lexSettingType = $lexSettingType;
    }

}