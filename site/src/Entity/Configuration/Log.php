<?php

namespace App\Entity\Configuration;


use ApiPlatform\Core\Annotation\ApiResource;
use App\Controller\Configuration\LogController;
use App\Controller\Configuration\LogFileController;


#[ApiResource(
    collectionOperations: [
        'get_logs_collection' => [
            'methods' => ['GET'],
            'path' => '/logs',
            'controller' => LogController::class,
        ]
    ],
    itemOperations: [
        'getLogFile' => [
            'method' => 'GET',
            'path' => '/logs/{id}/file',
            'controller' => LogFileController::class,
            'openapi_context' => [
                'summary' => 'Get log',
                'description' => 'Get log'
            ],
            'read' => false
        ]
    ]
)]
class Log
{
    private int $id;

    private string $name;

    private ?string $file;

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): Log
    {
        $this->id = $id;
        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): Log
    {
        $this->name = $name;
        return $this;
    }

    public function getFile(): ?string
    {
        return $this->file;
    }

    public function setFile(?string $file): Log
    {
        $this->file = $file;
        return $this;
    }

}