<?php

declare(strict_types=1);

namespace App\Entity\Configuration;

use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Controller\Configuration\Queue\QueueController;
use DateTime;

/**
 * Queue representation
 */
#[ApiResource(
    collectionOperations: [
        'get_files' => [
            'method' => 'GET',
            'path' => '/queues/files',
            'pagination_enabled' => false,
            'openapi_context' => [
                'summary' => 'Get all files in queue',
                'description' => 'Get all files in queue',
            ],
        ]
    ],
    itemOperations: [
        'get' => [
            'method' => 'GET',
            'path' => '/queues/files/{id}',
        ],
        'remove' => [
            'method' => 'DELETE',
            'path' => '/queues/files/{id}',
            'openapi_context' => [
                'summary' => 'Remove file from queue',
                'description' => 'Remove file from queue',
            ],
            'controller' => QueueController::class,
            'read' => false,
            '_api_receive' => false,
        ],
        'flush' => [
            'method' => 'DELETE',
            'path' => '/queues/files',
            'openapi_context' => [
                'summary' => 'Remove all file from queue',
                'description' => 'Remove all file from queue',
            ],
            'controller' => QueueController::class,
            'read' => false,
            '_api_receive' => false,
        ],
    ]
)]
class Queuefile
{
    #[ApiProperty(identifier: true)]
    private int $id;

    private ?string $file;

    private Datetime|string $date;

    private string $caller;

    private string $format;

    private string $projection;

    private string $tables;

    private string $metadata;

    private string $type;

    private ?string $spatialExtraction;

    private ?string $extractionType;

    private ?string $areaExtraction;

    private ?string $empriseExtraction;

    private ?string $area;

    private ?string $ref;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Queuefile
     */
    public function setId(int $id): Queuefile
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getFile(): ?string
    {
        return $this->file;
    }

    /**
     * @param string|null $file
     * @return Queuefile
     */
    public function setFile(?string $file): Queuefile
    {
        $this->file = $file;
        return $this;
    }

    /**
     * @return DateTime|string
     */
    public function getDate(): DateTime|string
    {
        return $this->date;
    }

    /**
     * @param DateTime|string $date
     * @return Queuefile
     */
    public function setDate(DateTime|string $date): Queuefile
    {
        $this->date = $date;
        return $this;
    }



    /**
     * @return string
     */
    public function getCaller(): string
    {
        return $this->caller;
    }

    /**
     * @param string $caller
     * @return Queuefile
     */
    public function setCaller(string $caller): Queuefile
    {
        $this->caller = $caller;
        return $this;
    }


    /**
     * @return string
     */
    public function getFormat(): string
    {
        return $this->format;
    }

    /**
     * @param string $format
     * @return Queuefile
     */
    public function setFormat(string $format): Queuefile
    {
        $this->format = $format;
        return $this;
    }

    /**
     * @return string
     */
    public function getProjection(): string
    {
        return $this->projection;
    }

    /**
     * @param string $projection
     * @return Queuefile
     */
    public function setProjection(string $projection): Queuefile
    {
        $this->projection = $projection;
        return $this;
    }

    /**
     * @return string
     */
    public function getTables(): string
    {
        return $this->tables;
    }

    /**
     * @param string $tables
     * @return Queuefile
     */
    public function setTables(string $tables): Queuefile
    {
        $this->tables = $tables;
        return $this;
    }

    /**
     * @return string
     */
    public function getMetadata(): string
    {
        return $this->metadata;
    }

    /**
     * @param string $metadata
     * @return Queuefile
     */
    public function setMetadata(string $metadata): Queuefile
    {
        $this->metadata = $metadata;
        return $this;
    }



    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return Queuefile
     */
    public function setType(string $type): Queuefile
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getSpatialExtraction(): ?string
    {
        return $this->spatialExtraction;
    }

    /**
     * @param string|null $spatialExtraction
     * @return Queuefile
     */
    public function setSpatialExtraction(?string $spatialExtraction): Queuefile
    {
        $this->spatialExtraction = $spatialExtraction;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getExtractionType(): ?string
    {
        return $this->extractionType;
    }

    /**
     * @param string|null $extractionType
     * @return Queuefile
     */
    public function setExtractionType(?string $extractionType): Queuefile
    {
        $this->extractionType = $extractionType;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getAreaExtraction(): ?string
    {
        return $this->areaExtraction;
    }

    /**
     * @param string|null $areaExtraction
     * @return Queuefile
     */
    public function setAreaExtraction(?string $areaExtraction): Queuefile
    {
        $this->areaExtraction = $areaExtraction;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getEmpriseExtraction(): ?string
    {
        return $this->empriseExtraction;
    }

    /**
     * @param string|null $empriseExtraction
     * @return Queuefile
     */
    public function setEmpriseExtraction(?string $empriseExtraction): Queuefile
    {
        $this->empriseExtraction = $empriseExtraction;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getArea(): ?string
    {
        return $this->area;
    }

    /**
     * @param string|null $area
     * @return Queuefile
     */
    public function setArea(?string $area): Queuefile
    {
        $this->area = $area;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getRef(): ?string
    {
        return $this->ref;
    }

    /**
     * @param string|null $ref
     * @return Queuefile
     */
    public function setRef(?string $ref): Queuefile
    {
        $this->ref = $ref;
        return $this;
    }


}