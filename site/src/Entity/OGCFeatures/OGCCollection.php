<?php

declare(strict_types=1);

namespace App\Entity\OGCFeatures;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Controller\Administration\Resource\LayerByUuidController;
use App\Controller\OpenApiController;
use App\Controller\Security\SecurityController;
use App\DataProvider\OGCCollectionProvider;
use App\Entity\Administration\Directory\Profile\ProfilePrivilegeObject;
use App\Entity\Administration\Organisation\Subdomain;
use App\Entity\Administration\Resource\MetadataSheet;
use App\Entity\Trait\TimestampsTrait;
use App\Repository\OGCCollectionRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JetBrains\PhpStorm\Pure;
use phpDocumentor\Reflection\Types\Boolean;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Nelmio\Annotations as Nelmio;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * OGCCollection
 */
#[ApiResource(
    collectionOperations: [
        'get_items_by_collection' => [
            'method' => 'get',
            'name' => 'items',
            'path' => '/collections/{collectionId}/items.{_format}',
            'openapi_context' => [
                'tags' => ['Items'],
                'summary' => 'A list of all items in collection.',
                'description' => 'A list of all items in collection.',
                'parameters' => [
                    [
                        'name' => 'collectionId',
                        'in' => 'path',
                        'required' => true,
                        'schema' => ['type' => 'string'],
                        'description' => 'The collection Id'
                    ],
                    [
                        'name' => 'filter',
                        'in' => 'query',
                        'required' => false,
                        'schema' => ['type' => 'string'],
                        'description' => 'Filters for query'
                    ],
                    [
                        'name' => 'bbox',
                        'in' => 'query',
                        'required' => false,
                        'schema' => ['type' => 'string'],
                        'description' => 'BBOX filter for query'
                    ],
                    [
                        'name' => 'bbox_crs',
                        'in' => 'query',
                        'required' => false,
                        'schema' => ['type' => 'string'],
                        'description' => 'BBOX geoformat for query'
                    ],
                    [
                        'name' => 'crs',
                        'in' => 'query',
                        'required' => false,
                        'schema' => ['type' => 'string'],
                        'description' => 'return geoformat'
                    ]
                ]
            ],
            'normalization_context' => ['groups' => 'GetItems'],
            'pagination_enabled' => true,
            'defaults' => [
                '_api_openapi_doc' => false,
                '_api_ressource_template' => 'items'
            ],
        ],
        'get_item_by_collection' => [
            'method' => 'get',
            'name' => 'item',
            'path' => '/collections/{collectionId}/items/{featureId}.{_format}',
            'openapi_context' => [
                'tags' => ['Items'],
                'summary' => 'A collection item.',
                'description' => 'A collection item.',
                'parameters' => [
                    [
                        'name' => 'collectionId',
                        'in' => 'path',
                        'required' => true,
                        'schema' => ['type' => 'string'],
                        'description' => 'The collection Id'
                    ],
                    [
                        'name' => 'featureId',
                        'in' => 'path',
                        'required' => true,
                        'schema' => ['type' => 'integer'],
                        'description' => 'The feature Id'
                    ]
                ]
            ],
            'normalization_context' => ['groups' => 'GetItem'],
            'pagination_enabled' => false,
            'defaults' => [
                '_api_openapi_doc' => false,
                '_api_ressource_template' => 'item'
            ],
        ],
        'landingpage' => [
            'method' => 'get',
            'name' => 'landingpage',
            'path' => '/landing.{_format}',
            'uriTemplate' => '/landing.{_format}',
            'openapi_context' => [
                'tags' => ['Landing Page'],
                'summary' => 'The landing page provides links to the API definition, the conformance statements and to the feature collections in this dataset.',
                'description' => 'The landing page provides links to the API definition, the conformance statements and to the feature collections in this dataset.',
            ],
            'normalization_context' => ['groups' => 'GetLandingPage'],
            'pagination_enabled' => false,
            'defaults' => [
                '_api_openapi_doc' => false,
                '_api_ressource_template' => 'landing'
            ]
        ],
        'conformance' => [
            'method' => 'get',
            'name' => 'conformance',
            'path' => '/conformance.{_format}',
            'uriTemplate' => '/conformance.{_format}',
            'openapi_context' => [
                'tags' => ['Conformance'],
                'summary' => 'A list of all conformance classes specified in a standard that the server conforms to.',
                'description' => 'A list of all conformance classes specified in a standard that the server conforms to.',
            ],
            'normalization_context' => ['groups' => 'GetConformance'],
            'pagination_enabled' => false,
            'defaults' => [
                '_api_openapi_doc' => false,
                '_api_ressource_template' => 'conformance'
            ]
        ],
        'get' =>
            [
                'defaults' => [
                    '_api_openapi_doc' => false,
                    '_api_ressource_template' => 'collections'
                ],
                'openapi_context' => [
                    'tags' => ['Collections'],
                    'summary' => 'Retrieve all collections',
                    'description' => 'list all platform collections',
                ],
            ]
    ],
    itemOperations: [
        'get' => [
            'defaults' => [
                '_api_openapi_doc' => false,
                '_api_ressource_template' => 'collection'
            ],
            'openapi_context' => [
                'tags' => ['Collections'],
                'summary' => 'Retrieves a collection',
                'description' => 'Retrieves a collection',
            ]
        ]
    ],
    shortName: "collection",
    formats: ['json', 'html'],
    normalizationContext: ['groups' => ['GetCollection', 'GetCollectionSingle', 'GetItems']],
    paginationEnabled: false,
    routePrefix: "ogc-features"
)]
#[ORM\Table(name: 'layer', schema: 'admin')]
#[ORM\Entity(repositoryClass: OGCCollectionRepository::class)]
#[ORM\HasLifecycleCallbacks]
class OGCCollection
{

    /**
     * @var MetadataSheet
     */
    #[ORM\ManyToOne(targetEntity: MetadataSheet::class, inversedBy: 'OGCCollection')]
    #[ORM\JoinColumn(name: 'metadata_sheet_id', referencedColumnName: 'id')]
    #[Assert\NotNull]
    private MetadataSheet $sheetMetadata;

    /**
     * @var string
     */
    #[Groups(['GetLandingPage'])]
    #[Assert\Type(type: 'string')]
    #[Assert\NotNull]
    #[Assert\NotBlank(groups: ['GetLandingPage'])]
    private string $apiTitle;

    /**
     * @var string
     */
    #[Groups(['GetLandingPage'])]
    #[Assert\Type(type: 'string')]
    #[Assert\NotNull]
    #[Assert\NotBlank(groups: ['GetLandingPage'])]
    private string $apiDescription;

    /**
     * @var string
     */
    #[ORM\Column(name: 'storage_path', type: 'string', length: 1024, nullable: true)]
    #[Groups(['GetCollection','GetCollectionSingle'])]
    #[ORM\Id]
    #[ApiProperty(identifier: true)]
    private string $id;

    /**
     * @var string
     */
    #[ORM\Column(name: 'name', type: 'string', nullable: false)]
    #[Groups(['GetCollection','GetCollectionSingle'])]
    private string $title;

    /**
     * @var string
     */
    #[Groups(['GetCollection','GetCollectionSingle'])]
    private array $crs;

    /**
     * @var string
     */
    #[Groups(['GetCollection','GetCollectionSingle'])]
    private string $itemType;

    /**
     * @var array
     */
    #[Groups(['GetCollection','GetCollectionSingle'])]
    private array $extent;

    /**
     * @var array
     */
    #[Groups(['GetCollection','GetCollectionSingle'])]
    private array $links;

    /**
     * @var string
     */
    #[ORM\Column(name: 'description', type: 'string', nullable: false)]
    #[Groups(['GetCollection','GetCollectionSingle'])]
    private string $description;

    /**
     * @var bool
     */
    #[ORM\Column(name: 'api_layer', type: 'boolean', nullable: false)]
    private bool $api_layer;



    public function __construct()
    {
    }

    /**
     * Get the value of title
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set the value of title
     *
     * @return  self
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get the value of apiTitle
     */
    public function getApiTitle()
    {
        return $this->apiTitle;
    }

    /**
     * Get the value of apiDescription
     */
    public function getApiDescription()
    {
        return $this->apiDescription;
    }

    /**
     * Get the value of id
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Get the value of description
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Get the value of crs
     */
    public function getCrs()
    {
        return $this->crs;
    }

    /**
     * Set the value of crs
     *
     * @return  self
     */
    public function setCrs($crs)
    {
        $this->crs = $crs;

        return $this;
    }

    /**
     * Get the value of extent
     */
    public function getExtent()
    {
        return $this->extent;
    }

    /**
     * Set the value of extent
     *
     * @return  self
     */
    public function setExtent($extent)
    {
        $this->extent = $extent;

        return $this;
    }

    /**
     * Get the value of itemType
     */
    public function getItemType()
    {
        return $this->itemType;
    }

    /**
     * Set the value of itemType
     *
     * @return  self
     */
    public function setItemType($itemType)
    {
        $this->itemType = $itemType;

        return $this;
    }

    /**
     * Get the value of links
     */
    public function getLinks()
    {
        return $this->links;
    }

    /**
     * Set the value of links
     *
     * @return  self
     */
    public function setLinks($links)
    {
        $this->links = $links;

        return $this;
    }

    /**
     * Get the value of api_layer
     */ 
    public function getApi_layer()
    {
        return $this->api_layer;
    }

    /**
     * Get the value of sheetMetadata
     */ 
    public function getSheetMetadata()
    {
        return $this->sheetMetadata;
    }
}