<?php

declare(strict_types=1);

namespace App\Entity\Administration\Directory\Profile;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Entity\Administration\Directory\User\User;
use App\Entity\Trait\TimestampsTrait;
use App\Filter\FullTextSearchFilter;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Admin.ProfileUser
 */
#[ApiResource(
    collectionOperations: ['get', 'post'],
    itemOperations: ['get', 'delete'],
    normalizationContext: ['groups' => ['GetUserProfile']]
)]
#[ApiFilter(SearchFilter::class, properties: ['user' => 'exact','user.name' => 'ipartial','privilege' => 'ipartial'])]
#[ApiFilter(FullTextSearchFilter::class, properties: ['privilege' => 'ipartial','user.name' => 'ipartial'])]
#[ORM\Table(name: 'profile_user', schema: 'admin')]
#[ORM\Entity]
#[ORM\HasLifecycleCallbacks]
class ProfileUser
{
    use TimestampsTrait;

    /**
     * @var int
     */
    #[ORM\Column(name: 'is_principal', type: 'integer', nullable: false)]
    #[Groups(['GetUserRespire'])]
    private int $isPrincipal = 0;

    /**
     * @var Profile
     */
    #[ORM\Id]
    #[ORM\ManyToOne(targetEntity: Profile::class, inversedBy: 'users')]
    #[ORM\JoinColumn(name: 'profile_id', referencedColumnName: 'id')]
    #[Groups(['GetUserProfile', 'GetUser', 'GetUserRespire'])]
    private Profile $profile;

    /**
     * @var User
     */
    #[ORM\Id]
    #[ORM\ManyToOne(targetEntity: User::class, inversedBy: 'profiles')]
    #[ORM\JoinColumn(name: 'user_id', referencedColumnName: 'id')]
    #[Groups(['GetUserProfile'])]
    private User $user;

    /**
     * @var string|null
     */
    #[ORM\Column(name: 'privilege', type: 'string', length: 56, nullable: true)]
    #[Groups(['GetUserProfile'])]
    #[Assert\Type(type: 'string'|null)]
    private ?string $privilege = 'NULL';

    public function getIsPrincipal(): ?int
    {
        return $this->isPrincipal;
    }

    public function setIsPrincipal(int $isPrincipal): self
    {
        $this->isPrincipal = $isPrincipal;

        return $this;
    }

    public function getProfile(): ?Profile
    {
        return $this->profile;
    }

    public function setProfile(?Profile $profile): self
    {
        $this->profile = $profile;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }
    public function getPrivilege(): ?string
    {
        return $this->privilege;
    }

    /**
     * @param string|null $privilege
     * @return ProfileUser
     */
    public function setPrivilege(?string $privilege): ProfileUser
    {
        $this->privilege = $privilege;
        return $this;
    }

}
