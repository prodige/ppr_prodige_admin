<?php

declare(strict_types=1);

namespace App\Entity\Administration\Directory\Profile;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\DateFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Serializer\Filter\PropertyFilter;
use App\Controller\Administration\Directory\GetProfileController;
use App\Entity\Administration\Directory\Skill;
use App\Entity\Administration\Organisation\Domain;
use App\Entity\Administration\Organisation\Subdomain;
use App\Entity\Configuration\Help;
use App\Entity\Lex\LexPrivilege;
use App\Entity\Trait\TimestampsTrait;
use App\Filter\FullTextSearchFilter;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Admin.Profile
 */
#[ApiResource(
    itemOperations: [
        'get',
        'patch',
        'delete',
        'getPrivilegeByProfile' => [
            'method' => 'GET',
            'path' => '/profiles/{id}/right',
            'controller' => GetProfileController::class,
            'openapi_context' => [
                'summary' => "Retrieves the privileges associated with this profile",
                'description' => 'Retrieves the privileges associated with this profile',
            ],
        ],
    ],
    denormalizationContext: ['groups' => ['InputProfile']],
    normalizationContext: ['groups' => ['GetProfile']]
)]
#[ApiFilter(DateFilter::class, properties: ['createdAt', 'updatedAt'])]
#[ApiFilter(FullTextSearchFilter::class, properties: ['name' => 'ipartial', 'privileges.name' => 'ipartial'])]
#[ApiFilter(OrderFilter::class, properties: ['name'])]
#[ApiFilter(PropertyFilter::class)]
#[ORM\Table(name: 'profile', schema: 'admin')]
#[ORM\Entity]
#[ORM\HasLifecycleCallbacks]
#[UniqueEntity(fields: 'name')]
class Profile
{
    use TimestampsTrait;

    /**
     * @var int
     */
    #[ORM\Column(name: 'id', type: 'integer', nullable: false)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    #[Groups(['GetProfile', 'GetUserRespire'])]
    private int $id;

    /**
     * @var string
     */
    #[ORM\Column(name: 'name', type: 'string', length: 128, unique: true, nullable: false)]
    #[Groups([
        'InputProfile',
        'GetProfile',
        'GetProfilePrivilegeObject',
        'GetProfilePrivilegeLayer',
        'GetUser',
        'GetSubdomain',
        'GetUserProfile',
        'GetUserRespire'
    ])]
    #[Assert\Type('string')]
    #[Assert\NotNull]
    private string $name;

    /**
     * @var string|null
     */
    #[ORM\Column(name: 'description', type: 'string', length: 1024, nullable: true)]
    #[Groups(['InputProfile', 'GetProfile'])]
    #[Assert\Type('string')]
    private ?string $description;

    /**
     * @var bool
     */
    #[ORM\Column(name: 'is_default_installation', type: 'boolean', nullable: false, options: ['default' => false])]
    #[Assert\Type('bool')]
    #[Assert\NotNull]
    private bool $isDefaultInstallation = false;

    /**
     * @var string|null
     */
    #[ORM\Column(name: 'template_mail', type: 'text', nullable: true)]
    private ?string $templateMail;

    /**
     * @var Collection
     */
    #[ORM\ManyToMany(targetEntity: Skill::class, inversedBy: 'profiles')]
    #[ORM\JoinTable(name: 'profiles_skills', schema: 'admin')]
    #[Groups(['InputProfile', 'GetProfile'])]
    private Collection $skills;

    /**
     * @var Collection
     */
    #[ORM\ManyToMany(targetEntity: Domain::class, inversedBy: 'profiles')]
    #[ORM\JoinTable(name: 'profile_domain_access', schema: 'admin')]
    #[Groups(['InputProfile', 'GetProfile'])]
    private Collection $domains;

    /**
     * @var Collection
     */
    #[ORM\ManyToMany(targetEntity: Subdomain::class, inversedBy: 'profiles')]
    #[ORM\JoinTable(name: 'profile_subdomain_access', schema: 'admin')]
    #[Groups(['InputProfile'])]
    private Collection $subdomains;

    /**
     * @var Collection
     */
    #[ORM\ManyToMany(targetEntity: Help::class, inversedBy: 'profiles')]
    #[ORM\JoinTable(name: 'profiles_helps', schema: 'admin')]
    private Collection $helps;

    /**
     * @var Collection
     */
    #[ORM\ManyToMany(targetEntity: LexPrivilege::class, inversedBy: 'profiles')]
    #[ORM\JoinTable(name: 'profile_privilege', schema: 'admin')]
    #[Groups(['InputProfile', 'GetProfile'])]
    private Collection $privileges;

    /**
     * @var Collection
     */
    #[ORM\OneToMany(mappedBy: 'profile', targetEntity: ProfileUser::class, orphanRemoval: true)]
    private Collection $users;

    /**
     * @var bool
     */
    #[ORM\Column(name: 'is_synchronised', type: 'boolean', nullable: false, options: ['default' => false])]
    #[Groups(['InputProfile', 'GetProfile'])]
    private bool $isSynchronised = false;

    #[Groups(['GetProfile'])]
    private int $countUser = 0;


    public function __construct()
    {
        $this->skills = new ArrayCollection();
        $this->helps = new ArrayCollection();
        $this->privileges = new ArrayCollection();
        $this->domains = new ArrayCollection();
        $this->subdomains = new ArrayCollection();
        $this->users = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getIsDefaultInstallation(): ?bool
    {
        return $this->isDefaultInstallation;
    }

    public function setIsDefaultInstallation(bool $isDefaultInstallation): self
    {
        $this->isDefaultInstallation = $isDefaultInstallation;

        return $this;
    }

    /**
     * @return bool
     */
    public function isSynchronised(): bool
    {
        return $this->isSynchronised;
    }

    /**
     * @param bool $isSynchronised
     * @return Profile
     */
    public function setIsSynchronised(bool $isSynchronised): Profile
    {
        $this->isSynchronised = $isSynchronised;
        return $this;
    }


    public function getTemplateMail(): ?string
    {
        return $this->templateMail;
    }

    public function setTemplateMail(?string $templateMail): self
    {
        $this->templateMail = $templateMail;

        return $this;
    }

    /**
     * @return Collection
     */
    public function getSkills(): Collection
    {
        return $this->skills;
    }

    public function addSkill(Skill $skill): self
    {
        if (!$this->skills->contains($skill)) {
            $this->skills[] = $skill;
        }

        return $this;
    }

    public function removeSkill(Skill $skill): self
    {
        $this->skills->removeElement($skill);

        return $this;
    }

    /**
     * @return Collection
     */
    public function getDomains(): Collection
    {
        return $this->domains;
    }

    public function addDomain(Domain $domain): self
    {
        if (!$this->domains->contains($domain)) {
            $this->domains[] = $domain;
        }

        return $this;
    }

    public function removeDomain(Domain $domain): self
    {
        $this->domains->removeElement($domain);

        return $this;
    }

    /**
     * @return Collection
     */
    public function getSubdomains(): Collection
    {
        return $this->subdomains;
    }

    public function addSubdomain(Subdomain $subdomain): self
    {
        if (!$this->subdomains->contains($subdomain)) {
            $this->subdomains[] = $subdomain;
        }

        return $this;
    }

    public function removeSubdomain(Subdomain $subdomain): self
    {
        $this->subdomains->removeElement($subdomain);

        return $this;
    }

    /**
     * @return Collection
     */
    public function getHelps(): Collection
    {
        return $this->helps;
    }

    public function addHelp(Help $help): self
    {
        if (!$this->helps->contains($help)) {
            $this->helps[] = $help;
        }

        return $this;
    }

    public function removeHelp(Help $help): self
    {
        $this->helps->removeElement($help);

        return $this;
    }

    /**
     * @return Collection
     */
    public function getPrivileges(): Collection
    {
        return $this->privileges;
    }

    public function addPrivilege(LexPrivilege $privilege): self
    {
        if (!$this->privileges->contains($privilege)) {
            $this->privileges[] = $privilege;
        }

        return $this;
    }

    public function removePrivilege(LexPrivilege $privilege): self
    {
        $this->privileges->removeElement($privilege);

        return $this;
    }

    #[Groups(['GetProfile'])]
    public function getCanDelete(): bool
    {
        return !$this->isDefaultInstallation;
    }

    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function addUser(ProfileUser $user): self
    {
        if (!$this->users->contains($user)) {
            $this->users[] = $user;
        }
        return $this;
    }

    public function removeUser(ProfileUser $user): self
    {
        $this->users->removeElement($user);
        return $this;
    }

    public function getCountUser(): int
    {
        return count($this->users);
    }
}
