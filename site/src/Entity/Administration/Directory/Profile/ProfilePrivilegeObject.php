<?php

declare(strict_types=1);

namespace App\Entity\Administration\Directory\Profile;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Serializer\Filter\PropertyFilter;
use App\Entity\Administration\Resource\MetadataSheet;
use App\Entity\Lex\LexPrivilege;
use App\Entity\Trait\TimestampsTrait;
use App\Filter\FullTextSearchFilter;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Admin.ProfilePrivilegeObject
 */
#[ApiResource(
    collectionOperations: ['get', 'post'],
    itemOperations: ['get', 'delete'],
    denormalizationContext: ['groups' => ['PostProfilePrivilegeObject']],
    normalizationContext: ['groups' => ['GetProfilePrivilegeObject']]
)]
#[ApiFilter(FullTextSearchFilter::class, properties: [
    'profile.name' => 'ipartial',
    'lexPrivilege.name' => 'ipartial',
    'metadataSheet.layers.name' => 'ipartial',
])]
#[ApiFilter(OrderFilter::class, properties: ['profile.name', 'lexPrivilege.name'])]
#[ApiFilter(PropertyFilter::class)]
#[ORM\Table(name: 'profile_privilege_object', schema: 'admin')]
#[ORM\Entity]
#[ORM\HasLifecycleCallbacks]
class ProfilePrivilegeObject
{
    use TimestampsTrait;

    /**
     * @var int
     */
    #[ORM\Column(name: 'status', type: 'integer', nullable: false)]
    #[Groups(['PostProfilePrivilegeObject', 'GetProfilePrivilegeObject'])]
    private int $status = 1;

    /**
     * @var Profile
     */
    #[ORM\Id]
    #[ORM\ManyToOne(targetEntity: Profile::class)]
    #[ORM\JoinColumn(name: 'profile_id', referencedColumnName: 'id')]
    #[Groups(['PostProfilePrivilegeObject', 'GetProfilePrivilegeObject'])]
    #[Assert\NotNull]
    private Profile $profile;

    /**
     * @var MetadataSheet
     */
    #[ORM\Id]
    #[ORM\ManyToOne(targetEntity: MetadataSheet::class, inversedBy: 'profilePrivilegeObjects')]
    #[ORM\JoinColumn(name: 'metadata_sheet_id', referencedColumnName: 'id')]
    #[Groups(['PostProfilePrivilegeObject', 'GetProfilePrivilegeObject'])]
    #[Assert\NotNull]
    private MetadataSheet $metadataSheet;

    /**
     * @var LexPrivilege
     */
    #[ORM\Id]
    #[ORM\ManyToOne(targetEntity: LexPrivilege::class)]
    #[ORM\JoinColumn(name: 'lex_privilege_id', referencedColumnName: 'id')]
    #[Groups(['PostProfilePrivilegeObject', 'GetProfilePrivilegeObject'])]
    #[Assert\NotNull]
    private LexPrivilege $lexPrivilege;

    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function setStatus(int $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getProfile(): ?Profile
    {
        return $this->profile;
    }

    public function setProfile(?Profile $profile): self
    {
        $this->profile = $profile;

        return $this;
    }

    public function getMetadataSheet(): ?MetadataSheet
    {
        return $this->metadataSheet;
    }

    public function setMetadataSheet(?MetadataSheet $metadataSheet): self
    {
        $this->metadataSheet = $metadataSheet;

        return $this;
    }

    public function getLexPrivilege(): ?LexPrivilege
    {
        return $this->lexPrivilege;
    }

    public function setLexPrivilege(?LexPrivilege $lexPrivilege): self
    {
        $this->lexPrivilege = $lexPrivilege;

        return $this;
    }
}
