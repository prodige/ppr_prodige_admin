<?php

declare(strict_types=1);

namespace App\Entity\Administration\Directory\Profile;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Serializer\Filter\PropertyFilter;
use App\Entity\Administration\Resource\Layer;
use App\Entity\Lex\LexPrivilege;
use App\Entity\Lex\LexStructureType;
use App\Entity\Trait\TimestampsTrait;
use App\Filter\FullTextSearchFilter;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Admin.ProfilePrivilegeLayer
 */
#[ApiResource(
    collectionOperations: ['get', 'post'],
    itemOperations: ['get', 'delete'],
    denormalizationContext: ['groups' => ['PostProfilePrivilegeLayer']],
    normalizationContext: ['groups' => ['GetProfilePrivilegeLayer']]
)]
#[ApiFilter(FullTextSearchFilter::class, properties: [
    'profile.name' => 'ipartial',
    'layer.name' => 'ipartial',
    'lexStructureType.name' => 'ipartial',
    'lexPrivilege.name' => 'ipartial',
    'field' => 'ipartial',
])]
#[ApiFilter(OrderFilter::class, properties: [
    'profile.name',
    'layer.name',
    'lexStructureType.name',
    'lexPrivilege.name',
    'field',
])]
#[ApiFilter(PropertyFilter::class)]
#[ORM\Table(name: 'profile_privilege_layer', schema: 'admin')]
#[ORM\Entity]
#[ORM\HasLifecycleCallbacks]
#[UniqueEntity(fields: ['profile', 'layer', 'lexPrivilege'])]
class ProfilePrivilegeLayer
{
    use TimestampsTrait;

    /**
     * @var Profile
     */
    #[ORM\Id]
    #[ORM\ManyToOne(targetEntity: Profile::class)]
    #[ORM\JoinColumn(name: 'profile_id', referencedColumnName: 'id')]
    #[Groups(['PostProfilePrivilegeLayer', 'GetProfilePrivilegeLayer'])]
    #[Assert\NotNull]
    private Profile $profile;

    /**
     * @var Layer
     */
    #[ORM\Id]
    #[ORM\ManyToOne(targetEntity: Layer::class, inversedBy: 'profileLayerPrivilege')]
    #[ORM\JoinColumn(name: 'layer_id', referencedColumnName: 'id')]
    #[Groups(['PostProfilePrivilegeLayer', 'GetProfilePrivilegeLayer'])]
    #[Assert\NotNull]
    private Layer $layer;

    /**
     * @var LexPrivilege
     */
    #[ORM\Id]
    #[ORM\ManyToOne(targetEntity: LexPrivilege::class)]
    #[ORM\JoinColumn(name: 'lex_privilege_id', referencedColumnName: 'id')]
    #[Groups(['PostProfilePrivilegeLayer', 'GetProfilePrivilegeLayer'])]
    #[Assert\NotNull]
    private LexPrivilege $lexPrivilege;

    /**
     * @var LexStructureType
     */
    #[ORM\Id]
    #[ORM\ManyToOne(targetEntity: LexStructureType::class)]
    #[ORM\JoinColumn(name: 'lex_structure_id', referencedColumnName: 'id')]
    #[Groups(['PostProfilePrivilegeLayer', 'GetProfilePrivilegeLayer'])]
    #[Assert\NotNull]
    private LexStructureType $lexStructureType;

    /**
     * @var string
     */
    #[ORM\Column(name: 'field', type: 'string', length: 254, nullable: false)]
    #[Groups(['PostProfilePrivilegeLayer', 'GetProfilePrivilegeLayer'])]
    #[Assert\Type('string')]
    #[Assert\NotNull]
    private string $field;

    public function getField(): ?string
    {
        return $this->field;
    }

    public function setField(string $field): self
    {
        $this->field = $field;

        return $this;
    }

    public function getProfile(): ?Profile
    {
        return $this->profile;
    }

    public function setProfile(?Profile $profile): self
    {
        $this->profile = $profile;

        return $this;
    }

    public function getLayer(): ?Layer
    {
        return $this->layer;
    }

    public function setLayer(?Layer $layer): self
    {
        $this->layer = $layer;

        return $this;
    }

    public function getLexPrivilege(): ?LexPrivilege
    {
        return $this->lexPrivilege;
    }

    public function setLexPrivilege(?LexPrivilege $lexPrivilege): self
    {
        $this->lexPrivilege = $lexPrivilege;

        return $this;
    }

    public function getLexStructureType(): ?LexStructureType
    {
        return $this->lexStructureType;
    }

    public function setLexStructureType(?LexStructureType $lexStructureType): self
    {
        $this->lexStructureType = $lexStructureType;

        return $this;
    }
}
