<?php

declare(strict_types=1);

namespace App\Entity\Administration\Directory;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\DateFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\ExistsFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Serializer\Filter\PropertyFilter;
use App\Entity\Administration\Directory\User\User;
use App\Entity\Trait\TimestampsTrait;
use App\Filter\FullTextSearchFilter;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * structure.
 */
#[ApiResource(
    itemOperations: [
        'get' => [
            'groups' => ['GetStructure'],
            'skip_null_values' => false
        ],
        'patch',
        'delete'
    ], denormalizationContext: ['groups' => ['InputStructure']]
)]
#[ApiFilter(DateFilter::class, properties: ['createdAt', 'updatedAt'])]
#[ApiFilter(SearchFilter::class, properties: [
    'parent.id' => 'exact',
    'name' => 'ipartial'
])]
#[ApiFilter(ExistsFilter::class, properties: ['parent'])]
#[ApiFilter(FullTextSearchFilter::class, properties: ['name' => 'ipartial'])]
#[ApiFilter(OrderFilter::class, properties: ['name'])]
#[ApiFilter(PropertyFilter::class)]
#[ORM\Table(name: 'structure', schema: 'admin')]
#[ORM\Entity]
#[ORM\HasLifecycleCallbacks]
class Structure
{
    use TimestampsTrait;

    #[ORM\Column(name: 'id', type: 'integer', nullable: false)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    #[Groups(['GetUserRespire', 'GetUser','GetUserMe'])]
    private int $id;

    /**
     * @var string
     */
    #[ORM\Column(name: 'name', type: 'string', length: 255, nullable: false)]
    #[Groups(['GetStructure', 'GetUser', 'InputStructure', 'GetUserMe'])]
    #[Assert\Regex(
        pattern: '/(\(|\))/',
        message: 'Your name cannot contain a ( and ) characters',
        match: false,
    )]
    #[Assert\Type('string')]
    #[Assert\NotNull()]
    private string $name;

    /**
     * @var string|null
     */
    #[ORM\Column(name: 'sigle', type: 'string', length: 255, nullable: true)]
    #[Groups(['InputStructure'])]
    #[Assert\Type('string')]
    private ?string $sigle;

    /**
     * @var string|null
     */
    #[ORM\Column(name: 'siren', type: 'string', length: 255, nullable: true)]
    #[Groups([ 'InputStructure'])]
    #[Assert\Type('string')]
    private ?string $siren;

    /**
     * @var string|null
     */
    #[ORM\Column(name: 'siret', type: 'string', length: 255, nullable: true)]
    #[Groups(['GetStructure','InputStructure'])]
    #[Assert\Type('string')]
    private ?string $siret;

    #[ORM\ManyToMany(targetEntity: User::class, mappedBy: 'structures')]
    private Collection $users;

    #[ORM\ManyToOne(targetEntity: Structure::class, inversedBy: "children")]
    #[ORM\JoinColumn(name: 'parent_id', referencedColumnName: 'id', nullable: true)]
    #[Groups(['InputStructure'])]
    private ?Structure $parent = null;

    #[ORM\OneToMany(mappedBy: 'parent', targetEntity: Structure::class)]
    private Collection $children;

    private bool $parentReferral = false;

    private bool $bottomLevel = false;

    #[Assert\Range(max: 4)]
    private int $level;

    #[ORM\Column(name: 'email', type: 'string', length: 128, nullable: true)]
    #[Groups(['GetStructure', 'GetUser', 'InputStructure', 'GetUserMe'])]
    #[Assert\Email]
    private ?string $email;

    #[ORM\Column(name: 'phone_number', type: 'string', length: 30, nullable: true)]
    #[Groups(['InputStructure'])]
    #[Assert\Type(type: 'string')]
    private ?string $phoneNumber;

    #[ORM\Column(name: 'address', type: 'string', length: 1024, nullable: true)]
    #[Groups(['InputStructure'])]
    #[Assert\Type(type: 'string')]
    private ?string $address;

    #[ORM\Column(name: 'zip_code', type: 'string', length: 128, nullable: true)]
    #[Groups(['InputStructure'])]
    #[Assert\Type(type: 'string')]
    private ?string $zipCode;

    #[ORM\Column(name: 'city', type: 'string', length: 128, nullable: true)]
    #[Groups(['InputStructure'])]
    #[Assert\Type(type: 'string')]
    private ?string $city;

    #[ORM\Column(name: 'country', type: 'string', length: 128, nullable: true)]
    #[Groups([ 'InputStructure'])]
    #[Assert\Type(type: 'string')]
    private ?string $country;

    /**
     * @var string|null
     */
    #[ORM\Column(name: 'reference_geonetwork', type: 'string', options: ['default' => null])]
    #[Groups(['GetUser', 'InputStructure', 'InputUser','GetUserMe'])]
    private ?string $referenceGn = null;

    #[Groups(['InputStructure'])]
    #[Assert\Type('bool')]
    private bool $isSynchronised = false;

    public function __construct()
    {
        $this->users = new ArrayCollection();
        $this->children = new ArrayCollection();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return strip_tags($this->name);
    }

    public function setName(string $name): void
    {
        $this->name = strip_tags($name);
    }

    public function getSigle(): ?string
    {
        return $this->sigle === null ? null : strip_tags($this->sigle);
    }

    public function setSigle(?string $sigle): Structure
    {
        $this->sigle = $sigle === null ? null : strip_tags($sigle);
        return $this;
    }


    public function getSiren(): ?string
    {
        return $this->siren === null ? null : strip_tags($this->siren);
    }

    public function setSiren(?string $siren): Structure
    {
        if ($siren !== null) {
            $siren = preg_replace('/\s+/', '', $siren);
            $siren = preg_replace('/_+/', '', $siren);
        }

        $this->siren = $siren === null ? null : strip_tags($siren);
        return $this;
    }

    public function getSiret(): ?string
    {
        return $this->siret === null ? null : strip_tags($this->siret);
    }

    public function setSiret(?string $siret): Structure
    {
        if ($siret !== null) {
            $siret = preg_replace('/\s+/', '', $siret);
            $siret = preg_replace('/_+/', '', $siret);
        }

        $this->siret = $siret === null ? null : strip_tags($siret);
        return $this;
    }

    public function addUser(User $user): self
    {
        if (!$this->users->contains($user)) {
            $this->users[] = $user;
            $user->addStructure($this);
        }

        return $this;
    }

    public function removeUser(User $user): self
    {
        if ($this->users->removeElement($user)) {
            $user->removeStructure($this);
        }

        return $this;
    }

    #[Groups(['GetStructure'])]
    public function getCanDelete(): bool
    {
        if ($this->getCountUser() === 0 && $this->getCountChildren() === 0) {
            return true;
        }
        return false;
    }

    #[Groups(['GetStructure'])]
    public function getCountUser(): int
    {
        return count($this->getUsers());
    }

    #[Groups(['GetStructure'])]
    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function getCountChildren(): int
    {
        return count($this->getChildren());
    }

    public function getChildren(): Collection
    {
        return $this->children;
    }

    public function removeChildren(Structure $structure): self
    {
        $this->childs->removeElement($structure);

        return $this;
    }

    public function isParentReferral(): bool
    {
        if ($this->getParent() !== null) {
            return $this->parentReferral;
        }
        return true;
    }

    public function setParentReferral(bool $parentReferral): Structure
    {
        $this->parentReferral = $parentReferral;
        return $this;
    }

    public function getParent(): ?Structure
    {
        return $this->parent;
    }

    public function setParent(?Structure $parent): ?Structure
    {
        if ($parent !== null) {
            $parent->addChildren($this);
            $this->parent = $parent;
        }

        return $this;
    }

    public function addChildren(Structure $structure): self
    {
        if (!$this->children->contains($structure)) {
            $this->children[] = $structure;
        }

        return $this;
    }

    public function isBottomLevel(): bool
    {
        $gotParent = $this->getParent();
        if ($gotParent !== null) {
            for ($i = 1; $i < 2; $i++) {
                $levelUp = $gotParent->getParent();
                if ($levelUp === null) {
                    return $this->bottomLevel;
                }
                $gotParent = $levelUp;
            }
            return true;
        }
        return $this->bottomLevel;
    }

    public function setBottomLevel(bool $bottomLevel): Structure
    {
        $this->bottomLevel = $bottomLevel;
        return $this;
    }

    public function getLevel(): int
    {
        $gotParent = $this->getParent();

        if ($gotParent === null) {
            $this->setLevel(1);
            return $this->level;
        }
        if ($gotParent !== null && $gotParent->getLevel() === 1) {
            $this->setLevel(2);
        }
        if ($gotParent->getParent() !== null && $gotParent->getLevel() === 2) {
            $this->setLevel(3);
        }
        if ($gotParent->getParent() !== null && $gotParent->getLevel() === 3) {
            $this->setLevel(4);
        }
        return $this->level;
    }

    public function setLevel(int $level): Structure
    {
        $this->level = $level;
        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): Structure
    {
        $this->email = $email;
        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(?string $address): Structure
    {
        $this->address = $address;
        return $this;
    }

    public function getPhoneNumber(): ?string
    {
        return $this->phoneNumber;
    }

    public function setPhoneNumber(?string $phoneNumber): Structure
    {
        $this->phoneNumber = $phoneNumber;
        return $this;
    }

    public function getZipCode(): ?string
    {
        return $this->zipCode;
    }

    public function setZipCode(?string $zipCode): Structure
    {
        $this->zipCode = $zipCode;
        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(?string $city): Structure
    {
        $this->city = $city;
        return $this;
    }

    public function getCountry(): ?string
    {
        return $this->country;
    }

    public function setCountry(?string $country): Structure
    {
        $this->country = $country;
        return $this;
    }

    public function getReferenceGn(): ?string
    {
        return $this->referenceGn;
    }

    public function setReferenceGn(?string $referenceGn): Structure
    {
        $this->referenceGn = $referenceGn;
        return $this;
    }


    public function isSynchronised(): bool
    {
        return $this->isSynchronised;
    }

    public function setIsSynchronised(bool $isSynchronised): Structure
    {
        $this->isSynchronised = $isSynchronised;
        return $this;
    }


}
