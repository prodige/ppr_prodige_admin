<?php

declare(strict_types=1);

namespace App\Entity\Administration\Directory;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Serializer\Filter\PropertyFilter;
use App\Entity\Administration\Resource\Layer;
use App\Filter\FullTextSearchFilter;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Admin.zoning
 */
#[ApiResource(
    itemOperations: ['get', 'patch', 'delete'],
    normalizationContext: ['groups' => ['GetZoning']]
)]
#[ApiFilter(FullTextSearchFilter::class, properties: [
    'name' => 'ipartial',
    'layer.name' => 'ipartial',
    'fieldId' => 'ipartial',
    'fieldName' => 'ipartial',
])]
#[ApiFilter(OrderFilter::class, properties: ['name', 'layer.name', 'fieldId', 'fieldName'])]
#[ApiFilter(PropertyFilter::class)]
#[ORM\Table(name: 'zoning', schema: 'admin')]
#[ORM\Entity]
class Zoning
{
    /**
     * @var int
     */
    #[ORM\Column(name: 'id', type: 'integer', nullable: false)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    #[Groups(['GetZoning'])]
    private int $id;

    /**
     * @var string|null
     */
    #[ORM\Column(name: 'name', type: 'string', nullable: true)]
    #[Groups(['GetZoning', 'GetArea'])]
    #[Assert\Type('string')]
    private ?string $name;

    /**
     * @var bool
     */
    #[ORM\Column(name: 'minimal', type: 'boolean', nullable: true, options: ['default' => false])]
    #[Assert\Type('bool')]
    private bool $minimal = false;

    /**
     * @var string|null
     */
    #[ORM\Column(name: 'field_name', type: 'string', nullable: true)]
    #[Groups(['GetZoning'])]
    #[Assert\Type('string')]
    private ?string $fieldName;

    /**
     * @var string|null
     */
    #[ORM\Column(name: 'field_id', type: 'string', nullable: true)]
    #[Groups(['GetZoning'])]
    #[Assert\Type('string')]
    private ?string $fieldId;

    /**
     * @var Layer
     */
    #[ORM\ManyToOne(targetEntity: Layer::class)]
    #[ORM\JoinColumn(name: 'layer_id', referencedColumnName: 'id')]
    #[Groups(['GetZoning'])]
    #[Assert\NotNull]
    private Layer $layer;


    /**
     * @var Collection
     */
    #[ORM\OneToMany(mappedBy: 'zoning', targetEntity: Area::class, cascade: ['remove'], orphanRemoval: false)]
    private Collection $areas;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLayer(): ?Layer
    {
        return $this->layer;
    }

    public function setLayer(?Layer $layer): self
    {
        $this->layer = $layer;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }


    public function getFieldName(): ?string
    {
        return $this->fieldName;
    }

    public function setFieldName(?string $fieldName): self
    {
        $this->fieldName = $fieldName;

        return $this;
    }

    public function getFieldId(): ?string
    {
        return $this->fieldId;
    }

    public function setFieldId(?string $fieldId): self
    {
        $this->fieldId = $fieldId;

        return $this;
    }

    public function getMinimal(): bool
    {
        return $this->minimal;
    }

    public function setMinimal(?bool $minimal): self
    {
        $this->minimal = $minimal;

        return $this;
    }

    /**
     * @return Collection
     */
    public function getAreas(): Collection
    {
        return $this->areas;
    }

    public function addArea(Area $area): self
    {
        if (!$this->areas->contains($area)) {
            $this->areas[] = $area;
        }

        return $this;
    }

    public function removeArea(Area $area): self
    {
        $this->areas->removeElement($area);

        return $this;
    }

}
