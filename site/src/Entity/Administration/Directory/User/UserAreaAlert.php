<?php

declare(strict_types=1);

namespace App\Entity\Administration\Directory\User;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Serializer\Filter\PropertyFilter;
use App\Entity\Administration\Directory\Area;
use App\Entity\Administration\Resource\Layer;
use App\Entity\Trait\TimestampsTrait;
use App\Filter\FullTextSearchFilter;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Admin.UserAreaAlert
 */
#[ApiResource(
    collectionOperations: ['get', 'post'],
    itemOperations: ['get', 'delete'],
    normalizationContext: ['groups' => ['GetUserAreaAlert']]
)]
#[ApiFilter(SearchFilter::class, properties: ['user' => 'exact'])]
#[ApiFilter(FullTextSearchFilter::class, properties: ['area.name' => 'ipartial', 'layer.name' => 'ipartial'])]
#[ApiFilter(PropertyFilter::class)]
#[ORM\Table(name: 'user_area_alert', schema: 'admin')]
#[ORM\Entity]
#[ORM\HasLifecycleCallbacks]
class UserAreaAlert
{
    use TimestampsTrait;

    /**
     * @var Area
     */
    #[ORM\Id]
    #[ORM\ManyToOne(targetEntity: Area::class, inversedBy: 'alerts')]
    #[ORM\JoinColumn(name: 'area_id', referencedColumnName: 'id')]
    #[Groups(['GetUserAreaAlert'])]
    private Area $area;

    /**
     * @var Layer
     */
    #[ORM\Id]
    #[ORM\ManyToOne(targetEntity: Layer::class)]
    #[ORM\JoinColumn(name: 'layer_id', referencedColumnName: 'id')]
    #[Groups(['GetUserAreaAlert'])]
    private Layer $layer;

    /**
     * @var User
     */
    #[ORM\Id]
    #[ORM\ManyToOne(targetEntity: User::class, inversedBy: 'alerts')]
    #[ORM\JoinColumn(name: 'user_id', referencedColumnName: 'id')]
    #[Groups(['GetUserAreaAlert'])]
    private User $user;

    public function getArea(): ?Area
    {
        return $this->area;
    }

    public function setArea(?Area $area): self
    {
        $this->area = $area;

        return $this;
    }

    public function getLayer(): ?Layer
    {
        return $this->layer;
    }

    public function setLayer(?Layer $layer): self
    {
        $this->layer = $layer;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }
}
