<?php

declare(strict_types=1);

namespace App\Entity\Administration\Directory\User;

use App\Entity\Trait\TimestampsTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * Admin.userContext
 */
#[ORM\Table(name: 'user_context', schema: 'admin')]
#[ORM\Entity]
#[ORM\HasLifecycleCallbacks]
class UserContext
{
    use TimestampsTrait;

    /**
     * @var int
     *
     *
     */
    #[ORM\Column(name: 'id', type: 'integer', nullable: false)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    private int $id;

    /**
     * @var string|null
     */
    #[ORM\Column(name: 'context', type: 'text', nullable: true)]
    private ?string $context;

    /**
     * @var User
     */
    #[ORM\ManyToOne(targetEntity: User::class)]
    #[ORM\JoinColumn(name: 'user_id', referencedColumnName: 'id')]
    private User $user;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getContext(): ?string
    {
        return $this->context;
    }

    public function setContext(?string $context): self
    {
        $this->context = $context;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }
}
