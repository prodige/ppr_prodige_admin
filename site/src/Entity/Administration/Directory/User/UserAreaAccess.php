<?php

declare(strict_types=1);

namespace App\Entity\Administration\Directory\User;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Serializer\Filter\PropertyFilter;
use App\Entity\Administration\Directory\Area;
use App\Entity\Trait\TimestampsTrait;
use App\Filter\FullTextSearchFilter;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Admin.UserAreaAccess
 */
#[ApiResource(
    collectionOperations: ['get', 'post'],
    itemOperations: ['get', 'delete'],
    normalizationContext: ['groups' => ['GetUserAreaAccess']]
)]
#[ApiFilter(SearchFilter::class, properties: ['user' => 'exact'])]
#[ApiFilter(FullTextSearchFilter::class, properties: ['area.name' => 'ipartial', 'layer.name' => 'ipartial'])]
#[ApiFilter(PropertyFilter::class)]
#[ORM\Table(name: 'user_area_access', schema: 'admin')]
#[ORM\Entity]
#[ORM\HasLifecycleCallbacks]
class UserAreaAccess
{
    use TimestampsTrait;

    #[ORM\Id]
    #[ORM\ManyToOne(targetEntity: Area::class, inversedBy: 'userAccess')]
    #[ORM\JoinColumn(name: 'area_id', referencedColumnName: 'id')]
    #[Groups(['GetUserAreaAccess'])]
    private Area $area;

    #[ORM\Id]
    #[ORM\ManyToOne(targetEntity: User::class, inversedBy: 'areaAccess')]
    #[ORM\JoinColumn(name: 'user_id', referencedColumnName: 'id')]
    #[Groups(['GetUserAreaAccess'])]
    private User $user;

    public function getArea(): ?Area
    {
        return $this->area;
    }

    public function setArea(?Area $area): self
    {
        $this->area = $area;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }
}
