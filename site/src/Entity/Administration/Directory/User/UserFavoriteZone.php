<?php

declare(strict_types=1);

namespace App\Entity\Administration\Directory\User;

use App\Entity\Trait\TimestampsTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * Admin.user_favorite_area
 */
#[ORM\Table(name: 'user_favorite_area', schema: 'admin')]
#[ORM\Entity]
#[ORM\HasLifecycleCallbacks]
class UserFavoriteZone
{
    use TimestampsTrait;

    /**
     * @var int
     *
     *
     */
    #[ORM\Column(name: 'id', type: 'integer', nullable: false)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    private int $id;

    /**
     * @var string|null
     */
    #[ORM\Column(name: 'zone_favorite', type: 'text', nullable: true, options: ['comment' => 'Zone favorite'])]
    private ?string $zoneFavorite;

    /**
     * @var User
     */
    #[ORM\ManyToOne(targetEntity: User::class)]
    #[ORM\JoinColumn(name: 'user_id', referencedColumnName: 'id')]
    private User $user;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getZoneFavorite(): ?string
    {
        return $this->zoneFavorite;
    }

    public function setZoneFavorite(?string $zoneFavorite): self
    {
        $this->zoneFavorite = $zoneFavorite;

        return $this;
    }

    public function getUser(): ?user
    {
        return $this->user;
    }

    public function setUser(?user $user): self
    {
        $this->user = $user;

        return $this;
    }
}
