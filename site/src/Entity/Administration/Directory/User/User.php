<?php

declare(strict_types=1);

namespace App\Entity\Administration\Directory\User;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\DateFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Serializer\Filter\PropertyFilter;
use App\Controller\Security\AskPasswordController;
use App\Controller\Security\SecurityController;
use App\Controller\User\FillTraitementController;
use App\Controller\User\HasTraitementController;
use App\Entity\Administration\Directory\Area;
use App\Entity\Administration\Directory\Profile\ProfileUser;
use App\Entity\Administration\Directory\Structure;
use App\Entity\Lex\LexAccountStatus;
use App\Entity\Trait\TimestampsTrait;
use App\Filter\FullTextSearchFilter;
use App\Repository\UserRepository;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

#[
    ApiResource(
        collectionOperations: [
            'get',
            'post',
            'login' => [
                'method' => 'POST',
                'path' => '/login',
                'controller' => SecurityController::class,
                'denormalization_context' => ['groups' => 'LoginUser'],
                'validation_groups' => ['loginValidation'],
                'openapi_context' => [
                    'summary' => 'Login',
                    'description' => 'Return a JWT',
                ],
            ],
            'login-ogc' => [
                'method' => 'POST',
                'path' => '/ogc-features/login',
                'name' => 'ogc-login',
                'controller' => SecurityController::class,
                'denormalization_context' => ['groups' => 'LoginUser'],
                'validation_groups' => ['loginValidation'],
                'openapi_context' => [
                    'tags' => ['Authentification'],
                    'summary' => 'Login',
                    'description' => 'Return a JWT',
                    'requestBody' => [
                        'content' => [
                            'application/ld+json' => [
                                'schema' => [
                                    'type' => 'object',
                                    'properties' => [
                                        'login' => ['type' => 'string', 'example' => 'username'],
                                        'password' => ['type' => 'string', 'example' => 'passWord']
                                    ]
                                ]
                            ]
                        ]
                    ]
                ],
                'defaults' => [
                    '_api_openapi_doc' => false,
                    '_api_ressource_template' => 'login'
                ]
            ],
            'password_reset' => [
                'method' => 'POST',
                'path' => '/password/reset',
                'controller' => AskPasswordController::class,
                'denormalization_context' => ['groups' => 'askPasswordUser'],
                'validation_groups' => ['changePasswordValidation'],
                'openapi_context' => [
                    'summary' => 'Ask for password reset',
                    'description' => 'Ask for password reset',
                ],

            ],
            'hasTraitement' => [
                'method' => 'GET',
                'path' => '/users/traitements/{traitements}',
                'controller' => HasTraitementController::class,
                'read' => false,
                'openapi_context' => [
                    'summary' => 'Has Traitement',
                    'description' => 'Check if you have a traitement right',
                    'parameters' => [
                        [
                            "name" => "domain_id",
                            "in" => "path",
                            "description" => "id of the domain",
                            "required" => false,
                            "schema" => ["type" => "in"]
                        ],
                        [
                            "name" => "subdomain_id",
                            "in" => "path",
                            "description" => "id of the subdomain",
                            "required" => false,
                            "schema" => ["type" => "in"]
                        ],
                        [
                            "name" => "metadata_id",
                            "in" => "path",
                            "description" => "id of the metadata",
                            "required" => false,
                            "schema" => ["type" => "in"]
                        ]
                    ]
                ],
            ],
            'fillTraitement' => [
                'method' => 'GET',
                'path' => '/users/matrix/traitements',
                'controller' => FillTraitementController::class,
                'read' => false
            ],
            'respire' => [
                'method' => 'GET',
                'path' => '/users/respire',
                'normalization_context' => ['groups' => 'GetUserRespire'],
                'openapi_context' => [
                    'summary' => 'Get last updated users',
                    'description' => 'Get last updated users',
                    'parameters' => [
                        [
                            "name" => "minutes",
                            "in" => "path",
                            "description" => "X last minutes",
                            "required" => false,
                            "schema" => ["type" => "in"]
                        ]
                    ]
                ]
            ]
        ],
        denormalizationContext: ['groups' => ['InputUser', 'LoginUser']],
        normalizationContext: ['groups' => ['GetUser', 'GetUserMe']]
    )]
#[ApiFilter(FullTextSearchFilter::class, properties: [
    'firstName' => 'ipartial',
    'login' => 'ipartial',
    'name' => 'ipartial',
    'email' => 'ipartial'
])]
#[ApiFilter(DateFilter::class, properties: ['createdAt', 'updatedAt'])]
#[ApiFilter(OrderFilter::class, properties: ['id', 'name', 'login', 'firstName', 'email'])]
#[ApiFilter(PropertyFilter::class)]
#[ApiFilter(SearchFilter::class, properties: [
    'structures.name' => 'exact',
    'structures.id' => 'exact',
    'login' => 'exact',
    'profiles.profile.name' => 'exact',
    'profiles.profile.id' => 'exact',
])]
#[ORM\Table(name: 'users', schema: 'admin')]
#[ORM\HasLifecycleCallbacks]
#[ORM\Entity(repositoryClass: UserRepository::class)]
#[UniqueEntity(fields: 'login')]
class User implements UserInterface, PasswordAuthenticatedUserInterface
{
    use TimestampsTrait;

    /**
     * @var int
     */
    #[ORM\Column(name: 'id', type: 'integer', nullable: false)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    #[Groups(['GetUser', 'GetUserRigth', 'GetUserMe', 'GetUserRespire', 'GetDataviz'])]
    private int $id;

    /**
     * @var string
     */
    #[ORM\Column(name: 'login', type: 'string', length: 128, unique: true, nullable: false)]
    #[Groups([
        'GetUser',
        'InputUser',
        'LoginUser',
        'askPasswordUser',
        'GetUserRigth',
        'GetUserMe',
        'GetDataviz',
        'GetUserRespire'
    ])]
    #[Assert\Type(type: 'string')]
    #[Assert\NotNull]
    #[Assert\NotBlank(groups: ['loginValidation', 'changePasswordValidation', 'GetUserRespire'])]
    #[Assert\Regex(pattern: '/[ÀÁÂÃÄÅàáâãäåÒÓÔÕÖØòóôõöøÈÉÊËèéêëÇçÌÍÎÏìíîïÙÚÛÜùúûüÿÑñ ]/', message: 'Login : Format invalide', match: false)]
    private string $login;

    /**
     * @var string
     */
    #[ORM\Column(name: 'name', type: 'string', length: 128, nullable: false)]
    #[Groups(['GetUser', 'InputUser', 'GetUserRigth', 'GetUserMe', 'GetUserProfile', 'GetUserRespire'])]
    #[Assert\Type(type: 'string')]
    #[Assert\NotNull]
    private string $name;

    /**
     * @var string|null
     */
    #[ORM\Column(name: 'first_name', type: 'string', length: 128, nullable: true)]
    #[Groups(['GetUser', 'InputUser', 'GetUserRigth', 'GetUserMe', 'GetUserRespire'])]
    #[Assert\Type(type: 'string')]
    private ?string $firstName;

    /**
     * @var string
     */
    #[ORM\Column(name: 'email', type: 'string', length: 128, nullable: true)]
    #[Assert\Email(groups: ['changePasswordValidation'])]
    #[Assert\NotNull(groups: ['changePasswordValidation'])]
    #[Groups(['GetUser', 'InputUser', 'askPasswordUser', 'GetUserRigth', 'GetUserMe', 'GetUserRespire'])]
    private string $email;

    /**
     * @var string|null
     */
    #[ORM\Column(name: 'phone_number', type: 'string', length: 30, nullable: true)]
    #[Groups(['GetUser', 'InputUser', 'GetUserRespire'])]
    #[Assert\Type(type: 'string')]
    private ?string $phoneNumber;

    /**
     * @var string|null
     */
    #[ORM\Column(name: 'phone_number2', type: 'string', length: 30, nullable: true)]
    #[Groups(['GetUser', 'InputUser', 'GetUserRespire'])]
    #[Assert\Type(type: 'string')]
    private ?string $phoneNumber2;

    /**
     * @var string|null
     */
    #[ORM\Column(name: 'service', type: 'string', length: 128, nullable: true)]
    #[Groups(['GetUser', 'InputUser'])]
    #[Assert\Type(type: 'string')]
    private ?string $service;

    /**
     * @var string|null
     */
    #[ORM\Column(name: 'description', type: 'string', length: 1024, nullable: true)]
    #[Groups(['GetUser', 'InputUser', 'GetUserRespire'])]
    #[Assert\Type(type: 'string')]
    private ?string $description;

    /**
     * @var string|null
     */
    #[ORM\Column(name: 'password', type: 'string', length: 128, nullable: true)]
    #[Groups(['InputUser', 'LoginUser'])]
    #[Assert\Type(type: 'string')]
    #[Assert\NotBlank(groups: ['loginValidation'])]
    private ?string $password = null;

    /**
     * @var DateTime
     */
    #[ORM\Column(name: 'password_expire', type: 'datetime', nullable: false)]
    private DateTime $passwordExpire;

    /**
     * @var bool
     */
    #[ORM\Column(name: 'is_expired', type: 'boolean', options: ['default' => false])]
    #[Groups(['GetUser', 'InputUser', 'GetUserRespire'])]
    #[Assert\Type(type: 'boolean')]
    private bool $isExpired = false;

    /**
     * @var bool
     */
    #[ORM\Column(name: 'generic', type: 'boolean')]
    #[Groups(['GetUser', 'InputUser'])]
    #[Assert\Type(type: 'boolean')]
    private bool $generic = false;

    /**
     * @var DateTime|null
     */
    #[ORM\Column(name: 'account_expiration', type: 'datetime', nullable: true)]
    #[Groups(['GetUser', 'InputUser', 'GetUserRespire'])]
    private ?DateTime $accountExpire;

    /**
     * @var Collection
     */
    #[ORM\ManyToMany(targetEntity: Structure::class, inversedBy: 'users')]
    #[ORM\JoinTable(name: 'users_structures', schema: 'admin')]
    #[Groups(['GetUser', 'GetUserMe', 'InputUser', 'GetUserRespire'])]
    #[Assert\count(max: 1, maxMessage: "You can have max {{ limit }} structure")]
    private Collection $structures;

    /**
     * @var Collection
     */
    #[ORM\OneToMany(mappedBy: 'user', targetEntity: UserAreaEdition::class, orphanRemoval: true)]
    private Collection $areaEdition;

    /**
     * @var Collection
     */
    #[ORM\OneToMany(mappedBy: 'user', targetEntity: UserAreaAccess::class, orphanRemoval: true)]
    private Collection $areaAccess;

    /**
     * @var Collection
     */
    #[ORM\OneToMany(mappedBy: 'user', targetEntity: UserAreaAlert::class, orphanRemoval: true)]
    private Collection $alerts;

    /**
     * @var Collection
     */
    #[ORM\OneToMany(mappedBy: 'user', targetEntity: ProfileUser::class, orphanRemoval: true)]
    #[Groups(['GetUser', 'GetUserRespire'])]
    private Collection $profiles;

    /**
     * @var int
     */
    #[ORM\Column(name: 'reference_gn', type: 'integer', options: ['default' => 0])]
    #[Groups(['GetUser', 'InputUser'])]
    private int $referenceGn;

    #[ORM\Column(name: 'ipv4Address', type: 'string', length: 128, nullable: true)]
    #[Groups(['GetUser', 'GetUserMe', 'InputUser'])]
    #[Assert\Ip(null, "4", "Adresse IPv4 invalide")]
    private ?string $ipv4Address;

    #[ORM\Column(name: 'ipv6Address', type: 'string', length: 128, nullable: true)]
    #[Groups(['GetUser', 'GetUserMe', 'InputUser'])]
    #[Assert\Ip(null, "6", "Adresse IPv6 invalide")]
    private ?string $ipv6Address;

    #[ORM\ManyToOne(targetEntity: LexAccountStatus::class)]
    #[ORM\JoinColumn(name: 'lex_account_id', referencedColumnName: 'id')]
    #[Groups(['GetUser', 'GetUserMe', 'InputUser'])]
    #[Assert\NotNull]
    private LexAccountStatus $lexAccountStatus;

    #[ORM\Column(name: 'is_default_installation', type: 'boolean', options: ['default' => false])]
    private bool $isDefaultInstallation = false;

    public function __construct()
    {
        $this->structures = new ArrayCollection();
        $this->areaAccess = new ArrayCollection();
        $this->areaEdition = new ArrayCollection();
        $this->profiles = new ArrayCollection();
        $this->alerts = new ArrayCollection();
        $this->passwordExpire = new DateTime();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLogin(): ?string
    {
        return $this->login;
    }

    public function setLogin(string $login): self
    {
        $this->login = $login;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name === null ? null : strip_tags($this->name);
    }

    public function setName(string $name): self
    {
        $this->name = strip_tags($name);

        return $this;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName === null ? null : strip_tags($this->firstName);
    }

    public function setFirstName(?string $firstName): self
    {
        $this->firstName = $firstName === null ? null : strip_tags($firstName);

        return $this;
    }

    public function getEmail(): string
    {
        return strip_tags($this->email);
    }

    public function setEmail(string $email): self
    {
        $this->email = strip_tags($email);

        return $this;
    }

    public function getPhoneNumber(): ?string
    {
        return $this->phoneNumber === null ? null : strip_tags($this->phoneNumber);
    }

    public function setPhoneNumber(?string $phoneNumber): self
    {
        $this->phoneNumber = $phoneNumber === null ? null : strip_tags($phoneNumber);

        return $this;
    }

    public function getPhoneNumber2(): ?string
    {
        return $this->phoneNumber2 === null ? null : strip_tags($this->phoneNumber2);
    }

    public function setPhoneNumber2(?string $phoneNumber2): self
    {
        $this->phoneNumber2 = $phoneNumber2 === null ? null : strip_tags($phoneNumber2);

        return $this;
    }

    public function getService(): ?string
    {
        return $this->service === null ? null : strip_tags($this->service);
    }

    public function setService(?string $service): self
    {
        $this->service = $service === null ? null : strip_tags($service);

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description === null ? null : strip_tags($this->description);
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description === null ? null : strip_tags($description);

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(?string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getPasswordExpire(): ?DateTime
    {
        return $this->passwordExpire;
    }

    public function setPasswordExpire(DateTime $passwordExpire): self
    {
        $this->passwordExpire = $passwordExpire;

        return $this;
    }

    public function getGeneric(): ?bool
    {
        return $this->generic;
    }

    public function setGeneric(bool $generic): self
    {
        $this->generic = $generic;

        return $this;
    }

    public function getAccountExpire(): ?\DateTimeInterface
    {
        return $this->accountExpire;
    }

    public function setAccountExpire(?\DateTimeInterface $accountExpire): self
    {
        $this->accountExpire = $accountExpire;

        return $this;
    }

    public function getStructures(): Collection
    {
        return $this->structures;
    }

    public function addStructure(Structure $structure): self
    {
        if (!$this->structures->contains($structure)) {
            $this->structures[] = $structure;
        }

        return $this;
    }

    public function removeStructure(Structure $structure): self
    {
        $this->structures->removeElement($structure);

        return $this;
    }

    public function getAreaAccess(): Collection
    {
        return $this->areaAccess;
    }

    public function addAreaAccess(Area $area): self
    {
        if (!$this->areaAccess->contains($area)) {
            $this->areaAccess[] = $area;
        }

        return $this;
    }

    public function removeAreaAccess(Area $area): self
    {
        $this->areaAccess->removeElement($area);

        return $this;
    }

    public function getAreaEdition(): Collection
    {
        return $this->areaEdition;
    }

    public function addAreaEdition(Area $area): self
    {
        if (!$this->areaEdition->contains($area)) {
            $this->areaEdition[] = $area;
        }

        return $this;
    }

    public function removeAreaEdition(Area $area): self
    {
        $this->areaEdition->removeElement($area);

        return $this;
    }

    public function getProfiles(): Collection
    {
        return $this->profiles;
    }

    public function addProfile(ProfileUser $profile): self
    {
        if (!$this->profiles->contains($profile)) {
            $this->profiles[] = $profile;
        }

        return $this;
    }

    public function removeProfile(ProfileUser $profile): self
    {
        $this->profiles->removeElement($profile);

        return $this;
    }

    public function getRoles(): array
    {
        return [];
    }

    public function getSalt()
    {
        return null;
    }

    public function eraseCredentials()
    {
        $this->password = null;
    }

    public function getUsername(): ?string
    {
        return $this->login;
    }

    public function getUserIdentifier(): ?string
    {
        return $this->login;
    }

    public function getAlerts(): Collection
    {
        return $this->alerts;
    }

    public function addAlert(UserAreaAlert $alert): self
    {
        if (!$this->alerts->contains($alert)) {
            $this->alerts[] = $alert;
        }

        return $this;
    }

    public function removeAlert(UserAreaAlert $alert): self
    {
        $this->alerts->removeElement($alert);

        return $this;
    }

    #[Groups(['GetUser'])]
    public function getCountAlert(): int
    {
        return count($this->alerts);
    }

    /**
     * @return bool
     */

    public function isExpired(): bool
    {
        return $this->isExpired;
    }

    public function setIsExpired(bool $isExpired): User
    {
        $this->isExpired = $isExpired;

        return $this;
    }

    /**
     * @return int
     */
    public function getReferenceGn(): int
    {
        return $this->referenceGn;
    }

    /**
     * @param int $referenceGn
     * @return User
     */
    public function setReferenceGn(int $referenceGn): User
    {
        $this->referenceGn = $referenceGn;
        return $this;
    }

    public function getIpv4Address(): ?string
    {
        return $this->ipv4Address;
    }

    public function setIpv4Address(?string $ipv4Address): User
    {
        $this->ipv4Address = $ipv4Address;
        return $this;
    }

    public function getIpv6Address(): ?string
    {
        return $this->ipv6Address;
    }

    public function setIpv6Address(?string $ipv6Address): User
    {
        $this->ipv6Address = $ipv6Address;
        return $this;
    }

    public function getLexAccountStatus(): LexAccountStatus
    {
        return $this->lexAccountStatus;
    }

    public function setLexAccountStatus(LexAccountStatus $lexAccountStatus): User
    {
        $this->lexAccountStatus = $lexAccountStatus;
        return $this;
    }


    public function isDefaultInstallation(): bool
    {
        return $this->isDefaultInstallation;
    }

    public function setIsDefaultInstallation(bool $isDefaultInstallation): User
    {
        $this->isDefaultInstallation = $isDefaultInstallation;
        return $this;
    }

    #[Groups(['GetUser'])]
    public function getCanDelete(): bool
    {
        if ($this->isDefaultInstallation === false) {
            return true;
        }
        return false;
    }
}
