<?php

declare(strict_types=1);

namespace App\Entity\Administration\Directory;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Serializer\Filter\PropertyFilter;
use App\Entity\Administration\Directory\User\UserAreaAccess;
use App\Entity\Administration\Directory\User\UserAreaAlert;
use App\Entity\Administration\Directory\User\UserAreaEdition;
use App\Entity\Trait\TimestampsTrait;
use App\Filter\FullTextSearchFilter;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Admin.area
 */
#[ApiResource(
    collectionOperations: ['get'],
    itemOperations: ['get'],
    normalizationContext: ['groups' => ['GetArea']]
)]
#[ApiFilter(FullTextSearchFilter::class, properties: [
    'name' => 'ipartial',
    'code' => 'ipartial',
    'zoning.name' => 'ipartial',
])]
#[ApiFilter(OrderFilter::class, properties: ['name', 'code', 'zoning.name'])]
#[ApiFilter(PropertyFilter::class)]
#[ORM\Table(name: 'area', schema: 'admin')]
#[ORM\Entity]
#[ORM\HasLifecycleCallbacks]
class Area
{
    use TimestampsTrait;

    /**
     * @var int
     */
    #[ORM\Column(name: 'id', type: 'integer', nullable: false)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    #[Groups(['GetArea'])]
    private int $id;

    /**
     * @var string|null
     */
    #[ORM\Column(name: 'code', type: 'string', nullable: true)]
    #[Groups(['GetArea',  'GetUserAreaAlert', 'GetUserAreaAccess', 'GetUserAreaEdition'])]
    #[Assert\Type('string')]
    private ?string $code;

    /**
     * @var string|null
     */
    #[ORM\Column(name: 'name', type: 'string', nullable: true)]
    #[Groups(['GetArea', 'GetUser', 'GetUserAreaAlert', 'GetUserAreaAccess', 'GetUserAreaEdition'])]
    #[Assert\Type('string')]
    private ?string $name;

    #[Groups(['GetArea', 'GetUser', 'GetUserAreaAlert', 'GetUserAreaAccess', 'GetUserAreaEdition'])]
    private ?string $nameAndCode;

    /**
     * @var Zoning
     */
    #[ORM\ManyToOne(targetEntity: Zoning::class, inversedBy: 'areas')]
    #[ORM\JoinColumn(name: 'zoning_id', referencedColumnName: 'id')]
    #[Groups(['GetArea'])]
    #[Assert\NotNull]
    private Zoning $zoning;

    #[ORM\OneToMany(mappedBy: 'area', targetEntity: UserAreaAccess::class, cascade: ['remove'])]
    private Collection $userAccess;

    #[ORM\OneToMany(mappedBy: 'area', targetEntity: UserAreaEdition::class, cascade: ['remove'])]
    private Collection $userEdition;

    #[ORM\OneToMany(mappedBy: 'area', targetEntity: UserAreaAlert::class, cascade: ['remove'], orphanRemoval: false)]
    private Collection $alerts;

    /**
     * @var bool
     */
    #[ORM\Column(name: 'expired', type: 'boolean', nullable: false, options: ['default' => false])]
    #[Assert\Type('bool')]
    #[Assert\NotNull]
    private bool $expired = false;

    public function __construct()
    {
        $this->userAccess = new ArrayCollection();
        $this->userEdition = new ArrayCollection();
        $this->alerts = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(?string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getNameAndCode(): ?string
    {
        return $this->name . ' ( ' . $this->code . ' )';
    }

    public function setNameAndCode(?string $nameAndCode): Area
    {
        $this->nameAndCode = $nameAndCode;
        return $this;
    }


    public function getZoning(): ?Zoning
    {
        return $this->zoning;
    }

    public function setZoning(?Zoning $zoning): self
    {
        $this->zoning = $zoning;

        return $this;
    }

    public function getUserAccess(): Collection
    {
        return $this->userAccess;
    }

    public function addUserAccess(UserAreaAccess $userAccess): self
    {
        if (!$this->userAccess->contains($userAccess)) {
            $this->userAccess[] = $userAccess;
        }

        return $this;
    }

    public function removeUserAccess(UserAreaAccess $userAccess): self
    {
        $this->userAccess->removeElement($userAccess);

        return $this;
    }

    public function getUserEdition(): Collection
    {
        return $this->userEdition;
    }

    public function addUserEdition(UserAreaEdition $userEdition): self
    {
        if (!$this->userEdition->contains($userEdition)) {
            $this->userEdition[] = $userEdition;
        }

        return $this;
    }

    public function removeUserEdition(UserAreaEdition $userEdition): self
    {
        $this->userEdition->removeElement($userEdition);

        return $this;
    }

    public function getAlerts(): Collection
    {
        return $this->alerts;
    }

    public function addAlert(UserAreaAlert $alert): self
    {
        if (!$this->alerts->contains($alert)) {
            $this->alerts[] = $alert;
        }

        return $this;
    }

    public function removeAlert(UserAreaAlert $alert): self
    {
        $this->alerts->removeElement($alert);

        return $this;
    }

    public function isExpired(): bool
    {
        return $this->expired;
    }

    public function setExpired(bool $expired): void
    {
        $this->expired = $expired;
    }

    #[Groups(['GetArea'])]
    public function getCanDelete(): bool
    {
        return (count($this->getUserAccess()) == 0 && (count($this->getUserEdition()) == 0));
    }

}
