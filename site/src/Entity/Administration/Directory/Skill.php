<?php

declare(strict_types=1);

namespace App\Entity\Administration\Directory;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Serializer\Filter\PropertyFilter;
use App\Entity\Administration\Directory\Profile\Profile;
use App\Entity\Administration\Resource\Layer;
use App\Entity\Administration\Resource\Map;
use App\Entity\Trait\TimestampsTrait;
use App\Filter\FullTextSearchFilter;
use App\Repository\SkillRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Admin.skill
 */
#[ApiResource(
    itemOperations: ['get', 'patch', 'delete'],
    normalizationContext: ['groups' => ['GetSkill']]
)]
#[ApiFilter(FullTextSearchFilter::class, properties: [
    'name' => 'ipartial',
    'layers.name' => 'ipartial',
    'maps.name' => 'ipartial',
])]
#[ApiFilter(OrderFilter::class, properties: ['name'])]
#[ApiFilter(PropertyFilter::class)]
#[ORM\Table(name: 'skill', schema: 'admin')]
#[ORM\UniqueConstraint(name: 'skill_pk_id', columns: ['id'])]
#[ORM\Entity(repositoryClass: SkillRepository::class)]
#[ORM\HasLifecycleCallbacks]
class Skill
{
    use TimestampsTrait;

    /**
     * @var int
     */
    #[ORM\Column(name: 'id', type: 'integer', nullable: false)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    #[Groups(['GetSkill'])]
    private int $id;

    /**
     * @var string
     */
    #[ORM\Column(name: 'name', type: 'string', length: 1024, nullable: false)]
    #[Groups(['GetSkill'])]
    #[Assert\Type('string')]
    private string $name;

    /**
     * @var Collection
     */
    #[ORM\ManyToMany(targetEntity: Profile::class, mappedBy: 'skills', cascade: ['persist'])]
    private Collection $profiles;

    /**
     * @var Collection|null
     */
    #[ORM\ManyToMany(targetEntity: Map::class, mappedBy: 'skills', cascade: ['persist'])]
    #[ORM\JoinTable(name: 'skills_maps', schema: 'admin')]
    #[Groups(['GetSkill'])]
    private ?Collection $maps;

    /**
     * @var Collection|null
     */
    #[ORM\ManyToMany(targetEntity: Layer::class, mappedBy: 'skills')]
    #[ORM\JoinTable(name: 'skills_layers', schema: 'admin')]
    #[Groups(['GetSkill'])]
    private ?Collection $layers;

    public function __construct()
    {
        $this->profiles = new ArrayCollection();
        $this->layers = new ArrayCollection();
        $this->maps = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection
     */
    public function getProfiles(): Collection
    {
        return $this->profiles;
    }

    public function addProfile(Profile $profile): self
    {
        if (!$this->profiles->contains($profile)) {
            $this->profiles[] = $profile;
            $profile->addSkill($this);
        }

        return $this;
    }

    public function removeProfile(Profile $profile): self
    {
        if ($this->profiles->removeElement($profile)) {
            $profile->removeSkill($this);
        }

        return $this;
    }

    /**
     * @return Collection|null
     */
    public function getMaps(): ?Collection
    {
        return $this->maps;
    }

    public function addMap(Map $map): self
    {
        if (!$this->maps->contains($map)) {
            $this->maps[] = $map;
            $map->addSkill($this);
        }

        return $this;
    }

    public function removeMap(Map $map): self
    {
        if ($this->profiles->removeElement($map)) {
            $map->removeSkill($this);
        }

        return $this;
    }

    /**
     * @return Collection|null
     */
    public function getLayers(): ?Collection
    {
        return $this->layers;
    }

    public function addLayer(Layer $layer): self
    {
        if (!$this->layers->contains($layer)) {
            $this->layers[] = $layer;
            $layer->addSkill($this);
        }

        return $this;
    }

    public function removeLayer(Layer $layer): self
    {
        if ($this->layers->removeElement($layer)) {
            $layer->removeSkill($this);
        }

        return $this;
    }
}