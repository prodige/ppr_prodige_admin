<?php

declare(strict_types=1);

namespace App\Entity\Administration\Organisation;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Serializer\Filter\PropertyFilter;
use App\Entity\Administration\Directory\Profile\Profile;
use App\Entity\Trait\TimestampsTrait;
use App\Filter\FullTextSearchFilter;
use App\Repository\DomainRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Admin.Domain
 */
#[ApiResource(
    itemOperations: ['get', 'patch', 'delete'],
    denormalizationContext: ['groups' => ['InputDomain']],
    normalizationContext: ['groups' => ['GetDomain']]
)]
#[ApiFilter(FullTextSearchFilter::class, properties: ['name' => 'ipartial', 'rubric.name' => 'ipartial','rubric.id' => 'exact'])]
#[ApiFilter(OrderFilter::class, properties: ['name', 'rubric.name'])]
#[ApiFilter(PropertyFilter::class)]
#[ORM\Table(name: 'domain', schema: 'admin')]
#[ORM\Entity(repositoryClass: DomainRepository::class)]
#[UniqueEntity(fields: 'name')]
#[ORM\HasLifecycleCallbacks]
class Domain
{
    use TimestampsTrait;

    /**
     * @var int
     */
    #[ORM\Column(name: 'id', type: 'integer', nullable: false)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    #[Groups(['GetDomain','OutputMetadataSheet', 'GetRubric','GetLayer','GetMap'])]
    private int $id;

    /**
     * @var string
     */
    #[ORM\Column(name: 'name', type: 'string', length: 128, unique: true, nullable: false)]
    #[Groups(['InputDomain', 'GetDomain', 'GetSubdomain','OutputMetadataSheet', 'GetRubric','GetLayer','GetMap'])]
    #[Assert\Type('string')]
    #[Assert\NotBlank]
    private string $name;

    /**
     * @var string|null
     */
    #[ORM\Column(name: 'description', type: 'string', length: 1024, nullable: true)]
    #[Groups(['InputDomain', 'GetDomain'])]
    #[Assert\Type('string')]
    private ?string $description = null;

    /**
     * @var Rubric
     */
    #[ORM\ManyToOne(targetEntity: 'Rubric', inversedBy: 'domains')]
    #[ORM\JoinColumn(name: 'rubric_id', referencedColumnName: 'id', nullable: false)]
    #[Groups(['InputDomain', 'GetDomain','OutputMetadataSheet', 'GetSubdomain','GetLayer','GetMap'])]
    #[Assert\NotNull]
    private Rubric $rubric;

    /**
     * @var Collection
     */
    #[ORM\OneToMany(mappedBy: 'domain', targetEntity: Subdomain::class, orphanRemoval: false)]
    #[Groups(['InputDomain', 'GetDomain', 'GetRubric'])]
    private Collection $subdomains;

    /**
     * @var Collection
     */
    #[ORM\ManyToMany(targetEntity: Profile::class, mappedBy: 'domains')]
    private Collection $profiles;

    public function __construct()
    {
        $this->profiles = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getRubric(): ?Rubric
    {
        return $this->rubric;
    }

    public function setRubric(?Rubric $rubric): self
    {
        $this->rubric = $rubric;

        return $this;
    }

    #[Groups(['GetDomain'])]
    public function getCountSubdomain(): ?int
    {
        return count($this->getsubdomains());
    }

    public function getSubdomains(): Collection
    {
        return $this->subdomains;
    }

    #[Groups(['GetDomain'])]
    public function getCanDelete(): bool
    {
        return ($this->getCountSubdomain() == 0);
    }

    /**
     * @return Collection
     */
    public function getProfiles(): Collection
    {
        return $this->profiles;
    }

    public function addProfile(Profile $profile): self
    {
        if (!$this->profiles->contains($profile)) {
            $this->profiles[] = $profile;
            $profile->addDomain($this);
        }

        return $this;
    }

    public function removeProfile(Profile $profile): self
    {
        if ($this->profiles->removeElement($profile)) {
            $profile->removeDomain($this);
        }

        return $this;
    }

}
