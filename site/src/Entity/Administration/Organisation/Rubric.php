<?php

declare(strict_types=1);

namespace App\Entity\Administration\Organisation;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Serializer\Filter\PropertyFilter;
use App\Entity\Trait\TimestampsTrait;
use App\Filter\FullTextSearchFilter;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Admin.Rubric
 */
#[ApiResource(
    itemOperations: ['get', 'patch', 'delete'],
    denormalizationContext: ['groups' => ['InputRubric']],
    normalizationContext: ['groups' => ['GetRubric']]
)]
#[ApiFilter(FullTextSearchFilter::class, properties: ['name' => 'ipartial'])]
#[ApiFilter(OrderFilter::class, properties: ['name'])]
#[ApiFilter(PropertyFilter::class)]
#[ORM\Table(name: 'rubric', schema: 'admin')]
#[ORM\Entity]
#[ORM\HasLifecycleCallbacks]
class Rubric
{
    use TimestampsTrait;

    /**
     * @var int
     */
    #[ORM\Column(name: 'id', type: 'integer', nullable: false)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    #[Groups(['GetRubric', 'OutputMetadataSheet','GetSubdomain','GetMap','GetLayer'])]
    private int $id;

    /**
     * @var string
     */
    #[ORM\Column(name: 'name', type: 'string', nullable: false)]
    #[Groups(['InputRubric', 'GetRubric', 'GetDomain', 'OutputMetadataSheet','GetSubdomain','GetMap','GetLayer'])]
    #[Assert\Type('string')]
    #[Assert\NotNull]
    private string $name;

    /**
     * @var Collection
     */
    #[ORM\OneToMany(mappedBy: 'rubric', targetEntity: Domain::class, orphanRemoval: false)]
    private Collection $domains;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    #[Groups(['GetRubric'])]
    public function getCanDelete(): bool
    {
        return ($this->getCountDomain() === 0);
    }

    #[Groups(['GetRubric'])]
    public function getCountDomain(): ?int
    {
        return count($this->getDomains());
    }

    #[Groups(['GetRubric'])]
    public function getDomains(): Collection
    {
        return $this->domains;
    }


}
