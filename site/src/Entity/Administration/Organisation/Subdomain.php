<?php

declare(strict_types=1);

namespace App\Entity\Administration\Organisation;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Serializer\Filter\PropertyFilter;
use App\Entity\Administration\Directory\Profile\Profile;
use App\Entity\Administration\Resource\MetadataSheet;
use App\Entity\Trait\TimestampsTrait;
use App\Filter\FullTextSearchFilter;
use App\Repository\SubDomainRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Admin.Subdomain.
 */
#[ApiResource(
    itemOperations: ['get', 'patch', 'delete'],
    denormalizationContext: ['groups' => ['InputSubdomain']],
    normalizationContext: ['groups' => ['GetSubdomain']]
)]
#[ApiFilter(FullTextSearchFilter::class, properties: [
    'name' => 'ipartial',
    'domain.name' => 'ipartial',
    'domain.id' => 'exact'
])]
#[ApiFilter(OrderFilter::class, properties: ['name', 'domain.name'])]
#[ApiFilter(PropertyFilter::class)]
#[ORM\Table(name: 'subdomain', schema: 'admin')]
#[ORM\UniqueConstraint(name: 'subdomain_un_name', columns: ['name'])]
#[ORM\Entity(repositoryClass: SubDomainRepository::class)]
#[ORM\HasLifecycleCallbacks]
#[UniqueEntity(fields: 'name')]
class Subdomain
{
    use TimestampsTrait;

    /**
     * @var int
     */
    #[ORM\Column(name: 'id', type: 'integer', nullable: false)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    #[Groups(['GetSubdomain', 'OutputMetadataSheet', 'GetLayer', 'GetRubric', 'GetMap', 'GetDataviz'])]
    private int $id;

    /**
     * @var string
     */
    #[ORM\Column(name: 'name', type: 'string', length: 128, unique: true, nullable: false)]
    #[Groups(['GetSubdomain', 'InputSubdomain', 'OutputMetadataSheet', 'GetLayer', 'GetRubric', 'GetMap', 'GetDataviz'])]
    #[Assert\Type('string')]
    #[Assert\Regex(
        pattern: '/\//',
        message: 'Your name cannot contain a /',
        match: false,
    )]
    #[Assert\NotNull]
    private string $name;

    /**
     * @var string|null
     */
    #[ORM\Column(name: 'description', type: 'string', length: 1024, nullable: true)]
    #[Groups(['InputSubdomain', 'GetSubdomain'])]
    #[Assert\Type('string')]
    private ?string $description = null;

    /**
     * @var bool
     */
    #[ORM\Column(name: 'wms_service', type: 'boolean', nullable: false, options: ['default' => false])]
    #[Groups(['InputSubdomain', 'GetSubdomain', 'OutputMetadataSheet'])]
    #[Assert\Type('bool')]
    #[Assert\NotNull]
    private bool $wmsService = false;

    /**
     * @var string|null
     */
    #[ORM\Column(name: 'wms_service_uuid', type: 'string', length: 250, nullable: true)]
    #[Groups(['GetSubdomain'])]
    #[Assert\Type('string')]
    private ?string $wmsServiceUuid;

    /**
     * @var Profile|null
     */
    #[ORM\ManyToOne(targetEntity: Profile::class)]
    #[ORM\JoinColumn(name: 'profile_creator_id', referencedColumnName: 'id')]
    #[Groups(['GetSubdomain'])]
    private ?Profile $profileCreator;

    /**
     * @var Profile|null
     */
    #[ORM\ManyToOne(targetEntity: Profile::class)]
    #[ORM\JoinColumn(name: 'profile_editor_id', referencedColumnName: 'id')]
    #[Groups(['InputSubdomain', 'GetSubdomain'])]
    private ?Profile $profileEditor;

    /**
     * @var Profile|null
     */
    #[ORM\ManyToOne(targetEntity: Profile::class)]
    #[ORM\JoinColumn(name: 'profile_admin_id', referencedColumnName: 'id')]
    #[Groups(['InputSubdomain', 'GetSubdomain'])]
    private ?Profile $profileAdmin;

    /**
     * @var Collection
     */
    #[ORM\ManyToMany(targetEntity: Profile::class, mappedBy: 'subdomains')]
    private Collection $profiles;

    /**
     * @var Domain
     */
    #[ORM\ManyToOne(targetEntity: Domain::class, inversedBy: "subdomains")]
    #[ORM\JoinColumn(name: 'domain_id', referencedColumnName: 'id', nullable: false)]
    #[Groups(['InputSubdomain', 'GetSubdomain', 'OutputMetadataSheet', 'GetLayer', 'GetMap'])]
    #[Assert\NotNull]
    private Domain $domain;

    /**
     * @var Collection
     */
    #[ORM\ManyToMany(targetEntity: MetadataSheet::class, mappedBy: 'subdomains')]
    #[ORM\JoinTable(name: 'subdomain_metadata_sheet', schema: 'admin')]
    private Collection $metadata_sheet;

    public function __construct()
    {
        $this->profiles = new ArrayCollection();
        $this->metadata_sheet = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getProfileCreator(): ?Profile
    {
        return $this->profileCreator;
    }

    public function setProfileCreator(?Profile $profileCreator): self
    {
        $this->profileCreator = $profileCreator;

        return $this;
    }

    public function getProfileEditor(): ?Profile
    {
        return $this->profileEditor;
    }

    public function setProfileEditor(?Profile $profileEditor): self
    {
        $this->profileEditor = $profileEditor;

        return $this;
    }

    public function getProfileAdmin(): ?Profile
    {
        return $this->profileAdmin;
    }

    public function setProfileAdmin(?Profile $profileAdmin): self
    {
        $this->profileAdmin = $profileAdmin;

        return $this;
    }

    public function getDomain(): ?Domain
    {
        return $this->domain;
    }

    public function setDomain(?Domain $domain): self
    {
        $this->domain = $domain;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): Subdomain
    {
        $this->description = $description;
        return $this;
    }


    public function getWmsService(): ?bool
    {
        return $this->wmsService;
    }

    public function setWmsService(bool $wmsService): self
    {
        $this->wmsService = $wmsService;

        return $this;
    }

    public function getWmsServiceUuid(): ?string
    {
        return $this->wmsServiceUuid;
    }

    public function setWmsServiceUuid(?string $wmsServiceUuid): self
    {
        $this->wmsServiceUuid = $wmsServiceUuid;

        return $this;
    }

    public function getProfiles(): Collection
    {
        return $this->profiles;
    }

    public function addProfile(Profile $profile): self
    {
        if (!$this->profiles->contains($profile)) {
            $this->profiles[] = $profile;
            $profile->addSubdomain($this);
        }

        return $this;
    }

    public function removeProfile(Profile $profile): self
    {
        if ($this->profiles->removeElement($profile)) {
            $profile->removeSubdomain($this);
        }

        return $this;
    }

    public function getMetadata(): Collection
    {
        return $this->metadata_sheet;
    }

    public function addMetadata(MetadataSheet $metadata): self
    {
        if (!$this->metadata_sheet->contains($metadata)) {
            $this->metadata_sheet[] = $metadata;
        }

        return $this;
    }

    public function removeDomainAccess(MetadataSheet $metadata): self
    {
        $this->metadata_sheet->removeElement($metadata);

        return $this;
    }

}
