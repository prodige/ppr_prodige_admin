<?php

declare(strict_types=1);

namespace App\Entity\Administration\Resource;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Controller\Administration\Resource\View\GetViewDataset;
use App\Controller\Administration\Resource\View\GetViewFields;

/**
 * Class to manage View's resources
 */
#[ApiResource(
    collectionOperations: [
        'get_dataset' => [
            'method' => 'GET',
            'path' => '/view/resource/dataset/{layer_type}',
            'pagination_enabled' => false,
            'controller' => GetViewDataset::class,
            'read' => false,
            '_api_receive' => false,
            'openapi_context' => [
                'parameters' => [
                    [
                        'name' => 'layer_type',
                        'in' => 'path',
                        'type' => 'string',
                    ],
                ],
                'summary' => 'Get layers by type',
                'description' => 'Get layers by type',
            ],
        ],
        'get_fields' => [
            'method' => 'GET',
            'path' => '/view/resource/dataset/fields/{layer_id}',
            'pagination_enabled' => false,
            'controller' => GetViewFields::class,
            'read' => false,
            '_api_receive' => false,
            'openapi_context' => [
                'parameters' => [
                    [
                        'name' => 'layer_id',
                        'in' => 'path',
                        'type' => 'string',
                    ],
                ],
                'summary' => 'Get a layer\'s fields',
                'description' => 'Get a layer\'s fields',
            ],
        ],
    ],
    itemOperations: []
)]
class View
{
}
