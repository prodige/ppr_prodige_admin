<?php

declare(strict_types=1);

namespace App\Entity\Administration\Resource;

use Doctrine\ORM\Mapping as ORM;

/**
 * admin.rasterInfo
 */
#[ORM\Table(name: 'admin.raster_info')]
#[ORM\Entity]
class RasterInfo
{
    /**
     * @var int
     */
    #[ORM\Column(name: 'id', type: 'integer', nullable: false)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    private int $id;

    /**
     * @var string
     */
    #[ORM\Column(name: 'name', type: 'string', length: 128, nullable: false)]
    private string $name;

    /**
     * @var int|null
     */
    #[ORM\Column(name: 'srid', type: 'integer', nullable: true)]
    private ?int $srid;

    /**
     * @var float
     */
    #[ORM\Column(name: 'ulx', type: 'float', precision: 10, scale: 0, nullable: false)]
    private float $ulx;

    /**
     * @var float
     */
    #[ORM\Column(name: 'uly', type: 'float', precision: 10, scale: 0, nullable: false)]
    private float $uly;

    /**
     * @var float
     */
    #[ORM\Column(name: 'lrx', type: 'float', precision: 10, scale: 0, nullable: false)]
    private float $lrx;

    /**
     * @var float
     */
    #[ORM\Column(name: 'lry', type: 'float', precision: 10, scale: 0, nullable: false)]
    private float $lry;

    /**
     * @var int|null
     */
    #[ORM\Column(name: 'bands', type: 'integer', nullable: true, options: ['default' => 1])]
    private ?int $bands = 1;

    /**
     * @var int
     */
    #[ORM\Column(name: 'cols', type: 'bigint', nullable: false)]
    private int $cols = 0;

    /**
     * @var int
     */
    #[ORM\Column(name: 'rows', type: 'bigint', nullable: false)]
    private int $rows = 0;

    /**
     * @var float
     */
    #[ORM\Column(name: 'resx', type: 'float', precision: 10, scale: 0, nullable: false, options: ['default' => 1.0])]
    private float $resx = 1.0;

    /**
     * @var float
     */
    #[ORM\Column(name: 'resy', type: 'float', precision: 10, scale: 0, nullable: false, options: ['default' => 1.0])]
    private float $resy = 1.0;

    /**
     * @var string
     */
    #[ORM\Column(name: 'format', type: 'string', nullable: false, options: ['default' => 'GTIFF'])]
    private string $format = 'GTIFF';

    /**
     * @var string
     */
    #[ORM\Column(name: 'vrt_path', type: 'string', nullable: false)]
    private string $vrtPath;

    /**
     * @var string|null
     */
    #[ORM\Column(name: 'compression', type: 'string', nullable: true, options: ['default' => 'NONE'])]
    private ?string $compression = 'NONE';

    /**
     * @var int|null
     */
    #[ORM\Column(name: 'color_interp', type: 'integer', nullable: true, options: ['default' => 1])]
    private ?int $colorInterp = 1;

    /**
     * @var MetadataSheet
     */
    #[ORM\ManyToOne(targetEntity: MetadataSheet::class)]
    #[ORM\JoinColumn(name: 'metadata_sheet_id', referencedColumnName: 'id')]
    private MetadataSheet $metadataSheet;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSrid(): ?int
    {
        return $this->srid;
    }

    public function setSrid(?int $srid): self
    {
        $this->srid = $srid;

        return $this;
    }

    public function getUlx(): ?float
    {
        return $this->ulx;
    }

    public function setUlx(float $ulx): self
    {
        $this->ulx = $ulx;

        return $this;
    }

    public function getUly(): ?float
    {
        return $this->uly;
    }

    public function setUly(float $uly): self
    {
        $this->uly = $uly;

        return $this;
    }

    public function getLrx(): ?float
    {
        return $this->lrx;
    }

    public function setLrx(float $lrx): self
    {
        $this->lrx = $lrx;

        return $this;
    }

    public function getLry(): ?float
    {
        return $this->lry;
    }

    public function setLry(float $lry): self
    {
        $this->lry = $lry;

        return $this;
    }

    public function getBands(): ?int
    {
        return $this->bands;
    }

    public function setBands(?int $bands): self
    {
        $this->bands = $bands;

        return $this;
    }

    public function getCols(): ?int
    {
        return $this->cols;
    }

    public function setCols(int $cols): self
    {
        $this->cols = $cols;

        return $this;
    }

    public function getRows(): ?int
    {
        return $this->rows;
    }

    public function setRows(int $rows): self
    {
        $this->rows = $rows;

        return $this;
    }

    public function getResx(): ?float
    {
        return $this->resx;
    }

    public function setResx(float $resx): self
    {
        $this->resx = $resx;

        return $this;
    }

    public function getResy(): ?float
    {
        return $this->resy;
    }

    public function setResy(float $resy): self
    {
        $this->resy = $resy;

        return $this;
    }

    public function getFormat(): ?string
    {
        return $this->format;
    }

    public function setFormat(string $format): self
    {
        $this->format = $format;

        return $this;
    }

    public function getVrtPath(): ?string
    {
        return $this->vrtPath;
    }

    public function setVrtPath(string $vrtPath): self
    {
        $this->vrtPath = $vrtPath;

        return $this;
    }

    public function getCompression(): ?string
    {
        return $this->compression;
    }

    public function setCompression(?string $compression): self
    {
        $this->compression = $compression;

        return $this;
    }

    public function getColorInterp(): ?int
    {
        return $this->colorInterp;
    }

    public function setColorInterp(?int $colorInterp): self
    {
        $this->colorInterp = $colorInterp;

        return $this;
    }

    public function getMetadataSheet(): MetadataSheet
    {
        return $this->metadataSheet;
    }

    public function setMetadataSheet(MetadataSheet $metadataSheet): void
    {
        $this->metadataSheet = $metadataSheet;
    }

}
