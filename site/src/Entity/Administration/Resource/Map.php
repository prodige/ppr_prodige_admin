<?php

declare(strict_types=1);

namespace App\Entity\Administration\Resource;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Serializer\Filter\PropertyFilter;
use App\Controller\Administration\Resource\MapByUuidController;
use App\Entity\Administration\Directory\Skill;
use App\Entity\Administration\Directory\User\User;
use App\Entity\Lex\LexMapFormat;
use App\Entity\Trait\TimestampsTrait;
use App\Filter\FullTextSearchFilter;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Map
 */
#[
    ApiResource(
        collectionOperations: ['get', 'post'],
        itemOperations: [
            'get',
            'patch',
            'delete',
            'getMapByUuid' => [
                'method' => 'GET',
                'path' => '/maps/uuid/{uuid}',
                'controller' => MapByUuidController::class,
                'openapi_context' => [
                    'summary' => 'Get map by metadata uuid',
                    'description' => 'Get map by metadata uuid',
                    'parameters' => [
                        [
                            "name" => "uuid",
                            "in" => "path",
                            "description" => "Uuid",
                            "required" => true,
                            "schema" => ["type" => "string"]
                        ],
                        [
                            "name" => "id",
                            "in" => "path",
                            "required" => false
                        ]
                    ]
                ],
                'normalization_context' => ['groups' => 'GetMap', "skip_null_values" => false],
                'read' => false
            ]
        ],
        denormalizationContext: ['groups' => ['PostMap']],
        normalizationContext: ['groups' => ['GetMap'], "skip_null_values" => false]
    )]
#[ApiFilter(SearchFilter::class, properties: [
    'name' => 'ipartial',
    'description' => 'ipartial',
    'lexMapFormat.name' => 'ipartial',
    'lexLayerType.name' => 'ipartial',
    'path' => 'ipartial',
    'sheetMetadata.publicMetadataId' => 'exact'
])]
#[ApiFilter(FullTextSearchFilter::class, properties: [
    'name' => 'ipartial',
    'description' => 'ipartial',
    'lexMapFormat.name' => 'ipartial',
    'lexLayerType.name' => 'ipartial',
    'path' => 'ipartial',
    'sheetMetadata' => 'exact'
])]
#[ApiFilter(OrderFilter::class, properties: [
    'name',
    'description',
    'lexMapFormat.name',
    'lexLayerType.name',
    'path',
    'state',
])]
#[ApiFilter(PropertyFilter::class)]
#[ORM\Table(name: 'map', schema: 'admin')]
#[ORM\Entity]
#[ORM\HasLifecycleCallbacks]
class Map
{
    use TimestampsTrait;

    /**
     * @var int
     */
    #[ORM\Column(name: 'id', type: 'integer', nullable: false)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    #[Groups(['GetMap'])]
    private int $id;

    /**
     * @var string
     */
    #[ORM\Column(name: 'path', type: 'string', length: 1024, nullable: false)]
    #[Groups(['GetMap', 'PostMap'])]
    #[Assert\Type('string')]
    private string $path;

    /**
     * @var string
     */
    #[ORM\Column(name: 'name', type: 'string', length: 255, nullable: false)]
    #[Groups(['GetMap', 'GetSkill', 'PostMap'])]
    #[Assert\Type('string')]
    private string $name;

    /**
     * @var string|null
     */
    #[ORM\Column(name: 'description', type: 'text', nullable: true)]
    #[Groups(['GetMap', 'PostMap'])]
    #[Assert\Type('string')]
    private ?string $description;

    /**
     * @var bool
     */
    #[ORM\Column(name: 'state', type: 'boolean', nullable: true, options: [
        'default' => false,
        'description' => 'true=carte proposée, false carte en cours',
    ])]
    #[Groups(['GetMap'])]
    #[Assert\Type('bool')]
    private bool $state = false;

    /**
     * @var string|null
     */
    #[ORM\Column(name: 'user_email', type: 'string', length: 128, nullable: true)]
    #[Groups(['GetMap', 'PostMap'])]
    #[Assert\Type('string')]
    private ?string $userEmail;

    /**
     * @var int|null
     */
    #[ORM\Column(name: 'public_metadata_wms', type: 'integer', nullable: true, options: ['default' => null])]
    #[Groups(['GetMap', 'PostMap'])]
    private ?int $publicMetadataWms;

    /**
     * @var MetadataSheet
     */
    #[ORM\ManyToOne(targetEntity: MetadataSheet::class, inversedBy: 'maps')]
    #[ORM\JoinColumn(name: 'metadata_sheet_id', referencedColumnName: 'id')]
    #[Groups(['GetMap', 'PostMap'])]
    private MetadataSheet $sheetMetadata;

    /**
     * @var User
     */
    #[ORM\ManyToOne(targetEntity: User::class)]
    #[ORM\JoinColumn(name: 'user_id', referencedColumnName: 'id')]
    #[Groups(['GetMap', 'PostMap'])]
    private User $user;

    /**
     * @var User
     */
    #[ORM\ManyToOne(targetEntity: User::class)]
    #[ORM\JoinColumn(name: 'admin_id', referencedColumnName: 'id')]
    #[Groups(['GetMap'])]
    private User $admin;

    /**
     * @var LexMapFormat
     */
    #[ORM\ManyToOne(targetEntity: LexMapFormat::class)]
    #[ORM\JoinColumn(name: 'lex_map_format_id', referencedColumnName: 'id')]
    #[Groups(['GetMap', 'PostMap'])]
    private LexMapFormat $lexMapFormat;

    /**
     * @var Collection
     */
    #[ORM\ManyToMany(targetEntity: Skill::class, inversedBy: 'maps')]
    #[ORM\JoinTable(name: 'skills_maps', schema: 'admin')]
    #[Groups(['GetMap'])]
    private Collection $skills;

    /**
     * @var string|null
     */
    #[ORM\Column(name: 'url_referer', type: 'string', length: 128, nullable: true)]
    #[Groups(['GetMap', 'PostMap'])]
    #[Assert\Type('string')]
    private ?string $urlReferer;

    #[Groups(['GetMap'])]
    private bool $wmsStatus;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getPath(): ?string
    {
        return $this->path;
    }

    /**
     * @param string $path
     * @return $this
     */
    public function setPath(string $path): self
    {
        $this->path = $path;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string|null $description
     * @return $this
     */
    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return bool|null
     */
    public function getState(): ?bool
    {
        return $this->state;
    }

    /**
     * @param bool|null $state
     * @return $this
     */
    public function setState(?bool $state): self
    {
        $this->state = $state;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getUserEmail(): ?string
    {
        return $this->userEmail;
    }

    /**
     * @param string|null $userEmail
     * @return $this
     */
    public function setUserEmail(?string $userEmail): self
    {
        $this->userEmail = $userEmail;

        return $this;
    }

    /**
     * @return MetadataSheet|null
     */
    public function getSheetMetadata(): ?MetadataSheet
    {
        return $this->sheetMetadata;
    }

    /**
     * @param MetadataSheet|null $sheetMetadata
     * @return $this
     */
    public function setSheetMetadata(?MetadataSheet $sheetMetadata): self
    {
        $this->sheetMetadata = $sheetMetadata;

        return $this;
    }

    /**
     * @return User|null
     */
    public function getUser(): ?User
    {
        return $this->user;
    }

    /**
     * @param User|null $user
     * @return $this
     */
    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return User|null
     */
    public function getAdmin(): ?User
    {
        return $this->admin;
    }

    /**
     * @param User|null $admin
     * @return $this
     */
    public function setAdmin(?User $admin): self
    {
        $this->admin = $admin;

        return $this;
    }

    /**
     * @return LexMapFormat|null
     */
    public function getLexMapFormat(): ?LexMapFormat
    {
        return $this->lexMapFormat;
    }

    /**
     * @param LexMapFormat|null $lexMapFormat
     * @return $this
     */
    public function setLexMapFormat(?LexMapFormat $lexMapFormat): self
    {
        $this->lexMapFormat = $lexMapFormat;

        return $this;
    }

    /**
     * @return Collection
     */
    public function getSkills(): Collection
    {
        return $this->skills;
    }

    public function addSkill(Skill $skill): self
    {
        if (!$this->skills->contains($skill)) {
            $this->skills[] = $skill;
            $skill->addMap($this);
        }

        return $this;
    }

    public function removeSkill(Skill $skill): self
    {
        if ($this->skills->removeElement($skill)) {
            $skill->removeMap($this);
        }

        return $this;
    }

    public function getUrlReferer(): ?string
    {
        return $this->urlReferer;
    }

    public function setUrlReferer(?string $urlReferer): Map
    {
        $this->urlReferer = $urlReferer;
        return $this;
    }

    public function isWmsStatus(): bool
    {
        if ($this->getPublicMetadataWms() !== null) {
            return true;
        }
        return false;
    }

    /**
     * @return int|null
     */
    public function getPublicMetadataWms(): ?int
    {
        return $this->publicMetadataWms;
    }

    /**
     * @param int|null $publicMetadataWms
     */
    public function setPublicMetadataWms(?int $publicMetadataWms): void
    {
        $this->publicMetadataWms = $publicMetadataWms;
    }

}
