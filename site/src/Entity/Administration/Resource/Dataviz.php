<?php

declare(strict_types=1);

namespace App\Entity\Administration\Resource;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Serializer\Filter\PropertyFilter;
use App\Entity\Administration\Directory\Skill;
use App\Entity\Administration\Directory\User\User;
use App\Entity\Lex\LexDatavizType;
use App\Entity\Lex\LexMapFormat;
use App\Entity\Trait\TimestampsTrait;
use App\Filter\FullTextSearchFilter;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Dataviz
 */
#[ApiResource(
    collectionOperations: ['get', 'post'],
    itemOperations: [
        'get',
        'patch',
        'delete'
    ],
    denormalizationContext: ['groups' => ['PostDataviz']],
    normalizationContext: ['groups' => ['GetDataviz']]
)]
#[ApiFilter(SearchFilter::class, properties: ['name' => 'ipartial',
    'description' => 'ipartial',
    'LexDatavizType.name' => 'ipartial',
    'path' => 'ipartial',
    'sheetMetadata.publicMetadataId' => 'exact'])]
#[ApiFilter(FullTextSearchFilter::class, properties: [
    'name' => 'ipartial',
    'description' => 'ipartial',
    'LexDatavizType.name' => 'ipartial',
    'sheetMetadata' => 'exact'
])]
#[ApiFilter(OrderFilter::class, properties: [
    'name',
    'description',
    'LexDatavizType.name',
])]
#[ApiFilter(PropertyFilter::class)]
#[ORM\Table(name: 'dataviz', schema: 'admin')]
#[ORM\Entity]
#[ORM\HasLifecycleCallbacks]
class Dataviz
{
    use TimestampsTrait;

    /**
     * @var int
     */
    #[ORM\Column(name: 'id', type: 'integer', nullable: false)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    #[Groups(['GetDataviz'])]
    private int $id;

    /**
     * @var string
     */
    #[ORM\Column(name: 'title', type: 'string', length: 255, nullable: false)]
    #[Groups(['GetDataviz', 'GetSkill', 'PostDataviz'])]
    #[Assert\Type('string')]
    private string $title;

    /**
     * @var string|null
     */
    #[ORM\Column(name: 'description', type: 'text', nullable: true)]
    #[Groups(['GetDataviz', 'PostDataviz'])]
    #[Assert\Type('string')]
    private ?string $description;

    #[ORM\Column(name: 'param_chart', type: 'json', nullable: true)]
    #[Groups(['GetDataviz', 'PostDataviz'])]
    private ?array $paramChart = [];

    #[ORM\Column(name: 'param_traitement', type: 'json', nullable: true)]
    #[Groups(['GetDataviz', 'PostDataviz'])]
    private ?array $paramTraitement = [];

    /**
     * @var MetadataSheet
     */
    #[ORM\ManyToOne(targetEntity: MetadataSheet::class, inversedBy: 'dataviz')]
    #[ORM\JoinColumn(name: 'metadata_sheet_id', referencedColumnName: 'id')]
    #[Groups(['GetDataviz', 'PostDataviz'])]
    private MetadataSheet $sheetMetadata;

    /**
     * @var Layer
     */
    #[ORM\ManyToOne(targetEntity: Layer::class, inversedBy: 'datavizs')]
    #[ORM\JoinColumn(name: 'layer_id', referencedColumnName: 'id')]
    #[Groups(['GetDataviz', 'PostDataviz'])]
    private Layer $layer;

    /**
     * @var User
     */
    #[ORM\ManyToOne(targetEntity: User::class)]
    #[ORM\JoinColumn(name: 'user_id', referencedColumnName: 'id')]
    #[Groups(['GetDataviz', 'PostDataviz'])]
    private User $creator;

    /**
     * @var LexDatavizType
     */
    #[ORM\ManyToOne(targetEntity: LexDatavizType::class)]
    #[ORM\JoinColumn(name: 'lex_dataviz_type_id', referencedColumnName: 'id')]
    #[Groups(['GetDataviz', 'PostDataviz'])]
    private LexDatavizType $lexDatavizType;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return Dataviz
     */
    public function setTitle(string $title): Dataviz
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string|null $description
     * @return Dataviz
     */
    public function setDescription(?string $description): Dataviz
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return MetadataSheet
     */
    public function getSheetMetadata(): MetadataSheet
    {
        return $this->sheetMetadata;
    }

    /**
     * @param MetadataSheet $sheetMetadata
     * @return Dataviz
     */
    public function setSheetMetadata(MetadataSheet $sheetMetadata): Dataviz
    {
        $this->sheetMetadata = $sheetMetadata;
        return $this;
    }

    /**
     * @return Layer
     */
    public function getLayer(): Layer
    {
        return $this->layer;
    }

    /**
     * @param Layer $layer
     * @return Dataviz
     */
    public function setLayer(Layer $layer): Dataviz
    {
        $this->layer = $layer;
        return $this;
    }

    /**
     * @return User
     */
    public function getCreator(): User
    {
        return $this->creator;
    }

    /**
     * @param User $creator
     * @return Dataviz
     */
    public function setCreator(User $creator): Dataviz
    {
        $this->creator = $creator;
        return $this;
    }

    /**
     * @return LexDatavizType
     */
    public function getLexDatavizType(): LexDatavizType
    {
        return $this->lexDatavizType;
    }

    /**
     * @param LexDatavizType $lexDatavizType
     * @return Dataviz
     */
    public function setLexDatavizType(LexDatavizType $lexDatavizType): Dataviz
    {
        $this->lexDatavizType = $lexDatavizType;
        return $this;
    }

    /**
     * @return array|null
     */
    public function getParamChart(): ?array
    {
        return $this->paramChart;
    }

    /**
     * @param array|null $paramChart
     * @return Dataviz
     */
    public function setParamChart(?array $paramChart): Dataviz
    {
        $this->paramChart = $paramChart;
        return $this;
    }


    /**
     * @return array|null
     */
    public function getParamTraitement(): ?array
    {
        return $this->paramTraitement;
    }

    /**
     * @param array|null $paramTraitement
     * @return Dataviz
     */
    public function setParamTraitement(?array $paramTraitement): Dataviz
    {
        $this->paramTraitement = $paramTraitement;
        return $this;
    }


}
