<?php

declare(strict_types=1);

namespace App\Entity\Administration\Resource;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Serializer\Filter\PropertyFilter;
use App\Controller\Administration\Resource\LayerByUuidController;
use App\Entity\Administration\Directory\Profile\ProfilePrivilegeLayer;
use App\Entity\Administration\Directory\Skill;
use App\Entity\Administration\Directory\Zoning;
use App\Entity\Lex\LexLayerType;
use App\Entity\Trait\TimestampsTrait;
use App\Filter\FullTextSearchFilter;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use DateTime;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Admin.layer
 */
#[ApiResource(
    itemOperations: [
        'get',
        'patch' => [
            'denormalization_context' => ['groups' => ['PatchLayer']],
        ],
        'get_fields' => [
            'method' => 'GET',
            'path' => '/layers/{id}/fields',
            'openapi_context' => [
                'summary' => 'Get fields from layer',
                'description' => 'Get fields from layer',
            ],
            'normalization_context' => ['groups' => 'GetLayerFields'],
        ],
        'delete',
        'getFieldByUuid' => [
            'method' => 'GET',
            'path' => '/layers/uuid/{uuid}',
            'controller' => LayerByUuidController::class,
            'openapi_context' => [
                'summary' => 'Get layer by metadata uuid',
                'description' => 'Get layer by metadata uuid',
                'parameters' => [
                    [
                        "name" => "uuid",
                        "in" => "path",
                        "description" => "Uuid",
                        "required" => true,
                        "schema" => ["type" => "string"]
                    ],
                    [
                        "name" => "id",
                        "in" => "path",
                        "required" => false
                    ]
                ]
            ],
            'normalization_context' => ['groups' => 'GetLayer', "skip_null_values" => false],
            'read' => false
        ]
    ],
    attributes: ['filters' => ['layer.name']],
    denormalizationContext: ['groups' => ['PostLayer']],
    normalizationContext: ['groups' => ['GetLayer'], "skip_null_values" => false]
)]
#[ApiFilter(SearchFilter::class, properties: [
    'name' => 'ipartial',
    'lexLayerType.name' => 'ipartial',
    'lexLayerType.id' => 'exact',
    'storagePath' => 'exact',
    'sheetMetadata.publicMetadataId' => 'exact'
])]
#[ApiFilter(FullTextSearchFilter::class, properties: ['name' => 'ipartial', 'lexLayerType.name' => 'ipartial'])]
#[ApiFilter(OrderFilter::class, properties: ['name' => 'ASC', 'lexLayerType.name', 'lastImport', 'wms', 'wfs'])]
#[ApiFilter(PropertyFilter::class)]
#[ORM\Table(name: 'layer', schema: 'admin')]
#[ORM\Entity]
#[UniqueEntity(fields: 'storagePath')]
#[ORM\HasLifecycleCallbacks]
class Layer
{
    use TimestampsTrait;

    /**
     * @var int
     */
    #[ORM\Column(name: 'id', type: 'integer', nullable: false)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    #[Groups(['GetLayer', 'GetMetadataSheet', 'GetLayerFields', 'OutputMetadataSheet', 'GetDataviz'])]
    private int $id;

    /**
     * @var string
     */
    #[ORM\Column(name: 'name', type: 'string', length: 255, nullable: false)]
    #[Groups([
        'PostLayer',
        'GetLayer',
        'GetSkill',
        'GetMetadataSheet',
        'GetProfilePrivilegeObject',
        'GetProfilePrivilegeLayer',
        'GetZoning',
        'GetUserAreaAlert',
        'GetEngine',
        'GetLayerFields',
        'PatchLayer',
        'OutputMetadataSheet',
        'GetDataviz'
    ])]
    #[Assert\Type('string')]
    private string $name;

    /**
     * @var string|null
     */
    #[ORM\Column(name: 'description', type: 'string', length: 2048, nullable: true)]
    #[Groups(['PostLayer', 'PatchLayer', 'GetLayer', 'OutputMetadataSheet'])]
    #[Assert\Type('string')]
    private ?string $description;

    /**
     * @var string|null
     */
    #[ORM\Column(name: 'storage_path', type: 'string', length: 1024, unique: true, nullable: true)]
    #[Groups(['PostLayer', 'PatchLayer', 'GetLayer', 'GetEngine', 'OutputMetadataSheet'])]
    #[Assert\Type('string')]
    private ?string $storagePath;

    /**
     * @var string|null
     */
    #[ORM\Column(name: 'wfs_metadata_uuid', type: 'string', length: 255, nullable: true)]
    #[Groups(['PostLayer', 'PatchLayer', 'GetLayer', 'OutputMetadataSheet'])]
    #[Assert\Type('string')]
    private ?string $wfsMetadataUuid = null;

    /**
     * @var bool
     */
    #[ORM\Column(name: 'wms', type: 'boolean', nullable: false, options: ['default' => false])]
    #[Groups(['PostLayer', 'PatchLayer', 'GetLayer', 'OutputMetadataSheet'])]
    #[Assert\Type('bool')]
    private bool $wms = false;

    /**
     * @var bool
     */
    #[ORM\Column(name: 'wfs', type: 'boolean', nullable: false, options: ['default' => false])]
    #[Groups(['PostLayer', 'PatchLayer', 'GetLayer', 'OutputMetadataSheet'])]
    #[Assert\Type('bool')]
    private bool $wfs = false;

    /**
     * @var bool
     */
    #[ORM\Column(name: 'download', type: 'boolean', nullable: false, options: ['default' => false])]
    #[Groups(['PostLayer', 'PatchLayer', 'GetLayer', 'OutputMetadataSheet'])]
    #[Assert\Type('bool')]
    private bool $download = false;

    /**
     * @var bool
     */
    #[ORM\Column(name: 'restriction', type: 'boolean', nullable: false, options: ['default' => false])]
    #[Groups(['PostLayer', 'PatchLayer', 'GetLayer', 'OutputMetadataSheet'])]
    #[Assert\Type('bool')]
    private bool $restriction = false;

    /**
     * @var bool
     */
    #[ORM\Column(name: 'exclusive_restriction', type: 'boolean', nullable: false, options: ['default' => false])]
    #[Groups(['PostLayer', 'PatchLayer', 'GetLayer', 'OutputMetadataSheet'])]
    #[Assert\Type('bool')]
    private bool $exclusiveRestriction = false;

    /**
     * @var float|null
     */
    #[ORM\Column(name: 'buffer_restriction', type: 'float', precision: 10, scale: 0, nullable: true)]
    #[Groups(['PostLayer', 'PatchLayer', 'GetLayer', 'OutputMetadataSheet'])]
    #[Assert\Type('float')]
    private ?float $bufferRestriction;

    /**
     * @var bool
     */
    #[ORM\Column(name: 'viewable', type: 'boolean', nullable: false, options: ['default' => '1'])]
    #[Groups(['PostLayer', 'PatchLayer', 'GetLayer', 'OutputMetadataSheet'])]
    #[Assert\Type('bool')]
    private bool $viewable = true;

    /**
     * @var bool
     */
    #[ORM\Column(name: 'assigned_extraction', type: 'boolean', nullable: false, options: ['default' => false])]
    #[Groups(['PostLayer', 'PatchLayer', 'GetLayer', 'OutputMetadataSheet'])]
    #[Assert\Type('bool')]
    private bool $assignedExtraction = false;

    /**
     * @var string|null
     */
    #[ORM\Column(name: 'field_assigned_extraction', type: 'string', nullable: true)]
    #[Groups(['PostLayer', 'PatchLayer', 'GetLayer'])]
    #[Assert\Type('string')]
    private ?string $fieldAssignedExtraction;

    /**
     * @var DateTime
     */
    #[ORM\Column(name: 'last_import', type: 'datetime', nullable: true)]
    #[Groups(['PostLayer', 'GetLayer', 'OutputMetadataSheet', 'PatchLayer'])]
    private DateTime $lastImport;

    #[Groups(['GetLayer', 'OutputMetadataSheet', 'GetDataviz'])]
    #[Assert\Type('string')]
    private ?string $metadataUuid;

    /**
     * @var MetadataSheet
     */
    #[ORM\ManyToOne(targetEntity: MetadataSheet::class, inversedBy: 'layers')]
    #[ORM\JoinColumn(name: 'metadata_sheet_id', referencedColumnName: 'id')]
    #[Groups(['PostLayer', 'GetLayer'])]
    #[Assert\NotNull]
    private MetadataSheet $sheetMetadata;

    /**
     * @var LexLayerType
     */
    #[ORM\ManyToOne(targetEntity: LexLayerType::class)]
    #[ORM\JoinColumn(name: 'lex_layer_type_id', referencedColumnName: 'id')]
    #[Groups(['PostLayer', 'GetLayer', 'OutputMetadataSheet'])]
    #[Assert\NotNull]
    private LexLayerType $lexLayerType;

    /**
     * @var Zoning|null
     */
    #[ORM\ManyToOne(targetEntity: Zoning::class)]
    #[ORM\JoinColumn(name: 'zoning_id', referencedColumnName: 'id')]
    #[Groups(['PostLayer', 'PatchLayer', 'GetLayer'])]
    private ?Zoning $zoning;

    /**
     * @var Collection
     */
    #[ORM\OneToMany(mappedBy: 'layer', targetEntity: ProfilePrivilegeLayer::class, orphanRemoval: true)]
    private Collection $profileLayerPrivilege;

    /**
     * @var Collection
     */
    #[ORM\ManyToMany(targetEntity: Skill::class, inversedBy: 'layers')]
    #[ORM\JoinTable(name: 'skills_layers', schema: 'admin')]
    private Collection $skills;

    #[Groups(['GetLayerFields'])]
    private ?array $dbFields;

    #[ORM\Column(name: 'wms_sdom_id', type: 'integer[]', nullable: true)]
    #[Groups(['PostLayer', 'PatchLayer', 'GetLayer'])]
    private ?array $wmsSdomId = null;

    #[ORM\Column(name: 'cgu_display', type: 'boolean', nullable: true, options: ['default' => false])]
    #[Groups(['GetLayer','PostLayer', 'PatchLayer'])]
    #[Assert\Type('bool')]
    private bool $cguDisplay = false;

    #[ORM\Column(name: 'cgu_message', type: 'string', length: 2048, nullable: true)]
    #[Groups(['GetLayer','PostLayer', 'PatchLayer'])]
    #[Assert\Type('string')]
    private ?string $cguMessage = '';

    #[ORM\Column(name: 'synchronized_layer', type: 'boolean', nullable: true, options: ['default' => false])]
    #[Groups(['GetLayer','PostLayer', 'PatchLayer'])]
    #[Assert\Type('bool')]
    private ?bool $synchronizedLayer = false;

    #[ORM\OneToMany(mappedBy: 'layer', targetEntity: Dataviz::class, orphanRemoval: true)]
    #[ORM\Column(name: 'api_layer', type: 'boolean', nullable: false, options: ['default' => false])]
    #[Groups(['GetLayer','PostLayer', 'PatchLayer'])]
    #[Assert\Type('bool')]
    private bool $apiLayer = false;

    #[ORM\Column(name: 'api_fields', type: 'json', nullable: true)]
    #[Groups(['GetLayer','PostLayer', 'PatchLayer'])]
    private ?array $apiFields;

    #[ORM\OneToMany(mappedBy: 'layer', targetEntity: Dataviz::class, orphanRemoval: true)]
    #[Groups(['GetLayer'])]
    private Collection $datavizs;

    /**
     * @var bool
     */
    #[ORM\Column(name: 'wms_secure', type: 'boolean', nullable: true, options: ['default' => false])]
    #[Groups(['PatchLayer', 'GetLayer'])]
    #[Assert\Type('bool')]
    private ?bool $wmsSecure = false;

    /**
     * @var bool
     */
    #[ORM\Column(name: 'wms_secure_ip_filter', type: 'boolean', nullable: true, options: ['default' => false])]
    #[Groups(['PatchLayer', 'GetLayer'])]
    #[Assert\Type('bool')]
    private ?bool $wmsSecureIpFilter = false;

    /**
     * @var bool
     */
    #[ORM\Column(name: 'wfs_secure', type: 'boolean', nullable: true, options: ['default' => false])]
    #[Groups(['PatchLayer', 'GetLayer'])]
    #[Assert\Type('bool')]
    private ?bool $wfsSecure = false;

    /**
     * @var bool
     */
    #[ORM\Column(name: 'wfs_secure_ip_filter', type: 'boolean', nullable: true, options: ['default' => false])]
    #[Groups(['PatchLayer', 'GetLayer'])]
    #[Assert\Type('bool')]
    private ?bool $wfsSecureIpFilter = false;

    public function __construct()
    {
        $this->skills = new ArrayCollection();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getStoragePath(): ?string
    {
        return $this->storagePath;
    }

    public function setStoragePath(?string $storagePath): self
    {
        $this->storagePath = $storagePath;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getWfsMetadataUuid(): ?string
    {
        return $this->wfsMetadataUuid;
    }

    /**
     * @param string|null $wfsMetadataUuid
     * @return Layer
     */
    public function setWfsMetadataUuid(?string $wfsMetadataUuid): Layer
    {
        $this->wfsMetadataUuid = $wfsMetadataUuid;
        return $this;
    }


    public function getWms(): ?bool
    {
        return $this->wms;
    }

    public function setWms(bool $wms): self
    {
        $this->wms = $wms;

        return $this;
    }

    public function getWfs(): ?bool
    {
        return $this->wfs;
    }

    public function setWfs(bool $wfs): self
    {
        $this->wfs = $wfs;

        return $this;
    }

    public function getDownload(): ?bool
    {
        return $this->download;
    }

    public function setDownload(bool $download): self
    {
        $this->download = $download;

        return $this;
    }

    public function getRestriction(): ?bool
    {
        return $this->restriction;
    }

    public function setRestriction(bool $restriction): self
    {
        $this->restriction = $restriction;

        return $this;
    }

    public function getExclusiveRestriction(): ?bool
    {
        return $this->exclusiveRestriction;
    }

    public function setExclusiveRestriction(bool $exclusiveRestriction): self
    {
        $this->exclusiveRestriction = $exclusiveRestriction;

        return $this;
    }

    public function getBufferRestriction(): ?float
    {
        return $this->bufferRestriction;
    }

    public function setBufferRestriction(?float $bufferRestriction): self
    {
        $this->bufferRestriction = $bufferRestriction;

        return $this;
    }

    public function getViewable(): ?bool
    {
        return $this->viewable;
    }

    public function setViewable(bool $viewable): self
    {
        $this->viewable = $viewable;

        return $this;
    }

    public function getAssignedExtraction(): ?bool
    {
        return $this->assignedExtraction;
    }

    public function setAssignedExtraction(bool $assignedExtraction): self
    {
        $this->assignedExtraction = $assignedExtraction;

        return $this;
    }

    public function getFieldAssignedExtraction(): ?string
    {
        return $this->fieldAssignedExtraction;
    }

    public function setFieldAssignedExtraction(?string $fieldAssignedExtraction): self
    {
        $this->fieldAssignedExtraction = $fieldAssignedExtraction === null ? null : strip_tags(
            $fieldAssignedExtraction
        );

        return $this;
    }

    public function getLastImport(): ?\DateTime
    {
        return $this->lastImport;
    }

    public function setLastImport(\DateTime $lastImport): self
    {
        $this->lastImport = $lastImport;

        return $this;
    }

    public function getLexLayerType(): ?LexLayerType
    {
        return $this->lexLayerType;
    }

    public function setLexLayerType(?LexLayerType $lexLayerType): self
    {
        $this->lexLayerType = $lexLayerType;

        return $this;
    }

    public function getZoning(): ?Zoning
    {
        return $this->zoning;
    }

    public function setZoning(?Zoning $zoning): self
    {
        $this->zoning = $zoning;

        return $this;
    }

    #[Groups(['GetLayer', 'GetProfilePrivilegeObject'])]
    public function getIsObjectManagement(): bool
    {
        return (count($this->getSheetMetadata()->getProfilePrivilegeObjects()) != 0);
    }

    public function getSheetMetadata(): ?MetadataSheet
    {
        return $this->sheetMetadata;
    }

    public function setSheetMetadata(?MetadataSheet $sheetMetadata): self
    {
        $this->sheetMetadata = $sheetMetadata;

        return $this;
    }

    #[Groups(['GetLayer'])]
    public function getIsAwardeeManagement(): bool
    {
        return (count($this->getProfileLayerPrivilege()) != 0);
    }

    /**
     * @return Collection
     */
    public function getProfileLayerPrivilege(): Collection
    {
        return $this->profileLayerPrivilege;
    }

    /**
     * @return Collection
     */
    public function getSkills(): Collection
    {
        return $this->skills;
    }

    public function addSkill(Skill $skill): self
    {
        if (!$this->skills->contains($skill)) {
            $this->skills[] = $skill;
            $skill->addLayer($this);
        }

        return $this;
    }

    public function removeSkill(Skill $skill): self
    {
        if ($this->skills->removeElement($skill)) {
            $skill->removeLayer($this);
        }

        return $this;
    }

    /**
     * @return array|null
     */
    public function getDbFields(): ?array
    {
        return $this->dbFields;
    }

    /**
     * @param array|null $dbFields
     * @return Layer
     */
    public function setDbFields(?array $dbFields): Layer
    {
        $this->dbFields = $dbFields;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getMetadataUuid(): ?string
    {
        return $this->metadataUuid;
    }

    /**
     * @param string|null $metadataUuid
     * @return Layer
     */
    public function setMetadataUuid(?string $metadataUuid): Layer
    {
        $this->metadataUuid = $metadataUuid;
        return $this;
    }

    /**
     * @return array|null
     */
    public function getWmsSdomId(): ?array
    {
        return $this->wmsSdomId;
    }

    /**
     * @param array|null $wmsSdomId
     * @return Layer
     */
    public function setWmsSdomId(?array $wmsSdomId): Layer
    {
        $this->wmsSdomId = $wmsSdomId;
        return $this;
    }

    /**
     * @return bool
     */
    public function isCguDisplay(): bool
    {
        return $this->cguDisplay;
    }

    /**
     * @param bool $cguDisplay
     * @return Layer
     */
    public function setCguDisplay(bool $cguDisplay): Layer
    {
        $this->cguDisplay = $cguDisplay;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getCguMessage(): ?string
    {
        return $this->cguMessage;
    }

    /**
     * @param string|null $cguMessage
     * @return Layer
     */
    public function setCguMessage(?string $cguMessage): Layer
    {
        $this->cguMessage = $cguMessage;
        return $this;
    }

    /**
     * @return bool|null
     */
    public function getSynchronizedLayer(): ?bool
    {
        return $this->synchronizedLayer;
    }

    /**
     * @param bool|null $synchronizedLayer
     * @return Layer
     */
    public function setSynchronizedLayer(?bool $synchronizedLayer): Layer
    {
        $this->synchronizedLayer = $synchronizedLayer;
        return $this;
    }

    public function getDatavizs(): ?Collection
    {
        return $this->datavizs;
    }

    public function addDataviz(Dataviz $dataviz): self
    {
        if (!$this->datavizs->contains($dataviz)) {
            $this->datavizs->add($dataviz);
        }

        return $this;
    }

    public function removeDataviz(Dataviz $dataviz): self
    {
        $this->datavizs->removeElement($dataviz);
    }

    public function isApiLayer(): bool
    {
        return $this->apiLayer;
    }

    public function setApiLayer(bool $apiLayer): Layer
    {
        $this->apiLayer = $apiLayer;
        return $this;
    }

    public function getApiFields(): ?array
    {
        return $this->apiFields;
    }

    public function setApiFields(?array $apiFields): Layer
    {
        $this->apiFields = $apiFields;
        return $this;
    }

    public function getWmsSecure(): ?bool
    {
        return $this->wmsSecure;
    }

    public function setWmsSecure(bool $wmsSecure): self
    {
        $this->wmsSecure = $wmsSecure;

        return $this;
    }

    public function getWmsSecureIpFilter(): ?bool
    {
        return $this->wmsSecureIpFilter;
    }

    public function setWmsSecureIpFilter(bool $wmsSecureIpFilter): self
    {
        $this->wmsSecureIpFilter = $wmsSecureIpFilter;

        return $this;
    }

    public function getWfsSecure(): ?bool
    {
        return $this->wfsSecure;
    }

    public function setWfsSecure(bool $wfsSecure): self
    {
        $this->wfsSecure = $wfsSecure;

        return $this;
    }

    public function getWfsSecureIpFilter(): ?bool
    {
        return $this->wfsSecureIpFilter;
    }

    public function setWfsSecureIpFilter(bool $wfsSecureIpFilter): self
    {
        $this->wfsSecureIpFilter = $wfsSecureIpFilter;

        return $this;
    }
}
