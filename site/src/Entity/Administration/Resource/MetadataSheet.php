<?php

declare(strict_types=1);

namespace App\Entity\Administration\Resource;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Entity\Administration\Directory\Profile\ProfilePrivilegeObject;
use App\Entity\Administration\Organisation\Subdomain;
use App\Entity\OGCFeatures\OGCCollection;
use App\Entity\OGCFeatures\OGCItem;
use App\Entity\Trait\TimestampsTrait;
use App\Repository\MetadataSheetRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JetBrains\PhpStorm\Pure;
use phpDocumentor\Reflection\Types\Boolean;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Admin.MetadataSheet
 */
#[ApiResource(
    itemOperations: [
        'delete',
        'get',
        'patch'
    ],
    denormalizationContext: ['groups' => ['InputMetadataSheet']],
    normalizationContext: ['groups' => ['OutputMetadataSheet']]
)]
#[ApiFilter(SearchFilter::class, properties: ['publicMetadataId' => 'exact'])]
#[ORM\Table(name: 'metadata_sheet', schema: 'admin')]
#[ORM\Entity(repositoryClass: MetadataSheetRepository::class)]
#[UniqueEntity(fields: 'publicMetadataId')]
#[ORM\HasLifecycleCallbacks]
class MetadataSheet
{
    use TimestampsTrait;

    /**
     * @var int
     */
    #[ORM\Column(name: 'id', type: 'integer', nullable: false)]
    #[Groups(['OutputMetadataSheet', 'GetLayer', 'GetMap', 'GetDataviz'])]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    #[ApiProperty(identifier: true)]
    private int $id;

    /**
     * Foreign key to public.metadata
     * @var int
     */
    #[ORM\Column(name: 'public_metadata_id', type: 'integer', unique: true, nullable: false)]
    #[Groups(['OutputMetadataSheet', 'InputMetadataSheet', 'DeleteMetadata', 'GetLayer', 'GetMap', 'GetDataviz'])]
    #[ApiProperty(
        openapiContext: [
            'type' => 'int',
            'example' => 1
        ]
    )]
    private int $publicMetadataId;

    /**
     * @var string|null
     */
    #[Groups(['OutputMetadataSheet'])]
    #[ORM\Column(name: 'schema', type: 'string', length: 128, nullable: true)]
    private ?string $schema;

    /**
     * @var Collection
     */
    #[ORM\ManyToMany(targetEntity: Subdomain::class, inversedBy: 'metadata_sheet')]
    #[ORM\JoinTable(name: 'subdomain_metadata_sheet', schema: 'admin')]
    #[Groups(['InputMetadataSheet', 'OutputMetadataSheet', 'GetLayer', 'GetMap', 'GetDataviz'])]
    private Collection $subdomains;

    /**
     * @var Collection
     */
    #[ORM\OneToMany(mappedBy: 'sheetMetadata', targetEntity: Map::class, orphanRemoval: true)]
    #[Groups(['OutputMetadataSheet'])]
    private Collection $maps;

    /**
     * @var Collection
     */
    #[ORM\OneToMany(mappedBy: 'sheetMetadata', targetEntity: Layer::class, orphanRemoval: true)]
    #[Groups(['GetProfilePrivilegeObject', 'OutputMetadataSheet'])]
    private Collection $layers;

    /**
     * @var Collection
     */
    #[ORM\OneToMany(mappedBy: 'sheetMetadata', targetEntity: OGCCollection::class, orphanRemoval: true)]
    private Collection $OGCCollection;

    /**
     * @var Collection
     */
    #[ORM\OneToMany(mappedBy: 'metadataSheet', targetEntity: ProfilePrivilegeObject::class, orphanRemoval: true)]
    #[Groups(['OutputMetadataSheet'])]
    private Collection $profilePrivilegeObjects;

    #[Groups(['OutputMetadataSheet'])]
    private array $typeOfMetadata;

    /**
     * @var Collection
     */
    #[ORM\OneToMany(mappedBy: 'sheetMetadata', targetEntity: Dataviz::class, orphanRemoval: true)]
    #[Groups(['OutputMetadataSheet'])]
    private Collection $datavizs;


    public function __construct()
    {
        $this->subdomains = new ArrayCollection();
        $this->maps = new ArrayCollection();
        $this->layers = new ArrayCollection();
        $this->OGCCollection = new ArrayCollection();
        $this->datavizs = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSchema(): ?string
    {
        return $this->schema;
    }

    public function setSchema(?string $schema): self
    {
        $this->schema = $schema;

        return $this;
    }

    public function getPublicMetadataId(): int
    {
        return $this->publicMetadataId;
    }

    public function setPublicMetadataId(int $publicMetadataId): void
    {
        $this->publicMetadataId = $publicMetadataId;
    }

    public function getProfilePrivilegeObjects(): Collection
    {
        return $this->profilePrivilegeObjects;
    }

    /**
     * @return Collection
     */
    public function getSubdomains(): Collection
    {
        return $this->subdomains;
    }

    public function addSubdomain(Subdomain $subdomain): self
    {
        if (!$this->subdomains->contains($subdomain)) {
            $this->subdomains[] = $subdomain;
        }

        return $this;
    }

    public function removeSubdomain(Subdomain $subdomain): self
    {
        $this->subdomains->removeElement($subdomain);

        return $this;
    }

    /**
     * @return bool[]
     */
    public function getTypeOfMetadata(): array
    {
        if ($this->isMap() === true) {
            return ["isMap" => $this->isMap()];
        }
        return ["isLayer" => $this->isLayer()];
    }

    #[Pure] public function isMap(): bool
    {
        return (bool)count($this->getMaps());
    }

    public function getMaps(): Collection
    {
        return $this->maps;
    }

    #[Pure] public function isLayer(): bool
    {
        return (bool)count($this->getLayers());
    }

    public function getLayers(): Collection
    {
        return $this->layers;
    }

    public function getDatavizs(): ?Collection
    {
        return $this->datavizs;
    }

    public function addDataviz(Dataviz $dataviz): self
    {
        if (!$this->datavizs->contains($dataviz)) {
            $this->datavizs->add($dataviz);
        }

        return $this;
    }

    public function removeDataviz(Dataviz $dataviz): self
    {
        $this->datavizs->removeElement($dataviz);

        return $this;
    }

    /**
     * Get the value of OGCCollection
     */ 
    public function getOGCCollection()
    {
        return $this->OGCCollection;
    }
}