<?php

declare(strict_types=1);

namespace App\Entity\Lex;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Entity\Administration\Directory\Profile\Profile;
use App\Repository\LexPrivilegeRepository;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Admin.LexPrivilege
 */
#[ApiResource(
    collectionOperations: ['get'],
    itemOperations: ['get'],
)]
#[ORM\Table(name: 'lex_privilege', schema: 'admin')]
#[ORM\Entity(repositoryClass: LexPrivilegeRepository::class)]
#[UniqueEntity(fields: 'name')]
class LexPrivilege
{
    /**
     * @var int
     *
     *
     */
    #[ORM\Column(name: 'id', type: 'integer', nullable: false)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    private int $id;

    /**
     * @var string
     */
    #[ORM\Column(name: 'name', type: 'string', length: 128, unique: true, nullable: false)]
    #[Groups(['GetProfile', 'GetProfilePrivilegeObject', 'GetProfilePrivilegeLayer'])]
    private string $name;

    /**
     * @var Collection
     */
    #[ORM\ManyToMany(targetEntity: Profile::class, mappedBy: 'privileges')]
    private Collection $profiles;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection
     */
    public function getProfiles(): Collection
    {
        return $this->profiles;
    }

    public function addPrivilege(Profile $profile): self
    {
        if (!$this->profiles->contains($profile)) {
            $this->profiles[] = $profile;
        }

        return $this;
    }

    public function removePrivilege(Profile $profile): self
    {
        $this->profiles->removeElement($profile);

        return $this;
    }
}
