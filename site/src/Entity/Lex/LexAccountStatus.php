<?php

namespace App\Entity\Lex;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;

#[ApiResource(
    collectionOperations: ['get'],
    itemOperations: ['get'],
    normalizationContext: ['groups' => ['GetStatus']]
)]
#[ORM\Table(name: 'lex_account_status', schema: 'admin')]
#[ORM\Entity]
#[UniqueEntity(fields: 'name')]
class LexAccountStatus
{
    /**
     * @var int
     *
     *
     */
    #[ORM\Column(name: 'id', type: 'integer', nullable: false)]
    #[ORM\Id]
    #[Groups(['GetUser','GetStatus'])]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    private int $id;

    /**
     * @var string
     */
    #[ORM\Column(name: 'name', type: 'string', length: 128, unique: true, nullable: false)]
    #[Groups(['GetUser','GetStatus'])]
    private string $name;

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): LexAccountStatus
    {
        $this->id = $id;
        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): LexAccountStatus
    {
        $this->name = $name;
        return $this;
    }


}