<?php

namespace App\Exception;

final class GeonetworkException extends \Exception
{
    public function __construct()
    {
        parent::__construct("Vous ne pouvez pas supprimer le dernier groupe de geonetwork. Un minimum d'un groupe est requis", 403);
    }
}