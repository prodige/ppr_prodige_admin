<?php

namespace App\Exception;

final class WrongCredentialException extends \Exception
{
    public function __construct()
    {
        parent::__construct("Wrong credential", 401);
    }
}