<?php

namespace App\Exception;

final class StructureDeleteException extends \Exception
{
    public function __construct()
    {
        parent::__construct("Vous ne pouvez pas supprimer cette structure. Elle posséde au minimum un utiliseur ou une sous structure", 403);
    }
}