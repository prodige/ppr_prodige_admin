<?php

namespace App\Exception;

final class CatalogueException extends \Exception
{
    public function __construct()
    {
        parent::__construct("Something is wrong with Catalogue", 400);
    }
}