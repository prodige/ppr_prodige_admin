<?php

namespace App\Exception;

use _PHPStan_82ef69dc5\Nette\Neon\Exception;

final class BadRequestException extends \Exception
{
    public function __construct()
    {
        parent::__construct("Bad request", 400);
    }
}