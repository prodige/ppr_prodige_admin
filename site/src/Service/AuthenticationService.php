<?php

namespace App\Service;

use App\Entity\Administration\Directory\User\User;
use App\Entity\Configuration\Setting;
use App\Exception\WrongCredentialException;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Doctrine\Persistence\ManagerRegistry;

/**
 * Service permettant l'utilisation de l'authentification OAuth2 du prodige cas.
 *
 * @author alkante
 */
class AuthenticationService
{
    private string $url;
    private string $port;

    private string $changePwdDelay;

    private string $adminPwdDelay;

    public function __construct(
        private HttpClientInterface $client,
        private ParameterBagInterface $bag,
        private MailerInterface $mailer,
        private TranslatorInterface $translator,
        private UrlGeneratorInterface $router,
        private ManagerRegistry $managerRegistry,
        private AlkRequestService $alkRequestService
    ) {
        $this->url = $bag->get('cas.host');
        $this->port = $bag->get('cas.port');
        $this->changePwdDelay = $bag->get('NEW_PASSWORD_DELAY');
        $this->adminPwdDelay = $bag->get('ADMIN_PASSWORD_DELAY');
    }

    /**
     * @throws TransportExceptionInterface
     * @throws WrongCredentialException
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ClientExceptionInterface
     */
    public function login(string $username, string $password): ?array
    {
        // Get Ticket from CAS API
        $response = $this->client->request(
            'POST',
            'https://' . $this->url . ':' . $this->port . '/v1/tickets',
            [
                'headers' => [
                    'Content-Type: application/x-www-form-urlencoded',
                    $this->bag->get('cas_mfa_token') . ': true',
                    'Accept: application/json',
                ],
                'body' => [
                    'username' => $username,
                    'password' => $password,
                    'token' => true
                ]
            ]
        );

        if (201 !== $response->getStatusCode()) {
            throw new WrongCredentialException();
        }

        return ['access_token' => $response->getContent(), 'token_type' => 'Bearer'];
    }

    public function sendMailForPassword(User $user, $isAdmin = false): void
    {
        if ($isAdmin) {
            $delay = $this->adminPwdDelay;
            $template = 'mail/admin_password_reset.html.twig';
        } else {
            $delay = $this->changePwdDelay;
            $template = 'mail/forgot_password_resp.html.twig';
        }

        $expiredTime = new \DateTime();
        $expiredTime->modify($delay);
        $strTokenPwd = $this->alkRequestService->getEncodeParam(
            'l=' . $user->getLogin() . '&p=' . base64_encode(time() . uniqid()) .
            '&e=' . $expiredTime->getTimestamp()
        );
        $signUpPage = $this->router->generate(
            'password_change',
            ['token' => $strTokenPwd],
            UrlGeneratorInterface::ABSOLUTE_URL
        );

        $setting = $this->managerRegistry->getRepository(Setting::class)->findOneBy(
            ['name' => 'PRO_CATALOGUE_EMAIL_AUTO']
        );

        $email = (new TemplatedEmail())
            ->from($setting->getValue())
            ->to($user->getEmail())
            ->replyTo($setting->getValue())
            ->priority(TemplatedEmail::PRIORITY_HIGH)
            ->subject('[Prodige] ' . $this->translator->trans('Password recovery'))
            ->htmlTemplate($template)
            ->context([
                'user' => $user,
                'signUpPage' => $signUpPage,
            ]);

        $this->mailer->send($email);
    }

    public function sendMailForAccount(User $user): void
    {
        $setting = $this->managerRegistry->getRepository(Setting::class)->findOneBy(
            ['name' => 'PRO_CATALOGUE_EMAIL_AUTO']
        );

        $body = implode('%0d%0a', [
            'Bonjour,',
            '',
            'Mon compte Prodige arrive à expiration le ' . $user->getAccountExpire()->format('Y-m-d H:i:s') . '. ',
            '',
            'Je souhaiterais prolonger mon accès au site Prodige. ',
            'Mon identifiant est ' . $user->getLogin(),
            '',
            'Cordialement, ',
            '',
            $user->getName() . ' ' . $user->getFirstName() . ' - ' . $user->getEmail(),
        ]);

        $mailto = 'mailto:' . $this->bag->get(
                'email.admin'
            ) . "?subject=Demande de prolongation des droits d'accès à Prodige&body=" . $body;

        $email = (new TemplatedEmail())
            ->from($setting->getValue())
            ->to($user->getEmail())
            ->replyTo($setting->getValue())
            ->priority(TemplatedEmail::PRIORITY_HIGH)
            ->subject('[Prodige] ' . $this->translator->trans('Account will expire'))
            ->htmlTemplate('mail/account_wil_expire.html.twig')
            ->context([
                'user' => $user,
                'mailto' => $mailto,
            ]);

        $this->mailer->send($email);
    }

}
