<?php

namespace App\Service;

use App\Entity\Administration\Directory\Structure;
use Doctrine\DBAL\Connection;
use Prodige\ProdigeBundle\Services\CurlUtilities;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class StructureService
{
    private Connection $prodigeCon;

    public function __construct(
        private GeonetworkService $geonetworkService
    ) {
    }

    /**
     * @param $data
     * @return array
     */
    public function createOrganisationAndContact($data): array
    {
        $fragXml = '<gmd:CI_ResponsibleParty xmlns:gmd="http://www.isotc211.org/2005/gmd"
                                                     xmlns:gco="http://www.isotc211.org/2005/gco"
                                                     xmlns:gts="http://www.isotc211.org/2005/gts"
                                                     xmlns:gml="http://www.opengis.net/gml/3.2">
                               <gmd:organisationName>
                                  <gco:CharacterString>' . $data->getName() . '</gco:CharacterString>
                               </gmd:organisationName>
                               <gmd:contactInfo>
                                  <gmd:CI_Contact>
                                        <gmd:phone>
                                           <gmd:CI_Telephone>
                                              <gmd:voice>
                                                 <gco:CharacterString>' . $data->getPhoneNumber() . '</gco:CharacterString>
                                              </gmd:voice>
                                           </gmd:CI_Telephone>
                                        </gmd:phone>
                                     <gmd:address>
                                        <gmd:CI_Address>
                                           <gmd:deliveryPoint>
                                              <gco:CharacterString>' . $data->getAddress() . '</gco:CharacterString>
                                           </gmd:deliveryPoint>
                                           <gmd:city>
                                              <gco:CharacterString>' . $data->getCity() . '</gco:CharacterString>
                                           </gmd:city>
                                           <gmd:administrativeArea>
                                              <gco:CharacterString/>
                                           </gmd:administrativeArea>
                                           <gmd:postalCode>
                                              <gco:CharacterString>' . $data->getZipCode() . '</gco:CharacterString>
                                           </gmd:postalCode>
                                           <gmd:country>
                                              <gco:CharacterString>' . $data->getCountry() . '</gco:CharacterString>
                                           </gmd:country>
                                           <gmd:electronicMailAddress>
                                              <gco:CharacterString>' . $data->getEmail() . '</gco:CharacterString>
                                           </gmd:electronicMailAddress>
                                        </gmd:CI_Address>
                                     </gmd:address>
                                  </gmd:CI_Contact>
                              </gmd:contactInfo>
                               <gmd:role/>
                            </gmd:CI_ResponsibleParty>';
        // Check si le contact existe chez Geonetwork
        $postXml = $this->geonetworkService->postXmlStructure($fragXml);
        if (array_key_exists('error', $postXml)) {
            return ['error' => $postXml['error'], 'status' => $postXml['status']];
        }

        $this->geonetworkService->publishXml($postXml['uuid']);

        return ["created" => true, "uuid" => $postXml['uuid']];
    }

    /**
     * @param $data
     * @return array
     */
    public function editOrganisationAndContact($data): array
    {
        /** @var Structure $data */
        $fragXml = '<gmd:CI_ResponsibleParty xmlns:gmd="http://www.isotc211.org/2005/gmd"
                                                     xmlns:gco="http://www.isotc211.org/2005/gco"
                                                     xmlns:gts="http://www.isotc211.org/2005/gts"
                                                     xmlns:gml="http://www.opengis.net/gml/3.2"
                                                     uuid="' . $data->getReferenceGn() . '">
                               <gmd:organisationName>
                                  <gco:CharacterString>' . $data->getName() . '</gco:CharacterString>
                               </gmd:organisationName>
                               <gmd:contactInfo>
                                  <gmd:CI_Contact>
                                        <gmd:phone>
                                           <gmd:CI_Telephone>
                                              <gmd:voice>
                                                 <gco:CharacterString>' . $data->getPhoneNumber() . '</gco:CharacterString>
                                              </gmd:voice>
                                           </gmd:CI_Telephone>
                                        </gmd:phone>
                                     <gmd:address>
                                        <gmd:CI_Address>
                                           <gmd:deliveryPoint>
                                              <gco:CharacterString>' . $data->getAddress() . '</gco:CharacterString>
                                           </gmd:deliveryPoint>
                                           <gmd:city>
                                              <gco:CharacterString>' . $data->getCity() . '</gco:CharacterString>
                                           </gmd:city>
                                           <gmd:administrativeArea>
                                              <gco:CharacterString/>
                                           </gmd:administrativeArea>
                                           <gmd:postalCode>
                                              <gco:CharacterString>' . $data->getZipCode() . '</gco:CharacterString>
                                           </gmd:postalCode>
                                           <gmd:country>
                                              <gco:CharacterString>' . $data->getCountry() . '</gco:CharacterString>
                                           </gmd:country>
                                           <gmd:electronicMailAddress>
                                              <gco:CharacterString>' . $data->getEmail() . '</gco:CharacterString>
                                           </gmd:electronicMailAddress>
                                        </gmd:CI_Address>
                                     </gmd:address>
                                  </gmd:CI_Contact>
                              </gmd:contactInfo>
                               <gmd:role/>
                            </gmd:CI_ResponsibleParty>';


        $this->geonetworkService->patchXmlStructure($fragXml, $data->getReferenceGn());
        $this->geonetworkService->publishXml($data->getReferenceGn());

        return [
            'message' => 'the xml modification has been successfully completed',
            'uuid' => $data->getReferenceGn(),
            "status" => 200
        ];
    }

}