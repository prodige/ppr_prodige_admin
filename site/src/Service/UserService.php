<?php

declare(strict_types=1);

namespace App\Service;

use Doctrine\Persistence\ManagerRegistry;
use App\Entity\Administration\Directory\Profile\ProfilePrivilegeLayer;
use App\Entity\Administration\Directory\Profile\ProfileUser;
use App\Entity\Administration\Directory\Skill;
use App\Entity\Administration\Directory\User\User;
use App\Entity\Administration\Directory\User\UserAreaAccess;
use App\Entity\Administration\Directory\User\UserAreaEdition;
use App\Entity\Administration\Organisation\Domain;
use App\Entity\Administration\Organisation\Subdomain;
use App\Entity\Administration\Resource\Layer;
use App\Entity\Administration\Resource\MetadataSheet;
use App\Entity\Configuration\Setting;
use App\Entity\Lex\LexPrivilege;
use Doctrine\DBAL\Exception;
use Doctrine\ORM\EntityManagerInterface;
use stdClass;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Security\Core\Security;

use function PHPUnit\Framework\isNull;

/**
 * Cette classe permet de gerer tous les fonctions autours d'un user
 *
 * @author alkante
 */
class UserService
{
    private ?User $user = null;
    private $prodigeCon;

    public function __construct(
        private EntityManagerInterface $entityManager,
        private Security               $security,
        private ParameterBagInterface  $bag,
        ManagerRegistry                $doctrine
    )
    {
        $this->prodigeCon = $doctrine->getConnection('prodige');
    }

    private function getUserConnected(): User
    {
        return $this->entityManager->getRepository(User::class)->findOneBy(
            ['login' => $this->security->getUser()->getUsername()]
        );
    }

    /**
     * Get all traitements for the authenticate user
     *
     * @param User $user
     * @return array|LexPrivilege|null
     */
    public function getTraitements(User $user): null|array|LexPrivilege
    {
        return $this->entityManager->getRepository(LexPrivilege::class)->findByUser($user);
    }

    /**
     * Return if user has traitement object
     *
     * @param User $user
     * @param MetadataSheet $metadataSheet
     * @param LexPrivilege $traitement
     * @return bool|null
     */
    public function hasTraitementObject(User $user, MetadataSheet $metadataSheet, LexPrivilege $traitement): ?bool
    {
        $privilege = $this->entityManager->getRepository(LexPrivilege::class)->findByUserAndMetadataSheet(
            $user,
            $metadataSheet,
            $traitement
        );

        if (empty($privilege)) {
            return null;
        }

        return (bool)$privilege[0];
    }

    /**
     * Return the matrix of privileges for a user
     *
     * @param User $user
     * @return array
     */
    public function fillTraitements(User $user): array
    {
        $lexPriviligesUser = $this->entityManager->getRepository(LexPrivilege::class)->findByUser($user);
        $matrixPrivilege = [];
        foreach ($lexPriviligesUser as $privilige) {
            match ($privilige['name']) {
                'ADMINISTRATION', 'CARTEPERSO' => $matrixPrivilege[$privilige['name']] = 1,
                'NAVIGATION', 'TELECHARGEMENT', 'PUBLIPOSTAGE' => $matrixPrivilege[$privilige['name']] = 2,
                'CMS', 'PARAMETRAGE', 'MAJIC', 'PARAMETRAGE_DATAVIZ' => $matrixPrivilege[$privilige['name']] = 3,
                'EDITION EN LIGNE', 'EDITION EN LIGNE (AJOUT)', 'EDITION EN LIGNE (MODIF)' => $matrixPrivilege[$privilige['name']] = 4
            };
        }
        return $matrixPrivilege;
    }

    /**
     * Retourne vrai si l'utilisateur administre le sous-domaine contenu dans le domaine. Old name 'Administre'
     *
     * @param User $user
     * @param Subdomain $subdomain
     * @return bool
     */
    public function canManage(User $user, Subdomain $subdomain): bool
    {
        return !empty(
        $this->entityManager->getRepository(Subdomain::class)->findByIdWhenManage(
            $subdomain->getId(),
            $user
        )
        );
    }

    /**
     * Retourne vrai si l'utilisateur a le droit d'éditer le sous-domaine $sousdomaine. Old name 'Edite'
     *
     * @param User $user
     * @param Subdomain $subdomain
     * @return bool
     */
    protected function canEdit(User $user, Subdomain $subdomain): bool
    {
        return !empty(
        $this->entityManager->getRepository(Subdomain::class)->findByIdWhenEditor(
            $subdomain->getId(),
            $user
        )
        );
    }

    /**
     * Retourne vrai si l'utilisateur accède au subDomain. Old name 'Accede'
     *
     * @param User $user
     * @param Subdomain $subdomain
     * @return bool
     */
    protected function canAccessSubDomain(User $user, Subdomain $subdomain): bool
    {
        return !empty(
        $this->entityManager->getRepository(Subdomain::class)->findByIdWhenAccess(
            $subdomain->getId(),
            $user
        )
        );
    }

    /**
     * Retourne vrai si l'utilisateur accède au domain. Old name 'Accede'
     *
     * @param User $user
     * @param Domain $domain
     * @return bool
     */
    protected function canAccessDomain(User $user, Domain $domain): bool
    {
        return !empty(
        $this->entityManager->getRepository(Domain::class)->findByIdWhenAccess(
            $domain->getId(),
            $user
        )
        );
    }

    /**
     * Retourne vrai si l'utilisateur possède le traitement $traitement sur l'objet $trtObjet
     * et l'objet $objet de type $objet_type contenus dans le sous-domaine $subdomain qui est contenu dans le domaine $domain
     */
    public function hasTraitement(
        string $traitement,
        int    $domain_id = null,
        int    $subdomain_id = null,
        int    $metadata_id = null //Object
    ): bool
    {
        // Récuperation du user
        $user = $this->getUserConnected();

        $metadata = $this->entityManager->getRepository(MetadataSheet::class)->findOneBy(['publicMetadataId' => $metadata_id]);
        $traitement = $this->entityManager->getRepository(LexPrivilege::class)->FindOneBy(['name' => $traitement]);

        // Gestion à l'objet
        if (!is_null($metadata)) {
            $status = $this->hasTraitementObject($user, $metadata, $traitement);
            if ($status === true) {
                return true;
            }
        }

        // Gestion au domaine
        $traitementsUser = $this->getTraitements($user);
        $matrixTraitements = $this->FillTraitements($user);

        if (empty(array_intersect(array_keys($matrixTraitements), array_column($traitementsUser, 'name')))) {
            return false;
        }

        $domain = $this->entityManager->getRepository(Domain::class)->findOneById($domain_id);
        $subdomain = $this->entityManager->getRepository(Subdomain::class)->findOneById($subdomain_id);

        // L'utilisateur a les droits pour le traitement en cours et on ne peut pas vérifier le domaine/sous-domaine. Les droits sont accordés car il s'agit d'une demande générale, pas du travail sur un élément précis (accès à la fonctionnalité, pas utilisation de la fonctionnalité)
        // Par exemple, l'utilisateur demande le téléchargement, pour lequel il a les droits, mais on ne sait pas encore sur quelles couches.
        if (is_null($domain) && is_null($subdomain) && isset($matrixTraitements[$traitement->getName()])) {
            return true;
        }
        if (isset($matrixTraitements[$traitement->getName()])) {
            switch ($matrixTraitements[$traitement->getName()]) {
                case 1:
                    return true;
                case 2:
                    if (!is_null($domain)) {
                        //check for domain, otherwise check subdomain
                        if ($this->canAccessDomain($user, $domain)) {
                            return true;
                        };
                    }
                    return $this->canAccessSubDomain($user, $subdomain);
                case 3:
                    return $this->canManage($user, $subdomain);
                case 4:
                    return $this->canEdit($user, $subdomain);
            }
        }
        return false;
    }

    /**
     * vérifie les restrictions de compétences
     * @param string $traitement TELECHARGEMENT ou NAVIGATION
     * @param int $id layer_id or map_id
     * @return true si le droit est accordé
     */
    public function hasTraitementCompetence(string $traitement, int $id): bool
    {
        if ($traitement === 'TELECHARGEMENT') {
            $skills = $this->entityManager->getRepository(Skill::class)->findByLayer($id);
        } else {
            $skills = $this->entityManager->getRepository(Skill::class)->findByMap($id);
        }

        // pas de restriction de compétence
        if (empty($skills)) {
            return true;
        }

        $user = $this->getUserConnected();
        $skillLayers = $this->entityManager->getRepository(Skill::class)->findByLayerAndUser($id, $user);
        $skillMaps = $this->entityManager->getRepository(Skill::class)->findByMapAndUser($id, $user);

        if (!empty($skillLayers) || !empty($skillMaps)) {
            return true;
        }
        return false;
    }

    /**
     * détermine si une donnée existe (couche ou carte) / appel d'un service de l'admincarto
     *
     * @param $objectName
     * @param $acces_adress_admin
     * @param $objectType
     * @param $objectSubType
     * @return bool
     * @throws Exception
     */
    public function existData($objectName, $acces_adress_admin, $objectType, $objectSubType)
    {
        $data = $objectName;
        $type_data = $objectType;
        $type_stockage = $objectSubType;
        if ($type_data === 'couche') {
            switch ($type_stockage) {
                //raster
                case 2 :
                    return file_exists(PRO_MAPFILE_PATH . $data);
                //vector
                case 1 :
                    //view
                case 4 :
                    //table
                case 3 :

                    $tabInfoTables = explode(".", $data);
                    $schemaName = (isset($tabInfoTables[1]) ? $tabInfoTables[0] : "public");
                    $tableName = (isset($tabInfoTables[1]) ? $tabInfoTables[1] : $tabInfoTables[0]);

                    $conn = $this->prodigeCon;
                    $sql = 'SELECT tablename FROM pg_tables where tablename=:tableName and schemaname=:schemaName';
                    $stmt = $conn->prepare($sql);
                    $stmt->bindValue(':schemaName', $schemaName);
                    $stmt->bindValue(':tableName', $tableName);
                    $resultSet = $stmt->executeQuery();

                    $tables = $resultSet->fetchAllAssociative();

                    if (empty($tables)) {
                        $sql = 'SELECT viewname FROM pg_views where viewname=:tableName and schemaname=:schemaName';
                        $stmt = $conn->prepare($sql);
                        $stmt->bindValue(':schemaName', $schemaName);
                        $stmt->bindValue(':tableName', $tableName);
                        $resultSet = $stmt->executeQuery();
                        $views = $resultSet->fetchAllAssociative();

                        return !empty($views);
                    } else {
                        return true;
                    }
                //MAJIC
                case -2 :
                    return true;
            }
        } elseif ($type_data === 'carte') {
            switch ($type_stockage) {
                case 0 :
                case 2 :
                    return file_exists(PRO_MAPFILE_PATH . $data);
                case 1 :
                    return file_exists(PRO_MAPFILE_PATH . 'CartesStatiques/' . $data);
            }
        }

        return false;
    }

    /**
     * Get map Rights according to object and privileges asked
     * @param int $public_metadata_id metadata_id
     * @return array
     * @throws Exception
     */
    public function getMapRights(int $public_metadata_id): array
    {
        $datas = $this->entityManager->getRepository(MetadataSheet::class)->findByRightsAndId($public_metadata_id);
        $bAllow = false;

        foreach ($datas as $data) {
            //vérification de l'autorisation sur le domaine et sur l'objet
            $bAllow = $this->hasTraitement(
                'NAVIGATION',
                (int)$data['domain_id'],
                (int)$data['subdomain_id'],
                (int)$public_metadata_id
            );
            if ($bAllow) {
                break;
            }
        }

        if (isset($data) && !is_null($data['pk_carte_projet']))  {
            //verify on skills
            $bAllow = $bAllow && $this->hasTraitementCompetence('NAVIGATION', $data['pk_carte_projet']);
            $bAllow = $bAllow && $this->existData($data['stkcard_path'], null, "carte", $data['cartp_format']);

            $tabRes["NAVIGATION"] = $bAllow;
            $tabRes["pk_data"] = $data['pk_carte_projet'];
            $tabRes["fmeta_id"] = $data['pk_fiche_metadonnees'];
            $tabRes["statut"] = $data['statut'];
            $tabRes["carte_nom"] = $data['cartp_nom'];
            $tabRes["path"] = $data['stkcard_path'];
            $tabRes["carte_type"] = $data['cartp_format'];
            $tabRes["carte_wms"] = $data['cartp_wms'];

            return $tabRes;
        }
        return [];
    }

    /**
     * Get chart Rights according to object and privileges asked
     *
     * @param int $public_metadata_id metadata_id schema public
     * @return array
     */
    public function getChartRights(int $public_metadata_id): array
    {
        //module chart deprecated in Prodige V5
        $bAllow = false;

        //loop on all domains for this map and check rights
        $datas = $this->entityManager->getRepository(MetadataSheet::class)->findByRightsAndId($public_metadata_id);

        foreach ($datas as $data) {
            $bAllow = $this->hasTraitement(
                'NAVIGATION',
                (int)$data['domain_id'],
                (int)$data['subdomain_id']
            );

            if ($bAllow) {
                break;
            }
        }

        return ["NAVIGATION" => $bAllow];
    }

    // TODO : copy ChartRights pour Dataviz

    /**
     * Get dataviz Rights according to object and privileges asked
     *
     * @param int $public_metadata_id metadata_id schema public
     * @param array $tabTraitement array of asked privileges
     */
    public function getDatavizRights(int $public_metadata_id, array $tabTraitement): array
    {
        $resp = [];
        if (in_array("NAVIGATION", $tabTraitement)) {
            $resp['NAVIGATION'] = false;
        }
        if (in_array("PARAMETRAGE_DATAVIZ", $tabTraitement)) {
            $resp['PARAMETRAGE_DATAVIZ'] = false;
        }

        //loop on all domains
        $datas = $this->entityManager->getRepository(MetadataSheet::class)->findAllDomainAndSubDomainById($public_metadata_id);

        foreach ($datas as $data) {
            if (in_array("NAVIGATION", $tabTraitement)) {
                if ($this->hasTraitement(
                    'NAVIGATION',
                    (int)$data['domain_id'],
                    (int)$data['subdomain_id']
                )) {
                    $resp['NAVIGATION'] = true;
                }
            }
            if (in_array("PARAMETRAGE_DATAVIZ", $tabTraitement)) {
                if ($this->hasTraitement(
                    'PARAMETRAGE_DATAVIZ',
                    (int)$data['domain_id'],
                    (int)$data['subdomain_id']
                )) {
                    $resp['PARAMETRAGE_DATAVIZ'] = true;
                }
            }
        }
        return $resp;
    }

    /**
     * Get Service Rights according to object and privileges asked
     *
     * @param int $public_metadata_id metadata_id schema public
     */
    public function getServiceRights(int $public_metadata_id): array
    {
        $datas = $this->entityManager->getRepository(MetadataSheet::class)->findByRightsAndId($public_metadata_id);
        $tabRes = [];
        if (!empty($datas)) {
            $tabRes["fmeta_id"] = $datas[0]['pk_fiche_metadonnees'];
            $tabRes["statut"] = $datas[0]['statut'];
        }
        return $tabRes;
    }

    /**
     * vérifie les restrictions territoriales
     * @param string $traitement TELECHARGEMENT ou CONSULTATION
     * @param int $layer_id Objet concerné (couche)
     * @return array|bool si il n'y a pas de restrictions territoriales, false si il y en a et que aucun droit n'est accordé, un tableau (code territoire, nom territoire, champ du territoire) si un droit territorial est accordé
     */
    public function getTraitementTerritoire(string $traitement, int $layer_id): array|bool
    {
        $layers = $this->entityManager->getRepository(Layer::class)->findBy(['id' => $layer_id]);

        /* @var Layer[] $layers */
        foreach ($layers as $layer) {
            $couchd_restriction = $layer->getRestriction();
            $couchd_restriction_exclusif = $layer->getExclusiveRestriction();
            $couchd_restriction_buffer = $layer->getBufferRestriction();
            $couchd_zonageid_fk = $layer->getZoning()?->getId();
            $zonage_field_id = $layer->getZoning()?->getFieldId();
            $zonage_field_name = $layer->getZoning()?->getFieldName();

            // pas de restriction
            if ($couchd_restriction === false) {
                return true;
            }

            /**
             * si caractère exclusif, test d'existence d'un territoire associé à l'utilisateur, compris dans le zonage associé à la donnée*
             */
            if ($couchd_restriction_exclusif) {
                $tabTerr = [];
                $tabCouple = [];

                if ($traitement === 'EDITION') {
                    /** @var UserAreaEdition $accesses */
                    $accesses = $this->getUserConnected()->getAreaEdition();
                } else {
                    /** @var UserAreaAccess $accesses */
                    $accesses = $this->getUserConnected()->getAreaAccess();
                }

                foreach ($accesses as $access) {
                    if ($access->getArea()->getZoning()->getId() == $couchd_zonageid_fk) {
                        $tabTerr[] = [
                            $access->getArea()->getCode(),
                            $access->getArea()->getName() . '(' . $access->getArea()->getCode() . ')',
                            $access->getArea()->getZoning()->getId()
                        ];
                        $tabCouple[] = [$access->getArea()->getCode(), $access->getArea()->getZoning()->getId()];
                    }
                }
                if (!empty($tabTerr)) {
                    return $tabTerr;
                }
                return false;
            }
        }
        // pas de restriction territoriale
        return true;
    }

    /**
     * vérifie les restrictions attributaires
     * @param string $traitement
     * @param int $layer_id
     * @return bool|array si pas de filtre, un tableau (clé, valeur) de paramètres de filtre sinon false
     */
    public function getFilterAttribut(string $traitement, int $layer_id): bool|array
    {
        /** @var Layer $layer */
        $layer = $this->entityManager->getRepository(Layer::class)->findOneBy(['id' => $layer_id]);
        if (!$layer) {
            return true;
        }
      
        $user = $this->getUserConnected();
        $ids = $user->getProfiles()->map(function (ProfileUser $p) {
            return $p->getProfile()->getId();
        })->toArray();

        $lexPrivilige = $this->entityManager->getRepository(LexPrivilege::class)->findOneBy(['name' => $traitement]);

        /** @var ProfilePrivilegeLayer $profilePriviligeLayer */
        $profilePriviligeLayer = $this->entityManager->getRepository(ProfilePrivilegeLayer::class)->findOneBy(
            [
                'layer' => $layer_id,
                'lexPrivilege' => $lexPrivilige->getId(),
                'profile' => $ids
            ]
        );

        if ($profilePriviligeLayer) {
            $value = match (strtoupper($profilePriviligeLayer->getLexStructureType()->getName())) {
                'SIREN' => $user->getStructures() ? array($user->getStructures()->first()->getSiren()) : array(),
                'SIRET' => $user->getStructures() ? array($user->getStructures()->first()->getSiret()) : array(),
                'TERRITOIRE' => ($traitement==='EDITION' ? $user->getAreaEdition() : $user->getAreaAccess()) ,
            };
             
            if(strtoupper($profilePriviligeLayer->getLexStructureType()->getName())=== 'TERRITOIRE'){
                $tabTerr = array();
                foreach ($value as $access) {
                        $tabTerr[] = $access->getArea()->getCode();
                }
                $value = $tabTerr;
            }


            return ['column' => $profilePriviligeLayer->getField(), 'value' => $value];
        }
        return false;
    }

    /**
     * retourne la liste des sous-domaines administrés
     * @return array liste des sous-domaines : id=>nom
     */
    public function getSubDomainsManaged(): array
    {
        $user = $this->getUserConnected();
        return $this->entityManager->getRepository(Subdomain::class)->findWhenManage(
            $user
        );
        
    }

    /**
     * Get layer Rights according to object and privileges asked
     * @param int $public_metadata_id metadata_id schema public
     * @param array $tabTraitement array of asked privileges
     * @return array
     * @throws Exception
     */
    public function getLayerRights(int $public_metadata_id, array $tabTraitement): array
    {
        $LayerAdminRepository = $this->entityManager->getRepository(Layer::class);
        
        //loop on all domains for this layer and check rights
        $datas = $this->entityManager->getRepository(MetadataSheet::class)->findLayerInfo($public_metadata_id);

        $bAllow = false;
        $bAllowDownload = false;
        $bAllowView = false;
        $bAllowEdition = false;
        $bAllowEditionAjout = false;
        $bAllowEditionModif = false;
        $tabRes = [];

        //if data is not set don't check TELECHARGELENT, EDITION, NAVIGATION
        if (!empty($datas) && !is_null($datas[0]['pk_couche_donnees'])) {
            foreach ($datas as $data) {
                $domaine = $data['dom_nom'];
                $sous_domaine = $data['ssdom_nom'];
                $objetCouche = $data['pk_couche_donnees'];
                $objetFicheMetadonnees = $data['pk_fiche_metadonnees'];
                $couche_table = $data['couche_table'];
                $couche_type = $data['couche_type'];
                $couche_statut = $data['statut'];
                $bFreeDownload = $data['couchd_download'];
                $domain_id = $data['domain_id'];
                $subdomain_id = $data['subdomain_id'];

                //vérification du caractère de visualisation libre de la donnée
                $bFreeVisu = ($data['couchd_wms'] && !$data['wms_secure']) || ($data['couchd_wfs'] && !$data['wfs_secure']);

                //vérification de l'autorisation sur le domaine et sur l'objet
                if (in_array('TELECHARGEMENT', $tabTraitement, true)) {
                    $bAllowDownload = $bAllowDownload || $this->hasTraitement(
                            'TELECHARGEMENT',
                            $domain_id,
                            $subdomain_id,
                            $public_metadata_id
                        );
                }

                if (in_array('NAVIGATION', $tabTraitement, true)) {
                    $bAllowView = $bAllowView ||
                        $this->hasTraitement('NAVIGATION', $domain_id, $subdomain_id, $objetCouche);
                }

                if (in_array('PARAMETRAGE_DATAVIZ', $tabTraitement, true)) {
                    $tabRes['PARAMETRAGE_DATAVIZ'] = $this->hasTraitement('PARAMETRAGE_DATAVIZ', $domain_id, $subdomain_id, $public_metadata_id);
                }

                $proEdition = $this->entityManager->getRepository(Setting::class)->findOneBy(['name' => 'PRO_EDITION']);
                if (in_array('EDITION', $tabTraitement, true) && strtolower($proEdition->getValue()) === 'on') {
                    if ($this->hasTraitement('EDITION EN LIGNE')) {
                        $bAllowEdition = $bAllowEdition ||
                            (($couche_type == 1 || $couche_type == 3) && $this->existData(
                                    $couche_table,
                                    null,
                                    "couche",
                                    $couche_type
                                )
                                && $this->hasTraitement('EDITION EN LIGNE', $domain_id, $subdomain_id, $public_metadata_id)
                            );
                    }
                    if ($this->hasTraitement('EDITION EN LIGNE (MODIF)')) {
                        $bAllowEditionModif = $bAllowEditionModif ||
                            (($couche_type == 1 || $couche_type == 3) && $this->existData(
                                    $couche_table,
                                    null,
                                    "couche",
                                    $couche_type
                                )
                                && $this->hasTraitement(
                                    'EDITION EN LIGNE (MODIF)',
                                    $domain_id,
                                    $subdomain_id,
                                    $public_metadata_id
                                )
                            );
                    }
                    if ($this->hasTraitement('EDITION EN LIGNE (AJOUT)')) {
                        $bAllowEditionAjout = $bAllowEditionAjout ||
                            (($couche_type == 1 || $couche_type == 3) && $this->existData(
                                    $couche_table,
                                    null,
                                    "couche",
                                    $couche_type
                                ) && $this->hasTraitement(
                                    'EDITION EN LIGNE (AJOUT)',
                                    $domain_id,
                                    $subdomain_id,
                                    $public_metadata_id
                                )
                            );
                    }
                }
            }
            if (in_array('TELECHARGEMENT', $tabTraitement, true)) {
                //vérification des restrictions de compétences
                $bAllowDownload = $bAllowDownload && $this->hasTraitementCompetence('TELECHARGEMENT', $objetCouche);
                //vérification des restrictions territoriales
                $trTerr = $this->GetTraitementTerritoire('TELECHARGEMENT', $objetCouche);
                if ($trTerr === false) {
                    $bAllowDownload = false;
                }
                //Téléchargement libre, prend le pas sur les autres droits
                $bAllowDownload = ($bFreeDownload || $bAllowDownload) && $couche_table && $this->existData(
                        $couche_table,
                        null,
                        "couche",
                        $couche_type
                    );
                $tabRes["TELECHARGEMENT"] = $bAllowDownload;
            }

            if (array_search('NAVIGATION', $tabTraitement, true) !== false) {
                //vérification des restrictions territoriales
                $trTerr = $this->getTraitementTerritoire('NAVIGATION', $objetCouche);
                if ($trTerr == false) {
                    $bAllow = false;
                }

                //restricition territoriale en place
                if (is_array($trTerr)) {
                    $tabRes["RESTRICTION_TERRITORIALE"] = true;
                    $tabRes["NAVIGATION_AREA"] = new stdClass();
                    $tabRes["NAVIGATION_AREA"]->filter_table = 'prodige_perimetre';
                    //on suppose que le filtre est identique sur tous les périmètres
                    $tabRes["NAVIGATION_AREA"]->filter_field = $trTerr[0][2];
                    foreach ($trTerr as $value) {
                        $tabRes["NAVIGATION_AREA"]->filter_values[] = $value[0];
                    }
                }
                //restriction attributaire
                $tabRes["NAVIGATION_ATTRIBUTE_FILTER"] = $this->getFilterAttribut('NAVIGATION', $objetCouche);
                if (is_array($tabRes["NAVIGATION_ATTRIBUTE_FILTER"])) {
                    $tabRes["NAVIGATION_AREA"] = new stdClass();
                    $tabRes["NAVIGATION_AREA"]->filter_table = $couche_table;
                    //on suppose que le filtre est identique sur tous les périmètres
                    $tabRes["NAVIGATION_AREA"]->filter_field = $tabRes["NAVIGATION_ATTRIBUTE_FILTER"]["column"];
                    $tabRes["NAVIGATION_AREA"]->filter_values = $tabRes["NAVIGATION_ATTRIBUTE_FILTER"]["value"];
                }
                $bAllowView = ($bFreeVisu || $bAllowView) && $this->existData(
                        $couche_table,
                        null,
                        "couche",
                        $couche_type
                    );
                $tabRes["NAVIGATION"] = $bAllowView;

                $setting = $this->entityManager->getRepository(Setting::class)->findOneBy(
                    ['name' => 'PRO_MODULE_TABLE_EDITION']
                );

                //droits identiques mais dépend de l'activation du module de visualisation de tables
                if (strtolower($setting->getValue()) == "on") {
                    $tabRes["NAVIGATION_TABLE"] = $bAllowView;
                }
                $tabRes["PRO_MODULE_TABLE_EDITION"] = $setting->getValue();
            }

            if (in_array('EDITION', $tabTraitement, true)) {
                //ajout d'informations sur les restrictions territoriales
                //vérification des restrictions territoriales
                $trTerr = $this->getTraitementTerritoire('EDITION', $objetCouche);
                //$trTerr= 1 : pas de restriction, $trTerr=1 : restriction mais aucun droit => pas de droit d'édition, sinon tableau des territoires autorisés
                if (is_array($trTerr)) {
                    $tabRes["EDITION_AREA"] = new stdClass();
                    $tabRes["EDITION_AREA"]->filter_table = 'prodige_perimetre';
                    //on suppose que le filtre est identique sur tous les périmètres
                    $tabRes["EDITION_AREA"]->filter_field = $trTerr[0][2];
                    foreach ($trTerr as $value) {
                        $tabRes["EDITION_AREA"]->filter_values[] = $value[0];
                    }
                } elseif ($trTerr === false) {
                    $bAllowEdition = false;
                    $bAllowEditionAjout = false;
                    $bAllowEditionModif = false;
                }
            }

            //vérification attributaire des droits : array[column, value] ou false (pas de filtre de droits)
            $tabRes["EDITION_ATTRIBUTE_FILTER"] = $this->getFilterAttribut('EDITION EN LIGNE', $objetCouche);
            $tabRes["EDITION"] = $bAllowEdition;
            $tabRes["EDITION_AJOUT"] = $bAllowEditionAjout;
            $tabRes["EDITION_MODIFICATION"] = $bAllowEditionModif;
            $tabRes["pk_data"] = $objetCouche;
            $tabRes["fmeta_id"] = $public_metadata_id;
            $tabRes["statut"] = $couche_statut;
            $tabRes["couche_nom"] = $data['couche_nom'];

            $tabRes["layer_table"] = $couche_table;

            //gestion du multi-couche avec types différents, à améliorer
            $tabRes["couche_type"] = $couche_type;// (isset($tabRes["couche_type"]) && $tabRes["couche_type"]==1 ? $tabRes["couche_type"] : $rsCouche->read(array_search("COUCHE_TYPE", $tabFieldsCouche)));
            $tabRes["visualisable"] = $data['couchd_visualisable'];
            $tabRes["download"] = $bFreeDownload;
            $tabRes["wms"] = $data['couchd_wms'];
            $tabRes["wfs"] = $data['couchd_wfs'];

            // adding secure rights info
            $layerAdmin = $LayerAdminRepository->find($objetCouche);
            if (!is_null($layerAdmin)) {
                $tabRes["wms_secure"] = $layerAdmin->getWmsSecure();
                $tabRes["wms_secure_ip_filter"] = $layerAdmin->getWmsSecureIpFilter();
                $tabRes["wfs_secure"] = $layerAdmin->getWfsSecure();
                $tabRes["wfs_secure_ip_filter"] = $layerAdmin->getWfsSecureIpFilter();
            }
        }

        return $tabRes;
    }

    /**
     * Get General Rights according to object and privileges asked
     * @param array $tabTraitement array of asked privileges
     * @param string $objectType
     * @param int|null $public_metadata_id
     * @return array
     * @throws Exception
     */
    public function getGeneralRights(array $tabTraitement, string $objectType, int $public_metadata_id = null)
    {
        $bAllow = false;
        $user = $this->getUserConnected();

        //droits généraux sur métadonnée
        if (!is_null($public_metadata_id) && $public_metadata_id !== 0) {
            $metadata = $this->entityManager->getRepository(MetadataSheet::class)->findOneBy(
                ['publicMetadataId' => $public_metadata_id]
            );

            /** @var Setting $setting */
            $setting = $this->entityManager->getRepository(Setting::class)->findOneBy(
                ['name' => 'PRO_MODULE_STANDARDS']
            );
            $tabRes["SYNCHRONIZE"] = false;
            $tabRes["PRO_EDITION"] = false;
            $tabRes["PRO_MODULE_STANDARDS"] = $setting->getValue();

            $datas = $this->entityManager->getRepository(MetadataSheet::class)->findAllDomainAndSubDomainById(
                $public_metadata_id
            );

            $tabRes['sdom_dispose_metadata'] = count($datas) > 0;
            if (array_search("CMS", $tabTraitement) !== false) {
                $bAllow = false;
                //droit de créer une structure de donnée = module edition actif + droit edition + donnée n'existe pas
                if ($objectType == "dataset") {
                    $setting = $this->entityManager->getRepository(Setting::class)->findOneBy(
                        ['name' => 'PRO_EDITION']
                    );
                    $metadata = $this->entityManager->getRepository(MetadataSheet::class)->findOneBy(
                        ['publicMetadataId' => $public_metadata_id]
                    );

                    if ($metadata) {
                        if ($metadata->isLayer()) {
                            $couche_type = $metadata->getLayers()->first()->getLexLayerType()->getId();
                            $tabRes["PRO_EDITION"] = (strtolower($setting->getValue()) == "on" &&
                                $this->hasTraitement('EDITION EN LIGNE') &&
                                !$this->existData(
                                    $metadata->getLayers()->first()->getStoragePath(),
                                    null,
                                    "couche",
                                    $couche_type
                                ));
                        } elseif ($metadata->isMap()) {
                            $couche_type = $metadata->getMaps()->first()->getLexMapFormat()->getId();
                        } else{//when metadata is not yet type defined
                            $tabRes["PRO_EDITION"] = (strtolower($setting->getValue()) == "on" &&
                                $this->hasTraitement('EDITION EN LIGNE') );

                        }

                    }
                }

                foreach ($datas as $data) {
                    $bAllow = $this->hasTraitement('CMS', $data['domain_id'], $data['subdomain_id']);
                    if ($bAllow) {
                        break;
                    }
                }
                $tabRes["CMS"] = $bAllow;
            }

            $rsSynchronised = $this->entityManager->getRepository(MetadataSheet::class)->findImportedTaskById(
                $public_metadata_id
            );

            $tabRes["SYNCHRONIZE"] = count($rsSynchronised) > 0;
            $strSql = $this->entityManager->getRepository(MetadataSheet::class)->findTemplateIso();
            if ($strSql) {
                $tabRes["id_template_catalogue"] = $strSql;
            }
        }

        $tabRes["isAdmProdige"] = $this->isProdigeAdmin();
        $tabRes["isConnected"] = $this->isConnected();
        $tabRes["userNom"] = $user->getName();
        $tabRes["userPrenom"] = $user->getFirstName();
        $tabRes["userLogin"] = $user->getUsername();
        $tabRes["userId"] = $user->getId();
//        $tabRes["userSignature"] = $user->GetUserSignature();
        $tabRes["userEmail"] = $user->getEmail();
        $tabRes["success"] = true;

        if (array_search("GLOBAL_RIGHTS", $tabTraitement) !== false) {
            $tabRes["CMS"] = $this->hasTraitement('CMS');
            //administrated subdomains
            $tabRes["MANAGED_SUBDOMAINS"] = $this->getSubDomainsManaged();
            $tabRes["PARAMETRAGE"] = $this->hasTraitement('PARAMETRAGE');
            $tabRes["PARAMETRAGE_DATAVIZ"] = $this->hasTraitement('PARAMETRAGE_DATAVIZ');
            $tabRes["MAJIC"] = false;//deprecated
            $tabRes["ADMINISTRATION"] = $this->hasTraitement('ADMINISTRATION');
            $tabRes["PRO_IS_REQ_JOINTURES_ACTIF"] = PRO_IS_REQ_JOINTURES_ACTIF;
            $tabRes["PRO_INCLUDED"] = defined("PRO_INCLUDED") && PRO_INCLUDED;
            $tabRes["USER_GENERIC"] = $user->getGeneric();
            $tabRes["USER_ID"] = $user->getId();
        }

        if (array_search("PUBLIPOSTAGE", $tabTraitement) !== false) {
            $tabRes["PUBLIPOSTAGE"] = defined('PRO_PUBLIPOSTAGE') && PRO_PUBLIPOSTAGE == "on" && $this->hasTraitement('PUBLIPOSTAGE');
        }

        return $tabRes;
    }

    /**
     * Méthode qui renvoie vrai si l'utilisateur est administrateur PRODIGE
     * @return boolean
     */
    public function isProdigeAdmin()
    {
        return $this->HasTraitement("ADMINISTRATION");
    }

    public function isConnected(): bool
    {
        return $this->getUserConnected()->getUsername() != PRO_USER_INTERNET_USR_ID;
    }
}
