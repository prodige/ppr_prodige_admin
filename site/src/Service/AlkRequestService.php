<?php

namespace App\Service;

class AlkRequestService
{


    /**
     * Encode puis retourne une chaine de caractères
     * représentant un paramètre dans une URL http
     * @param string $strParam
     * @param bool $bCheckSum
     * @return string
     */
    public static function getEncodeParam(string $strParam, bool $bCheckSum = true)
    {
        $strEncode = "";
        for ($i = 0; $i < strlen($strParam); $i++) {
            $strEncode .= dechex(ord(substr($strParam, $i, 1)));
        }
        return $strEncode;
    }

    /**
     * Récupère le paramètre http selon la méthode REQUEST Décode le param encodée par getEncodeParam()
     * Vérifie le format en fonction de $strDefaultValue et $strFunctionTestType Puis retourne la valeur du paramètre
     * @param string $strParamName
     * @param string $strDefaultValue
     * @param string $strFunctionTestType
     * @return mixed|string
     */
    public static function getDecodeParam(
        string $strParamName,
        string $strDefaultValue = "",
        string $strFunctionTestType = ""
    ) {
        //décodage
        $strDecode = self::decodeValue($strParamName);

        // vérifie le format du param décodé
        $bTest = true;
        if ($strFunctionTestType != "") {
            eval("\$bTest = $strFunctionTestType(\$strDecode);");
        }
        $strDecode = (($bTest && $strDecode != "") ? $strDecode : $strDefaultValue);

        return $strDecode;
    }

    /**
     * Décode la valeur passée en paramètre puis retourne le résultat
     * @param string $strValue
     * @return string
     */
    public static function decodeValue(string $strValue): string
    {
        //décodage
        $strDecode = "";
        for ($i = 0; $i < strlen($strValue) - 1; $i += 2) {
            $strDecode .= chr(hexdec(substr($strValue, $i, 1) . substr($strValue, $i + 1, 1)));
        }
        return $strDecode;
    }

}