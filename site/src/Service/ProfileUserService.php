<?php

namespace App\Service;

use App\Entity\Administration\Directory\Profile\ProfileUser;
use Prodige\ProdigeBundle\Services\GeonetworkInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

class ProfileUserService
{
    private GeonetworkInterface $geonetwork;
    private array $schemaDefault;
    private string $url;

    public function __construct(
        private UserGeonetworkService $userGeonetworkService,
        private ProfileGeonetworkService $profileGeonetworkService
    ) {
        $this->geonetwork = new GeonetworkInterface();
        $this->url = "api/users";
    }

    /**
     * @param ProfileUser $data
     * @return string Returns "" if it's ok else json with error
     */
    public function postPrivilegeUserInGroupGeonetwork(ProfileUser $data): string
    {
        // When updating the user, all the old information must be reinjected
        $userGeonetworkId = $data->getUser()->getReferenceGn();
        $profileName = $data->getProfile()->getName();
        $groups = $this->profileGeonetworkService->getGroups();
        $inGroup = array_search($profileName, array_column($groups, 'name'), true);
        $groupId = $groups[$inGroup]['id'];
        $userPreviousInfo = $this->userGeonetworkService->getUserInGeonetwork($userGeonetworkId);
        $userGroups = $this->getUserAllGeonetworkGroup($userGeonetworkId);

        $schemaDefault = array(
            "id" => $userGeonetworkId,
            "profile" => $userPreviousInfo["profile"],
            "username" => $data->getUser()->getLogin(),
            "name" => $data->getUser()->getName(),
            "surname" => $data->getUser()->getFirstName(),
            "emailAddresses" => [$data->getUser()->getEmail()],
            "groupsEditor" => [],
            "groupsUserAdmin" => [],
            "enabled" => true
        );

        if ($userPreviousInfo["profile"] !== "Administrator") {
            //Recovery of old geonetwork data
            $groupRegisteredUser = [];
            $groupReviewer = [];
            foreach ($userGroups as $userGroup) {
                if (array_values($userGroup['id'])[0] === "RegisteredUser") {
                    $groupRegisteredUser[] = array_values($userGroup['id'])[2];
                } elseif (array_values($userGroup['id'])[0] === "Reviewer") {
                    $groupReviewer[] = array_values($userGroup['id'])[2];
                }
            }

            //Adding the new group
            if ($data->getPrivilege() === "RegisteredUser") {
                $groupRegisteredUser[] = $groupId;
                if (empty($groupReviewer)) {
                    $schemaDefault["profile"] = "RegisteredUser";
                }
            } elseif ($data->getPrivilege() === "Reviewer") {
                $groupReviewer[] = $groupId;
                if ($schemaDefault["profile"] === "RegisteredUser") {
                    $schemaDefault["profile"] = "Reviewer";
                }
            }

            $groupArray = array(
                "groupsRegisteredUser" => $groupRegisteredUser,
                "groupsReviewer" => $groupReviewer
            );
            $schemaDefault = array_merge($schemaDefault, $groupArray);

            return $this->geonetwork->put($this->url . '/' . $userGeonetworkId, $schemaDefault, false, true);
        }
        return new JsonResponse('This user already has all geonetwork rights', 200);
    }

    /**
     * @param int $geonetworkId
     * @throws \JsonException
     */
    public function getUserAllGeonetworkGroup(int $geonetworkId)
    {
        $curlOptions = array(
            CURLOPT_HTTPHEADER => array(
                'Accept: application/json'
            )
        );
        $response = $this->geonetwork->get($this->url . '/' . $geonetworkId . '/groups', false, $curlOptions);
        return json_decode($response, true, 512, JSON_THROW_ON_ERROR);
    }
}


