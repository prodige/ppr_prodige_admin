<?php

namespace App\Service;

use App\Entity\Administration\Directory\Area;
use Doctrine\DBAL\Exception;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Cette classe permet de générer les territoires en fonction d'un zoning.
 *
 * @author alkante
 */
class AreaService
{
    public function __construct(
        private EntityManagerInterface $entityManager,
        private CatalogueService $catalogueService
    ) {
    }

    /**
     * @throws Exception
     */
    public function updateTerritories($zonings, $id): bool
    {
        set_time_limit(2000);
        
        $fields = [];

        foreach ($zonings as $zoning) {
            if ($zoning->getId() === (int)$id) {
                $newZone = $zoning;
            } else {
                $oldZoneFieldName = $zoning->getFieldName();
                $oldZone = $zoning;
            }
            $fields[] = $zoning->getFieldName();
            $fields[] = $zoning->getFieldId();
        }
        // Set existing area expired for this zoning

        $oldAreas = isset($oldZone) ? $oldZone->getAreas() : [];
        foreach ($oldAreas as $area) {
            $area->setExpired(true);
            $this->entityManager->persist($area);
        }

        // Create new ones
        $fieldName = isset($newZone) ? $newZone->getFieldName() : null;
        $fieldId = isset($newZone) ? $newZone->getFieldId() : null;
        $catalogueValues = $this->catalogueService->getValues(
            isset($newZone) ? $newZone->getLayer()->getStoragePath() : null,
            $fields
        );
        if (!array_key_exists('error', $catalogueValues)) {
            if (!isset($oldZoneFieldName)) {
                foreach ($catalogueValues as $values) {
                    $area = new Area();
                    $area->setCode($values[$fieldId]);
                    $area->setName($values[$fieldName]);
                    $area->setZoning($newZone);
                    $this->entityManager->persist($area);
                }
            } else {
                foreach ($catalogueValues as $values) {
                    $area = new Area();
                    $area->setCode($values[$fieldName]);
                    $area->setName($values[$oldZoneFieldName]);
                    $area->setZoning($newZone);
                    $this->entityManager->persist($area);
                }
            }


            $this->entityManager->flush();

            // Move users to new ones & delete old ones with no more user
            foreach ($oldAreas as $oldArea) {
                $newArea = $this->entityManager->getRepository(Area::class)->findOneBy(
                    ['code' => $oldArea->getCode(), 'expired' => false]
                );

                // Move users to new areas
                if ($newArea) {
                    foreach ($oldArea->getUserAccess() as $userAccess) {
                        $userAccess->setArea($newArea);
                        $oldArea->removeUserAccess($userAccess);
                        $this->entityManager->persist($userAccess);
                    }

                    foreach ($oldArea->getUserEdition() as $userEdition) {
                        $userEdition->setArea($newArea);
                        $oldArea->removeUserEdition($userEdition);
                        $this->entityManager->persist($userEdition);
                    }

                    foreach ($oldArea->getAlerts() as $userAlert) {
                        $userAlert->setArea($newArea);
                        $oldArea->removeAlert($userAlert);
                        $this->entityManager->persist($userAlert);
                    }
                    $this->entityManager->persist($newArea);
                }

                // Delete old area with no more user
                if (0 == count($oldArea->getUserAccess()) && 0 == count($oldArea->getUserEdition())) {
                    $this->entityManager->remove($oldArea);
                }
            }
            $this->entityManager->flush();

            return true;
        }

        return false;
    }
}
