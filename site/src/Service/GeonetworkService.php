<?php

namespace App\Service;

use Prodige\ProdigeBundle\Services\GeonetworkInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class GeonetworkService
{
    public function __construct(
        private ParameterBagInterface $parameterBag
    ) {
    }


    /**
     * @param $new_metadata_data
     * @return array
     * Crée une structure dans geonetwork à partir d'un template Xml
     */
    public function postXmlStructure($new_metadata_data): array
    {
        $geonetwork = new GeonetworkInterface($this->parameterBag->get('PRO_GEONETWORK_URLBASE'), 'srv/fre/');
        $params = [
            "metadataType" => "SUB_TEMPLATE",
            "uuidProcessing" => "NOTHING",
            "transformWith" => "_none_"
        ];

        $jsonResp = $geonetwork->put(
            'api/records?' . http_build_query($params),
            $new_metadata_data,
            false,
            false,
            true
        );

        preg_match('/apiError/', $jsonResp, $response);
        if (count($response) === 0) {
            $newxml = json_decode($jsonResp, true);
            $id = array_keys($newxml["metadataInfos"])[0];
            $uuid = $newxml["metadataInfos"][$id][0]['uuid'];

            return [
                'message' => 'Creating has been successfully completed',
                'uuid' => $uuid,
                "status" => 200
            ];
        }

        return ['error' => array_key_exists(1, $response) ? $response[1] : $jsonResp, 'status' => 404];
    }


    /**
     * @param $metadataUuid
     * @return array
     * Publie une metadonnée à partir de son uuid
     */
    public function publishXml($metadataUuid): array
    {
        $geonetwork = new GeonetworkInterface($this->parameterBag->get('PRO_GEONETWORK_URLBASE'), 'srv/fre/');

        $response = $geonetwork->put(
            'api/records/' . urlencode($metadataUuid) . '/publish',
            null,
            false
        );

        preg_match('/apiError/', $response, $error);
        if (count($error) === 0) {
            return ['message' => 'the xml modification has been successfully completed', "status" => 200];
        }

        return ['error' => $error[1], 'status' => 404];
    }

    /**
     * @param $new_metadata_data
     * @param $metadataUuid
     * @return array
     * Edition d'une structure
     */
    public function patchXmlStructure($new_metadata_data, $metadataUuid): array
    {
        $geonetwork = new GeonetworkInterface($this->parameterBag->get('PRO_GEONETWORK_URLBASE'), 'srv/fre/');
        $params = [
            "template" => "s",
            "commit" => true,
            "tab" => "xml",
            "data" => $new_metadata_data
        ];

        $jsonResp = $geonetwork->post(
            'api/records/' . urlencode($metadataUuid) . '/editor',
            $params,
            false
        );

        preg_match('/apiError/', $jsonResp, $response);
        if (count($response) === 0) {
            return [
                'message' => 'the xml modification has been successfully completed',
                'uuid' => $metadataUuid,
                "status" => 200
            ];
        }
        $this->logger->error('patch metadata xml vers geonetwork', ['error' => $response[1]]);
        return ['error' => $response[1], 'status' => 404];
    }


    public function deleteXmlStructure($metadataUuid)
    {
        $geonetwork = new GeonetworkInterface($this->parameterBag->get('PRO_GEONETWORK_URLBASE'), 'srv/fre/');
        $response = $geonetwork->delete(
            'api/records/' . urlencode($metadataUuid) . '?withBackup=true',
            false,
        );

        if ($response === "") {
            return ['error' => "xml not found", 'status' => 404];
        }
        return ['data' => $response, 'status' => 200];
    }
}
