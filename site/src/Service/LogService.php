<?php

namespace App\Service;


use App\Entity\Configuration\Log;
use Exception;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

class LogService
{
    private string $directory;

    private array $logsReference;

    private array $values;


    public function __construct(private ParameterBagInterface $parameterBag)
    {
        $this->directory = $this->parameterBag->get('PRODIGE_PATH_LOGS');

        $this->logsReference = [
            1 => $this->directory . 'Connexions.csv',
            2 => $this->directory . 'ConsultationMetadonnees.csv',
            3 => $this->directory . 'ConsultationCarte.csv',
            4 => $this->directory . 'Sessions.csv',
            5 => $this->directory . 'Telechargements.csv',
            6 => $this->directory . 'WFS.csv',
            7 => $this->directory . 'WMS.csv',
        ];

        $this->values = [
            1 => 'Connexions',
            2 => 'Consultation de métadonnées',
            3 => 'Consultation de cartes',
            4 => 'Nombre de visiteurs',
            5 => 'Téléchargements',
            6 => 'Consultation WFS',
            7 => 'Consultation WMS'
        ];
    }

    /**
     * @param Log $log
     * @return BinaryFileResponse|string
     */
    public function getFile(Log $log): BinaryFileResponse|string
    {
        try {
            $displayName = basename($log->getName());
            $fileName = $log->getFile();

            if ($fileName === null) {
                return 'Fichier inconnu';
            }
            if (file_exists($fileName)) {
                $response = new BinaryFileResponse ($fileName);
                $response->headers->set('Content-Description', 'File Transfer');
                $response->headers->set('Content-Transfer-Encoding', 'binary');
                $response->headers->set('Content-Length', filesize($fileName));
                $response->headers->set('Content-Type', 'application/csv');
                $response->headers->set('Content-Disposition', "attachment; filename=$displayName");
                $response->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT, $displayName . '.csv');
                return $response;
            }
            return 'Fichier inconnu';
        } catch (Exception $e) {
            return 'erreur :' . $e->getMessage();
        }
    }

    public function getAllLogs()
    {
        return $this->values;
    }

    public function getOneLogs(int $id): ?Log
    {
        $log = new Log();
        $log->setId($id);
        if(array_key_exists($id, $this->logsReference)) {
            $log->setName($this->values[$id]);
            $log->setFile($this->logsReference[$id]);

            return $log;
        }

        return null;
    }
}