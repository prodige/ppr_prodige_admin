<?php

namespace App\Service;

use App\Entity\Administration\Directory\Profile\Profile;
use App\Entity\Administration\Directory\Profile\ProfileUser;
use Doctrine\ORM\EntityManagerInterface;
use Prodige\ProdigeBundle\Services\GeonetworkInterface;

class ProfileService
{
    private GeonetworkInterface $geonetwork;
    private string $url;

    public function __construct(
        private EntityManagerInterface $entityManager,
        private ProfileGeonetworkService $profileGeonetworkService
    ) {
        $this->geonetwork = new GeonetworkInterface();
        $this->url = "api/groups";
    }

    /**
     * @param Profile $data
     * @return string|false
     */
    public function postGroup(Profile $data): string|false
    {
        if ($data->isSynchronised()) {
            $schemaDefault = [
                "logo" => null,
                "website" => null,
                "defaultCategory" => null,
                "allowedCategories" => [],
                "enableAllowedCategories" => false,
                "name" => $data->getName(),
                // Id imposed by Geonetwork
                "id" => -99,
                "description" => "",
                "email" => "",
                "referrer" => null,
            ];
            return $this->geonetwork->put($this->url, $schemaDefault, false, true);
        }
        return false;
    }

    /**
     * @param int $id geonetwork id
     * @return mixed
     */
    public function deleteGroup(int $id): mixed
    {
        $response = $this->geonetwork->delete($this->url . '/' . $id . '?force=true', false);
        if ($response === "") {
            return 'The data has been deleted';
        }
        return json_decode($response, true, 512, JSON_THROW_ON_ERROR);
    }

    /**
     * When modifying a group name, the labels are removed and must be reinjected
     * @param int $id geonetwork id
     * @param string $name
     * @return string
     */
    public function putGroup(int $id, string $name): string
    {
        $schemaDefault = [
            "logo" => null,
            "website" => null,
            "defaultCategory" => null,
            "allowedCategories" => [],
            "enableAllowedCategories" => false,
            "name" => $name,
            "id" => $id,
            "description" => "",
            "email" => "",
            "referrer" => null,
            "label" => [
                "dut" => $name,
                "chi" => $name,
                "spa" => $name,
                "fin" => $name,
                "nor" => $name,
                "ara" => $name,
                "rus" => $name,
                "por" => $name,
                "vie" => $name,
                "ger" => $name,
                "cat" => $name,
                "fre" => $name,
                "eng" => $name
            ]
        ];

        return $this->geonetwork->put($this->url . '/' . $id, $schemaDefault, false, true);
    }

    /**
     * @param Profile $data
     * @param string $action
     * add ou remove
     */
    public function privilegeAction(Profile $data, string $action)
    {
        if ($data->getUsers()->count() !== 0) {
            //For each user of the group we check its highest level on another profile
            $allUserInGroup = $this->entityManager->getRepository(ProfileUser::class)->findBy(
                ['profile' => $data]
            );

            foreach ($allUserInGroup as $pU) {
                $this->profileGeonetworkService->userInGeonteworkGroupAction($pU, $action);
            }
        }
        return true;
    }

}