<?php

declare(strict_types=1);

namespace App\Service;

use App\Exception\BadRequestException;
use App\Exception\WrongCredentialException;
use Doctrine\ORM\EntityManager;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

/**
 * Service to manage Mapserver API.
 *
 * @author alkante
 */
class MapserverApiService
{
    /**
     * @param LoggerInterface $logger
     * @param ParameterBagInterface $bag
     * @param HttpClientInterface $client
     */
    public function __construct(
        private LoggerInterface       $logger,
        private ParameterBagInterface $bag,
        private HttpClientInterface   $client
    )
    {
    }

    /**
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function getMap(string $file, array $data = []): ?array
    {
        $this->logger->info(
            'Call API mapserver', ['GET', $this->getMapserverApiUrl('/map/' . $file), $data]
        );

        $response = $this->client->request(
            'GET',
            $this->getMapserverApiUrl('/map/' . $file), [
                'query' => $data,
            ]
        );

        return $this->checkResponse($response);
    }

    /**
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     * @throws \Exception
     */
    public function postMap(string $file, array $data, ?bool $directory): ?array
    {

        $url = $this->getMapserverApiUrl('/map/' . $file);
        if ($directory === true) {
            $data['directory'] = $this->bag->get('mapserver.mapfile_dir');
        }

        $this->logger->info(
            'Call API mapserver', ['POST', $url, $data]
        );

        try {
            $response = $this->client->request(
                'POST',
                $url,
                [
                    'headers' => [
                        'Content-Type: application/x-www-form-urlencoded',
                        'Accept: */*',
                    ],
                    'body' => $data,
                ]
            );

        } catch (\Exception $e) {
            throw new \Exception('Unknown error : ' . $e->getMessage(), 400);
        }

        return $this->checkResponse($response);
    }

    /**
     * @param string $file
     * @param string $layername
     * @param array $data
     * @return array|null
     * @throws BadRequestException
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     * @throws WrongCredentialException
     */
    public function getLayer(string $file, string $layername, $data = []): ?array
    {
        $this->logger->info(
            'Call API mapserver', ['GET', $this->getMapserverApiUrl('/layer/' . $file), $data]
        );

        $response = $this->client->request(
            'GET',
            $this->getMapserverApiUrl('/map/' . $file . '/layer/' . $layername),
            [
                'query' => $data,
            ]
        );

        return $this->checkResponse($response);
    }

    public function addLayerToFile(string $file, string $layer, array $data): ?array
    {
        $this->logger->info(
            'Call API mapserver', ['ADD LAYER', $this->getMapserverApiUrl('/map/' . $file), [$file, $layer, $data]]
        );

        try {
            $response = $this->client->request(
                'PUT',
                $this->getMapserverApiUrl('/map/' . $file . '/layer/' . $layer), [
                    'headers' => [
                        'Content-Type: application/x-www-form-urlencoded',
                        'Accept: */*',
                    ],
                    'body' => $data,

                ]
            );
        } catch (\Exception $e) {
            throw new \Exception('Unknown error', 400);
        }

        return $this->checkResponse($response);
    }

    public function deleteLayerToFile(string $file, string $layer, array $data): ?array
    {
        $this->logger->info(
            'Call API mapserver', ['DELETE LAYER', $this->getMapserverApiUrl('/map/' . $file), [$file, $layer, $data]]
        );

        try {
            $response = $this->client->request(
                'DELETE',
                $this->getMapserverApiUrl('/map/' . $file . '/layer/' . $layer), [
                    'query' => $data,
                ]
            );
        } catch (\Exception $e) {
            throw new \Exception('Unknown error', 400);
        }

        return $this->checkResponse($response);
    }

    /**
     * Get full API url.
     *
     * @param $requestUri
     */
    private function getMapserverApiUrl($requestUri): string
    {
        return $this->bag->get('mapserver.api_url') . $requestUri;
    }

    /**
     * Check Http Response.
     *
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     * @throws BadRequestException
     * @throws WrongCredentialException
     * @throws \Exception
     */
    private function checkResponse(ResponseInterface $response): array
    {

        match ($response->getStatusCode()) {
            403 => throw new WrongCredentialException(),
            400, 404, 500 => throw new BadRequestException(),
            default => null
        };

        try {
            return $response->toArray();
        } catch (\Exception) {
            try {
                return json_decode($response->getContent(false));
            } catch (\Exception) {
                throw new \Exception('Invalid response', 400);
            }
        }
    }
}
