<?php

namespace App\Service;

use App\Entity\Administration\Organisation\Domain;
use App\Entity\Administration\Organisation\Rubric;
use App\Entity\Administration\Organisation\Subdomain;
use Doctrine\ORM\EntityManagerInterface;
use EasyRdf\Graph;
use Prodige\ProdigeBundle\Services\GeonetworkInterface;
use Symfony\Component\Security\Core\Security;

/**
 * Cette classe permet de générer un fichier Thésaurus et de le mettre à jour sur la plateforme Geosource.
 *
 * @author alkante
 */
class ThesaurusSerializer
{

    private static ?ThesaurusSerializer $_instance = null;
    private EntityManagerInterface $entityManager;
    private string $urlGeonetwork;
    private string $urlThesaurus;
    private string $urlThesaurusUpload;
    private string $urlThesaurusRemove;
    private string $name;
    private string $path;
    private string $fullPath;
    private string $theme;

    function __construct(EntityManagerInterface $entityManager, string $url, string $name)
    {
        $this->name = $name;
        $this->theme = "theme";

        $this->urlGeonetwork = $url;
        $this->path = sys_get_temp_dir();
        $this->entityManager = $entityManager;
        $this->urlThesaurus = $this->urlGeonetwork."thesaurus/theme/domaine";
        $this->urlThesaurusUpload = "registries/vocabularies";
        $this->urlThesaurusRemove = "registries/vocabularies/external.".$this->theme.".".$this->name;
        $this->fullPath = $this->path.'/'.$this->name.".rdf";
    }

    /**
     * Méthode qui crée l'unique instance de la classe
     * si elle n'existe pas encore puis la retourne.
     *
     * @param EntityManagerInterface $entityManager
     * @param string $name
     *
     * @return ThesaurusSerializer
     */
    public static function getInstance(EntityManagerInterface $entityManager, string $name): ThesaurusSerializer
    {
        if (is_null(self::$_instance)) {
            self::$_instance = new ThesaurusSerializer($entityManager, PRO_GEONETWORK_URLBASE, $name);
        }

        return self::$_instance;
    }

    /**
     * Méthode qui crée l'unique instance de la classe
     * si elle n'existe pas encore puis la retourne.
     * Dans un context "hors" contrôlleur
     *
     * @param EntityManagerInterface $entityManager
     * @param string $url
     * @param string $name
     * @return ThesaurusSerializer
     */
    public static function getStandaloneInstance(
        EntityManagerInterface $entityManager,
        string $url,
        string $name
    ): ThesaurusSerializer {
        if (is_null(self::$_instance)) {
            self::$_instance = new ThesaurusSerializer($entityManager, $url, $name);
        }

        return self::$_instance;
    }

    /**
     * Génère un fichier Thesaurus à partir des Rubriques,domaines et sous domaines et le transmet à Geosource
     * Si le Thesaurus est déjà présent sur Geosource il est supprimé, puis recréer.
     *
     * @param bool $bUpload
     * @return void
     */
    public function generateDocument(bool $bUpload = true)
    {
        $graph = new Graph();
        $this->processConceptScheme($graph);
        $this->processSousDomaine($graph);
        $this->processDomaine($graph);
        $this->processRubric($graph);

        $data = $graph->serialise('rdf');
        if (!is_scalar($data)) {
            $data = var_export($data, true);
        }

        //upload it to geonetwork with geonetwork tools
        if ($bUpload) {
            @unlink($this->fullPath);
            @file_put_contents($this->fullPath, $data);
            @$this->deleteExistingThesaurus();
            @$this->uploadThesaurus();
        } else {
            //TODO put in in param directory;
            @file_put_contents($this->fullPath, $data);
        }
    }

    /**
     * Upload d'un Thésaurus sur geosource
     * - un appel post est effectué avec un fichier dans le répertoire theme
     *
     * @return void
     */
    private function uploadThesaurus()
    {
        $geonetwork = new GeonetworkInterface(
            $this->urlGeonetwork,
            "srv/api",
            array(CURLOPT_HTTPHEADER => array('Content-Type: multipart/form-data'))
        );

        $formData = array('url' => '', 'registryUrl' => '',  'dir' => $this->theme);
        $geonetwork->upload(
            $this->urlThesaurusUpload,
            $formData,
            array("file" => $this->fullPath),
            "application/rdf+xml"
        );
    }

    /**
     * Suppression d'un Thésaurus sur geosource
     *  - un appel get est effectué
     *
     * @return void
     */
    private function deleteExistingThesaurus()
    {
        $geonetwork = new GeonetworkInterface($this->urlGeonetwork, 'srv/api/');
        $geonetwork->delete($this->urlThesaurusRemove);
    }

    /**
     * Génération du header du Thésaurus
     * @param Graph $graph
     * @return void
     */
    private function processConceptScheme($graph)
    {
        $me = $graph->resource('http://geonetwork-opensource.org/domaines', 'skos:ConceptScheme');
        $me->set('dc:title', 'Domaines');
        $me->set('dc:description', 'Dictionnaire des domaines de la plateforme');
        $orga = $graph->newBnode('foaf:organization');
        $orga->set('foaf:name', '');
        $me->set('dc:creator', $orga);
        $me->set('dcterms:issued', date("Y-m-d"));
        $me->set('dcterms:modified', date("Y-m-d"));
    }

    /**
     * Process des rubriques
     * @param Graph $graph
     * @return void
     */
    private function processRubric(Graph $graph)
    {
        $rubricRepository = $this->entityManager->getRepository(Rubric::class);
        $rubrics = $rubricRepository->findAll();
        foreach ($rubrics as $rubric) {
            $rubric_name = html_entity_decode($rubric->getName(), ENT_QUOTES, 'UTF-8');
            $rubric_graph = $graph->resource($this->urlThesaurus.'#rubric_'.$rubric->getId(), 'skos:Concept');
            $rubric_graph->addLiteral('skos:scopeNote', $rubric_name, 'fr');
            $rubric_graph->addLiteral('skos:scopeNote', $rubric_name, 'en');
            $rubric_graph->addLiteral('skos:prefLabel', $rubric_name, 'fr');
            $rubric_graph->addLiteral('skos:prefLabel', $rubric_name, 'en');
            foreach ($rubric->getDomains() as $domain) {
                $rubric_graph->add(
                    'skos:narrower',
                    $graph->resource($this->urlThesaurus.'#domaine_'.$domain->getId())
                );
            }
        }
    }

    /**
     * Process des domaines
     * @param Graph $graph
     */
    private function processDomaine(Graph $graph)
    {
        $domainRepository = $this->entityManager->getRepository(Domain::class);
        $domains = $domainRepository->findAll();
        foreach ($domains as $domain) {
            $domain_name = html_entity_decode($domain->getName(), ENT_QUOTES, 'UTF-8');
            $domain_description = html_entity_decode($domain->getDescription(), ENT_QUOTES, 'UTF-8');
            $domain_graph = $graph->resource($this->urlThesaurus.'#domaine_'.$domain->getId(), 'skos:Concept');
            $domain_graph->addLiteral('skos:scopeNote', $domain_description, 'fr');
            $domain_graph->addLiteral('skos:scopeNote', $domain_description, 'en');
            $domain_graph->addLiteral('skos:prefLabel', $domain_name, 'fr');
            $domain_graph->addLiteral('skos:prefLabel', $domain_name, 'en');
            $domain_graph->add(
                'skos:broader',
                $graph->resource($this->urlThesaurus.'#rubric_'.$domain->getRubric()->getId())
            );
            foreach ($domain->getSubdomains() as $subdomain) {
                $domain_graph->add(
                    'skos:narrower',
                    $graph->resource($this->urlThesaurus.'#sousdomaine_'.$subdomain->getId())
                );
            }
        }
    }

    /**
     * Process des sous domaines
     * @param Graph $graph
     */
    private function processSousDomaine(Graph $graph)
    {
        $subdomainRepository = $this->entityManager->getRepository(Subdomain::class);
        $subdomains = $subdomainRepository->findAll();
        foreach ($subdomains as $subdomain) {
            $subdomain_name = html_entity_decode($subdomain->getName(), ENT_QUOTES, 'UTF-8');
            $subdomain_description = html_entity_decode($subdomain->getDescription(), ENT_QUOTES, 'UTF-8');
            $subdomain_graph = $graph->resource(
                $this->urlThesaurus.'#sousdomaine_'.$subdomain->getId(),
                'skos:Concept'
            );
            $subdomain_graph->addLiteral('skos:scopeNote', $subdomain_description, 'fr');
            $subdomain_graph->addLiteral('skos:scopeNote', $subdomain_description, 'en');
            $subdomain_graph->addLiteral('skos:prefLabel', $subdomain_name, 'fr');
            $subdomain_graph->addLiteral('skos:prefLabel', $subdomain_name, 'en');
            $subdomain_graph->add(
                'skos:broader',
                $graph->resource($this->urlThesaurus.'#domaine_'.$subdomain->getDomain()->getId())
            );
        }
    }
}
