<?php

namespace App\Service;

use App\Entity\OGCFeatures\OGCCollection;
use App\Normalizer\OGCApiNormalizer;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Exception;
use Doctrine\Persistence\ManagerRegistry;
use Prodige\ProdigeBundle\Services\CurlUtilities;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Twig\Environment;

use function PHPUnit\Framework\isInstanceOf;

class OGCApiService
{

    public function __construct(private Environment $twig, private OGCApiNormalizer $normalizer)
    {

    }

    public function findTemplate(string $template, array|OGCCollection $data, Request $request): Response
    {
        switch ($template) {
            case 'landing' :
                return new Response(
                    $this->twig->render('ogc/landing.html.twig', ['data' => $data])
                );
                break;
            case 'conformance' :
                return new Response(
                    $this->twig->render('ogc/conformance.html.twig', ['data' => $data])
                );
                break;
            case 'collections' :
                return new Response(
                    $this->twig->render('ogc/collections.html.twig', ['data' => $data["collections"]])
                );
                break;
            case 'collection' :
                return new Response(
                    $this->twig->render('ogc/collection.html.twig', ['data' => $data])
                );
                break;
            default:
                $data_normalized = [];
                if($data instanceof OGCCollection){
                    //Normalize OGC Collections
                    $data_normalized[] = $this->normalizer->normalize($data);
                }elseif(is_array($data)){
                    //if array is array of OGCCollections
                    if($data[array_key_first($data)] instanceof OGCCollection){
                        foreach($data as $datapoint){//Normalize OGC Collections
                            $data_normalized[] = $this->normalizer->normalize($datapoint);
                        }
                    }else{
                        //Data already possible to read
                        $data_normalized = $data;
                    }
                }

                $response = new Response(
                    $this->twig->render('ogc/template.html.twig',
                        [
                            'request' => $request,
                            'data' => $data_normalized,
                            'route' => $request->attributes->get('_route'),
                            'identifiers' => $request->attributes->get('_api_identifiers'),
                            'ressource' => $request->attributes->get('_api_ressource_title'),
                        ]
                    )
                );
                return $response;

        }
    }

}
