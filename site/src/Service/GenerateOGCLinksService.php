<?php

namespace App\Service;

use App\Entity\Configuration\Setting;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Doctrine\DBAL\Connection;

class GenerateOGCLinksService
{
    private Connection $catalogueCon;

    public function __construct(
        private UrlGeneratorInterface $urlGenerator, 
        private ManagerRegistry $managerRegistry,
        private ParameterBagInterface $params
    ){
        $this->catalogueCon = $managerRegistry->getConnection('catalogue');
    }

    protected function getUuidFromId($id){
        $desc = $this->catalogueCon->fetchOne(
            "SELECT uuid from public.metadata where id=:id",
            array(
                "id" => $id,
            )
        );

        return $desc;
    }

    /**
     * Generates OGC-compliant links for the Landing Page response.
     *
     * @return array
     */
    public function generateLandingPageLinks($type = 'json'): array
    {
        return [
            [
                'href' => $this->urlGenerator->generate('api_collections_landingpage_collection', [], UrlGeneratorInterface::ABSOLUTE_URL) . ".json",
                'type' => 'application/json',
                'rel' => 'self',
                'title' => 'Landing Page as json'
            ],
            [
                'href' => $this->urlGenerator->generate('api_collections_landingpage_collection', [], UrlGeneratorInterface::ABSOLUTE_URL),
                'type' => 'text/html',
                'rel' => 'alternate',
                'title' => 'Landing Page as html'
            ],
            [
                'href' => $this->urlGenerator->generate('api_collections_conformance_collection', [], UrlGeneratorInterface::ABSOLUTE_URL) . ".json",
                'type' => 'application/json',
                'rel' => 'conformance',
                'title' => 'Conformance Page'
            ],
            [
                'href' => $this->urlGenerator->generate('api_collections_get_collection', [], UrlGeneratorInterface::ABSOLUTE_URL) . ".json",
                'type' => 'application/json',
                'rel' => 'data',
                'title' => 'Collections.json'
            ],
            [
                'href' => $this->urlGenerator->generate('api_collections_get_collection', [], UrlGeneratorInterface::ABSOLUTE_URL) . ".html",
                'type' => 'text/html',
                'rel' => 'data',
                'title' => 'Collections.html'
            ],
            [
                "href" => $type == "json" ? 
                    $this->params->get('PRODIGE_URL_ADMIN').'/ogcapi_doc.json' : 
                    str_replace('landing','docs',$this->urlGenerator->generate('api_collections_landingpage_collection', [], UrlGeneratorInterface::ABSOLUTE_URL)),
                "rel" => "service-desc",
                "title" => "API definition",
                "type" => "application/vnd.oai.openapi+json;version=3.0"
            ],
        ];
    }

    /**
     * Generates OGC-compliant links for each collections response.
     *
     * @return array
     */
    public function generateCollectionsLinks(): array
    {

        return [
            [
                'href' => $this->urlGenerator->generate('api_collections_get_collection', [], UrlGeneratorInterface::ABSOLUTE_URL),
                'type' => 'application/json',
                'rel' => 'self',
                'title' => 'Collections'
            ],
            [
                'href' => $this->urlGenerator->generate('api_collections_landingpage_collection', [], UrlGeneratorInterface::ABSOLUTE_URL) . ".json",
                'type' => 'application/json',
                'rel' => 'root',
                'title' => 'Landing Page as json'
            ],
            [
                'href' => $this->urlGenerator->generate('api_collections_landingpage_collection', [], UrlGeneratorInterface::ABSOLUTE_URL) . ".json",
                'type' => 'application/json',
                'rel' => 'parent',
                'title' => 'Parent Page as json'
            ],
        ];
    }

    /**
     * Generates OGC-compliant links for each collections response.
     *
     * @return array
     */
    public function generateCollectionLinks(string $collection_id = null, int $public_metadata_id = null): array
    {
        $links = [
            [
                'href' => $this->urlGenerator->generate('api_collections_get_item', ['id' => $collection_id], UrlGeneratorInterface::ABSOLUTE_URL).".json",
                'type' => 'application/json',
                'rel' => 'self',
                'title' => 'this collection'
            ],
            [
                'href' => $this->urlGenerator->generate(
                    'api_collections_get_items_by_collection_collection', 
                    ['collectionId' => $collection_id], 
                    UrlGeneratorInterface::ABSOLUTE_URL) . ".json",
                'type' => 'application/json',
                'rel' => 'items',
                'title' => 'This document\'s Items as json'
            ],
            [
                'href' => $this->urlGenerator->generate(
                    'api_collections_get_items_by_collection_collection', 
                    ['collectionId' => $collection_id], 
                    UrlGeneratorInterface::ABSOLUTE_URL) . ".json",
                'type' => 'application/geo+json',
                'rel' => 'items',
                'title' => 'This document\'s Items as geojson features'
            ],
            [
                'href' => $this->urlGenerator->generate(
                    'api_collections_get_items_by_collection_collection', 
                    ['collectionId' => $collection_id], 
                    UrlGeneratorInterface::ABSOLUTE_URL) . ".html",
                'type' => 'text/html',
                'rel' => 'items',
                'title' => 'This document\'s Items as html'
            ]
        ];

        $uuid = $this->getUuidFromId($public_metadata_id);

        if($uuid){
            $links[] = [
                'href' => $this->params->get("PRODIGE_URL_CATALOGUE") . "/geonetwork/srv/" . $uuid,
                'type' => 'application/xml',
                'rel' => 'describedby',
                'title' => 'Accès à la métadonnée au format XML'
            ];
            $links[] = [
                'href' => $this->params->get("PRODIGE_URL_CATALOGUE") . "/geonetwork/srv/fre/catalog.search#/metadata/" . $uuid,
                'type' => 'text/html',
                'rel' => 'describedby',
                'title' => 'Accès à la métadonnée au format HTML'
            ];
        }

        return $links;
    }

    /**
     * Generates epsg link from epsg
     * 
     * @return string
     */
    public function generateCrsLink($epsg): string
    {
        return "https://www.opengis.net/def/crs/EPSG/0/" . strval($epsg);
    }

    

    /**
     * Generates OGC-compliant links for openGis EPSG allowed for API
     *
     * @return array
     */
    public function generateAllowedCrsLinks(): array 
    {
        $setting = $this->managerRegistry->getRepository(Setting::class)->findOneBy(["name" => ["PRO_PROJ_OGCAPI"]]);
        if($setting){
            $links = [];
            $projections = $setting->getProjections()->getValues();
            foreach($projections as $proj){
                $links[] = "https://www.opengis.net/def/crs/EPSG/0/" . strval($proj->getEpsg());
            }
            return $links;
        }

        return [];
    }

    /**
     * Generates links for Items operation
     * 
     * @return array
     */
    public function generateItemLinks($collection_id): array 
    {
        return [
            [
                'href' => $this->urlGenerator->generate(
                    'api_collections_get_items_by_collection_collection', 
                    ['collectionId' => $collection_id], 
                    UrlGeneratorInterface::ABSOLUTE_URL) . ".json",
                'type' => 'application/json',
                'rel' => 'self',
                'title' => 'This document as json'
            ],
            [
                'href' => $this->urlGenerator->generate(
                    'api_collections_get_items_by_collection_collection', 
                    ['collectionId' => $collection_id], 
                    UrlGeneratorInterface::ABSOLUTE_URL),
                'type' => 'text/html',
                'rel' => 'self',
                'title' => 'This document as html'
            ],
            [
                'href' => $this->urlGenerator->generate('api_collections_get_item', ['id' => $collection_id], UrlGeneratorInterface::ABSOLUTE_URL) . ".json",
                'type' => 'application/json',
                'rel' => 'parent',
                'title' => 'Parent'
            ],
            [
                'href' => $this->urlGenerator->generate('api_collections_get_item', ['id' => $collection_id], UrlGeneratorInterface::ABSOLUTE_URL) . ".json",
                'type' => 'application/json',
                'rel' => 'collection',
                'title' => 'Collection'
            ],
            [
                'href' => $this->urlGenerator->generate('api_collections_landingpage_collection', [], UrlGeneratorInterface::ABSOLUTE_URL) . ".json",
                'type' => 'application/json',
                'rel' => 'root',
                'title' => 'Landing Page as json'
            ],
        ];
    }
}
