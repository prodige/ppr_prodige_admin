<?php

namespace App\Service;

use App\Entity\Administration\Directory\Profile\Profile;
use App\Entity\Administration\Directory\Profile\ProfileUser;
use App\Entity\Administration\Directory\User\User;
use App\Entity\Administration\Organisation\Subdomain;
use Doctrine\ORM\EntityManagerInterface;
use Prodige\ProdigeBundle\Services\GeonetworkInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class ProfileGeonetworkService
{
    private GeonetworkInterface $geonetwork;

    private string $url;

    public function __construct(
        private TranslatorInterface $translator,
        private UserGeonetworkService $userGeonetworkService,
        private EntityManagerInterface $entityManager
    ) {
        $this->geonetwork = new GeonetworkInterface();
        $this->url = "api/users";
    }

    /**
     * @param User $user
     * @return string Returns "" if it's ok else json with error
     */
    public function postProfileAdministrator(User $user): string
    {
        $userGeonetworkId = $user->getReferenceGn();
        $userPreviousInfo = $this->userGeonetworkService->getUserInGeonetwork($userGeonetworkId);

        if ($userPreviousInfo["profile"] !== "Administrator") {
            $schemaDefault = array(
                "id" => $userGeonetworkId,
                "profile" => "Administrator",
                "username" => $user->getLogin(),
                "name" => $user->getName(),
                "surname" => $user->getFirstName(),
                "emailAddresses" => [$user->getEmail()],
                "groupsRegisteredUser" => [],
                "groupsEditor" => [],
                "groupsReviewer" => [],
                "groupsUserAdmin" => [],
                "enabled" => true
            );

            return $this->geonetwork->put($this->url . '/' . $userGeonetworkId, $schemaDefault, false, true);
        }
        return true;
    }

    /**
     * @param ProfileUser $data
     * @return string
     * @throws \JsonException
     */
    public function userInGeonteworkGroupAction(ProfileUser $data, string $action): string
    {
        // When updating the user, all the old information must be reinjected
        $userGeonetworkId = $data->getUser()->getReferenceGn();

        //tous les profiles geonetwork
        $geonetworkGroup = $this->getGroups();

        //il a un autre profile geonetwork ?
        $allUserProfiles = $data->getUser()->getProfiles();
        $checkProfiles = [];
        foreach ($allUserProfiles as $profileUser) {
            switch ($action) {
                case 'remove':
                    $groupPrivilege = [];
                    if ($profileUser->getProfile()->getName() !== $data->getProfile()->getName()) {
                        if ($profileUser->getProfile()->isSynchronised()) {
                            //on recupere son id geonetwork
                            $groupId = $this->getGeonetworkId(
                                $profileUser->getProfile()->getName(),
                                $geonetworkGroup
                            );
                            if ($groupId !== null) {
                                $groupPrivilege = $this->getPrivilegeForGeonetwork(
                                    $profileUser->getProfile()
                                );
                                $groupPrivilege = json_decode($groupPrivilege, true, 512, JSON_OBJECT_AS_ARRAY);
                                //on regarde les info du groupe
                                $checkProfiles[$groupId] = array_values($groupPrivilege['privilege'])[0];
                            }
                        }
                    }
                    $this->updatesPrivilegeAdminSide($groupPrivilege, $profileUser);
                    break;
                case 'add':
                    //on recupere son id geonetwork
                    $groupId = $this->getGeonetworkId(
                        $profileUser->getProfile()->getName(),
                        $geonetworkGroup
                    );
                    if ($groupId !== null) {
                        $groupPrivilege = $this->getPrivilegeForGeonetwork(
                            $profileUser->getProfile()
                        );
                        $groupPrivilege = json_decode($groupPrivilege, true, 512, JSON_OBJECT_AS_ARRAY);
                        //on regarde les info du groupe
                        $checkProfiles[$groupId] = array_values($groupPrivilege['privilege'])[0];
                        $this->updatesPrivilegeAdminSide($groupPrivilege, $profileUser);
                    }
                    break;
            }
        }
        // checkProfile contient tous les groups auquel appartient le user et qui sont synchro avec geonetwork
        $getAdminGroup = in_array('Administrator', $checkProfiles, true);

        $schemaDefault = array(
            "id" => $userGeonetworkId,
            "username" => $data->getUser()->getLogin(),
            "name" => $data->getUser()->getName(),
            "surname" => $data->getUser()->getFirstName(),
            "emailAddresses" => [$data->getUser()->getEmail()]
        );
        //cas 1 : il lui reste un group qui lui donne des droits admin
        if ($getAdminGroup === true) {
            $rights = [
                "profile" => "Administrator",
                "enabled" => true
            ];
        } else {
            //cas 2: il n'est plus admin
            $rights = [
                "profile" => 'RegisteredUser',
                "groupsRegisteredUser" => [],
                "groupsEditor" => [],
                "groupsReviewer" => [],
                "enabled" => true
            ];

            $rights = $this->sortGroup($rights, $checkProfiles);
        }

        $schemaDefault = array_merge($schemaDefault, $rights);

        return $this->geonetwork->put(
            $this->url . '/' . $userGeonetworkId,
            $schemaDefault,
            false,
            true
        );
    }

    /**
     * @return mixed
     * @throws \JsonException
     */
    public function getGroups(): mixed
    {
        $curlOptions = array(
            CURLOPT_HTTPHEADER => array(
                'Accept: application/json'
            )
        );

        $response = $this->geonetwork->get('api/groups', false, $curlOptions);
        return json_decode($response, true, 512, JSON_THROW_ON_ERROR);
    }

    public function getGeonetworkId(string $profileName, array $Allgroups)
    {
        foreach ($Allgroups as $group) {
            if (array_key_exists('name', $group) && $group['name'] === $profileName) {
                return $group['id'];
            }
        }
        return null;
    }

    /**
     * @param Profile $profile
     * @return string Returns a json file with the privilege of choice for geonetwork according to its privileges prodige.
     * Created for the ProfileUserType Interactif
     */
    public function getPrivilegeForGeonetwork(Profile $profile): string
    {
        if ($profile->isSynchronised()) {
            $priviliges = $profile->getPrivileges();

            if (count($priviliges) !== 0) {
                $data = [];
                foreach ($priviliges as $privilige) {
                    if ($privilige->getName() === 'ADMINISTRATION') {
                        $response = [
                            $this->translator->trans(
                                "This profile opens the Administrator rights in geonetwork"
                            ) => "Administrator"
                        ];
                        $data = ["privilege" => $response];
                        return json_encode($data);
                    }
                }
                $ssdomGetProfil = $this->entityManager->getRepository(Subdomain::class)->findBy(
                    ['profileAdmin' => $profile]
                );

                if (count($ssdomGetProfil) !== 0) {
                    $response = ['Reviewer' => 'Reviewer'];
                    $data = ["privilege" => $response];
                } else {
                    $response = ['RegisteredUser' => 'RegisteredUser'];
                    $data = ["privilege" => $response];
                }
            } else {
                $response = [$this->translator->trans('This profile does not give you any privileges') => null];
                $data = ["privilege" => $response];
            }
        } else {
            $response = [$this->translator->trans('This profile does not give any privilege on geonetwork') => null];
            $data = ["privilege" => $response];
        }
        return json_encode($data);
    }

    /*
     *
     */
    public function updatesPrivilegeAdminSide(array $groupPrivilege, ProfileUser $profileUser): void
    {
        if (empty($groupPrivilege)) {
            $profileUser->setPrivilege(null);
        } else {
            if (array_key_exists('privilege', $groupPrivilege)) {
                $profileUser->setPrivilege(array_values($groupPrivilege['privilege'])[0]);
            }
        }

        $this->entityManager->persist($profileUser);
        $this->entityManager->flush();
    }

    public function sortGroup(array $rights, array $checkProfiles): array
    {
        foreach ($checkProfiles as $key => $profile) {
            switch ($profile) {
                case "Reviewer":
                    if ($rights['profile'] !== 'Reviewer') {
                        $rights['profile'] = 'Reviewer';
                    }
                    $rights['groupsEditor'][] = $key;
                    $rights['groupsReviewer'][] = $key;
                    break;
                case "RegisteredUser":
                    if ($rights['profile'] === null) {
                        $rights['profile'] = 'RegisteredUser';
                    }
                    $rights['groupsRegisteredUser'][] = $key;
                    break;
            }
        }

        return $rights;
    }
}