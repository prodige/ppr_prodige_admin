<?php

namespace App\Service;


use App\Entity\Administration\Directory\User\User;
use Prodige\ProdigeBundle\Services\GeonetworkInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;


class UserGeonetworkService
{
    private GeonetworkInterface $geonetwork;
    private string $url;
    private array $schemaDefault;

    public function __construct(private ParameterBagInterface $parameterBag)
    {
        $this->geonetwork = new GeonetworkInterface();
        $this->url = "api/users";
    }

    /**
     * @return string|null
     * @throws \JsonException
     */
    public function getUsers(): string|null|array
    {
        $curlOptions = array(
            CURLOPT_HTTPHEADER => array(
                'Accept: application/json'
            )
        );
        $response = $this->geonetwork->get($this->url, false, $curlOptions);
        return json_decode($response, true, 512, JSON_THROW_ON_ERROR);
    }

    /**
     * @param User $user
     * @return string|null Returns null if the operation was successful
     */
    public function postUser(User $user): string|null
    {
        $password = $this->parameterBag->get('GEONETWORK_USER_PASSWORD');
        $this->schemaDefault = array(
            "id" => -99,
            "profile" => "RegisteredUser",
            "username" => $user->getLogin(),
            "name" => $user->getName(),
            "surname" => $user->getFirstName(),
            "emailAddresses" => [$user->getEmail()],
            "organisation" => "",
            "addresses" => [],
            "kind" => "",
            "password" => $password,
            "groupsRegisteredUser" => [],
            "groupsEditor" => [],
            "groupsReviewer" => [],
            "groupsUserAdmin" => [],
            "enabled" => true,
            "email" => [$user->getEmail()],
        );

        return $this->geonetwork->put($this->url, $this->schemaDefault, false, true);
    }

    /**
     * @param int $referenceGn geonetwork id
     * @return string
     */
    public function deleteUser(int $referenceGn): string
    {
        return $this->geonetwork->delete($this->url . '/' . $referenceGn . '?force=true', false);
    }

    /**
     * @param User $user
     * @param int $referenceGn
     * @return string
     * @throws \JsonException
     */
    public function putUser(User $user, int $referenceGn): string
    {
        $userPreviousInfo = $this->getUserInGeonetwork($referenceGn);

        $this->schemaDefault = array(
            "profile" => $userPreviousInfo["profile"],
            "username" => $user->getLogin(),
            "name" => $user->getName(),
            "surname" => $user->getFirstName(),
            "emailAddresses" => [$user->getEmail()],
            "email" => [$user->getEmail()],
            "enabled" => $userPreviousInfo["enabled"],
        );
        return $this->geonetwork->put($this->url . '/' . $referenceGn, $this->schemaDefault, false, true);
    }

    /**
     * @param int $referenceGn
     * @return mixed
     * @throws \JsonException
     */
    public function getUserInGeonetwork(int $referenceGn): mixed
    {
        $curlOptions = array(
            CURLOPT_HTTPHEADER => array(
                'Accept: application/json'
            )
        );
        $response = $this->geonetwork->get($this->url . '/' . $referenceGn, false, $curlOptions);
        return json_decode($response, true, 512, JSON_THROW_ON_ERROR);
    }
}