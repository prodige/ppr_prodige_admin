<?php

namespace App\Service;

use App\Entity\Configuration\DatahubKeyword;
use App\Entity\Configuration\DatahubSetting;
use App\Entity\Configuration\Template;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Yosymfony\Toml\Toml;
use Yosymfony\Toml\TomlBuilder;

class DatahubService
{
    public function __construct(private ManagerRegistry $managerRegistry, private ParameterBagInterface $parameterBag)
    {
        $this->cartoUrl = $parameterBag->get('PRODIGE_URL_FRONTCARTO');
    }

    public function generateToml()
    {
        $settingValues = $this->getSettingKeyValue();
        $advencedFiltres = ['publisher', 'format', 'topic', 'isSpatial'];
        $builder = new TomlBuilder();
        $keywords = $this->managerRegistry->getRepository(DatahubKeyword::class)->findAll();

        $toml = $builder
            ->addComment(' GeoNetwork-UI configuration')
            ->addComment(' Note: this file\'s syntax is TOML (https://toml.io/)')
            ->addComment('## GLOBAL SETTINGS')
            ->addTable('global')
            ->addComment(' This URL (relative or absolute) must point to the API endpoint of a GeoNetwork4 instance')
            ->addValue('geonetwork4_api_url', '/geonetwork/srv/api')
            ->addComment(
                ' This should point to a proxy to avoid CORS errors on some requests (data preview, OGC capabilities etc.)'
            )
            ->addComment(
                ' The actual URL will be appended after this path, e.g. : https://my.proxy/?url=http%3A%2F%2Fencoded.url%2Fows`'
            )
            ->addComment(' This is an optional parameter: leave empty to disable proxy usage')
            ->addValue('proxy_path', $this->cartoUrl.'/core/proxy?proxyUrl=' )
            ->addComment(
                ' This optional parameter defines, in which language metadata should be queried in elasticsearch.'
            )
            ->addComment(
                ' Use ISO 639-2/B (https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes) format to indicate the language of the metadata.'
            )
            ->addComment(' If not indicated, a wildcard is used and no language preference is applied for the search.')
            ->addValue('metadata_language', 'fre')
            ->addValue('login_url', '/cas/login?service=${current_url}')

            ->addComment('## MAP SETTINGS')
            ->addTable('map')
            ->addValue('max_zoom', '14')


            ->addComment('## VISUAL THEME')
            ->addTable('theme')
            ->addValue('primary_color', $settingValues['primaryColorTheme'])
            ->addValue('secondary_color', $settingValues['secondaryColorTheme'])
            ->addValue('main_color', $settingValues['mainColorTheme'])
            ->addValue('background_color', $settingValues['backgroundColorTheme'])
            ->addValue('header_background', "center /cover url('" . $settingValues['backgroundImgUrl'] . "')")
            ->addValue('main_font', $settingValues['mainFont'])
            ->addValue('title_font', $settingValues['titleFont'])
            ->addValue(
                'fonts_stylesheet_url',
                "https://fonts.googleapis.com/css2?family=DM+Serif+Display&family=Inter:wght@200;300;400;500;600;700&display=swap"
            )
            ->addComment('## METADATA QUALITY SETTINGS')
            ->addComment(' This section contains settings used for fine-tuning the metadata quality experience')
            ->addTable('metadata-quality')
            ->addValue('enabled', $settingValues['qualityWidget'] == 1 ? true : false)
            ->addValue('display_widget_in_detail', $settingValues['qualityWidget'] == 1 ? true : false)
            ->addValue('display_widget_in_search', $settingValues['qualityWidget'] == 1 ? true : false)
            ->addTable('translations.en')
            ->addTable('translations.en.datahub.header.title')
            ->addValue('html', '<div class="text-white">' . '<button> connection</button>' . '</div>')
            ->addTable('translations.fr')
            ->addTable('translations.fr.datahub.header')
            ->addValue('html', '<div class="text-white">' . $settingValues['title'] . '</div>')
            ->addTable('translations.fr.datahub.header.title')
            ->addValue('html', '<div class="text-white">' . $settingValues['title'] . '</div>')
            ->addTable('translations.fr.results.records.hits')
            ->addValue(
                'found',
                '{hits, plural, =0{Aucune correspondance.} one{1 donnée trouvée.} other{{hits} données trouvées.}}'
            );

        if(count($keywords) > 0 ){
            $toml->addTable('search');
            $toml->addValue('advanced_filters', $advencedFiltres);
            foreach ($keywords as $keyword){
                $toml->addArrayOfTable('search_preset')
                    ->addValue('name', $keyword->getName())
                    ->addTable('search_preset.filters')
                    ->addValue('q', $keyword->getFilter());
            }
        }else{
            $toml->addTable('search');
            $toml->addValue('advanced_filters', $advencedFiltres);
        }

        return $toml;
    }

    /**
     * @return array
     */
    public function getSettingKeyValue()
    {
        $settings = $this->managerRegistry->getRepository(DatahubSetting::class)->findAll();
        $settingValues = [];
        foreach ($settings as $setting) {
            $settingValues[$setting->getName()] = $setting->getValue();
        }

        return $settingValues;
    }

    public function getMapModele(){
        return $this->managerRegistry->getRepository(Template::class)->findAll();
    }
}
