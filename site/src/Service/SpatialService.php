<?php

namespace App\Service;

use App\Entity\Configuration\Projection;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Service autour des references spatial
 *
 * @author alkante
 */
class SpatialService
{
    public function __construct(
        private HttpClientInterface   $client,
        private ParameterBagInterface $bag,
        private SerializerInterface   $serializer,
        private ManagerRegistry       $managerRegistry,
        private TranslatorInterface   $translator
    )
    {
    }

    /**
     * @param int $epsg
     * @param Projection $projection
     * @return Projection|null
     * @throws ExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     */
    public function getSpatialRefFromAPI(int $epsg, Projection $projection): ?Projection
    {
        $response = $this->client->request(
            'GET',
            'http://spatialreference.org/ref/epsg/' . $epsg . '/postgis/'
        );
        if ('200' == $response->getStatusCode()) {
            preg_match('/^insert into.+\((.+)\)\s*values\s*\((.+)\)/is', $response->getContent(), $resultPostgis);
            $resultPostgis = str_replace('auth_name', 'authName', $resultPostgis);
            $resultPostgis = str_replace('auth_srid', 'authSrid', $resultPostgis);
            $resultPostgis = str_replace('proj4text', 'proj4', $resultPostgis);
            $resultPostgis = str_replace('srtext', 'srText', $resultPostgis);
            if (!empty($resultPostgis)) {
                $fields = trim($resultPostgis[1]);
                $values = trim($resultPostgis[2]);

                $fields = explode(", ", $fields);
                $values = preg_split('/(?<=[\d\']), (?=[\d\'])/is', $values);
                $values = array_map(function ($v) {
                    $va = trim($v, "'");

                    return is_numeric($va) ? (int)$va : $va;
                }, $values);
                $result_array = array_combine($fields, $values);
                $result_array["epsg"] = $epsg;

                return $this->serializer->denormalize(
                    $result_array,
                    Projection::class,
                    "array",
                    ['object_to_populate' => $projection]
                );
            }
        }

        return null;
    }


    /**
     * @param int $epsg
     * @return string|null
     */
    public function getNameFromEpsg(int $epsg): ?string
    {
        $epsg_nom = array();
        $input_file = file_get_contents($this->bag->get('CMD_PROJFILE'));
        preg_match("!(<>\s+|^)# ([^#]+?)\s+<" . $epsg . ">.+?<>!s", $input_file, $epsg_nom);
        $epsg_nom = end($epsg_nom);

        return empty(utf8_encode($epsg_nom)) ? null : utf8_encode($epsg_nom);
    }

    /**
     * @return array Array of Projection
     * @throws ExceptionInterface
     */
    public function intersectProjection(): array
    {
        // INTERSECT Projection file epsg et Spatial_ref_sys
        $projections = $this->managerRegistry->getConnection('prodige')
            ->fetchAllAssociative(
                'SELECT srid, auth_name as "authName", auth_srid as "authSrid", srtext as "srText", proj4text as proj4  
                 FROM public.spatial_ref_sys'
            );

        foreach ($projections as $projection) {
            $allProjections[$projection['srid']] = $projection;
        }

        $projections_merge = $allProjections;
        $file = file_get_contents($this->bag->get('CMD_PROJFILE'));
        $file = explode("<>", $file);

        foreach ($file as $projection) {
            $srid_proj = array();
            $name_proj = array();
            preg_match('/\<\w+\>/', $projection, $srid_proj);
            preg_match('/\#.+/', $projection, $name_proj);
            if (isset($srid_proj[0])) {
                $srid_proj = substr($srid_proj[0], 1, strlen($srid_proj[0]) - 2);
                $name_proj = trim($name_proj[0]);
                $name_proj = substr($name_proj, 2);
                //On test si la projection actuelle du fichier est presente en base
                //Si c'est le cas, on l'ajoute au tableau resultats
                if (isset($projections_merge[$srid_proj])) {
                    $projections_merge[$srid_proj]["name"] = $name_proj;
                }
            }
        }
        $projReturn = [];
        foreach ($projections_merge as $projection) {
            $proj = new Projection();
            $proj->setEpsg($projection['srid']);
            if (!array_key_exists('name', $projection)) {
                $projection['name'] = $this->translator->trans('Unknown name');
            }
            $projReturn[] = $this->serializer->denormalize(
                $projection,
                Projection::class,
                "array",
                ['object_to_populate' => $proj]
            );
        }

        return $projReturn;
    }

    /**
     * @param Projection $projection
     * @return bool
     */
    public function addOrUpdateSpatialRef(Projection $projection): bool
    {
        try {
            $upsert = $this->managerRegistry->getConnection('prodige')->executeStatement(
                'INSERT INTO public.spatial_ref_sys(srid, auth_name, auth_srid, srtext, proj4text)
            VALUES(?, ?, ?, ?, ?)
            ON CONFLICT(srid) DO
                UPDATE SET auth_name = excluded.auth_name, auth_srid = excluded.auth_srid,
                        srtext = excluded.srtext, proj4text = excluded.proj4text;',
                [
                    $projection->getSrid(),
                    strtoupper($projection->getAuthName()),
                    $projection->getAuthSrid(),
                    $projection->getSrText(),
                    $projection->getProj4(),
                ]
            );
        } catch (\Exception $exception) {
            return false;
        }

        return (bool)$upsert;
    }

    // TODO : Test EPSG sur infra docker
    public function addOrUpdateEPSG(Projection $projection): bool
    {
        try {
            $contents = file_get_contents($this->bag->get('CMD_PROJFILE'));
            $instruction = "# " . $projection->getName() . "\n<" . $projection->getAuthSrid() . "> " . $projection->getProj4() . " <>";
            if (!str_contains($contents, "<" . $projection->getAuthSrid() . ">")) {
                $contents .= $instruction . "\n";
            } else {
                $contents = preg_replace("/^.*<" . $projection->getAuthSrid() . ">.*$/", $instruction, $contents);
            }
            file_put_contents($this->bag->get('CMD_PROJFILE'), $contents);
        } catch (\Exception $exception) {
            error_log($exception->getMessage());
            return false;
        }

        return true;
    }

    public function addOrUpdateCamenLexProjection(Projection $projection): bool
    {
        try {
            $upsert = $this->managerRegistry->getConnection('prodige')->executeStatement(
                'INSERT INTO carmen.lex_projection(projection_epsg, projection_name, projection_provider)
            VALUES(?, ?, ?)
            ON CONFLICT(projection_epsg) DO
                UPDATE SET projection_epsg = excluded.projection_epsg, projection_name = excluded.projection_name,
                        projection_provider = excluded.projection_provider;',
                [
                    $projection->getAuthSrid(),
                    $projection->getName(),
                    strtoupper($projection->getAuthName()),
                ]
            );
        } catch (\Exception $exception) {
            return false;
        }

        return (bool)$upsert;
    }

}
