<?php

namespace App\Service;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Exception;
use Doctrine\Persistence\ManagerRegistry;
use Prodige\ProdigeBundle\Services\AdminClientService;
use Prodige\ProdigeBundle\Services\CurlUtilities;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

/**
 * Cette classe permet de récuperer les appels vers admin carto
 *
 * @author alkante
 */
class AdminCartoService
{
    private Connection $prodigeCon;

    public function __construct(
        ManagerRegistry               $doctrine,
        private ParameterBagInterface $bag,
        private HttpClientInterface   $client,
        private AdminClientService    $adminClientService
    )
    {
        $this->prodigeCon = $doctrine->getConnection('prodige');
    }

    /**
     * Add layer to Carmen map
     *
     * @param string $layer
     * @return array|null
     * @throws Exception
     * @throws \Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function addMapCarmen(string $layer): ?array
    {
        $mapId = $this->prodigeCon->fetchOne(
            "SELECT map_id from carmen.map where map_file =:map_file and published_id is null order by map_id desc limit 1",
            array("map_file" => "Modele")
        );

        if ($mapId === false) {
            throw new \Exception("Modele map is not present in database");
        }

        $carmenEditMapAction = $this->bag->get('PRODIGE_URL_ADMINCARTO') . "/api/map/edit/" . $mapId . "/resume";
        $curl = CurlUtilities::curl_init($carmenEditMapAction);
        try {
            // 1 edit model map
            $request = $this->adminClientService->curlAdminWithAdmincli(
                curl_getinfo($curl)['url'], 'GET'
            );

            if (isset($request['success']) && $request['success'] === true && isset($request['map'])) {
                $map_id = $request['map']['mapId'];
                // 2 change map parameters
                $uuid = uniqid();
                $this->prodigeCon->executeStatement(
                    "update carmen.map set published_id =:map_id, map_wmsmetadata_uuid=:uuid where map_id=:map_id",
                    array(
                        "map_id" => $map_id,
                        "uuid" => $uuid,
                    )
                );
                // 3 save / publish map
                $carmenSaveMapAction = $this->bag->get(
                        'PRODIGE_URL_ADMINCARTO'
                    ) . "/api/map/publish/" . $map_id . "/" . $map_id;

                $curl = CurlUtilities::curl_init($carmenSaveMapAction);
                $id = $this->getNextModel();
                curl_close($curl);

                try {
                    $request = $this->adminClientService->curlAdminWithAdmincli(curl_getinfo($curl)['url'], 'POST', [
                        "mapModel" => "true",
                        "mapFile" => basename($id, ".map"),
                        'mapTitle' => $layer,
                    ], ['Accept' => 'application/json', 'Content-Type' => 'application/json']);
                    return $request;
                } catch (\Exception $e) {
                    throw new \Exception('Admincarto POST map error', 400);
                }
            }
        } catch (\Exception $e) {
            throw new \Exception('addMapCarmen POST map error', 400);
        }

        return null;
    }

    /**
     * @throws Exception|TransportExceptionInterface
     * @throws \Exception
     */
    public function openMapCarmen(array $tile): ?string
    {
        $adminCartoUrl = $this->bag->get('PRODIGE_URL_ADMINCARTO');
        $sql = 'SELECT map_id FROM carmen.map WHERE map_wmsmetadata_uuid = :uuid AND published_id IS NULL';
        $stmt = $this->prodigeCon->prepare($sql);
        $results = $stmt->executeQuery(['uuid' => $tile['uuid']])->fetchAllAssociative();
        if (!empty($results)) {
            foreach ($results as $val) {
                $mapId = $val['map_id'];

                // Generate mapfile from existing model
                $carmenEditMapAction = $adminCartoUrl . "/api/map/edit/" . $mapId . "/resume";
                $curl = CurlUtilities::curl_init($carmenEditMapAction);
                curl_close($curl);
                $jsonResp = $this->client->request('GET', curl_getinfo($curl)['url']);
                $jsonObj = json_decode($jsonResp->getContent());
                if ($jsonObj && $jsonObj->success && $jsonObj->map && $jsonObj->map->mapId) {
                    $map_id = $jsonObj->map->mapId;

                    // Update map parameters
                    $this->prodigeCon->executeStatement(
                        "update carmen.map set published_id =:map_id,  map_wmsmetadata_uuid=:uuid where map_id=:map_id",
                        [
                            "map_id" => $map_id,
                            "uuid" => $tile['mapUuid'],
                        ]
                    );

                    // Save & Publish map
                    $carmenSaveMapAction = $adminCartoUrl . "/api/map/publish/" . $map_id . "/" . $map_id;
                    $curl = CurlUtilities::curl_init($carmenSaveMapAction);
                    curl_close($curl);
                    $jsonResp = $this->client->request('POST', curl_getinfo($curl)['url'], [
                            'body' => [
                                "mapModel" => "false",
                                "mapFile" => basename($tile['mapFile']),
                            ],
                        ]
                    );
                    $jsonObj = json_decode($jsonResp->getContent());
                    if ($jsonObj && $jsonObj->success && $jsonObj->map) {
                        return $adminCartoUrl . '/edit_map/' . $tile['mapUuid'];
                    }
                }
            }

            return "";
        } else {
            return $this->bag->get('PRODIGE_URL_CATALOGUE') . '/geosource/layerAddToMap/' . $tile['uuid'] . '?mapUuid=' . $tile['mapUuid'] . '&prefixMapfile=layers/wmts_';
        }
    }

    /**
     * Retourne l'identifiant du prochain modèle
     * @return string
     */
    private function getNextModel(): string
    {
        $tabId = array();
        $PRO_MAPFILE_PATH = $this->bag->get('mapserver.mapfile_dir');
        $MyDirectory = opendir($PRO_MAPFILE_PATH) or die('Erreur');
        while ($entry = readdir($MyDirectory)) {
            if (!is_dir($PRO_MAPFILE_PATH . '/' . $entry)) {
                $info = pathinfo($entry);
                if ($info['extension'] == "map" && substr($info['filename'], 0, 13) == "modele_MODEL_") {
                    preg_match('/^modele_MODEL_([0-9]+)/', $info['filename'], $matches);
                    $id = (int)$matches[1];
                    array_push($tabId, $id);
                }
            }
        }

        $id = (!empty($tabId) ? max($tabId) : 0);

        return "MODEL_" . ($id + 1) . ".map";
    }

    /**
     * Remove Mapfile from Catalogue
     *
     * @param string $mapfile
     * @return bool
     * @throws Exception
     * @throws \Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function removeMapCarmen(string $mapfile): bool
    {
        $query = $this->prodigeCon->executeQuery(
            'SELECT map_wmsmetadata_uuid FROM carmen.map where map_file = ? and published_id is null',
            [$mapfile]
        );
        $uuids = $query->fetchAssociative();

        $returnStatus = true;
        if (!is_array($uuids)) {
            return false;
        }
        foreach ($uuids as $uuid) {
            $carmenDeleteMapAction = $this->bag->get('PRODIGE_URL_CATALOGUE') . "/geosource/mapDelete/" . $uuid;
            $curl = CurlUtilities::curl_init($carmenDeleteMapAction);

            try {
                // 1 edit model map
                $response = $this->client->request(
                    'GET',
                    curl_getinfo($curl)['url']
                );
                $response->getContent();
                curl_close($curl);

                if ($response->getStatusCode() !== 200) {
                    $returnStatus = false;
                }
            } catch (\Exception $e) {
                $returnStatus = false;
            }
        }

        return $returnStatus;
    }
}
