<?php

namespace App\Service;

use App\Entity\Administration\Organisation\Subdomain;
use Carmen\ApiBundle\Services\CarmenConfig;
use Doctrine\ORM\EntityManagerInterface;
use Prodige\ProdigeBundle\Services\CurlUtilities;
use Prodige\ProdigeBundle\Services\GeonetworkInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class SubdomainService
{
    private string $directory;
    private string $defaultESPG;
    private string $mapServerUrl;
    private string $dataCarto;
    private string $geonetworkUrl;
    private string $partition;

    public function __construct(
        EntityManagerInterface $entityManager,
        ParameterBagInterface $parameterBag
    ) {
        $this->directory = $parameterBag->get('mapserver.mapfile_dir');
        $this->defaultESPG = $parameterBag->get('PRODIGE_DEFAULT_EPSG');
        $this->mapServerUrl = $parameterBag->get('mapserver.api_url');
        $this->dataCarto = $parameterBag->get('PRODIGE_URL_DATACARTO');
        $this->geonetworkUrl = $parameterBag->get('PRO_GEONETWORK_URLBASE');
        $this->partition = "wms_sdom";
    }

    /**
     * @param Subdomain $data
     * @param array $subdomain
     */
    public function createServiceWMSFiche(
        array $subdomain
    ): array {
        $geonetwork = new GeonetworkInterface($this->geonetworkUrl, 'srv/fre/');
        $serviceUuid = PRO_WMS_METADATA_UUID;
        $doc = $geonetwork->get(
            'api/records/' . urlencode($serviceUuid) . "/formatters/xml",
            false,
        );
        $group_id = 1;
        $version = "1.0";
        $metadata_doc = new \DOMDocument($version, "UTF-8");
        $entete = "<?xml version=\"" . $version . "\" encoding=\"UTF-8\"?>\n";

        $title = "Service WMS - " . html_entity_decode(urldecode($subdomain['name']));
        if (@$metadata_doc->loadXML($doc) !== false) {
            $xpath = new \DOMXPath($metadata_doc);
            $objXPath = @$xpath->query(
                "/gmd:MD_Metadata/gmd:identificationInfo/srv:SV_ServiceIdentification/gmd:citation/gmd:CI_Citation/gmd:title/gco:CharacterString"
            );
            if ($objXPath->length > 0) {
                $element = $objXPath->item(0);
                $newelement = $metadata_doc->createElement('gco:CharacterString', $title);
                $element->parentNode->replaceChild($newelement, $element);
            }
            $objXPath = @$xpath->query(
                "/gmd:MD_Metadata/gmd:identificationInfo/srv:SV_ServiceIdentification/gmd:abstract/gco:CharacterString"
            );
            if ($objXPath->length > 0) {
                $element = $objXPath->item(0);
                $newelement = $metadata_doc->createElement(
                    'gco:CharacterString',
                    "Service WMS du domaine " . html_entity_decode(urldecode($subdomain['name']))
                );
                $element->parentNode->replaceChild($newelement, $element);
            }
            $serviceType = $metadata_doc->getElementsByTagName("serviceType");
            $objXPath = @$xpath->query("//gco:LocalName", $serviceType->item(0));
            if ($objXPath && $objXPath->length > 0) {
                $element = $objXPath->item(0);
                $newelement = $metadata_doc->createElement('gco:LocalName', "view");
                $element->parentNode->replaceChild($newelement, $element);
            }

            $strXml = '<srv:coupledResource xmlns:srv="http://www.isotc211.org/2005/srv" xmlns:gco="http://www.isotc211.org/2005/gco">';
            $strXmlOp = "<tag xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:srv=\"http://www.isotc211.org/2005/srv\">";
            $strXmlOp .= "</tag>";
            $strXml .= "</srv:coupledResource>";
            $tagRepport = new \DOMDocument();
            $tagRepport->loadXML($strXml);
            $nodesToImport = $tagRepport->getElementsByTagName("SV_CoupledResource");
            $coupledResource = $metadata_doc->getElementsByTagName("SV_ServiceIdentification");
            if ($coupledResource && $coupledResource->length > 0) {
                for ($c = 0; $c < $nodesToImport->length; $c++) {
                    $newNodeDesc = $metadata_doc->importNode($nodesToImport->item($c), true);

                    if ($metadata_doc->getElementsByTagName("coupledResource") && $metadata_doc->getElementsByTagName(
                            "coupledResource"
                        )->length > 0) {
                        $firstCoupledResource = $metadata_doc->getElementsByTagName("coupledResource")->item(0);
                        $firstCoupledResource->parentNode->insertBefore($newNodeDesc, $firstCoupledResource);
                    } else {
                        $coupledResource->item(0)->appendChild($newNodeDesc);
                    }
                }
            }

            $tagRepport = new \DOMDocument();
            $tagRepport->loadXML($strXmlOp);
            $nodes = $tagRepport->getElementsByTagName("operatesOn");
            $serviceIdentification = $metadata_doc->getElementsByTagName("SV_ServiceIdentification");

            if ($serviceIdentification && $serviceIdentification->length > 0) {
                for ($c = 0; $c < $nodes->length; $c++) {
                    $nodeToImport = $nodes->item($c);
                    $newNodeDesc = $metadata_doc->importNode($nodeToImport, true);

                    if ($metadata_doc->getElementsByTagName("operatesOn") && $metadata_doc->getElementsByTagName(
                            "operatesOn"
                        )->length > 0) {
                        $firstOperatesOn = $metadata_doc->getElementsByTagName("operatesOn")->item(0);
                        $firstOperatesOn->parentNode->insertBefore($newNodeDesc, $firstOperatesOn);
                    } else {
                        $serviceIdentification->item(0)->appendChild($newNodeDesc);
                    }
                }
            }

            $strXml = '<test xmlns:gmd="http://www.isotc211.org/2005/gmd" xmlns:srv="http://www.isotc211.org/2005/srv" xmlns:gco="http://www.isotc211.org/2005/gco">
                             <srv:containsOperations>
                              <srv:SV_OperationMetadata>
                                <srv:operationName>
                                  <gco:CharacterString>GetCapabilities</gco:CharacterString>
                                </srv:operationName>
                                <srv:DCP>
                                  <srv:DCPList codeList="http://www.isotc211.org/2005/iso19119/resources/Codelist/gmxCodelists.xml#DCPList" codeListValue="WebServices"/>
                                </srv:DCP>
                                <srv:connectPoint>
                                  <gmd:CI_OnlineResource>
                                    <gmd:linkage>
                                      <gmd:URL>' . $subdomain['urlWebService'] . '</gmd:URL>
                                    </gmd:linkage>
                                    <gmd:protocol>
                                      <gco:CharacterString>OGC:WMS-1.1.1-http-get-map</gco:CharacterString>
                                    </gmd:protocol>
                                  </gmd:CI_OnlineResource>
                                </srv:connectPoint>
                              </srv:SV_OperationMetadata>
                            </srv:containsOperations>
                            <srv:containsOperations>
                              <srv:SV_OperationMetadata>
                                <srv:operationName>
                                  <gco:CharacterString>GetMap</gco:CharacterString>
                                </srv:operationName>
                                <srv:DCP>
                                  <srv:DCPList codeList="http://www.isotc211.org/2005/iso19119/resources/Codelist/gmxCodelists.xml#DCPList" codeListValue="WebServices"/>
                                </srv:DCP>
                                <srv:connectPoint>
                                  <gmd:CI_OnlineResource>
                                    <gmd:linkage>
                                      <gmd:URL>' . $subdomain['urlWebService'] . '</gmd:URL>
                                    </gmd:linkage>
                                    <gmd:protocol>
                                      <gco:CharacterString>OGC:WMS-1.1.1-http-get-map</gco:CharacterString>
                                    </gmd:protocol>
                                  </gmd:CI_OnlineResource>
                                </srv:connectPoint>
                              </srv:SV_OperationMetadata>
                            </srv:containsOperations>
                            <srv:containsOperations>
                              <srv:SV_OperationMetadata>
                                <srv:operationName>
                                  <gco:CharacterString>GetFeatureInfo</gco:CharacterString>
                                </srv:operationName>
                                <srv:DCP>
                                  <srv:DCPList codeList="http://www.isotc211.org/2005/iso19119/resources/Codelist/gmxCodelists.xml#DCPList" codeListValue="WebServices"/>
                                </srv:DCP>
                                <srv:connectPoint>
                                  <gmd:CI_OnlineResource>
                                    <gmd:linkage>
                                      <gmd:URL>' . $subdomain['urlWebService'] . '</gmd:URL>
                                    </gmd:linkage>
                                    <gmd:protocol>
                                      <gco:CharacterString>OGC:WMS-1.1.1-http-get-map</gco:CharacterString>
                                    </gmd:protocol>
                                  </gmd:CI_OnlineResource>
                                </srv:connectPoint>
                              </srv:SV_OperationMetadata>
                            </srv:containsOperations>
                        </test>';

            $tagRepport = new \DOMDocument();
            $tagRepport->loadXML($strXml);

            // suppression des tags containsOperations existant
            $nodeToDelete = $xpath->query("/gmd:MD_Metadata/gmd:identificationInfo/*/srv:containsOperations");
            for ($c = 0; $c < $nodeToDelete->length; $c++) {
                $nodeToDelete->item($c)->parentNode->removeChild($nodeToDelete->item($c));
            }

            $objXPath = $xpath->query(
                "/gmd:MD_Metadata/gmd:identificationInfo/srv:SV_ServiceIdentification/srv:coupledResource"
            );
            if ($objXPath->length > 0) {
                foreach ($objXPath as $item) {
                    $item->parentNode->removeChild($item);
                }
            }
            $objXPath = $xpath->query(
                "/gmd:MD_Metadata/gmd:identificationInfo/srv:SV_ServiceIdentification/srv:operatesOn"
            );
            if ($objXPath->length > 0) {
                foreach ($objXPath as $item) {
                    $item->parentNode->removeChild($item);
                }
            }
            $objXPath = $xpath->query(
                "/gmd:MD_Metadata/gmd:distributionInfo/gmd:MD_Distribution/gmd:transferOptions/gmd:MD_DigitalTransferOptions/gmd:onLine"
            );
            if ($objXPath->length > 0) {
                foreach ($objXPath as $item) {
                    $item->parentNode->removeChild($item);
                }
            }
            $objXPath = $xpath->query(
                "/gmd:MD_Metadata/gmd:identificationInfo/srv:SV_ServiceIdentification/gmd:citation/gmd:CI_Citation/gmd:date/gmd:CI_Date/gmd:date/gco:Date"
            );
            if ($objXPath->length > 0) {
                $item = $objXPath->item(0);
                $item->nodeValue = date("Y-m-d");
            }

            // création des tags containsOperations nécessaire (WMS protocole)
            $coupledResource = $metadata_doc->getElementsByTagName("SV_ServiceIdentification");
            if ($coupledResource && $coupledResource->length > 0) {
                $nodes = $tagRepport->getElementsByTagName("containsOperations");
                for ($c = 0; $c < $nodes->length; $c++) {
                    $nodeToImport = $nodes->item($c);
                    $newNodeDesc = $metadata_doc->importNode($nodeToImport, true);
                    $coupledResource->item(0)->appendChild($newNodeDesc);
                }
            }


            $strXmlDistributionInfo = "<tag  xmlns:gmd=\"http://www.isotc211.org/2005/gmd\" xmlns:gco=\"http://www.isotc211.org/2005/gco\">
                                             <gmd:onLine>
                                                 <gmd:CI_OnlineResource>
                                                     <gmd:linkage>
                                                         <gmd:URL>" . $subdomain['urlWebService'] . "</gmd:URL>
                                                     </gmd:linkage>
                                                     <gmd:protocol>
                                                         <gco:CharacterString>application/vnd.ogc.wms_xml</gco:CharacterString>
                                                     </gmd:protocol>
                                                     <gmd:description>
                                                         <gco:CharacterString>Accès au service WMS</gco:CharacterString>
                                                     </gmd:description>
                                                 </gmd:CI_OnlineResource>
                                             </gmd:onLine></tag>";

            $tagDistInfo = new \DOMDocument();
            $tagDistInfo->loadXML($strXmlDistributionInfo);

            //first delete all onlineResource links
            $objXPath = @$xpath->query(
                "/gmd:MD_Metadata/gmd:distributionInfo/gmd:MD_Distribution/gmd:transferOptions/gmd:MD_DigitalTransferOptions/gmd:onLine"
            );
            if ($objXPath->length > 0) {
                foreach ($objXPath as $item) {
                    $item->parentNode->removeChild($item);
                }
            }

            $transferOptions = $metadata_doc->getElementsByTagName("MD_DigitalTransferOptions");
            if ($transferOptions && $transferOptions->length > 0) {
                $nodes = $tagDistInfo->getElementsByTagName("onLine");
                for ($c = 0; $c < $nodes->length; $c++) {
                    $nodeToImport = $nodes->item($c);
                    $newNodeDesc = $metadata_doc->importNode($nodeToImport, true);
                    $transferOptions->item(0)->appendChild($newNodeDesc);
                }
            }

            $new_metadata_data = $metadata_doc->saveXML();
            $new_metadata_data = str_replace($entete, "", $new_metadata_data);

            $params = [
                "uuidProcessing" => "GENERATEUUID"
            ];

            $saveMetadata = $geonetwork->put(
                'api/records?' . http_build_query($params),
                $new_metadata_data,
                false,
                false,
                true
            );
            $jsonResp = json_decode($saveMetadata, JSON_FORCE_OBJECT);
            if ($jsonResp === null) {
                return ["error" => $saveMetadata, "status" => 409];
            }
            if (isset($jsonResp["message"]) && $jsonResp["message"] === "IllegalArgumentException") {
                return [
                    "error" => "Unable to create the metadata from the card template",
                    "status" => 422
                ];
            }
            $id = array_keys($jsonResp["metadataInfos"])[0];
            return [
                'id' => $id,
                'uuid' => $jsonResp["metadataInfos"][$id][0]['uuid']
            ];
        }

        return [
            "error" => "Template recovery failed",
            "status" => 404
        ];
    }

    /**
     * @param Subdomain $data
     * @return array
     * @throws \Exception
     */
    public function createMapFile(Subdomain $data): array
    {
        $endString = " - Données WMS";
        $subdomainName = preg_replace("!\W!", "_", $data->getName());
        $rootUrlWebservice = $this->dataCarto . "/" . ($this->partition) . "/" . $subdomainName;
        $urlWebservice = $rootUrlWebservice . "?service=WMS&amp;request=GetCapabilities";
        $mapfile = $this->directory . $this->partition . "_" . $subdomainName . ".map";
        $url = $this->mapServerUrl . "/map/" . $this->partition . "_" . $subdomainName;
        //rubric_name / domaine_name / sous_doumaine_name
        $name = $data->getDomain()->getRubric()->getName() . " / "
            . $data->getDomain()->getName() . " / "
            . $data->getName();
        $title = $name . $endString;
        $extent = [-1870.33947299157, 6100048.8679937, 1120552.77850281, 7222471.9859695];
        $metadata = [
            "wms_srs" => "EPSG:" . $this->defaultESPG,
            "wms_onlineresource" => $rootUrlWebservice,
            "wms_enable_request" => "*",
            "wms_title" => $title
        ];

        $newMap = [
            "fontset" => "./etc/fonts.txt",
            "symbolset" => "./etc/symbols.sym",
            "name" => $title,
            "imagecolor" => "#ffffff",
            "directory" => $this->directory,
            "imagetype" => "png",
            "extent" => array_values($extent),
            "units" => "METERS",
            "projection" => "init=epsg:" . $this->defaultESPG,
            "maxscaledenom" => 10000000,
            "minscaledenom" => 1,
            "metadata" => $metadata,
        ];

        $curl = CurlUtilities::curl_init($url);
        curl_setopt(
            $curl,
            CURLOPT_HTTPHEADER,
            array('Accept: */*', 'Content-Type: application/x-www-form-urlencoded')
        );
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, "xml=" . http_build_query($newMap));
        $response = curl_exec($curl);
        $jsonResponse = json_decode($response, true);
        curl_close($curl);

        if ($jsonResponse === null) {
            return ["error" => "mapserver injoignable", "status" => 409];
        }
        if (array_key_exists("status", $jsonResponse) && $jsonResponse["status"] === 500) {
            return ["error" => "mapserver injoignable", "status" => 409];
        }

        return [
            "subdomain" => [
                'name' => $name,
                'urlWebService' => $urlWebservice,
                'mapFile' => $mapfile
            ]
            ,
            "status" => 201
        ];
    }

    /**
     * @param string $uuid
     * @return array
     */
    public function removeServiceWMSFiche(string $uuid){
        $geonetwork = new GeonetworkInterface($this->geonetworkUrl, 'srv/fre/');
        $response = $geonetwork->delete(
            'api/records/' . urlencode($uuid),
            false,
        );
        $jsonResponse = json_decode($response, true);
        if ($jsonResponse !== null) {
            return ['error' => $jsonResponse, 'status' => 404];
        }
        return ['data' => $response, 'status' => 200];
    }

    /**
     * @param Subdomain $data
     * @return array
     * @throws \Exception
     */
    public function removeMapFile(Subdomain $data): array
    {
        $subdomainName = preg_replace("!\W!", "_", $data->getName());
        $url = $this->mapServerUrl . "/map/" . rawurlencode($this->partition . "_" . $subdomainName) . "?" . http_build_query(
        ['directory' => $this->directory]);

        $curl = CurlUtilities::curl_init($url);

        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Accept: /'));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'DELETE');
        $deleteInfo = curl_exec($curl);
        $deleteInfoJson = json_decode($deleteInfo, true);
        curl_close($curl);
        if ($deleteInfoJson === null) {
            $this->logger->error('curl deleteData error ', ['error' => $deleteInfo]);
            return ["error" => $deleteInfo, "status" => 409];
        }
        if (array_key_exists("error", $deleteInfoJson)) {
            return ["error" => $deleteInfoJson['error'], "status" => 404];
        }
        return ["data" => "The service card associated with subdomain " . $data->getId() . " has been deleted", "status" => 200];
    }
}