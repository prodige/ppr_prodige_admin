<?php

namespace App\Service;

use App\Entity\Administration\Resource\Layer;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Exception;
use Doctrine\Persistence\ManagerRegistry;

/**
 * Cette classe permet de récuperer les liste des champs d'une table du catalogue.
 *
 * @author alkante
 */
class CatalogueService
{
    private Connection $prodigeCon;

    public function __construct(private ManagerRegistry $doctrine)
    {
        $this->prodigeCon = $doctrine->getConnection('prodige');
    }

    /**
     * Get layer table column list.
     *
     * @throws Exception
     */
    public function getFields(string $layer, $forSelect = false): array
    {
        $fields = [];
        $sql = 'SELECT * FROM information_schema.columns WHERE table_name = :layer';
        $stmt = $this->prodigeCon->prepare($sql);
        $results = $stmt->executeQuery(['layer' => $layer])->fetchAllAssociative();
        foreach ($results as $field) {
            $fields[$field['column_name']] = $forSelect ? $field['column_name'] : $field['data_type'];
        }

        return $fields;
    }

    /**
     * Get Uuid metadata for layer
     *
     */
    public function getUuidFromMetadata(Layer $layer): ?string
    {
        $id = $layer->getSheetMetadata()->getPublicMetadataId();
        $sql = 'SELECT uuid FROM public.metadata WHERE id = :id';
        $stmt = $this->doctrine->getConnection('catalogue')->prepare($sql);
        $result = $stmt->executeQuery(['id' => $id])->fetchOne();

        if ($result === false) {
            return null;
        }

        return $result;
    }

    /**
     * Get layer table values.
     *
     * @throws Exception
     */
    public function getValues(string $storagePath, array $fieldName): array
    {
        if ($storagePath !== null) {
            $columnName = implode(',', $fieldName);
            $sql = 'SELECT DISTINCT ' . $columnName . ' from "public"."' . $storagePath . '"';
            $stmt = $this->prodigeCon->prepare($sql);

            $response = $stmt->executeQuery()->fetchAllAssociative();
        } else {
            $response = ['error' => "Le nom de la table n'existe pas"];
        }
        return $response;
    }

    /**
     * @param string $uuid
     * @return string|null
     * Reçoit l'uuid de la metadonnée (ex : "f046945b-8530-40d8-ac52-0f983b916b7a")
     * Renvoi l'id de la metadonnée (ex : 41175)
     */
    public function getIdFromUuid(string $uuid): ?string
    {
        $sql = 'SELECT id FROM public.metadata WHERE uuid = :uuid';
        $stmt = $this->doctrine->getConnection('catalogue')->prepare($sql);
        $result = $stmt->executeQuery(['uuid' => $uuid])->fetchOne();

        if ($result === false) {
            return null;
        }

        return $result;
    }

}
