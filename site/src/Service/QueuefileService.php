<?php

namespace App\Service;

use App\Entity\Configuration\Queuefile;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Serializer\SerializerInterface;


/**
 * Cette classe gère les queues de 'télécarto'
 *
 * @author alkante
 */
class QueuefileService
{
    private string $rootPath;

    public function __construct(
        private ParameterBagInterface $bag,
        private SerializerInterface $serializer,
    ) {
        $this->rootPath = $this->bag->get('telecarto.path.queue');
    }

    public function get(string $file): ?Queuefile
    {
        $filename = $this->rootPath . "/" . $file;
        try {
            $handle = fopen($filename, "r");
            $contents = fread($handle, filesize($filename));
            fclose($handle);

            $contents = trim($contents) . $file;
            $contents = $contents . '|' . $file;
            // new QueueFile
            return $this->stringToQueufile($contents);
        } catch (\Exception $e) {
            return null;
        }
    }

    /**
     * List all file in queue
     *
     * @return array
     */
    public function list(): array
    {
        $tabListQueue = array();
        $tabFolder = scandir($this->rootPath);

        unset($tabFolder[0]);
        unset($tabFolder[1]);
        while (!empty($tabFolder)) {
            $strFileName = array_shift($tabFolder);
            if (substr($strFileName, -6) != ".queue") {   // fichiers non traités
                continue;
            }
            $filename = $this->rootPath . "/" . $strFileName;
            $handle = fopen($filename, "r");
            $contents = fread($handle, filesize($filename));
            fclose($handle);

            $contents = trim($contents) . $strFileName;
            $contents = $contents . '|' . $strFileName;
            // new QueueFile
            $queueFile = $this->stringToQueufile($contents);
            $tabListQueue[] = $queueFile;
        }
        return $tabListQueue;
    }

    /**
     * Remove a file from queue
     *
     * @param $filename
     * @return bool
     */
    public function removeFile($filename): bool
    {
        if (file_exists($this->rootPath . "/" . $filename)) {
            return unlink($this->rootPath . "/" . $filename);
        }
        return false;
    }

    /**
     * Remove all queue files
     *
     * @return bool
     */
    public function removeAllFile(): bool
    {
        $tabFolder = scandir($this->rootPath);
        unset($tabFolder[0]);
        unset($tabFolder[1]);
        while (!empty($tabFolder)) {
            $strFileName = array_shift($tabFolder);
            if (substr($strFileName, -6) != ".queue") {   // fichiers non traités
                continue;
            }
            $this->removeFile($strFileName);
        }
        return true;
    }

    private function stringToQueufile(string $str): QueueFile
    {
        $keys = [
            'index',
            'caller',
            'format',
            'projection',
            'tables',
            'metadata',
            'type',
            'champ 8',
            'champ 9',
            'spatialExtraction',
            'extractionType',
            'areaExtraction',
            'area',
            'empriseExtraction',
            '15',
            '16',
            '17',
            '18',
            'date',
            '19',
//            '20',
            'ref',
            'file'
        ];

        $arrayMerge = array_combine($keys, array_values(explode('|', $str)));
        $arrayMerge['metadata'] = (int)$arrayMerge['metadata'];
        $queueFile = new Queuefile();
        $this->serializer->denormalize(
            $arrayMerge,
            Queuefile::class,
            "array",
            ['object_to_populate' => $queueFile]
        );

        return $queueFile;
    }

}
