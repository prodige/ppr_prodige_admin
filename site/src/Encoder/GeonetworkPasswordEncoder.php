<?php

namespace App\Encoder;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class GeonetworkPasswordEncoder
{
    private const DEFAULT_ITERATIONS = 1024;

    private const DEFAULT_KEYLENGTH = 8;

    private string $secret;

    private string $username;

    public function __construct(private ParameterBagInterface $bag, private EntityManagerInterface $entityManager)
    {
        // Get secret fron config
        $this->secret = $this->utf8Encode($this->bag->get('GEONETWORK_SALT'));
        $this->username = $this->utf8Encode($this->bag->get('phpcli_default_login'));
    }

    public function encode($rawPassword): string
    {
        // Generate random Salt
        $salt = $this->string2ByteArray(random_bytes(self::DEFAULT_KEYLENGTH));

        // Encode rawPassword
        $digested = array_merge($salt, $this->string2ByteArray($this->secret), $this->string2ByteArray($rawPassword));

        // iterator X times
        $tmpDigested = $this->byteArray2String($digested);
        for ($i = 0; $i < self::DEFAULT_ITERATIONS; $i++) {
            $tmpDigested = hash("sha256", $tmpDigested, true);
        }

        // Convert to Bytes Array
        $digested = $this->string2ByteArray($tmpDigested);
        return $this->byteArray2Hex($salt) . $this->byteArray2Hex($digested);
    }

    public function persist($password): string
    {
        try {
            $stmt = $this->entityManager->getConnection('prodige')->prepare(
                'UPDATE public.users SET password = :password WHERE username = :username'
            );
            $result = $stmt->executeQuery(['password' => $password, 'username' => $this->username]);
            return (string)$result->rowCount();

        } catch (\Exception $e) {
            return 'Erreur lors de l\'enregistrement du mot de passe géonetwork';
        }


    }

    private function utf8Encode($string)
    {
        return mb_convert_encoding($string, 'UTF-8');
    }

    private function string2ByteArray($string)
    {
        return unpack('C*', $string);
    }

    private function byteArray2String($byteArray)
    {
        $chars = array_map("chr", $byteArray);
        return join($chars);
    }

    private function byteArray2Hex($byteArray)
    {
        $chars = array_map("chr", $byteArray);
        $bin = join($chars);
        return bin2hex($bin);
    }

}