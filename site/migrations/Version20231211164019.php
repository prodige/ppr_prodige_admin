<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use Doctrine\Persistence\ManagerRegistry;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231211164019 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Theme prodige par défaut';
    }

    public function up(Schema $schema): void
    {
        $mapfile = $this->connection->fetchOne("select mapfile from admin.template ORDER BY id ASC LIMIT 1;");
        $admin_url = $_ENV["PRODIGE_URL_ADMIN"];
        $admin_url = substr($admin_url,8);

        $contribution_url = $_ENV["PRODIGE_URL_CONTRIBUTION"];
        $contribution_url = substr($contribution_url,8);

        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql(
            "INSERT INTO admin.datahubsetting (lex_setting_type_id, name, title, value, setting_order) 
                       values(1,'title',
                       'Le titre du Catalogue', 
                       'Datahub Prodige', 0 );"
        );
        $this->addSql(
            "INSERT INTO admin.datahubsetting (lex_setting_type_id, name, title, value, setting_order) 
                       values(1,'primaryColorTheme',
                       'Couleur primaire du thème', 
                       '#3277b3', 1 );"
        );
        $this->addSql(
            "INSERT INTO admin.datahubsetting (lex_setting_type_id, name, title, value, setting_order) 
                       values(1,'secondaryColorTheme',
                       'Couleur secondaire du thème', 
                       '#deecfd', 2 );"
        );
        $this->addSql(
            "INSERT INTO admin.datahubsetting (lex_setting_type_id, name, title, value, setting_order) 
                       values(1,'mainColorTheme',
                       'Couleur principale du thème', 
                       '#212029', 3 );"
        );
        $this->addSql(
            "INSERT INTO admin.datahubsetting (lex_setting_type_id, name, title, value, setting_order) 
                       values(1,'backgroundColorTheme',
                       'Couleur de fond du thème', 
                       '#ffffff', 4 );"
        );
        $this->addSql(
            "INSERT INTO admin.datahubsetting (lex_setting_type_id, name, title, value, setting_order) 
                       values(1,'mainFont',
                       'Police principale', 
                       'fallback-font', 5 );"
        );
        $this->addSql(
            "INSERT INTO admin.datahubsetting (lex_setting_type_id, name, title, value, setting_order) 
                       values(1,'titleFont',
                       'Police du titre', 
                       'fallback-font-title', 6 );"
        );
        $this->addSql(
            "INSERT INTO admin.datahubsetting (lex_setting_type_id, name, title, value, setting_order) 
                       values(1,'backgroundImgUrl',
                       E'URL de l\'image de fond', 
                       E'https://$contribution_url/banniere-dark\.790600f8d88a5bda\.png', 7 );"
        );
        $this->addSql(
            "INSERT INTO admin.datahubsetting (lex_setting_type_id, name, title, value, setting_order) 
                       values(1,'thumbnailPlaceholder',
                       E'URL de l\'imagette par défaut', 
                        E'https://$admin_url/dist/img/logo\.png', 8 );"
        );
        $this->addSql(
            "INSERT INTO admin.datahubsetting (lex_setting_type_id, name, title, value, setting_order) 
                       values(2,'mapLayerUrl',
                       'Le modèle de carte', 
                       '$mapfile', 9 );"
        );
        $this->addSql(
            "INSERT INTO admin.datahubsetting (lex_setting_type_id, name, title, value, setting_order) 
                       values(3,'qualityWidget',
                       'Activation du widget : Qualité', 
                       false, 9 );"
        );
        $this->addSql(
            "INSERT INTO admin.datahubsetting (lex_setting_type_id, name, title, value, setting_order) 
                       values(3,'respireHome',
                       E'Activer le lien vers l\’accueil', 
                       false, 10 );"
        );
    }

    public function down(Schema $schema): void
    {
        $this->addSql('DROP TABLE admin.datahub');
    }
}
