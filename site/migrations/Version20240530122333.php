<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240530122333 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Add Territoire to table lex_structure_type';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql("insert into admin.lex_structure_type (id, name) values (3, 'Territoire')");
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql("DELETE FROM admin.lex_structure_type WHERE id = 3 and name = 'Territoire'");
    }
}
