<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20250217120000 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'remove setting Dataviz';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        //add missing migration
        $this->addSql("delete from admin.profile_privilege where lex_privilege_id in (select id from admin.lex_privilege where name='DATAVIZ')");
        $this->addSql("delete from admin.lex_privilege where name='DATAVIZ'");
    }

    public function down(Schema $schema): void
    {
         
    }
}
