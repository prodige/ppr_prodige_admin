<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240930104500 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Add Settings for OGC API 2';
    }

    public function up(Schema $schema): void
    {
        $this->addSql("INSERT INTO admin.setting
        (
            id, 
            lex_setting_type_id, 
            lex_setting_category_id, 
            name, 
            title, 
            description, 
            value, 
            setting_order
        )
        VALUES
        (
            nextval('admin.setting_id_seq'::regclass), 
            2, 
            3, 
            'PRO_PROJ_OGCAPI', 
            'Projection(s) utilisable(s) par les services OGC API', 
            'Projection(s) utilisable(s) par les services OGC API', 
            '', 
            6
        )");
    }

    public function down(Schema $schema): void
    {
        // remove secure wms fields from table layer
        $this->addSql("DELETE FROM admin.setting WHERE name = 'PRO_PROJ_OGCAPI'");
    }
}
