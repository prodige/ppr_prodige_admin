<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240102000000 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Migrate to new telechargement links';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs

//        $tabMetadata = $this->connection->fetchAllAssociative("select metadata.uuid, id, data, dom_nom, pk_sous_domaine from catalogue.couche_sdom, metadata where metadata.id = couche_sdom.fmeta_id::bigint ");
        $tabMetadata = $this->connection->fetchAllAssociative(
            "select m.uuid, ms.public_metadata_id , m.data, d.name , s.id
                    from admin.metadata_sheet ms
                    inner join admin.layer l on l.metadata_sheet_id = ms.id 
                    inner join public.metadata m on m.id = ms.public_metadata_id
                    inner join admin.subdomain_metadata_sheet sms on sms.metadata_sheet_id = ms.id 
                    inner join admin.subdomain s on s.id = sms.subdomain_id 
                    inner join admin.domain d on d.id = s.domain_id;"
        );

        foreach($tabMetadata as $ind => $tabInfo){
            
            $id = $tabInfo["public_metadata_id"];
            $data = $tabInfo["data"];
            $uuid = $tabInfo["uuid"];
            $version = "1.0";
            $encoding = "UTF-8";
            $metadata_doc = new \DOMDocument($version, $encoding);
            $entete = "<?xml version=\"" . $version . "\" encoding=\"" . $encoding . "\"?>\n";
            $metadata_data = $entete . $data;
            $telecartoUrl = $_ENV["PRODIGE_URL_TELECHARGEMENT"];
            // $metadata_data = str_replace("&", "&amp;", $metadata_data);
            if (@$metadata_doc->loadXML($metadata_data)) {
                $this->migrateDatas($metadata_doc, $uuid, $telecartoUrl);
                if (isset($metadata_doc)) {
                    $new_metadata_data = $metadata_doc->saveXML();
                    
                    $new_metadata_data = str_replace($entete, "", $new_metadata_data);
                    $info = array("id" => $id, "data" =>$new_metadata_data);
                    $this->addSql("update metadata set data=:data where id=:id", $info );
         
                }
            }
        }   

    }

    protected function migrateDatas(&$metadata_doc, $uuid, $telecartoUrl){
        
        $xpath = new \DOMXpath($metadata_doc);
        $xmlFragment = "<gmd:Link 
        xmlns:gco=\"http://www.isotc211.org/2005/gco\" xmlns:gmd=\"http://www.isotc211.org/2005/gmd\" >
            <gmd:linkage>
                <gmd:URL>".$telecartoUrl."/download/".$uuid."</gmd:URL>
            </gmd:linkage>
        </gmd:Link>";
        
        $appProfile = new \DOMDocument();
        $appProfile->loadXML($xmlFragment);
        $nodeToImport = $appProfile->getElementsByTagName("URL")->item(0);

        //remove existant node
        $elements = @$xpath->query("//gmd:onLine/gmd:CI_OnlineResource/gmd:linkage/gmd:URL");
        if ($elements && $elements->length > 0) {
            foreach($elements as $element){
                if(strpos($element->nodeValue, "panierDownloadFrontalParametrage")!==false){
                    $newnode = $metadata_doc->importNode($nodeToImport, true);
                    $element->parentNode->replaceChild($newnode, $element);
                }   
            }
        }
    
    }

    public function down(Schema $schema): void
    {
        $this->addSql('');
    }
}
