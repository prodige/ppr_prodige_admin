<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231205084646 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'migrate settings Dataviz';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        //add missing migration

        $this->addSql("INSERT INTO admin.setting (lex_setting_type_id, lex_setting_category_id, 
                       module_id, name, title, description, value, setting_order) 
                       values(1, 2, NULL, 'PRO_DATAVIZ_METADATA_UUID',
                       'Uuid de la métadonnée de dataviz modèle', 
                       'Uuid de la métadonnée de dataviz modèle', 
                       '8c018199-b0f9-41f9-83b0-b2b076e5bba1', 13 );"
        );
        $this->addSql(
            "insert into admin.lex_privilege (name) values ('DATAVIZ')"
        );
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql("delete from admin.setting where name in ('PRO_DATAVIZ_METADATA_UUID')");
        $this->addSql("delete from admin.lex_privilege where name='DATAVIZ'");
    }
}
