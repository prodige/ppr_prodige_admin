<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230322000000 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'migrate settings';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        //add missing migration
        
        $this->addSql("INSERT INTO admin.setting (lex_setting_type_id, lex_setting_category_id, 
                       module_id, name, title, description, value, setting_order) 
                       select  1, 2, NULL, 'PRO_NONGEO_METADATA_UUID',
                       'Uuid de la métadonnée de série de données non géographiques modèle', 
                       'Uuid de la métadonnée de série de données non géographiques modèle', 
                       uuid, 12  from public.metadata where id = (select value from admin.setting where name='PRO_NONGEO_METADATA_ID')::int");

    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql("delete from  admin.setting where name in ('PRO_NONGEO_METADATA_ID')");

    }
}
