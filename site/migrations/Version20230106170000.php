<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230106170000 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'migrate table to views in catalogue';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        //add missing migration
        $this->addSql("alter table admin.layer add column wfs_metadata_uuid character varying(255)");
        $this->addSql("alter table admin.layer add column wms_sdom_id integer[]");
        $this->addSql("alter table admin.layer add column cgu_display boolean default false");
        $this->addSql("alter table admin.layer add column cgu_message character varying(2048)");
        $this->addSql("alter table admin.layer add column synchronized_layer boolean default false");
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs

//        $this->addSql("alter table admin.layer drop column wfs_metadata_uuid");
//        $this->addSql("alter table admin.layer drop column wms_sdom_id");
//        $this->addSql("alter table admin.layer drop column cgu_display");
//        $this->addSql("alter table admin.layer drop column cgu_message");
//        $this->addSql("alter table admin.layer drop column synchronized_layer");

    }
}
