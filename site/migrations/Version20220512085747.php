<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220512085747 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE admin.users ADD is_expired BOOLEAN DEFAULT false NOT NULL');

        $this->addSql('CREATE SEQUENCE IF NOT exists admin.engine_id_seq;');
        $this->addSql('SELECT setval(\'admin.engine_id_seq\', (SELECT MAX(id) FROM admin.engine))');
        $this->addSql('ALTER TABLE admin.engine ALTER id SET DEFAULT nextval(\'admin.engine_id_seq\')');

        $this->addSql('CREATE SEQUENCE IF NOT exists admin.format_id_seq;');
        $this->addSql('SELECT setval(\'admin.format_id_seq\', (SELECT MAX(id) FROM admin.format));');
        $this->addSql('ALTER TABLE admin.format ALTER id SET DEFAULT nextval(\'admin.format_id_seq\')');

        $this->addSql('CREATE SEQUENCE IF NOT exists admin.setting_id_seq;');
        $this->addSql('SELECT setval(\'admin.setting_id_seq\', (SELECT MAX(id) FROM admin.setting));');
        $this->addSql('ALTER TABLE admin.setting ALTER id SET DEFAULT nextval(\'admin.setting_id_seq\')');

        $this->addSql('CREATE SEQUENCE IF NOT exists admin.template_id_seq;');
        $this->addSql('SELECT setval(\'admin.template_id_seq\', (SELECT MAX(id) FROM admin.template));');
        $this->addSql('ALTER TABLE admin.template ALTER id SET DEFAULT nextval(\'admin.template_id_seq\')');

        $this->addSql('CREATE SEQUENCE IF NOT exists admin.projection_id_seq;');
        $this->addSql('SELECT setval(\'admin.projection_id_seq\', (SELECT MAX(id) FROM admin.projection));');
        $this->addSql('ALTER TABLE admin.projection ALTER id SET DEFAULT nextval(\'admin.projection_id_seq\')');

        $this->addSql('CREATE SEQUENCE IF NOT exists admin.module_id_seq;');
        $this->addSql('SELECT setval(\'admin.module_id_seq\', (SELECT MAX(id) FROM admin.module));');
        $this->addSql('ALTER TABLE admin.module ALTER id SET DEFAULT nextval(\'admin.module_id_seq\')');

        $this->addSql('CREATE SEQUENCE IF NOT exists admin.lex_layer_type_id_seq;');
        $this->addSql('SELECT setval(\'admin.lex_layer_type_id_seq\', (SELECT MAX(id) FROM admin.lex_layer_type));');
        $this->addSql('ALTER TABLE admin.lex_layer_type ALTER id SET DEFAULT nextval(\'admin.lex_layer_type_id_seq\')');

        $this->addSql('CREATE SEQUENCE IF NOT exists admin.lex_engine_type_id_seq;');
        $this->addSql('SELECT setval(\'admin.lex_engine_type_id_seq\', (SELECT MAX(id) FROM admin.lex_engine_type));');
        $this->addSql('ALTER TABLE admin.lex_engine_type ALTER id SET DEFAULT nextval(\'admin.lex_engine_type_id_seq\')');

        $this->addSql('CREATE SEQUENCE IF NOT exists admin.lex_template_type_id_seq;');
        $this->addSql('SELECT setval(\'admin.lex_template_type_id_seq\', (SELECT MAX(id) FROM admin.lex_template_type));');
        $this->addSql(
            'ALTER TABLE admin.lex_template_type ALTER id SET DEFAULT nextval(\'admin.lex_template_type_id_seq\')'
        );

        $this->addSql('CREATE UNIQUE INDEX UNIQ_C96D98B95E237E06 ON admin.format (name)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_C96D98B977153098 ON admin.format (code)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_3BB2AB1F5E237E06 ON admin.setting (name)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_E418CF135E237E06 ON admin.template (mapfile)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_7796E3905E237E06 ON admin.projection (name)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_7796E390AE896EB3 ON admin.projection (epsg)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_1BF3CC4E5E237E06 ON admin.module (name)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE admin.users DROP is_expired');
    }
}
