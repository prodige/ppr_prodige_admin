<?php

declare(strict_types=1);

namespace DoctrineMigrations\FreshInstall;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20241003115712 extends AbstractMigration
{
    private ContainerInterface $container;
    private Connection $prodige;
    private Connection $catalogue;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
        $this->prodige = $this->container->get('doctrine')->getConnection('prodige');
        $this->catalogue = $this->connection;
    }

    public function getDescription(): string
    {
        return 'Fresh Install : add data';
    }

    public function up(Schema $schema): void
    {
        $password = '$2y$13$ez8xoype7HQii/s9xaDQxuEOqqmI2U6Qg4T2OF3Ic.RVhcJMS5ECO';

        $this->addSql("INSERT INTO admin.metadata_sheet VALUES (2, 10, NULL, '2024-10-02 16:08:52', NULL)");
        $this->addSql("INSERT INTO admin.metadata_sheet VALUES (1, 9, NULL, '2024-10-02 16:08:52', NULL)");
        $this->addSql("select setval('admin.metadata_sheet_id_seq', (select MAX(id) from admin.metadata_sheet))");

        $this->addSql("INSERT INTO admin.layer VALUES (1, 1, 1, NULL, 'departements', 'departements', 'departements', false, false, false, false, false, NULL, true, false, '', '2017-04-26 11:00:16', '2024-10-02 16:08:52', NULL, NULL, NULL, false, null, false, false, null, false, false, false, false)");
        $this->addSql("select setval('admin.layer_id_seq', (select MAX(id) from admin.layer))");

        $this->addSql("INSERT INTO admin.users VALUES (3, 'vinternet', 'Internet', 'Internet', 'NEANT', NULL, NULL, NULL, NULL, 'dcaa6e60155776107c638af755498759', '2024-07-01 00:00:00', true, NULL, '2024-10-02 16:08:52', NULL, false, 0, NULL, NULL, 1, false)");
        $this->addSql("INSERT INTO admin.users VALUES (2, 'admincli', 'admincli', 'admincli', 'no-reply@prodige-opensource.org', NULL, NULL, NULL, 'utilisateur administrateur système', '" . $password . "', '2050-01-01 00:00:00', false, NULL, '2024-10-02 16:08:52', NULL, false, 40784, NULL, NULL, 1, false)");
        $this->addSql("INSERT INTO admin.users VALUES (1, 'admin@prodige-opensource.org', 'PRODIGE_ADM', 'ADM', 'admin@prodige-opensource.org', '', '', 'Service', '', '$2a$13$0tmnOQ5VQm1xxsg6tCRF6Osu4sYC.VzdS8tl7diWLa8vimmulSigK', '2024-07-01 00:00:00', false, NULL, '2024-10-02 16:08:52', NULL, false, 40102, NULL, NULL, 1, false)");
        $this->addSql("select setval('admin.users_id_seq', (select MAX(id) from admin.users))");

        $this->addSql("INSERT INTO admin.rubric VALUES (1, 'PRODIGE', '2024-10-02 16:08:52', NULL)");
        $this->addSql("select setval('admin.rubric_id_seq', (select MAX(id) from admin.rubric))");

        $this->addSql("INSERT INTO admin.domain VALUES (1, 1, 'Demonstration', 'D&eacute;monstration', '2009-03-06 14:40:21', NULL)");
        $this->addSql("select setval('admin.domain_id_seq', (select MAX(id) from admin.domain))");

        $this->addSql("INSERT INTO admin.help VALUES (1, 'Manuel de l&#039;outil de consultation de cartes', 'Ce manuel présente les fonctions du module de consultation de cartes.', '2024-10-02 16:08:52', NULL, './ressources/Manuel Outil de consultation cartographique.pdf')");
        $this->addSql("INSERT INTO admin.help VALUES (2, 'Manuel de l&#039;outil de gestion des m&eacute;tadonn&eacute;es', 'Ce manuel présente les fonctions d''administration et de consultation du catalogue.', '2024-10-02 16:08:52', NULL, './ressources/Manuel Outil de gestion des metadonnees.pdf')");
        $this->addSql("INSERT INTO admin.help VALUES (3, 'Manuel de l&#039;outil d&#039;administration de cartes', 'Ce manuel présente les fonctions du module d''administration de cartes.', '2024-10-02 16:08:52', NULL, './ressources/Manuel Outil d&#039;administration cartographique.pdf')");
        $this->addSql("select setval('admin.help_id_seq', (select MAX(id) from admin.help))");

        $this->addSql("INSERT INTO admin.map VALUES (1, 2, NULL, NULL, 1, 'limites_administratives.map', 'limites_administratives', 'limites_administratives', false, NULL, NULL, '2009-03-06 17:53:49', NULL)");
        $this->addSql("select setval('admin.map_id_seq', (select MAX(id) from admin.map))");

        $this->addSql("INSERT INTO admin.profile VALUES (219, 'Internet', 'Internet', true, NULL, '2012-04-11 12:19:46', NULL, false)");
        $this->addSql("INSERT INTO admin.profile VALUES (200, 'Administrateur Prodige', 'Administrateur principal de l''application', true, NULL, '2007-02-26 16:58:27', NULL, true)");
        $this->addSql("select setval('admin.profile_id_seq', (select MAX(id) from admin.profile))");

        $this->addSql("INSERT INTO admin.profile_domain_access VALUES (200, 1)");

        $this->addSql("INSERT INTO admin.profile_privilege VALUES (200, 3)");
        $this->addSql("INSERT INTO admin.profile_privilege VALUES (200, 2)");
        $this->addSql("INSERT INTO admin.profile_privilege VALUES (200, 7)");
        $this->addSql("INSERT INTO admin.profile_privilege VALUES (200, 4)");
        $this->addSql("INSERT INTO admin.profile_privilege VALUES (200, 1)");

        $this->addSql("INSERT INTO admin.subdomain VALUES (1, 200, 200, 200, 1, 'France', 'France', false, NULL, '2009-03-06 14:57:45', NULL)");
        $this->addSql("select setval('admin.subdomain_id_seq', (select MAX(id) from admin.subdomain))");

        $this->addSql("INSERT INTO admin.profile_subdomain_access VALUES (200, 1)");

        $this->addSql("INSERT INTO admin.profile_user VALUES (200, 1, 0, '2007-03-02 11:20:55', NULL, NULL)");
        $this->addSql("INSERT INTO admin.profile_user VALUES (219, 3, 0, '2012-04-11 12:19:46', NULL, NULL)");
        $this->addSql("INSERT INTO admin.profile_user VALUES (200, 2, 1, '2024-10-02 16:08:52', NULL, NULL)");

        $this->addSql("INSERT INTO admin.profiles_helps VALUES (200, 1)");
        $this->addSql("INSERT INTO admin.profiles_helps VALUES (219, 1)");
        $this->addSql("INSERT INTO admin.profiles_helps VALUES (200, 3)");
        $this->addSql("INSERT INTO admin.profiles_helps VALUES (200, 2)");

        $this->addSql("INSERT INTO admin.setting (lex_setting_type_id, lex_setting_category_id, module_id, name, title, description, value, setting_order) VALUES (5, 8, NULL, 'PRO_DELAI_ALERTE_DEMANDE_NON_TRAITE', 'Gestion de la file d''attente : délai d''alerte de demande non traitée (jours)', 'Gestion de la file d''attente : délai d''alerte de demande non traitée (jours)', '5', 0) on conflict (name) do nothing");
        $this->addSql("INSERT INTO admin.setting (lex_setting_type_id, lex_setting_category_id, module_id, name, title, description, value, setting_order) VALUES (5, 8, NULL, 'PRO_DELAI_ALERTE_DEMANDE_NON_TRAITE', 'Gestion de la file d''attente : délai d''alerte de demande non traitée (jours)', 'Gestion de la file d''attente : délai d''alerte de demande non traitée (jours)', '5', 0) on conflict (name) do nothing");
        $this->addSql("INSERT INTO admin.setting (lex_setting_type_id, lex_setting_category_id, module_id, name, title, description, value, setting_order) VALUES (1, 7, NULL, 'PRO_PUBLIPOSTAGE_AUTOMATIC_MESSAGE', 'Publipostage : Message automatique en pied de message', 'Publipostage : Message automatique en pied de message', 'Ce message est généré automatiquemenet, merci de ne pas répondre.', 0) on conflict (name) do nothing");
        $this->addSql("INSERT INTO admin.setting (lex_setting_type_id, lex_setting_category_id, module_id, name, title, description, value, setting_order) VALUES (1, 7, NULL, 'PRO_REQUETEUR_PARADRESSE', 'URL du service web Openls utilisé pour la recherche par adresse', 'URL du service web Openls utilisé pour la recherche par adresse', 'https://data.geopf.fr/geocodage/completion', 0) on conflict (name) do nothing");
        $this->addSql("INSERT INTO admin.setting (lex_setting_type_id, lex_setting_category_id, module_id, name, title, description, value, setting_order) VALUES (5, 8, NULL, 'PRO_TIMEOUT_TELE_DIRECT', 'Timeout du service de téléchargement (en ms)', 'Timeout du service de téléchargement (en ms)', '18000', 0) on conflict (name) do nothing");
        $this->addSql("INSERT INTO admin.setting (lex_setting_type_id, lex_setting_category_id, module_id, name, title, description, value, setting_order) VALUES (3, 1, NULL, 'PRO_IS_CARTEPERSO_ACTIF', 'Module carte personnelle', 'Module carte personnelle', 'off', 0) on conflict (name) do nothing");
        $this->addSql("INSERT INTO admin.setting (lex_setting_type_id, lex_setting_category_id, module_id, name, title, description, value, setting_order) VALUES (2, 3, NULL, 'PRO_IMPORT_EPSG', 'Projection principale de la plateforme (code EPSG)', 'Projection principale de la plateforme (code EPSG)', 'EPSG:2154', 0) on conflict (name) do nothing");
        $this->addSql("INSERT INTO admin.setting (lex_setting_type_id, lex_setting_category_id, module_id, name, title, description, value, setting_order) VALUES (5, 8, NULL, 'PRO_RASTER_INFO_ECW_SIZE_LIMIT', 'Taille maximale des fichiers Raster ECW téléchargés (en octet)', 'Taille maximale des fichiers Raster ECW téléchargés (en octet)', '500000000', 0) on conflict (name) do nothing");
        $this->addSql("INSERT INTO admin.setting (lex_setting_type_id, lex_setting_category_id, module_id, name, title, description, value, setting_order) VALUES (3, 1, NULL, 'PRO_IS_REQ_JOINTURES_ACTIF', 'Module requêtes et jointures', 'Module requêtes et jointures', 'off', 0) on conflict (name) do nothing");
        $this->addSql("INSERT INTO admin.setting (lex_setting_type_id, lex_setting_category_id, module_id, name, title, description, value, setting_order) VALUES (5, 8, NULL, 'PRO_RASTER_INFO_GTIFF_SIZE_LIMIT', 'Taille maximale des fichiers Raster GeoTIFF téléchargés (en octet)', 'Taille maximale des fichiers Raster GeoTIFF téléchargés (en octet)', '100000000000', 0) on conflict (name) do nothing");
        $this->addSql("INSERT INTO admin.setting (lex_setting_type_id, lex_setting_category_id, module_id, name, title, description, value, setting_order) VALUES (3, 2, NULL, 'PRO_CATALOGUE_NB_SESSION_USER', 'Afficher le nombre de visiteur', 'Afficher le nombre de visiteur', 'off', 0) on conflict (name) do nothing");
        $this->addSql("INSERT INTO admin.setting (lex_setting_type_id, lex_setting_category_id, module_id, name, title, description, value, setting_order) VALUES (1, 8, NULL, 'PRO_CATALOGUE_TELECHARGEMENT_LICENCE_URL', 'Url du fichier de licence', 'Url du fichier de licence', '', 0) on conflict (name) do nothing");
        $this->addSql("INSERT INTO admin.setting (lex_setting_type_id, lex_setting_category_id, module_id, name, title, description, value, setting_order) VALUES (3, 8, NULL, 'PRO_CATALOGUE_TELECHARGEMENT_LICENCE', 'Affichage d''une licence dans l''interface de téléchargement', 'Affichage d''une licence dans l''interface de téléchargement', 'off', 0) on conflict (name) do nothing");
        $this->addSql("INSERT INTO admin.setting (lex_setting_type_id, lex_setting_category_id, module_id, name, title, description, value, setting_order) VALUES (3, 1, NULL, 'PRO_PUBLIPOSTAGE', 'Module publipostage', 'Module publipostage', 'off', 0) on conflict (name) do nothing");
        $this->addSql("INSERT INTO admin.setting (lex_setting_type_id, lex_setting_category_id, module_id, name, title, description, value, setting_order) VALUES (3, 1, NULL, 'PRO_EDITION', 'Module édition en ligne', 'Module édition en ligne', 'off', 0) on conflict (name) do nothing");
        $this->addSql("INSERT INTO admin.setting (lex_setting_type_id, lex_setting_category_id, module_id, name, title, description, value, setting_order) VALUES (3, 1, NULL, 'PRO_MODULE_STANDARDS', 'Module qualité', 'Module qualité', 'off', 0) on conflict (name) do nothing");
        $this->addSql("INSERT INTO admin.setting (lex_setting_type_id, lex_setting_category_id, module_id, name, title, description, value, setting_order) VALUES (3, 1, NULL, 'PRO_MODULE_BASE_TERRITORIALE', 'Module Base territoriale', 'Module Base territoriale', 'off', 0) on conflict (name) do nothing");
        $this->addSql("INSERT INTO admin.setting (lex_setting_type_id, lex_setting_category_id, module_id, name, title, description, value, setting_order) VALUES (3, 1, NULL, 'PRO_IS_OPENDATA_ACTIF', 'Module opendata', 'Module opendata', 'off', 0) on conflict (name) do nothing");
        $this->addSql("INSERT INTO admin.setting (lex_setting_type_id, lex_setting_category_id, module_id, name, title, description, value, setting_order) VALUES (3, 1, NULL, 'PRO_IS_RAWGRAPH_ACTIF', 'Module graphe', 'Module graphe', 'off', 0) on conflict (name) do nothing");
        $this->addSql("INSERT INTO admin.setting (lex_setting_type_id, lex_setting_category_id, module_id, name, title, description, value, setting_order) VALUES (3, 1, NULL, 'PRO_MODULE_TABLE_EDITION', 'Module édition de table', 'Module édition de table', 'off', 0) on conflict (name) do nothing");
        $this->addSql("INSERT INTO admin.setting (lex_setting_type_id, lex_setting_category_id, module_id, name, title, description, value, setting_order) VALUES (2, 6, NULL, 'PRO_FLUXATOM_PROJECTION', 'Projection(s) disponible(s) dans les flux ATOM', 'Projection(s) disponible(s) dans les flux ATOM', '', 0) on conflict (name) do nothing");
        $this->addSql("INSERT INTO admin.setting (lex_setting_type_id, lex_setting_category_id, module_id, name, title, description, value, setting_order) VALUES (1, 4, NULL, 'wms_server_title', 'Nom du serveur WMS', 'Nom du serveur WMS', 'WMS Demo Server', 0) on conflict (name) do nothing");
        $this->addSql("INSERT INTO admin.setting (lex_setting_type_id, lex_setting_category_id, module_id, name, title, description, value, setting_order) VALUES (1, 4, NULL, 'wms_server_abstract', 'Résumé WMS', 'Résumé WMS', 'abstract', 0) on conflict (name) do nothing");
        $this->addSql("INSERT INTO admin.setting (lex_setting_type_id, lex_setting_category_id, module_id, name, title, description, value, setting_order) VALUES (1, 5, NULL, 'wfs_server_title', 'Nom du serveur WFS', 'Nom du serveur WFS', 'WFS demo server', 0) on conflict (name) do nothing");
        $this->addSql("INSERT INTO admin.setting (lex_setting_type_id, lex_setting_category_id, module_id, name, title, description, value, setting_order) VALUES (1, 5, NULL, 'wfs_server_abstract', 'Résumé WFS', 'Résumé WFS', 'abstract', 0) on conflict (name) do nothing");
        $this->addSql("INSERT INTO admin.setting (lex_setting_type_id, lex_setting_category_id, module_id, name, title, description, value, setting_order) VALUES (2, 3, NULL, 'PRO_PROJ_DEFAULT_WMS', 'Projection par défaut des services WMS', 'Projection par défaut des services WMS', 'EPSG:3857', 1) on conflict (name) do nothing");
        $this->addSql("INSERT INTO admin.setting (lex_setting_type_id, lex_setting_category_id, module_id, name, title, description, value, setting_order) VALUES (2, 3, NULL, 'PRO_PROJ_WMS', 'Projection(s) utilisable(s) par les services WMS', 'Projection(s) utilisable(s) par les services WMS', 'EPSG:2154 EPSG:4326 EPSG:3857', 2) on conflict (name) do nothing");
        $this->addSql("INSERT INTO admin.setting (lex_setting_type_id, lex_setting_category_id, module_id, name, title, description, value, setting_order) VALUES (2, 3, NULL, 'PRO_PROJ_DEFAULT_WFS', 'Projection par défaut des services WFS', 'Projection par défaut des services WFS', 'EPSG:3857', 3) on conflict (name) do nothing");
        $this->addSql("INSERT INTO admin.setting (lex_setting_type_id, lex_setting_category_id, module_id, name, title, description, value, setting_order) VALUES (2, 3, NULL, 'PRO_PROJ_WFS', 'Projection(s) utilisable(s) par les services WFS', 'Projection(s) utilisable(s) par les services WFS', 'EPSG:2154 EPSG:4326 EPSG:3857', 4) on conflict (name) do nothing");
        $this->addSql("INSERT INTO admin.setting (lex_setting_type_id, lex_setting_category_id, module_id, name, title, description, value, setting_order) VALUES (2, 6, NULL, 'PRO_FLUXATOM_FORMAT', 'Format(s) disponible(s) dans les flux ATOM', 'Format(s) disponible(s) dans les flux ATOM', '', 1) on conflict (name) do nothing");
        $this->addSql("INSERT INTO admin.setting (lex_setting_type_id, lex_setting_category_id, module_id, name, title, description, value, setting_order) VALUES (3, 2, NULL, 'PRO_CATALOGUE_CONTACT_ADMIN', 'Possibilité de contacter un admininstrateur', 'Possibilité de contacter un admininstrateur', 'off', 1) on conflict (name) do nothing");
        $this->addSql("INSERT INTO admin.setting (lex_setting_type_id, lex_setting_category_id, module_id, name, title, description, value, setting_order) VALUES (1, 2, NULL, 'PRO_CATALOGUE_EMAIL_AUTO', 'Adresse des mails automatiques', 'Adresse des mails automatiques', 'no_reply@prodige.fr', 2) on conflict (name) do nothing");
        $this->addSql("INSERT INTO admin.setting (lex_setting_type_id, lex_setting_category_id, module_id, name, title, description, value, setting_order) VALUES (1, 2, NULL, 'PRO_WMS_METADATA_ID', 'Identifiant de la métadonnée de service WMS', 'Identifiant de la métadonnée de service WMS', '40020', 3) on conflict (name) do nothing");
        $this->addSql("INSERT INTO admin.setting (lex_setting_type_id, lex_setting_category_id, module_id, name, title, description, value, setting_order) VALUES (1, 2, NULL, 'PRO_WFS_METADATA_ID', 'Identifiant de la métadonnée de service WFS', 'Identifiant de la métadonnée de service WFS', '40021', 4) on conflict (name) do nothing");
        $this->addSql("INSERT INTO admin.setting (lex_setting_type_id, lex_setting_category_id, module_id, name, title, description, value, setting_order) VALUES (1, 2, NULL, 'PRO_NONGEO_METADATA_ID', 'Identifiant de la métadonnée de série de données non géographiques modèle', 'Identifiant de la métadonnée de série de données non géographiques modèle', '40787', 5) on conflict (name) do nothing");
        $this->addSql("INSERT INTO admin.setting (lex_setting_type_id, lex_setting_category_id, module_id, name, title, description, value, setting_order) VALUES (1, 2, NULL, 'PRO_DOWNLOAD_METADATA_ID', 'Identifiant de la métadonnée de service de téléchargement libre', 'Identifiant de la métadonnée de service de téléchargement libre', '40022', 6) on conflict (name) do nothing");
        $this->addSql("INSERT INTO admin.setting (lex_setting_type_id, lex_setting_category_id, module_id, name, title, description, value, setting_order) VALUES (1, 2, NULL, 'PRO_GEO_METADATA_UUID', 'Uuid de la métadonnée de série de données géographiques modèle', 'Uuid de la métadonnée de série de données géographiques modèle', '493039f0-eef0-11da-a2ce-000c2929140b', 7) on conflict (name) do nothing");
        $this->addSql("INSERT INTO admin.setting (lex_setting_type_id, lex_setting_category_id, module_id, name, title, description, value, setting_order) VALUES (1, 2, NULL, 'PRO_DOWNLOAD_METADATA_UUID', 'Uuid de la métadonnée de service de téléchargment libre', 'Uuid de la métadonnée de service de téléchargment libre', '80a7ceef-0bd5-4238-a631-f72fb3330ded', 8) on conflict (name) do nothing");
        $this->addSql("INSERT INTO admin.setting (lex_setting_type_id, lex_setting_category_id, module_id, name, title, description, value, setting_order) VALUES (1, 2, NULL, 'PRO_WMS_METADATA_UUID', 'Uuid de la métadonnée de service WMS', 'Uuid de la métadonnée de service WMS', '1c661f3e-33b5-4410-aa0d-c85f21a8ae7d', 9) on conflict (name) do nothing");
        $this->addSql("INSERT INTO admin.setting (lex_setting_type_id, lex_setting_category_id, module_id, name, title, description, value, setting_order) VALUES (1, 2, NULL, 'PRO_WFS_METADATA_UUID', 'Uuid de la métadonnée de service WFS', 'Uuid de la métadonnée de service WFS', '83aa3f50-c5ed-4048-930f-91ee4b983e5c', 10) on conflict (name) do nothing");
        $this->addSql("INSERT INTO admin.setting (lex_setting_type_id, lex_setting_category_id, module_id, name, title, description, value, setting_order) VALUES (1, 2, NULL, 'PRO_FEATURE_CATALOGUE_METADATA_UUID', 'Uuid de la métadonnée modèle de catalogue d''attribut', 'Uuid de la métadonnée modèle de catalogue d''attribut', 'c4e06459-add2-47e3-86a7-14addeb70e92', 11) on conflict (name) do nothing");
        $this->addSql("INSERT INTO admin.setting (lex_setting_type_id, lex_setting_category_id, module_id, name, title, description, value, setting_order) VALUES (1, 2, NULL, 'PRO_NONGEO_METADATA_UUID', 'Uuid de la métadonnée de série de données non géographiques modèle', 'Uuid de la métadonnée de série de données non géographiques modèle', '0f9a5c54-0a56-436e-b33f-a24cf270097b', 12) on conflict (name) do nothing");
        $this->addSql("INSERT INTO admin.setting (lex_setting_type_id, lex_setting_category_id, module_id, name, title, description, value, setting_order) VALUES (1, 2, NULL, 'PRO_DATAVIZ_METADATA_UUID', 'Uuid de la métadonnée de dataviz modèle', 'Uuid de la métadonnée de dataviz modèle', '8c018199-b0f9-41f9-83b0-b2b076e5bba1', 13) on conflict (name) do nothing");

        $this->addSql("INSERT INTO admin.subdomain_metadata_sheet VALUES (2, 1)");
        $this->addSql("INSERT INTO admin.subdomain_metadata_sheet VALUES (1, 1)");
    }

    public function down(Schema $schema): void
    {
        
    }

}
