<?php

declare(strict_types=1);

namespace DoctrineMigrations\FreshInstall;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20241003115714 extends AbstractMigration implements ContainerAwareInterface
{
    private ContainerInterface $container;
    private Connection $catalogue;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
        $this->catalogue = $this->connection;
    }

    public function getDescription(): string
    {
        return 'Migrate bdd special html characters html encoded';
    }

    public function up(Schema $schema): void
    {
        $table = 'layer';
        $fields = ['name', 'description'];
        $this->updateDataFields($table, $fields);
        $table = 'map';
        $fields = ['name', 'description'];
        $this->updateDataFields($table, $fields);
        $table = 'profile';
        $fields = ['name', 'description'];
        $this->updateDataFields($table, $fields);
        $table = 'profile';
        $fields = ['name', 'description'];
        $this->updateDataFields($table, $fields);
        $table = 'rubric';
        $fields = ['name'];
        $this->updateDataFields($table, $fields);
        $table = 'structure';
        $fields = ['name', 'sigle'];
        $this->updateDataFields($table, $fields);
        $table = 'subdomain';
        $fields = ['name', 'description'];
        $this->updateDataFields($table, $fields);
        $table = 'domain';
        $fields = ['name', 'description'];
        $this->updateDataFields($table, $fields);
        $table = 'help';
        $fields = ['title', 'description'];
        $this->updateDataFields($table, $fields);
        $table = 'skill';
        $fields = ['name'];
        $this->updateDataFields($table, $fields);

    }

    private function updateDataFields($data, $fields)
    {
        $results = $this->catalogue->fetchAllAssociative('SELECT id, ' . implode(',', $fields) . ' from admin.' . $data);
        foreach ($results as $result) {
            foreach ($fields as $field) {
                $strSql = $field . '=:' . $field;
                $result[$field] = !is_null($result[$field]) ? html_entity_decode($result[$field]) : $result[$field];
                $this->addSql("update admin." . $data . " set " . $strSql . " where id =:id ", $result);
            }

        }
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }

}
