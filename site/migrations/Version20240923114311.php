<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240923114311 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Add Settings for OGC';
    }

    public function up(Schema $schema): void
    {
        // add secure wms fields to table layer
        $this->addSql("INSERT INTO admin.lex_setting_category
        (id, name)
        VALUES(nextval('admin.lex_setting_category_id_seq'::regclass), 'ogcapi');");

        $this->addSql("INSERT INTO admin.setting
        (
            id, 
            lex_setting_type_id, 
            lex_setting_category_id, 
            name, 
            title, 
            description, 
            value, 
            setting_order
        )
        VALUES(
            nextval('admin.setting_id_seq'::regclass), 
            1, 
            (SELECT id FROM admin.lex_setting_category WHERE name='ogcapi'), 
            'OGC_API_TITLE', 
            'Prodige OGC API title', 
            'title for ogc api', 
            'Prodige OGC API', 
            0
        ),(
            nextval('admin.setting_id_seq'::regclass), 
            1, 
            (SELECT id FROM admin.lex_setting_category WHERE name='ogcapi'), 
            'OGC_API_ABSTRACT', 
            'Prodige OGC API abstract', 
            'abstract for ogc api', 
            'abstract', 
            0
        )");
    }

    public function down(Schema $schema): void
    {
        // remove secure wms fields from table layer
        $this->addSql("DELETE FROM admin.setting WHERE name = 'OGC_API_TITLE'");
        $this->addSql("DELETE FROM admin.setting WHERE name = 'OGC_API_ABSTRACT'");
        $this->addSql("DELETE FROM admin.lex_setting_category WHERE name = 'ogcapi'");
    }
}
