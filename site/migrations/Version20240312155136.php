<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240312155136 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Add structure and uniqueness of storage path';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE admin.structure ADD parent_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE admin.structure ADD CONSTRAINT FK_9F7DDC7E727ACA70 FOREIGN KEY (parent_id) REFERENCES admin.structure (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_9F7DDC7E727ACA70 ON admin.structure (parent_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_848F0484279401A ON admin.layer (storage_path)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE admin.structure DROP CONSTRAINT FK_9F7DDC7E727ACA70');
        $this->addSql('DROP INDEX IDX_9F7DDC7E727ACA70');
        $this->addSql('ALTER TABLE admin.structure DROP parent_id');
        $this->addSql('DROP INDEX UNIQ_A71DFD475E237E06');
        $this->addSql('DROP INDEX UNIQ_848F0484279401A');
    }
}
