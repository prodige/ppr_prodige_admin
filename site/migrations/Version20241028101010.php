<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use Ramsey\Uuid\Uuid;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20241028101010 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Add XML for API';
    }

    public function up(Schema $schema): void
    {
        $uuid = Uuid::uuid4()->toString();

        $this->addSql("INSERT INTO admin.setting
        (id, lex_setting_type_id, lex_setting_category_id, module_id, name, title, description, value, setting_order)
        VALUES(
            nextval('admin.setting_id_seq'::regclass), 
            1, 
            2, 
            NULL, 
            'PRO_OGCAPI_FEATURES_METADATA_UUID', 
            'Uuid de la métadonnée de service OGCAPI', 
            'Uuid de la métadonnée de service OGCAPI', 
            '$uuid', 
            10);");

        $this->addSql("INSERT INTO public.metadata (id, uuid, schemaid, istemplate, isharvested, createdate, changedate, data, source, title, root, harvestuuid, owner, groupowner, harvesturi, rating, popularity, displayorder, doctype, extra) 
        (select max ( id) + 1, '$uuid', 'iso19139', 'n', 'n', '2024-10-31T11:00:41', '2024-10-31T11:00:41', '<gmd:MD_Metadata xmlns:gmd=\"http://www.isotc211.org/2005/gmd\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:gco=\"http://www.isotc211.org/2005/gco\" xmlns:srv=\"http://www.isotc211.org/2005/srv\" xmlns:gmx=\"http://www.isotc211.org/2005/gmx\" xmlns:gts=\"http://www.isotc211.org/2005/gts\" xmlns:gsr=\"http://www.isotc211.org/2005/gsr\" xmlns:gmi=\"http://www.isotc211.org/2005/gmi\" xmlns:gml=\"http://www.opengis.net/gml/3.2\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xsi:schemaLocation=\"http://www.isotc211.org/2005/gmd http://schemas.opengis.net/csw/2.0.2/profiles/apiso/1.0.0/apiso.xsd\">
<gmd:fileIdentifier>
    <gco:CharacterString>$uuid</gco:CharacterString>
</gmd:fileIdentifier>
<gmd:language xmlns:gfc=\"http://www.isotc211.org/2005/gfc\">
    <gco:CharacterString>fre</gco:CharacterString>
</gmd:language>
<gmd:characterSet xmlns:gfc=\"http://www.isotc211.org/2005/gfc\">
    <gmd:MD_CharacterSetCode codeListValue=\"utf8\" codeList=\"http://standards.iso.org/iso/19139/resources/gmxCodelists.xml#MD_CharacterSetCode\" />
</gmd:characterSet>
<gmd:hierarchyLevel xmlns:gfc=\"http://www.isotc211.org/2005/gfc\">
    <gmd:MD_ScopeCode codeListValue=\"service\" codeList=\"http://standards.iso.org/iso/19139/resources/gmxCodelists.xml#MD_ScopeCode\" />
</gmd:hierarchyLevel>
<gmd:hierarchyLevelName xmlns:gfc=\"http://www.isotc211.org/2005/gfc\">
    <gco:CharacterString>Service</gco:CharacterString>
</gmd:hierarchyLevelName>
<gmd:contact xmlns:gfc=\"http://www.isotc211.org/2005/gfc\">
    <gmd:CI_ResponsibleParty>
    <gmd:organisationName>
        <gco:CharacterString>Prodige opensource</gco:CharacterString>
    </gmd:organisationName>
    <gmd:contactInfo>
        <gmd:CI_Contact>
        <gmd:phone>
            <gmd:CI_Telephone />
        </gmd:phone>
        <gmd:address>
            <gmd:CI_Address>
            <gmd:electronicMailAddress>
                <gco:CharacterString>contact@prodige-opensource.org</gco:CharacterString>
            </gmd:electronicMailAddress>
            </gmd:CI_Address>
        </gmd:address>
        </gmd:CI_Contact>
    </gmd:contactInfo>
    <gmd:role>
        <gmd:CI_RoleCode codeList=\"http://standards.iso.org/iso/19139/resources/gmxCodelists.xml#CI_RoleCode\" codeListValue=\"pointOfContact\" />
    </gmd:role>
    </gmd:CI_ResponsibleParty>
</gmd:contact>
<gmd:dateStamp xmlns:gfc=\"http://www.isotc211.org/2005/gfc\">
    <gco:DateTime>2024-09-25T09:43:27.85505Z</gco:DateTime>
</gmd:dateStamp>
<gmd:metadataStandardName xmlns:gfc=\"http://www.isotc211.org/2005/gfc\">
    <gco:CharacterString>ISO 19115:2003/19139</gco:CharacterString>
</gmd:metadataStandardName>
<gmd:metadataStandardVersion xmlns:gfc=\"http://www.isotc211.org/2005/gfc\">
    <gco:CharacterString>1.0</gco:CharacterString>
</gmd:metadataStandardVersion>
<gmd:referenceSystemInfo xmlns:gfc=\"http://www.isotc211.org/2005/gfc\">
    <gmd:MD_ReferenceSystem>
    <gmd:referenceSystemIdentifier>
        <gmd:RS_Identifier>
        <gmd:code>
            <gco:CharacterString>RGF93 / Lambert-93 (EPSG:2154)</gco:CharacterString>
        </gmd:code>
        <gmd:codeSpace>
            <gco:CharacterString>EPSG</gco:CharacterString>
        </gmd:codeSpace>
        <gmd:version>
            <gco:CharacterString>7.4</gco:CharacterString>
        </gmd:version>
        </gmd:RS_Identifier>
    </gmd:referenceSystemIdentifier>
    </gmd:MD_ReferenceSystem>
</gmd:referenceSystemInfo>
<gmd:identificationInfo xmlns:date=\"http://exslt.org/dates-and-times\" xmlns:gfc=\"http://www.isotc211.org/2005/gfc\">
    <srv:SV_ServiceIdentification>
    <gmd:citation>
        <gmd:CI_Citation>
        <gmd:title>
            <gco:CharacterString>Service OGC API Features de la plateforme</gco:CharacterString>
        </gmd:title>
        <gmd:date>
            <gmd:CI_Date>
            <gmd:date>
                <gco:Date>2010-01-01</gco:Date>
            </gmd:date>
            <gmd:dateType>
                <gmd:CI_DateTypeCode codeList=\"http://standards.iso.org/iso/19139/resources/gmxCodelists.xml#CI_DateTypeCode\" codeListValue=\"creation\" />
            </gmd:dateType>
            </gmd:CI_Date>
        </gmd:date>
        <gmd:identifier>
            <gmd:MD_Identifier>
            <gmd:code>
                <gco:CharacterString>https://catalogue-test.prodige-ecolab.fr/geonetwork/srv/27879905-936b-41ba-87d2-7f843cd5d1eb</gco:CharacterString>
            </gmd:code>
            </gmd:MD_Identifier>
        </gmd:identifier>
        </gmd:CI_Citation>
    </gmd:citation>
    <gmd:abstract>
        <gco:CharacterString>Service OGC API Features de la plateforme</gco:CharacterString>
    </gmd:abstract>
    <gmd:pointOfContact>
        <gmd:CI_ResponsibleParty>
        <gmd:organisationName>
            <gco:CharacterString>Contact</gco:CharacterString>
        </gmd:organisationName>
        <gmd:contactInfo>
            <gmd:CI_Contact>
            <gmd:address>
                <gmd:CI_Address>
                <gmd:electronicMailAddress>
                    <gco:CharacterString>contact@prodige-opensource.org</gco:CharacterString>
                </gmd:electronicMailAddress>
                </gmd:CI_Address>
            </gmd:address>
            </gmd:CI_Contact>
        </gmd:contactInfo>
        <gmd:role>
            <gmd:CI_RoleCode codeList=\"http://standards.iso.org/iso/19139/resources/gmxCodelists.xml#CI_RoleCode\" codeListValue=\"owner\" />
        </gmd:role>
        </gmd:CI_ResponsibleParty>
    </gmd:pointOfContact>
    <gmd:descriptiveKeywords>
        <gmd:MD_Keywords>
        <gmd:keyword>
            <gco:CharacterString>OGC:API Features</gco:CharacterString>
        </gmd:keyword>
        </gmd:MD_Keywords>
    </gmd:descriptiveKeywords>
    <gmd:descriptiveKeywords>
        <gmd:MD_Keywords>
        <gmd:keyword>
            <gco:CharacterString>infoFeatureAccessService</gco:CharacterString>
        </gmd:keyword>
        <gmd:type>
            <gmd:MD_KeywordTypeCode codeList=\"http://standards.iso.org/iso/19139/resources/gmxCodelists.xml#MD_KeywordTypeCode\" codeListValue=\"theme\" />
        </gmd:type>
        <gmd:thesaurusName>
            <gmd:CI_Citation>
            <gmd:title>
                <gco:CharacterString>INSPIRE Service taxonomy</gco:CharacterString>
            </gmd:title>
            <gmd:date>
                <gmd:CI_Date>
                <gmd:date>
                    <gco:Date>2010-01-01</gco:Date>
                </gmd:date>
                <gmd:dateType>
                    <gmd:CI_DateTypeCode codeList=\"http://standards.iso.org/iso/19139/resources/gmxCodelists.xml#CI_DateTypeCode\" codeListValue=\"publication\" />
                </gmd:dateType>
                </gmd:CI_Date>
            </gmd:date>
            <gmd:identifier>
                <gmd:MD_Identifier>
                <gmd:code>
                    <gmx:Anchor xlink:href=\"https://catalogue-test.prodige-ecolab.fr/geonetwork/srv/api/registries/vocabularies/external.theme.inspire-service-taxonomy\">geonetwork.thesaurus.external.theme.inspire-service-taxonomy</gmx:Anchor>
                </gmd:code>
                </gmd:MD_Identifier>
            </gmd:identifier>
            </gmd:CI_Citation>
        </gmd:thesaurusName>
        </gmd:MD_Keywords>
    </gmd:descriptiveKeywords>
    <gmd:resourceConstraints>
        <gmd:MD_LegalConstraints>
        <gmd:useLimitation>
            <gco:CharacterString>Aucune condition ne s’applique</gco:CharacterString>
        </gmd:useLimitation>
        <gmd:accessConstraints>
            <gmd:MD_RestrictionCode codeList=\"http://standards.iso.org/iso/19139/resources/gmxCodelists.xml#MD_RestrictionCode\" codeListValue=\"otherRestrictions\" />
        </gmd:accessConstraints>
        <gmd:otherConstraints>
            <gco:CharacterString>Pas de restriction d’accès public selon INSPIRE</gco:CharacterString>
        </gmd:otherConstraints>
        </gmd:MD_LegalConstraints>
    </gmd:resourceConstraints>
    <gmd:resourceConstraints>
        <gmd:MD_Constraints />
    </gmd:resourceConstraints>
    <srv:serviceType>
        <gco:LocalName>download</gco:LocalName>
    </srv:serviceType>
    <srv:serviceTypeVersion>
        <gco:CharacterString>OGC API-Features</gco:CharacterString>
    </srv:serviceTypeVersion>
    <srv:extent>
        <gmd:EX_Extent>
        <gmd:description>
            <gco:CharacterString> Description de l''étendue géographique (eg France métropolitaine) </gco:CharacterString>
        </gmd:description>
        <gmd:geographicElement>
            <gmd:EX_GeographicBoundingBox>
            <gmd:westBoundLongitude>
                <gco:Decimal>-5.449218749023903</gco:Decimal>
            </gmd:westBoundLongitude>
            <gmd:eastBoundLongitude>
                <gco:Decimal>11.777343747890368</gco:Decimal>
            </gmd:eastBoundLongitude>
            <gmd:southBoundLatitude>
                <gco:Decimal>40.1116886595881</gco:Decimal>
            </gmd:southBoundLatitude>
            <gmd:northBoundLatitude>
                <gco:Decimal>51.34433865388288</gco:Decimal>
            </gmd:northBoundLatitude>
            </gmd:EX_GeographicBoundingBox>
        </gmd:geographicElement>
        </gmd:EX_Extent>
    </srv:extent>
    <srv:couplingType>
        <srv:SV_CouplingType codeList=\"http://www.isotc211.org/2005/iso19119/resources/Codelist/gmxCodelists.xml#SV_CouplingType\" codeListValue=\"tight\" />
    </srv:couplingType>
    <srv:containsOperations>
        <srv:SV_OperationMetadata>
        <srv:operationName>
            <gco:CharacterString>API-Specification</gco:CharacterString>
        </srv:operationName>
        <srv:DCP>
            <srv:DCPList codeList=\"http://www.isotc211.org/2005/iso19119/resources/Codelist/gmxCodelists.xml#DCPList\" codeListValue=\"WebServices\" />
        </srv:DCP>
        <srv:operationDescription>
            <gco:CharacterString>Landing page</gco:CharacterString>
        </srv:operationDescription>
        <srv:connectPoint>
            <gmd:CI_OnlineResource>
            <gmd:linkage>
                <gmd:URL>https://api-test.prodige-ecolab.fr/api/ogc-features</gmd:URL>
            </gmd:linkage>
            <gmd:protocol>
                <gmx:Anchor xlink:href=\"http://www.opengis.net/def/docs/17-069r3\">OGC API-Features</gmx:Anchor>
            </gmd:protocol>
            </gmd:CI_OnlineResource>
        </srv:connectPoint>
        </srv:SV_OperationMetadata>
    </srv:containsOperations>
    </srv:SV_ServiceIdentification>
</gmd:identificationInfo>
<gmd:distributionInfo>
    <gmd:MD_Distribution>
    <gmd:transferOptions>
        <gmd:MD_DigitalTransferOptions>
        <gmd:onLine>
            <gmd:CI_OnlineResource>
            <gmd:linkage>
                <gmd:URL>https ://api.prodige.internal/api/ogc-features</gmd:URL>
            </gmd:linkage>
            <gmd:protocol>
                <gco:CharacterString>OGC API - Features</gco:CharacterString>
            </gmd:protocol>
            <gmd:name>
                <gco:CharacterString>API OGC Features</gco:CharacterString>
            </gmd:name>
            <gmd:function>
                <gmd:CI_OnLineFunctionCode codeList=\"http://standards.iso.org/iso/19139/resources/gmxCodelists.xml#CI_OnLineFunctionCode\" codeListValue=\"information\" />
            </gmd:function>
            </gmd:CI_OnlineResource>
        </gmd:onLine>
        </gmd:MD_DigitalTransferOptions>
    </gmd:transferOptions>
    </gmd:MD_Distribution>
</gmd:distributionInfo>
<gmd:dataQualityInfo xmlns:gfc=\"http://www.isotc211.org/2005/gfc\">
    <gmd:DQ_DataQuality>
    <gmd:scope>
        <gmd:DQ_Scope>
        <gmd:level>
            <gmd:MD_ScopeCode codeListValue=\"service\" codeList=\"http://standards.iso.org/iso/19139/resources/gmxCodelists.xml#MD_ScopeCode\" />
        </gmd:level>
        <gmd:levelDescription />
        </gmd:DQ_Scope>
    </gmd:scope>
    <gmd:report>
        <gmd:DQ_DomainConsistency>
        <gmd:result>
            <gmd:DQ_ConformanceResult>
            <gmd:specification>
                <gmd:CI_Citation>
                <gmd:title>
                    <gco:CharacterString>Technical Guidance for the implementation of INSPIRE Download Services – level2 - v3.0</gco:CharacterString>
                </gmd:title>
                <gmd:date>
                    <gmd:CI_Date>
                    <gmd:date>
                        <gco:Date>2012-06-12</gco:Date>
                    </gmd:date>
                    <gmd:dateType>
                        <gmd:CI_DateTypeCode codeList=\"http://standards.iso.org/iso/19139/resources/gmxCodelists.xml#CI_DateTypeCode\" codeListValue=\"publication\" />
                    </gmd:dateType>
                    </gmd:CI_Date>
                </gmd:date>
                </gmd:CI_Citation>
            </gmd:specification>
            <gmd:explanation>
                <gco:CharacterString>cf. la spécification citée</gco:CharacterString>
            </gmd:explanation>
            <gmd:pass>
                <gco:Boolean>true</gco:Boolean>
            </gmd:pass>
            </gmd:DQ_ConformanceResult>
        </gmd:result>
        </gmd:DQ_DomainConsistency>
    </gmd:report>
    <gmd:report />
    <gmd:lineage>
        <gmd:LI_Lineage>
        <gmd:statement>
            <gco:CharacterString>Service OGC API Features de PRODIGE. Contient l''ensemble des ressource couplées de la plateforme</gco:CharacterString>
        </gmd:statement>
        </gmd:LI_Lineage>
    </gmd:lineage>
    </gmd:DQ_DataQuality>
</gmd:dataQualityInfo>
</gmd:MD_Metadata>
', '7fc45be3-9aba-4198-920c-b8737112d522', NULL, NULL, NULL, 1, 1, NULL, 0, 0, NULL, NULL, NULL from public.metadata )
      ");

    }

    public function down(Schema $schema): void
    {
        // remove secure wms fields from table layer
        $this->addSql("");
    }
}
