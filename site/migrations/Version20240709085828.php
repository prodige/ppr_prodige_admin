<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20240709085828 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Add field to structure and clean migrations';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('set search_path TO admin, public');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_A71DFD475E237E06 ON admin.datahubsetting (name)');
        $this->addSql('CREATE INDEX IF NOT EXISTS IDX_A71DFD47DCA6FBAC ON admin.datahubsetting (lex_setting_type_id)');
        $this->addSql('ALTER INDEX IF EXISTS admin.constraint_uuid RENAME TO UNIQ_9F01EE0EF076628');
        $this->addSql('ALTER TABLE admin.structure ADD email VARCHAR(128) DEFAULT NULL');
        $this->addSql('ALTER TABLE admin.structure ADD phone_number VARCHAR(30) DEFAULT NULL');
        $this->addSql('ALTER TABLE admin.structure ADD address VARCHAR(1024) DEFAULT NULL');
        $this->addSql('ALTER TABLE admin.structure ADD zip_code VARCHAR(128) DEFAULT NULL');
        $this->addSql('ALTER TABLE admin.structure ADD city VARCHAR(128) DEFAULT NULL');
        $this->addSql('ALTER TABLE admin.structure ADD country VARCHAR(128) DEFAULT NULL');
        $this->addSql('ALTER TABLE admin.structure ADD reference_geonetwork INT DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('set search_path TO admin, public');
        $this->addSql('ALTER TABLE admin.structure DROP email');
        $this->addSql('ALTER TABLE admin.structure DROP phone_number');
        $this->addSql('ALTER TABLE admin.structure DROP address');
        $this->addSql('ALTER TABLE admin.structure DROP zip_code');
        $this->addSql('ALTER TABLE admin.structure DROP city');
        $this->addSql('ALTER TABLE admin.structure DROP country');
        $this->addSql('ALTER TABLE admin.structure DROP reference_geonetwork');
        $this->addSql('DROP INDEX UNIQ_A71DFD475E237E06');
        $this->addSql('DROP INDEX IF EXISTS IDX_A71DFD47DCA6FBAC');

    }
}
