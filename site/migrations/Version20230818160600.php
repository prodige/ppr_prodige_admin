<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230818160600 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Add template contribution';
    }

    public function up(Schema $schema): void
    {
        $this->addSql(
            "INSERT INTO admin.lex_template_type(id, name) VALUES (3, 'Contribution')"
        );
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs

        $this->addSql("delete from admin.lex_template_type where id=3");

    }
}
