<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220825083000 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE UNIQUE INDEX UNIQ_B07EF46D5E237E06 ON admin.domain (name)');
        $this->addSql('DROP INDEX admin.uniq_c96d98b95e237e06');
        $this->addSql('DROP INDEX admin.uniq_c96d98b977153098');
        $this->addSql('CREATE UNIQUE INDEX format_unique ON admin.format (name, code)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_7A8103035E237E06 ON admin.lex_layer_type (name)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_B85426935E237E06 ON admin.lex_map_format (name)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_EFB953315E237E06 ON admin.lex_privilege (name)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_FF038E505E237E06 ON admin.lex_setting_category (name)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_1F52BEE25E237E06 ON admin.lex_setting_type (name)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_F6E01BE35E237E06 ON admin.lex_structure_type (name)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_1C9D19F45E237E06 ON admin.lex_template_type (name)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_2591B9885E237E06 ON admin.profile (name)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_74D78077AA08CB10 ON admin.users (login)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP INDEX admin.UNIQ_74D78077AA08CB10');
        $this->addSql('DROP INDEX admin.UNIQ_2591B9885E237E06');
        $this->addSql('DROP INDEX admin.UNIQ_1C9D19F45E237E06');
        $this->addSql('DROP INDEX admin.UNIQ_F6E01BE35E237E06');
        $this->addSql('DROP INDEX admin.UNIQ_1F52BEE25E237E06');
        $this->addSql('DROP INDEX admin.UNIQ_FF038E505E237E06');
        $this->addSql('DROP INDEX admin.UNIQ_EFB953315E237E06');
        $this->addSql('DROP INDEX admin.UNIQ_B85426935E237E06');
        $this->addSql('DROP INDEX admin.UNIQ_7A8103035E237E06');
        $this->addSql('DROP INDEX admin.formate_unique');
        $this->addSql('CREATE UNIQUE INDEX uniq_c96d98b95e237e06 ON admin.format (name)');
        $this->addSql('CREATE UNIQUE INDEX uniq_c96d98b977153098 ON admin.format (code)');
        $this->addSql('DROP INDEX admin.UNIQ_B07EF46D5E237E06');
    }
}
