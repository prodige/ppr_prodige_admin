<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220309083844 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Migrate Data from catalogue';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql("insert into admin.lex_structure_type (id, name) values (1, 'SIREN'),(2, 'SIRET')");
        $this->addSql(
            "select setval('admin.lex_structure_type_id_seq', (select MAX(id) from admin.lex_structure_type))"
        );

        $this->addSql(
            "insert into admin.lex_map_format (id, name) values (1, 'carte interactive'),(2, 'carte statique'),
               (3, 'carte personnelle')"
        );
        $this->addSql("select setval('admin.lex_map_format_id_seq', (select MAX(id) from admin.lex_map_format))");

        $this->addSql(
            "insert into admin.lex_privilege  (id, name)  values (1, 'TELECHARGEMENT'), (2, 'NAVIGATION'),
                   (3, 'ADMINISTRATION'), (4, 'CMS'), (7, 'PARAMETRAGE'), (8, 'MAJIC'), (9, 'CARTEPERSO'),(10, 'PUBLIPOSTAGE'),
                   (11, 'EDITION EN LIGNE'), (12, 'EDITION EN LIGNE (AJOUT)'), (13, 'EDITION EN LIGNE (MODIF)')"
        );
        $this->addSql("select setval('admin.lex_privilege_id_seq', (select MAX(id) from admin.lex_privilege))");

        $this->addSql(
            "insert into admin.lex_layer_type  (id, name) values (1, 'vecteur'),(2, 'raster'),
                (3, 'tabulaire'),(4, 'vue')"
        );
        $this->addSql("select setval('admin.lex_layer_type_id_seq', (select MAX(id) from admin.lex_layer_type))");
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
