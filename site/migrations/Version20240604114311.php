<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240604114311 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Add support for secure wms';
    }

    public function up(Schema $schema): void
    {
        // add secure wms fields to table layer
        $this->addSql('ALTER TABLE admin.layer ADD wms_secure BOOLEAN DEFAULT false');
        $this->addSql('ALTER TABLE admin.layer ADD wms_secure_ip_filter BOOLEAN DEFAULT false');
        $this->addSql('ALTER TABLE admin.layer ADD wfs_secure BOOLEAN DEFAULT false');
        $this->addSql('ALTER TABLE admin.layer ADD wfs_secure_ip_filter BOOLEAN DEFAULT false');
    }

    public function down(Schema $schema): void
    {
        // remove secure wms fields from table layer
        $this->addSql('ALTER TABLE admin.layer DROP wms_secure');
        $this->addSql('ALTER TABLE admin.layer DROP wms_secure_ip_filter');
        $this->addSql('ALTER TABLE admin.layer DROP wfs_secure');
        $this->addSql('ALTER TABLE admin.layer DROP wfs_secure_ip_filter');
    }
}
