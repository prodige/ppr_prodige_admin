<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231206161313 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Add PARAMETRAGE_DATAVIZ';
    }

    public function up(Schema $schema): void
    {
        $this->addSql("delete from admin.lex_privilege where name = 'PARAMETRAGE_DATAVIZ'");
        $this->addSql("insert into admin.lex_privilege  (name)  values ('PARAMETRAGE_DATAVIZ')");
    }

    public function down(Schema $schema): void
    {
        $this->addSql("delete from admin.lex_privilege where name ='PARAMETRAGE_DATAVIZ'");
    }
}
