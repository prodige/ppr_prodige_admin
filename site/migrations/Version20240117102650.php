<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240117102650 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Liaison format & lex_layer_type';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE admin.format ADD lex_layer_type_id INT DEFAULT 1 NOT NULL');
        $this->addSql('ALTER TABLE admin.format ADD CONSTRAINT lex_layer_type_id FOREIGN KEY (lex_layer_type_id) REFERENCES admin.lex_layer_type (id)');
        $this->addSql("UPDATE admin.format SET code = 'xlsx' WHERE name = 'XLS (fichier *.xls)'");
        $this->addSql("UPDATE admin.format SET lex_layer_type_id = 3 WHERE code = 'xlsx'");
        $this->addSql("UPDATE admin.format SET lex_layer_type_id = 3 WHERE code = 'ods'");
        $this->addSql("UPDATE admin.format SET lex_layer_type_id = 3 WHERE code = 'sql'");
        $this->addSql("UPDATE admin.format SET lex_layer_type_id = 3 WHERE code = 'csv'");
        $this->addSql("INSERT INTO admin.format (name,code,lex_layer_type_id) VALUES ('Geotiff (fichier *.tiff)','kmgeotiffl',2)");
        $this->addSql("INSERT INTO admin.format (name,code,lex_layer_type_id) VALUES ('Ecw (fichier *.ecw)','ecw',2)");
        $this->addSql("INSERT INTO admin.format (name,code,lex_layer_type_id) VALUES ('Jpeg (fichier *.jpeg)','jpeg',2)");
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }
}
