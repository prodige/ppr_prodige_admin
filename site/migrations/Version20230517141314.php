<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230517141314 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Format migration with conflict management for those who already have them';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql(
            "INSERT INTO admin.format (name,code) VALUES ('MapInfo tab','tab') ON CONFLICT (name,code) DO NOTHING"
        );
        $this->addSql(
            "INSERT INTO admin.format (name,code) VALUES ('ESRI Shapefile','shp') ON CONFLICT (name,code) DO NOTHING"
        );
        $this->addSql(
            "INSERT INTO admin.format (name,code) VALUES ('Atlas BNA','bna') ON CONFLICT (name,code) DO NOTHING"
        );
        $this->addSql(
            "INSERT INTO admin.format (name,code) VALUES ('Comma Separated Value (fichier *.csv)','csv') ON CONFLICT (name,code) DO NOTHING"
        );
        $this->addSql(
            "INSERT INTO admin.format (name,code) VALUES ('Géoconcept Export (fichier *.gxt)','gxt') ON CONFLICT (name,code) DO NOTHING"
        );
        $this->addSql(
            "INSERT INTO admin.format (name,code) VALUES ('MapInfo mif/mid (fichier *.mif,*.mid)','mif') ON CONFLICT (name,code) DO NOTHING"
        );
        $this->addSql(
            "INSERT INTO admin.format (name,code) VALUES ('GML (fichier *.gml)','gml') ON CONFLICT (name,code) DO NOTHING"
        );
        $this->addSql(
            "INSERT INTO admin.format (name,code) VALUES ('GMT (fichier *.gmt)','gmt') ON CONFLICT (name,code) DO NOTHING"
        );
        $this->addSql(
            "INSERT INTO admin.format (name,code) VALUES ('KML (fichier *.kml)','kml') ON CONFLICT (name,code) DO NOTHING"
        );
        $this->addSql(
            "INSERT INTO admin.format (name,code) VALUES ('GeoJson (fichier *.json)','json') ON CONFLICT (name,code) DO NOTHING"
        );
        $this->addSql(
            "INSERT INTO admin.format (name,code) VALUES ('Microstation DGN (fichier *.dgn)','dgn') ON CONFLICT (name,code) DO NOTHING"
        );
        $this->addSql(
            "INSERT INTO admin.format (name,code) VALUES ('DXF (fichier *.dxf)','dxf') ON CONFLICT (name,code) DO NOTHING"
        );
        $this->addSql(
            "INSERT INTO admin.format (name,code) VALUES ('Dump Postgis (fichier *.sql)','sql') ON CONFLICT (name,code) DO NOTHING"
        );
        $this->addSql(
            "INSERT INTO admin.format (name,code) VALUES ('ODS (fichier *.ods)','ods') ON CONFLICT (name,code) DO NOTHING"
        );
        $this->addSql(
            "INSERT INTO admin.format (name,code) VALUES ('XLS (fichier *.xls)','xls') ON CONFLICT (name,code) DO NOTHING"
        );
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql("TRUNCATE TABLE admin.format");
    }
}
