<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240725143811 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Creation of user status management table and import of values';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE admin.lex_account_status (id SERIAL NOT NULL, name VARCHAR(128) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_971095875E237E06 ON admin.lex_account_status (name)');
        $this->addSql("INSERT INTO admin.lex_account_status (id, name) VALUES (1,'actif')" );
        $this->addSql("INSERT INTO admin.lex_account_status (id, name) VALUES (2,'désactivé')");
        $this->addSql("INSERT INTO admin.lex_account_status (id, name) VALUES (3,'en attente d''activation')");
        $this->addSql('ALTER TABLE admin.structure ALTER reference_geonetwork TYPE VARCHAR(255)');
        $this->addSql('ALTER TABLE admin.users ADD lex_account_id INT DEFAULT 1');
        $this->addSql('ALTER TABLE admin.users ADD CONSTRAINT FK_74D78077D5AEE7AA FOREIGN KEY (lex_account_id) REFERENCES admin.lex_account_status (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_74D78077D5AEE7AA ON admin.users (lex_account_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE admin.lex_account_status');
        $this->addSql('ALTER TABLE admin.structure ALTER reference_geonetwork TYPE VARCHAR(252)');
        $this->addSql('ALTER TABLE admin.users DROP lex_account_id');
    }
}
