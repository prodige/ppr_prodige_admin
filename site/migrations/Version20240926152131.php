<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240926152131 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Fix layer with cgu_display null';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('UPDATE admin.layer SET cgu_display = FALSE where cgu_display is null;');
    }

    public function down(Schema $schema): void
    {
    }
}
