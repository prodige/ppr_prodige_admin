<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20221229080400 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'insert PRO_GEO_METADATA_ID';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql("update metadata set data = '<gmd:MD_Metadata xmlns:gts=\"http://www.isotc211.org/2005/gts\"
        xmlns:gml=\"http://www.opengis.net/gml/3.2\"
        xmlns:gmd=\"http://www.isotc211.org/2005/gmd\"
        xmlns:gmi=\"http://www.isotc211.org/2005/gmi\"
        xmlns:gmx=\"http://www.isotc211.org/2005/gmx\"
        xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"
        xmlns:srv=\"http://www.isotc211.org/2005/srv\"
        xmlns:gco=\"http://www.isotc211.org/2005/gco\"
        xmlns:xlink=\"http://www.w3.org/1999/xlink\"
        xmlns:gsr=\"http://www.isotc211.org/2005/gsr\">
<gmd:fileIdentifier>
<gco:CharacterString>95f4718b-b9f2-4beb-be87-e6352c349ceb</gco:CharacterString>
</gmd:fileIdentifier>
<gmd:language xmlns:gfc=\"http://www.isotc211.org/2005/gfc\">
<gmd:LanguageCode codeList=\"http://www.loc.gov/standards/iso639-2/\" codeListValue=\"fre\"/>
</gmd:language>
<gmd:characterSet xmlns:gfc=\"http://www.isotc211.org/2005/gfc\">
<gmd:MD_CharacterSetCode codeListValue=\"utf8\"
                      codeList=\"http://standards.iso.org/iso/19139/resources/gmxCodelists.xml#MD_CharacterSetCode\"/>
</gmd:characterSet>
<gmd:hierarchyLevel xmlns:gfc=\"http://www.isotc211.org/2005/gfc\">
<gmd:MD_ScopeCode codeList=\"http://standards.iso.org/iso/19139/resources/gmxCodelists.xml#MD_ScopeCode\"
               codeListValue=\"dataset\"/>
</gmd:hierarchyLevel>
<gmd:hierarchyLevelName xmlns:gfc=\"http://www.isotc211.org/2005/gfc\">
<gco:CharacterString>Série de données</gco:CharacterString>
</gmd:hierarchyLevelName>
<gmd:contact xmlns:gfc=\"http://www.isotc211.org/2005/gfc\">
<gmd:CI_ResponsibleParty>
<gmd:organisationName>
   <gco:CharacterString>-- Nom du point de contact des métadonnées --</gco:CharacterString>
</gmd:organisationName>
<gmd:contactInfo>
   <gmd:CI_Contact>
      <gmd:address>
         <gmd:CI_Address>
            <gmd:electronicMailAddress>
               <gco:CharacterString>-- Adresse email --</gco:CharacterString>
            </gmd:electronicMailAddress>
         </gmd:CI_Address>
      </gmd:address>
   </gmd:CI_Contact>
</gmd:contactInfo>
<gmd:role>
   <gmd:CI_RoleCode codeList=\"http://standards.iso.org/iso/19139/resources/gmxCodelists.xml#CI_RoleCode\"
                    codeListValue=\"pointOfContact\"/>
</gmd:role>
</gmd:CI_ResponsibleParty>
</gmd:contact>
<gmd:dateStamp xmlns:gfc=\"http://www.isotc211.org/2005/gfc\">
<gco:DateTime>2022-12-29T08:34:14</gco:DateTime>
</gmd:dateStamp>
<gmd:metadataStandardName xmlns:gfc=\"http://www.isotc211.org/2005/gfc\">
<gco:CharacterString>ISO 19115:2003/19139</gco:CharacterString>
</gmd:metadataStandardName>
<gmd:metadataStandardVersion xmlns:gfc=\"http://www.isotc211.org/2005/gfc\">
<gco:CharacterString>1.0</gco:CharacterString>
</gmd:metadataStandardVersion>
<gmd:identificationInfo xmlns:gfc=\"http://www.isotc211.org/2005/gfc\">
<gmd:MD_DataIdentification>
<gmd:citation>
   <gmd:CI_Citation>
      <gmd:title>
         <gco:CharacterString>Modèle de métadonnée INSPIRE</gco:CharacterString>
      </gmd:title>
      <gmd:date>
         <gmd:CI_Date>
            <gmd:date>
               <gco:Date>2011-01-01</gco:Date>
            </gmd:date>
            <gmd:dateType>
               <gmd:CI_DateTypeCode codeList=\"http://standards.iso.org/iso/19139/resources/gmxCodelists.xml#CI_DateTypeCode\"
                                    codeListValue=\"creation\"/>
            </gmd:dateType>
         </gmd:CI_Date>
      </gmd:date>
   </gmd:CI_Citation>
</gmd:citation>
<gmd:abstract>
   <gco:CharacterString>-- Court résumé explicatif du contenu de la ressource --</gco:CharacterString>
</gmd:abstract>
<gmd:pointOfContact>
   <gmd:CI_ResponsibleParty>
      <gmd:organisationName>
         <gco:CharacterString>-- Nom de l''organisation responsable de la série de données --</gco:CharacterString>
      </gmd:organisationName>
      <gmd:contactInfo>
         <gmd:CI_Contact>
            <gmd:address>
               <gmd:CI_Address>
                  <gmd:electronicMailAddress>
                     <gco:CharacterString>-- Adresse email --</gco:CharacterString>
                  </gmd:electronicMailAddress>
               </gmd:CI_Address>
            </gmd:address>
         </gmd:CI_Contact>
      </gmd:contactInfo>
      <gmd:role>
         <gmd:CI_RoleCode codeList=\"http://standards.iso.org/iso/19139/resources/gmxCodelists.xml#CI_RoleCode\"
                          codeListValue=\"owner\"/>
      </gmd:role>
   </gmd:CI_ResponsibleParty>
</gmd:pointOfContact>
<gmd:spatialRepresentationType>
   <gmd:MD_SpatialRepresentationTypeCode codeList=\"http://standards.iso.org/iso/19139/resources/gmxCodelists.xml#MD_SpatialRepresentationTypeCode\"
                                         codeListValue=\"vector\"/>
</gmd:spatialRepresentationType>
<gmd:spatialResolution>
   <gmd:MD_Resolution>
      <gmd:equivalentScale>
         <gmd:MD_RepresentativeFraction>
            <gmd:denominator>
               <gco:Integer/>
            </gmd:denominator>
         </gmd:MD_RepresentativeFraction>
      </gmd:equivalentScale>
   </gmd:MD_Resolution>
</gmd:spatialResolution>
<gmd:language>
   <gco:CharacterString>fre</gco:CharacterString>
</gmd:language>
<gmd:extent>
   <gmd:EX_Extent xmlns:xs=\"http://www.w3.org/2001/XMLSchema\">
      <gmd:geographicElement>
         <gmd:EX_GeographicBoundingBox>
            <gmd:westBoundLongitude>
               <gco:Decimal>-5.00</gco:Decimal>
            </gmd:westBoundLongitude>
            <gmd:eastBoundLongitude>
               <gco:Decimal>10.00</gco:Decimal>
            </gmd:eastBoundLongitude>
            <gmd:southBoundLatitude>
               <gco:Decimal>42.00</gco:Decimal>
            </gmd:southBoundLatitude>
            <gmd:northBoundLatitude>
               <gco:Decimal>51.00</gco:Decimal>
            </gmd:northBoundLatitude>
         </gmd:EX_GeographicBoundingBox>
      </gmd:geographicElement>
   </gmd:EX_Extent>
</gmd:extent>
</gmd:MD_DataIdentification>
</gmd:identificationInfo>
<gmd:distributionInfo xmlns:gfc=\"http://www.isotc211.org/2005/gfc\">
<gmd:MD_Distribution>
<gmd:distributionFormat>
   <gmd:MD_Format>
      <gmd:name gco:nilReason=\"missing\">
         <gco:CharacterString/>
      </gmd:name>
      <gmd:version gco:nilReason=\"unknown\">
         <gco:CharacterString/>
      </gmd:version>
   </gmd:MD_Format>
</gmd:distributionFormat>
<gmd:transferOptions>
   <gmd:MD_DigitalTransferOptions/>
</gmd:transferOptions>
</gmd:MD_Distribution>
</gmd:distributionInfo>
<gmd:dataQualityInfo xmlns:gfc=\"http://www.isotc211.org/2005/gfc\">
<gmd:DQ_DataQuality>
<gmd:scope>
   <gmd:DQ_Scope>
      <gmd:level>
         <gmd:MD_ScopeCode codeListValue=\"dataset\"
                           codeList=\"http://standards.iso.org/iso/19139/resources/gmxCodelists.xml#MD_ScopeCode\"/>
      </gmd:level>
   </gmd:DQ_Scope>
</gmd:scope>
<gmd:lineage>
   <gmd:LI_Lineage>
      <gmd:statement>
         <gco:CharacterString>-- Historique du traitement et/ou de la qualité générale de la série de
données géographiques --</gco:CharacterString>
      </gmd:statement>
   </gmd:LI_Lineage>
</gmd:lineage>
</gmd:DQ_DataQuality>
</gmd:dataQualityInfo>
</gmd:MD_Metadata>' where id=2");
        $this->addSql("INSERT INTO admin.setting (lex_setting_type_id, lex_setting_category_id, ".
                      "module_id, name, title, description, value, setting_order) ".
                      "VALUES (1, 2, NULL, 'PRO_GEO_METADATA_UUID',".
                      "'Uuid de la métadonnée de série de données géographiques modèle', ".
                      "'Uuid de la métadonnée de série de données géographiques modèle', '493039f0-eef0-11da-a2ce-000c2929140b', 7);");

    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql("DELETE FROM admin.setting where name= 'PRO_GEO_METADATA_UUID';");
    }
}
