<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220620120620 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE admin.engine ALTER field_id TYPE VARCHAR(255)');
        $this->addSql('ALTER TABLE admin.engine ALTER field_id DROP DEFAULT');
        $this->addSql('ALTER TABLE admin.engine RENAME COLUMN "order" TO row_order');
        $this->addSql('ALTER TABLE admin.engine ALTER join_field DROP NOT NULL');
        $this->addSql('ALTER TABLE admin.engine RENAME COLUMN "table" TO layer');
        $this->addSql('ALTER TABLE admin.engine ADD layer_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE admin.engine DROP layer');
        $this->addSql('ALTER TABLE admin.engine ADD CONSTRAINT FK_FF7FF0EBEA6EFDCD FOREIGN KEY (layer_id) REFERENCES admin.layer (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_FF7FF0EBEA6EFDCD ON admin.engine (layer_id)');
        $this->addSql('ALTER TABLE admin.setting ADD setting_order INT DEFAULT 0 NOT NULL');
        $this->addSql("UPDATE admin.setting SET setting_order = 1 WHERE name = 'PRO_PROJ_DEFAULT_WMS'");
        $this->addSql("UPDATE admin.setting SET setting_order = 2 WHERE name = 'PRO_PROJ_WMS'");
        $this->addSql("UPDATE admin.setting SET setting_order = 3 WHERE name = 'PRO_PROJ_DEFAULT_WFS'");
        $this->addSql("UPDATE admin.setting SET setting_order = 4 WHERE name = 'PRO_PROJ_WFS'");
        $this->addSql("UPDATE admin.setting SET setting_order = 1 WHERE name = 'PRO_FLUXATOM_FORMAT'");
        $this->addSql("UPDATE admin.setting SET setting_order = 1 WHERE name = 'PRO_CATALOGUE_CONTACT_ADMIN'");
        $this->addSql("UPDATE admin.setting SET setting_order = 2 WHERE name = 'PRO_CATALOGUE_EMAIL_AUTO'");
        $this->addSql("UPDATE admin.setting SET setting_order = 3 WHERE name = 'PRO_WMS_METADATA_ID'");
        $this->addSql("UPDATE admin.setting SET setting_order = 4 WHERE name = 'PRO_WFS_METADATA_ID'");
        $this->addSql("UPDATE admin.setting SET setting_order = 5 WHERE name = 'PRO_NONGEO_METADATA_ID'");
        $this->addSql("UPDATE admin.setting SET setting_order = 6 WHERE name = 'PRO_DOWNLOAD_METADATA_ID'");
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE admin.engine ALTER field_id TYPE INT');
        $this->addSql('ALTER TABLE admin.engine ALTER field_id DROP DEFAULT');
        $this->addSql('ALTER TABLE admin.engine RENAME COLUMN row_order TO "order"');
        $this->addSql('ALTER TABLE admin.engine ALTER join_field SET NOT NULL');
        $this->addSql('ALTER TABLE admin.engine RENAME COLUMN layer TO "table"');
        $this->addSql('ALTER TABLE admin.engine DROP CONSTRAINT FK_FF7FF0EBEA6EFDCD');
        $this->addSql('DROP INDEX IDX_FF7FF0EBEA6EFDCD');
        $this->addSql('ALTER TABLE admin.engine ADD layer VARCHAR(128) NOT NULL');
        $this->addSql('ALTER TABLE admin.engine DROP layer_id');
        $this->addSql('ALTER TABLE admin.setting DROP setting_order');
    }
}
