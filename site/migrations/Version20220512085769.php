<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220512085769 extends AbstractMigration implements ContainerAwareInterface
{
    private ContainerInterface $container;
    private Connection $prodige;
    private Connection $catalogue;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
        $this->prodige = $this->container->get('doctrine')->getConnection('prodige');
        $this->catalogue = $this->connection;
    }

    public function getDescription(): string
    {
        return 'Migrate adminsite data';
    }

    public function up(Schema $schema): void
    {
        $types = [
            'textfield' => 1,
            'selectfield' => 2,
            'multiselectfield' => 2,
            'checkboxfield' => 3,
            'timefield' => 4,
        ];

        // LEX SETTING TYPE
        $this->addSql(
            "INSERT INTO admin.lex_setting_type(id, name)
                    VALUES (1, 'TextType'), (2, 'ChoiceType'), (3, 'CheckboxType'), (4, 'DateType'), (5, 'NumberType')"
        );

        // LEX SETTING CATEGORIES
        $this->addSql(
            "INSERT INTO admin.lex_setting_category(id, name)
                    VALUES (1, 'module'),(2, 'catalogue'), (3, 'projection'), (4, 'wms'), (5, 'wfs'), (6, 'atom'), (7, 'viewer'), (8, 'download')"
        );

        // LEX ENGINE TYPE
        $this->addSql("INSERT INTO admin.lex_engine_type(id, name) VALUES (1, 'download'), (2, 'search')");

        // LEX TEMPLATE TYPE
        $this->addSql(
            "INSERT INTO admin.lex_template_type(id, name) VALUES (1, 'Visualisation'), (2, 'Extraction')"
        );

        $publicationPath = $this->container->getParameter('PRODIGE_PATH_DATA').'/cartes/Publication';

        // IMPORT Modeles
        if (file_exists($publicationPath."/Modeles_Administration.xml")) {
            $xmlDoc = simplexml_load_file($publicationPath."/Modeles_Administration.xml");
            foreach ($xmlDoc->MODELE as $modele) {
                $this->addSql(
                    "INSERT INTO admin.template (lex_template_type_id, name, mapfile) VALUES (".(int)$modele["TYPE"].", ".$this->catalogue->quote($modele["NAME"]).", ".$this->catalogue->quote($modele["ID"]).")"
                );
            }
        }

        // IMPORT Formats
        if (file_exists($publicationPath."/Formats_Administration.xml")) {
            $xmlDoc = simplexml_load_file($publicationPath."/Formats_Administration.xml");
            foreach ($xmlDoc->FORMAT as $format) {
                $this->addSql(
                    "INSERT INTO admin.format (name, code) VALUES (".$this->catalogue->quote($format["NOM"]).", ".$this->catalogue->quote($format["CODE"]).")"
                );
            }
        }

        // IMPORT Projections
        if (file_exists($publicationPath."/Projections_Administration.xml")) {
            $xmlDoc = simplexml_load_file($publicationPath."/Projections_Administration.xml");
            foreach ($xmlDoc->PROJECTION as $projection) {
                $this->addSql(
                    "INSERT INTO admin.projection (name, epsg)
                            VALUES (".$this->catalogue->quote($projection["NOM"]).", ".$this->catalogue->quote($projection["EPSG"]).")"
                );
            }
        }

//        // IMPORT SETTING (default)
//        $settings = $this->prodige->fetchAllAssociative('SELECT * from parametrage.prodige_settings');
//        $this->importSettings($settings);
//
//        // IMPORT SETTING (catalogue)
//        $settings = $this->catalogue->fetchAllAssociative('SELECT * from catalogue.prodige_settings');
//        $this->importSettings($settings);
//
//        // IMPORT SETTING (fluxatom)
//        $settings = $this->prodige->fetchAllAssociative('SELECT * from parametrage.prodige_settings_fluxatom');
//        $this->importSettings($settings);

        // New
        $this->addSql(
            "INSERT INTO admin.setting (lex_setting_type_id, lex_setting_category_id, module_id, name, title, description, value)
                     VALUES(2, 3, null, 'PRO_PROJ_WFS', 'Projection(s) utilisable(s) par les services WFS', 'Projection(s) utilisable(s) par les services WFS', '');"
        );

    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }

    /**
     * @param array $settings
     */
    protected function importSettings(array $settings): void
    {
        $types = [
            'textfield' => 1,
            'selectfield' => 2,
            'multiselectfield' => 2,
            'checkboxfield' => 3,
            'timefield' => 4,
        ];
        foreach ($settings as $setting) {
            $desc = $setting['prodige_settings_desc'] ? addslashes($setting['prodige_settings_desc']) : null;
            $value = $setting['prodige_settings_value'] ? addslashes($setting['prodige_settings_value']) : null;
            $name = $setting['prodige_settings_constant'];
            $type = $types[$setting['prodige_settings_type']];
            $title = empty($setting['prodige_settings_title']) ? $name : addslashes($setting['prodige_settings_title']);
            if (in_array(
                $name,
                [
                    'PRO_IS_REQ_JOINTURES_ACTIF',
                    'PRO_IS_CARTEPERSO_ACTIF',
                    'PRO_MODULE_BASE_TERRITORIALE',
                    'PRO_MODULE_TABLE_EDITION',
                    'PRO_EDITION',
                    'PRO_IS_RAWGRAPH_ACTIF',
                    'PRO_IS_OPENDATA_ACTIF',
                    'PRO_PUBLIPOSTAGE',
                    'PRO_MODULE_STANDARDS',
                ]
            )) {
                $lexCatId = 1;  // Module
                $type = 3;
                $desc = str_replace('activé', '', $desc);
                $desc = str_replace('Activation du', '', $desc);
                $desc = str_replace('Activation des', 'Module', $desc);
            } elseif (in_array(
                $name,
                [
                    'PRO_CATALOGUE_CONTACT_ADMIN',
                    'PRO_CATALOGUE_NB_SESSION_USER',
                    'PRO_CATALOGUE_EMAIL_AUTO',
                    'PRO_WMS_METADATA_ID',
                    'PRO_WFS_METADATA_ID',
                    'PRO_DOWNLOAD_METADATA_ID',
                    'PRO_NONGEO_METADATA_ID',
                ]
            )) {
                if (in_array($name, ['PRO_CATALOGUE_NB_SESSION_USER', 'PRO_CATALOGUE_CONTACT_ADMIN'])) {
                    $type = 3;
                }
                $lexCatId = 2;  // Catalogue
            } elseif (in_array($name, ['PRO_PROJ_DEFAULT_WMS', 'PRO_PROJ_WMS', 'PRO_PROJ_WFS', 'PRO_IMPORT_EPSG'])) {
                if ($name == 'PRO_PROJ_WFS') {
                    $name = 'PRO_PROJ_DEFAULT_WFS';
                }
                if ($name == 'PRO_IMPORT_EPSG') {
                    $value = 'EPSG:'.$value;
                }
                $type = 2;
                $lexCatId = 3; // Projection
            } elseif (in_array($name, ['PRO_FLUXATOM_PROJECTION', 'PRO_FLUXATOM_FORMAT'])) {
                $lexCatId = 6; // ATOM
                $type = 2;
            } elseif (in_array(
                $name,
                [
                    'PRO_REQUETEUR_PARADRESSE',
                    'PRO_PUBLIPOSTAGE_AUTOMATIC_MESSAGE',
                ]
            )) {
                $lexCatId = 7; // Viewer
            } elseif (in_array(
                $name,
                [
                    'PRO_TIMEOUT_TELE_DIRECT',
                    'PRO_RASTER_INFO_ECW_SIZE_LIMIT',
                    'PRO_RASTER_INFO_GTIFF_SIZE_LIMIT',
                    'PRO_CATALOGUE_TELECHARGEMENT_LICENCE',
                    'PRO_CATALOGUE_TELECHARGEMENT_LICENCE_URL',
                    'PRO_DELAI_ALERTE_DEMANDE_NON_TRAITE',
                ]
            )) {
                if ($name != 'PRO_CATALOGUE_TELECHARGEMENT_LICENCE_URL') {
                    $type = 5; // Force number type
                    if ($name == 'PRO_CATALOGUE_TELECHARGEMENT_LICENCE') {
                        $type = 3; // Force checkbox type
                    }
                }
                $lexCatId = 8; // Download
            }

            $desc = ucfirst(trim($desc));
            if (isset($lexCatId)) {
                $this->addSql(
                    "INSERT INTO admin.setting (lex_setting_type_id, lex_setting_category_id, module_id, name, title, description, value)
                     VALUES(".$type.", ".$lexCatId.", null, '".$name."', E'".$desc."', E'".$desc."', E'".$value."');"
                );
            }
            unset($lexCatId);
        }
    }
}
