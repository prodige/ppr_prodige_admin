<?php

declare(strict_types=1);

namespace DoctrineMigrations\From44;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use Prodige\ProdigeBundle\Services\AdminClientService;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240404091208 extends AbstractMigration implements ContainerAwareInterface
{
    private Connection $prodige;
    private Connection $catalogue;

    private ContainerInterface $container;

    private AdminClientService $adminClientService;


    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
        $this->prodige = $container->get('doctrine')->getConnection('prodige');
        $this->catalogue = $container->get('doctrine')->getConnection('catalogue');
        $this->adminClientService = $this->container->get('prodige.adminclient');
    }

    public function getDescription(): string
    {
        return 'Layers migration for api management';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        #GetAllLayers (lex_layer_type 1,3,4)
        
        $allLayers = $this->catalogue->fetchAllAssociative(
            "select metadata.uuid, layer.id, metadata_sheet_id from admin.layer inner join admin.metadata_sheet on layer.metadata_sheet_id = metadata_sheet.id 
            inner join public.metadata on metadata_sheet.public_metadata_id = metadata.id where lex_layer_type_id in (1,3,4);"
        );
        #For each layers get layer
        foreach ($allLayers as $layer) {

            $layerId = $layer['id'];
            $layerUuid = $layer['uuid'];
            $metadata_sheet_id = $layer['metadata_sheet_id'];
             
            $multiLayers = $this->catalogue->fetchOne(
                "select count(id) from admin.layer where metadata_sheet_id in (select id from admin.metadata_sheet where id=:metadata_sheet_id)",
                array("metadata_sheet_id" =>$metadata_sheet_id)
            );
            if($multiLayers==1){

                $fieldsCarmen = $this->prodige->fetchAllAssociative(
                    "  SELECT * from carmen.field where layer_id in (
                       select layer_id from carmen.layer where map_id in (select map_id from carmen.map where map_wmsmetadata_uuid = :uuid 
                       and published_id is null) and layer_metadata_uuid = :uuid)
                ", array("uuid" => $layerUuid ));
                
                $fieldsOpen = [];
                foreach ($fieldsCarmen as $field) {

                    if ($field['field_queries'] === true) {
                        $fieldsOpen[] = $field['field_name'];
                    }
                }
                #if all fields = field_query false = apiLayer = false
                #else apiLayer = true + array all fields
                if (!empty($fieldsOpen)) {

                    $this->addSql(
                        "update admin.layer set api_layer = true, api_fields=:api_fields where id=:layer_id ", 
                        [
                        'layer_id' => $layerId,
                        'api_fields' => json_encode($fieldsOpen)
                        ]
                    );
                }
            }
        }
        
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }
}
