<?php

declare(strict_types=1);

namespace DoctrineMigrations\From44;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230102169999 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'migrate metadata roles';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        // 0/ backup tables temporary
        $this->addSql("create table public.usergroups_bkp_v5 as select * from public.usergroups");
        $this->addSql("create table public.groups_bkp_v5 as select * from public.groups");
        $this->addSql("create table public.operationallowed_bkp_v5 as select * from public.operationallowed");
        $this->addSql("create table public.metadata_bkp_v5 as select * from public.metadata");

        // 1/ remove rights, groups and user/groups
        $this->addSql("delete from public.operationallowed where groupid not in (-1,0,1)");
        $this->addSql("delete from public.usergroups");
        $this->addSql("update metadata set groupowner = null;");
        $this->addSql("delete from public.groupsdes where iddes not in (-1, 0, 1)");
        $this->addSql("delete from public.groups where id not in (-1, 0, 1)");

        // 2/ insert groups and usergroups
        $this->addSql("insert into public.groups(id, name, description) 
                       select nextval('hibernate_sequence'), a.grp_nom, a.grp_description from (
                       select distinct grp_nom, grp_description from catalogue.sous_domaine sd inner join catalogue.groupe_profil gp on sd.ssdom_admin_fk_groupe_profil = gp.pk_groupe_profil ) a");
        $this->addSql("insert into public.groupsdes(iddes, langid, label) select id, 'fre', name from groups where id not in (-1, 0, 1) ");
        $this->addSql("update admin.profile set is_synchronised = true where name in (select distinct grp_nom from catalogue.sous_domaine sd inner join catalogue.groupe_profil gp on sd.ssdom_admin_fk_groupe_profil = gp.pk_groupe_profil) ");
        //insert reviewers
        $this->addSql("insert into usergroups (userid, groupid, profile) select distinct users.id, groups.id, 2 from catalogue.administrateurs_sous_domaine 
                        inner join public.users on usr_id = username 
                        inner join public.groups on administrateurs_sous_domaine.grp_nom = groups.name
                        where usr_id is not null;");
        $this->addSql("insert into usergroups (userid, groupid, profile) select distinct users.id, groups.id, 3 from catalogue.administrateurs_sous_domaine 
                        inner join public.users on usr_id = username 
                        inner join public.groups on administrateurs_sous_domaine.grp_nom = groups.name
                        where usr_id is not null;");
        //insert rights for reviewer only
        $this->addSql("insert into operationallowed (groupid, metadataid, operationid) 
                        select distinct groups.id, public_metadata_id, 2 from admin.metadata_sheet 
                        inner join admin.subdomain_metadata_sheet on metadata_sheet.id = subdomain_metadata_sheet.metadata_sheet_id
                        inner join admin.subdomain on subdomain_metadata_sheet.subdomain_id = subdomain.id
                        inner join admin.profile on subdomain.profile_admin_id =profile.id
                        inner join groups on profile.name=groups.name ");
       
        //insert as viewer too
        $this->addSql("insert into operationallowed (groupid, metadataid, operationid) 
                        select distinct groups.id, public_metadata_id, 0 from admin.metadata_sheet 
                        inner join admin.subdomain_metadata_sheet on metadata_sheet.id = subdomain_metadata_sheet.metadata_sheet_id
                        inner join admin.subdomain on subdomain_metadata_sheet.subdomain_id = subdomain.id
                        inner join admin.profile on subdomain.profile_admin_id =profile.id
                        inner join groups on profile.name=groups.name ");               
                

        //update groupowner with old group, ie subdomain adminisrator
        $this->addSql("update public.metadata set groupowner = (
                        select g.id 
                        from public.metadata_bkp_v5 mbv  
                        inner join public.groups_bkp_v5 gbv on mbv.groupowner = gbv.id 
                        inner join admin.subdomain sd on sd.id = mbv.groupowner 
                        inner join catalogue.groupe_profil gp on sd.profile_admin_id = gp.pk_groupe_profil 
                        inner join public.groups g on gp.grp_nom = g.name
                        where mbv.id = metadata.id) ");
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql("");
    }
}
