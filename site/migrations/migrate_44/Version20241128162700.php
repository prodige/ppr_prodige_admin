<?php

declare(strict_types=1);

namespace DoctrineMigrations\From44;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20241128162700 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'add missing clean catalogue table : unsed table & add view';
    }

    public function up(Schema $schema): void
    {
     
        $this->addSql("ALTER TABLE catalogue.groupe_profil RENAME TO unused_groupe_profil;");
        //create views from admin schema
        $this->addSql("create view catalogue.groupe_profil
                      ( pk_groupe_profil ,ts ,grp_id ,grp_nom, grp_description , grp_is_default_installation, grp_nom_ldap , grp_template_mail) as
                      select id ,created_at, name, name, description , case when is_default_installation then 1 else 0 end , '', template_mail from admin.profile");
        
    }

    public function down(Schema $schema): void
    {
        //NOT TO DOWN
       
    }
}
