<?php

declare(strict_types=1);

namespace DoctrineMigrations\From44;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;


/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20250214000000 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'add missing settings';
    }

    public function up(Schema $schema): void
    {
     
        $this->addSql("INSERT INTO admin.setting (lex_setting_type_id, lex_setting_category_id, 
                       module_id, name, title, description, value, setting_order) 
                       select  1, 2, NULL, 'PRO_DOWNLOAD_METADATA_UUID',
                       'Uuid de la métadonnée de service de téléchargment libre', 
                       'Uuid de la métadonnée de service de téléchargment libre', 
                       uuid, 8  from public.metadata where id = (select value from admin.setting where name='PRO_DOWNLOAD_METADATA_ID')::int
                       AND NOT EXISTS (
                        SELECT 1 
                        FROM admin.setting  
                        WHERE name = 'PRO_DOWNLOAD_METADATA_UUID'
                        )");

        $this->addSql("INSERT INTO admin.setting (lex_setting_type_id, lex_setting_category_id, 
                       module_id, name, title, description, value, setting_order) 
                       select  1, 2, NULL, 'PRO_WMS_METADATA_UUID',
                       'Uuid de la métadonnée de service WMS', 
                       'Uuid de la métadonnée de service WMS', 
                       uuid, 9  from public.metadata where id = (select value from admin.setting where name='PRO_WMS_METADATA_ID')::int
                       AND NOT EXISTS (
                        SELECT 1 
                        FROM admin.setting  
                        WHERE name = 'PRO_WMS_METADATA_UUID'
                        )");
        $this->addSql("INSERT INTO admin.setting (lex_setting_type_id, lex_setting_category_id, 
                       module_id, name, title, description, value, setting_order) 
                       select  1, 2, NULL, 'PRO_WFS_METADATA_UUID',
                       'Uuid de la métadonnée de service WFS', 
                       'Uuid de la métadonnée de service WFS', 
                       uuid, 10  from public.metadata where id = (select value from admin.setting where name='PRO_WFS_METADATA_ID')::int
                       AND NOT EXISTS (
                        SELECT 1 
                        FROM admin.setting  
                        WHERE name = 'PRO_WFS_METADATA_UUID'
                        )");
        
    }

    public function down(Schema $schema): void
    {
        //NOT TO DOWN
       
    }
} 