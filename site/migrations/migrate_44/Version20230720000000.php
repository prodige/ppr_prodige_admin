<?php

declare(strict_types=1);

namespace DoctrineMigrations\From44;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230720000000 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'migrate gn reference value';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        //add missing migration
        
        $this->addSql("update admin.users set reference_gn = p.id 
                       from admin.users a inner join public.users p
                       on p.username=a.login
                       where 
                       p.username=users.login");

    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs

        $this->addSql("update admin.users set reference_gn = 0");

    }
}
