<?php

declare(strict_types=1);

namespace DoctrineMigrations\From44;

use App\Entity\Configuration\Projection;
use App\Entity\Configuration\Setting;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Exception;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Exception\NotSupported;
use Doctrine\ORM\Exception\ORMException;
use Doctrine\ORM\OptimisticLockException;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240308150024 extends AbstractMigration implements ContainerAwareInterface
{
    private Connection $prodige;
    private ContainerInterface $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
        $this->prodige = $container->get('doctrine')->getConnection('prodige');
    }

    public function getDescription(): string
    {
        return 'Synchronize settings PRO_PROJ_WMS + PRO_PROJ_WFS to admin';
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     * @throws NotSupported
     * @throws Exception
     */
    public function up(Schema $schema): void
    {
        $settingWMS = $this->prodige->fetchOne(
            "SELECT prodige_settings_value from parametrage.prodige_settings where prodige_settings_constant='PRO_PROJ_WMS'"
        );
        $settingWFS = $this->prodige->fetchOne(
            "SELECT prodige_settings_value from parametrage.prodige_settings where prodige_settings_constant='PRO_PROJ_WFS'"
        );

        if ($settingWMS !== "") {
            $this->addProjectionSetting($settingWMS, 'PRO_PROJ_WMS');
        }

        if ($settingWFS !== "") {
            $this->addProjectionSetting($settingWFS, 'PRO_PROJ_WFS');
        }
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }

    /**
     * @throws OptimisticLockException
     * @throws ORMException
     * @throws NotSupported
     */
    private function addProjectionSetting(string $settingWxs, string $service): void
    {
        /** @var EntityManager $em */
        $em = $this->container->get('doctrine.orm.entity_manager');
        $settingWXS = explode(' ; ', $settingWxs);
        $settingWXSDB = $em->getRepository(Setting::class)->findOneBy(['name' => $service]);
        if (!is_null($settingWXSDB)) {
            $allWXS = [];
            foreach ($settingWXS as $setting) {
                preg_match('/EPSG:([0-9]+).*/', $setting, $match);
                $allWXS[] = $match[1];
            }
            $projectionsWXS = $em->getRepository(Projection::class)->findBy(['epsg' => $allWXS]);
            foreach ($projectionsWXS as $projectionWXS) {
                $settingWXSDB->addProjection($projectionWXS);
            }
            $em->persist($settingWXSDB);
            $em->flush();
        }
    }
}
