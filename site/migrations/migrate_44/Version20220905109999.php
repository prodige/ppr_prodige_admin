<?php

declare(strict_types=1);

namespace DoctrineMigrations\From44;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220905109999 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Add migration data for right object layer and map';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql(
            'insert into admin.profile_privilege_object as a 
                    with trt_object_2 as (
                            select pk_trt_objet, CASE
                                WHEN to2.trt_id = \'EDITION\' THEN \'EDITION EN LIGNE\'
                                when to2.trt_id = \'EDITION (AJOUT)\' then \'EDITION EN LIGNE (AJOUT)\'
                                when to2.trt_id = \'CONSULTATION\' then \'NAVIGATION\'
                                else to2.trt_id
                            END AS trt_id
                            from catalogue.trt_objet to2
                    )
                    select gto.grptrtobj_fk_grp_id as profile_id, l.metadata_sheet_id as metadata_id, lp.id as lex_privilige_id, gto.grp_trt_objet_status as status, now() as created_at
                    from catalogue.grp_trt_objet gto 
                    inner join admin.profile p on p.id = gto.grptrtobj_fk_grp_id 
                    inner join trt_object_2 to2 on to2.pk_trt_objet = gto.grptrtobj_fk_trt_id
                    inner join admin.lex_privilege lp on lp."name" = to2.trt_id
                    inner join admin.layer l on l.id = gto.grptrtobj_fk_objet_id and gto.grptrtobj_fk_obj_type_id = 1;'
        );

        $this->addSql(
            'insert into admin.profile_privilege_object as a 
                    with trt_object_2 as (
                            select pk_trt_objet, CASE
                                WHEN to2.trt_id = \'EDITION\' THEN \'EDITION EN LIGNE\'
                                when to2.trt_id = \'EDITION (AJOUT)\' then \'EDITION EN LIGNE (AJOUT)\'
                                when to2.trt_id = \'CONSULTATION\' then \'NAVIGATION\'
                                else to2.trt_id
                            END AS trt_id
                            from catalogue.trt_objet to2
                    )
                    select gto.grptrtobj_fk_grp_id as profile_id, m.metadata_sheet_id as metadata_id, lp.id as lex_privilige_id, gto.grp_trt_objet_status as status, now() as created_at
                    from catalogue.grp_trt_objet gto 
                    inner join admin.profile p on p.id = gto.grptrtobj_fk_grp_id 
                    inner join trt_object_2 to2 on to2.pk_trt_objet = gto.grptrtobj_fk_trt_id
                    inner join admin.lex_privilege lp on lp."name" = to2.trt_id
                    inner join admin.map m on m.id = gto.grptrtobj_fk_objet_id and gto.grptrtobj_fk_obj_type_id = 2
                    where m.metadata_sheet_id is not null;'
        );
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('truncate admin.profile_privilege_object;');
    }
}
