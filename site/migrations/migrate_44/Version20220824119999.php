<?php

declare(strict_types=1);

namespace DoctrineMigrations\From44;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220824119999 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Migrate Data from catalogue';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('insert into admin.subdomain_metadata_sheet as a
                        select ms.id as metadata_sheet_id, b.ssdcouch_fk_sous_domaine as subdomain_id 
                        from catalogue.ssdom_dispose_couche b
                        inner join catalogue.couche_donnees cd on cd.pk_couche_donnees = b.ssdcouch_fk_couche_donnees 
                        inner join admin.layer l on l.id = cd.pk_couche_donnees
                        inner join admin.metadata_sheet ms on ms.id = l.metadata_sheet_id;');

    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
