<?php

declare(strict_types=1);

namespace DoctrineMigrations\From44;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230106179999 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'migrate table to views in catalogue';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        //add missing migration

        $this->addSql("update admin.layer set (wfs_metadata_uuid, wms_sdom_id, cgu_display ,
                       cgu_message, synchronized_layer) =  
                       (SELECT couchd_wfs_uuid, couchd_wms_sdom, couchd_cgu_display::bool ,
                       couchd_cgu_message, couchd_synchro::bool FROM catalogue.couche_donnees WHERE couche_donnees.pk_couche_donnees = layer.id)");

        //CLEAN CATALOGUE DATABASE / remove view to re-generate later
        $this->addSql("DROP VIEW catalogue.metadata_list");

        $this->addSql("DROP VIEW catalogue.statistique_dernieres_mises_a_jour");
        $this->addSql("DROP VIEW catalogue.statistique_dernieres_nouveautes");
        $this->addSql("DROP VIEW catalogue.statistique_liste_metadata_publiees");
        $this->addSql("DROP VIEW catalogue.statistique_liste_metadonnees_prod_date_couche");
        $this->addSql("DROP VIEW catalogue.statistique_metadata_date_creation");
        $this->addSql("DROP VIEW catalogue.statistique_metadata_date_publication");
        $this->addSql("DROP VIEW catalogue.statistique_metadata_date_revision");
        $this->addSql("DROP VIEW catalogue.statistique_nombre_cartes_publiees_domaine");
        $this->addSql("DROP VIEW catalogue.statistique_nombre_cartes_publiees_sousdomaine");
        $this->addSql("DROP VIEW catalogue.statistique_nombre_couches_publiees_domaine");
        $this->addSql("DROP VIEW catalogue.statistique_nombre_couches_publiees_sousdomaine");
        $this->addSql("DROP VIEW catalogue.statistique_ogc");
        $this->addSql("DROP VIEW catalogue.statistique_liste_carte_publiees");
        $this->addSql("DROP VIEW catalogue.statistique_liste_couche_publiees");


        $this->addSql("DROP VIEW catalogue.administrateurs_sous_domaine ");
        $this->addSql("DROP VIEW catalogue.cartes_sdom_pub");
        $this->addSql("DROP VIEW catalogue.cartes_sdom");

        $this->addSql("DROP VIEW catalogue.couche_sdom");
        $this->addSql("DROP VIEW catalogue.dom_sdom");
        $this->addSql("DROP VIEW catalogue.v_desc_metadata");
        $this->addSql("DROP VIEW catalogue.v_desc_raster");


        //REMOVE definitively this views
        $this->addSql("DROP VIEW catalogue.editeurs_sous_domaine");
        $this->addSql("DROP VIEW catalogue.incoherence_carte_fiche_metadonnees");
        $this->addSql("DROP VIEW catalogue.incoherence_carte_sous_domaines");
        $this->addSql("DROP VIEW catalogue.incoherence_couche_fiche_metadonnees");
        $this->addSql("DROP VIEW catalogue.v_acces_couche");
        $this->addSql("DROP VIEW catalogue.v_utilisateur");
        $this->addSql("DROP VIEW catalogue.incoherence_couche_sous_domaines");
        $this->addSql("DROP VIEW catalogue.incoherence_fiche_metadonnees_metadata");

        $this->addSql("DROP VIEW catalogue.metadonnees_sdom");
        $this->addSql("DROP VIEW catalogue.metadonnees_sdom_pub");
        $this->addSql("DROP VIEW catalogue.objet");
        $this->addSql("DROP VIEW catalogue.traitements_utilisateur");
        $this->addSql("DROP VIEW catalogue.utilisateurs_domaine");
        $this->addSql("DROP VIEW catalogue.utilisateurs_sous_domaine");

        //drop unused tables
        $this->addSql("ALTER TABLE catalogue.prodige_param RENAME TO unused_prodige_param");
        $this->addSql("ALTER TABLE catalogue.prodige_carto_colors RENAME TO unsed_prodige_carto_colors");
        $this->addSql("ALTER TABLE catalogue.prodige_colors RENAME TO unsed_prodige_colors");
        $this->addSql("ALTER TABLE catalogue.prodige_database_requests RENAME TO unsed_prodige_database_requests");
        $this->addSql("ALTER TABLE catalogue.prodige_external_access RENAME TO unsed_external_access");
        $this->addSql("ALTER TABLE catalogue.prodige_fonts RENAME TO unsed_prodige_fonts");
        $this->addSql("ALTER TABLE catalogue.prodige_geosource_colors RENAME TO unsed_prodige_geosource_colors");

        //$this->addSql("ALTER TABLE catalogue.test_requests RENAME TO unsed_test_requests");

        //rename tables usefull but migrated as views from admin
        $this->addSql("ALTER TABLE catalogue.couche_donnees RENAME TO unused_couche_donnees");
        $this->addSql("ALTER TABLE catalogue.prodige_help RENAME TO unused_prodige_help");
        $this->addSql("ALTER TABLE catalogue.prodige_help_group RENAME TO unused_prodige_help_group");
        $this->addSql("ALTER TABLE catalogue.grp_regroupe_usr RENAME TO unused_grp_regroupe_usr");
        $this->addSql("ALTER TABLE catalogue.carte_projet RENAME TO unused_carte_projet");
        $this->addSql("ALTER TABLE catalogue.stockage_carte  RENAME TO unused_stockage_carte");
        $this->addSql("ALTER TABLE catalogue.fiche_metadonnees  RENAME TO unused_fiche_metadonnees");
        $this->addSql("ALTER TABLE catalogue.sous_domaine  RENAME TO unused_sous_domaine");
        $this->addSql("ALTER TABLE catalogue.domaine RENAME TO unused_domaine");
        $this->addSql("ALTER TABLE catalogue.rubric_param RENAME TO unused_rubric_param ");
        $this->addSql("ALTER TABLE catalogue.ssdom_dispose_couche RENAME TO unused_ssdom_dispose_couche");
        $this->addSql("ALTER TABLE catalogue.ssdom_dispose_metadata RENAME TO unused_ssdom_dispose_metadata");
        $this->addSql("ALTER TABLE catalogue.ssdom_visualise_carte RENAME TO unused_ssdom_visualise_carte");
        $this->addSql("ALTER TABLE catalogue.utilisateur RENAME TO unused_utilisateur");
        //rename tables deprecated in v5
        $this->addSql("ALTER TABLE catalogue.acces_serveur RENAME TO unused_acces_serveur");
        $this->addSql("ALTER TABLE catalogue.competence RENAME TO unused_competence");
        $this->addSql("ALTER TABLE catalogue.competence_accede_carte RENAME TO unused_competence_accede_carte");
        $this->addSql("ALTER TABLE catalogue.competence_accede_couche RENAME TO unused_competence_accede_couche");
        $this->addSql("ALTER TABLE catalogue.grp_accede_competence RENAME TO unused_grp_accede_competence");
        $this->addSql("ALTER TABLE catalogue.grp_accede_dom RENAME TO unused_grp_accede_dom");
        $this->addSql("ALTER TABLE catalogue.grp_accede_ssdom RENAME TO unused_grp_accede_ssdom");
        $this->addSql("ALTER TABLE catalogue.grp_autorise_trt RENAME TO unused_grp_autorise_trt");
        $this->addSql("ALTER TABLE catalogue.grp_restriction_attributaire RENAME TO unused_grp_restriction_attributaire");
        $this->addSql("ALTER TABLE catalogue.grp_trt_objet RENAME TO unused_grp_trt_objet");
        $this->addSql("ALTER TABLE catalogue.objet_type RENAME TO unused_objet_type");
        $this->addSql("ALTER TABLE catalogue.perimetre RENAME TO unused_perimetre");
        $this->addSql("ALTER TABLE catalogue.prodige_settings RENAME TO unused_prodige_settings");
        $this->addSql("ALTER TABLE catalogue.structure RENAME TO unused_structure");
        $this->addSql("ALTER TABLE catalogue.traitement RENAME TO unused_traitement");
        $this->addSql("ALTER TABLE catalogue.trt_autorise_objet RENAME TO unused_trt_autorise_objet");
        $this->addSql("ALTER TABLE catalogue.trt_objet RENAME TO unused_trt_objet");
        $this->addSql("ALTER TABLE catalogue.usr_accede_perimetre RENAME TO unused_usr_accede_perimetre");
        $this->addSql("ALTER TABLE catalogue.usr_accede_perimetre_edition RENAME TO unused_usr_accede_perimetre_edition");
        $this->addSql("ALTER TABLE catalogue.usr_alerte_perimetre_edition RENAME TO unused_usr_alerte_perimetre_edition");
        $this->addSql("ALTER TABLE catalogue.utilisateur_structure RENAME TO unused_utilisateur_structure");
        $this->addSql("ALTER TABLE catalogue.zonage RENAME TO unused_zonage");


        //create views from admin schema
        $this->addSql("create view catalogue.prodige_help 
                       (pk_prodige_help_id, prodige_help_title, prodige_help_desc, prodige_help_url,
                        prodige_help_row_number) as 
                       select id, title, description, file_path, 0 from admin.help");
        $this->addSql("create view catalogue.prodige_help_group
                       (hlpgrp_fk_help_id,hlpgrp_fk_groupe_profil, pk_prodige_help_group) as 
                       select  	help_id, profile_id,0  from admin.profiles_helps");
        $this->addSql("create view catalogue.grp_regroupe_usr
                       (pk_grp_regroupe_usr, ts, grpusr_fk_groupe_profil, grpusr_fk_utilisateur, grpusr_is_principal) as 
                       select 0, updated_at, profile_id, user_id,  is_principal from admin.profile_user;");

        //couchd_restriction_attributaire not matched, not to be used in SELECT               
        $this->addSql("create view catalogue.couche_donnees 
                      (pk_couche_donnees, ts, couchd_id, couchd_nom, couchd_description, couchd_type_stockage, 
                       couchd_emplacement_stockage, couchd_fk_acces_server, couchd_wms, couchd_wfs, couchd_download, 
                       couchd_restriction, couchd_restriction_exclusif,
                       couchd_zonageid_fk, couchd_restriction_buffer, 
                       couchd_visualisable, couchd_extraction_attributaire, couchd_extraction_attributaire_champ,
                       changedate, couchd_alert_msg_id, couchd_wfs_uuid, couchd_wms_sdom, couchd_restriction_attributaire,
                       couchd_restriction_attributaire_propriete, couchd_help_edition_msg, couchd_cgu_display, couchd_cgu_message,
                       couchd_synchro)  as 
                       select  	id, created_at, '', name, description, 
                       CASE
                         WHEN lex_layer_type_id=2 THEN 0
                         WHEN lex_layer_type_id=1 THEN 1
                         WHEN lex_layer_type_id=3 THEN -3
                         WHEN lex_layer_type_id=4 THEN -4
                       end 
                       ,
                       storage_path, 1,	wms::int ,	 wfs::int ,	download::int, 
                       restriction::int, exclusive_restriction::int, null, buffer_restriction,
                       viewable::int, assigned_extraction::int, field_assigned_extraction, 
                       last_import, null, wfs_metadata_uuid, wms_sdom_id, null, 
                       null, null, cgu_display::int, cgu_message, synchronized_layer::int from admin.layer ;");
        //couchd_restriction_attributaire not matched, not to be used in SELECT               

        $this->addSql("create view catalogue.carte_projet
                      (pk_carte_projet, ts, cartp_id, cartp_nom, cartp_description, cartp_format, 
                      cartp_fk_stockage_carte, cartp_fk_fiche_metadonnees, cartp_utilisateur, cartep_etat, 
                      cartp_administrateur, cartp_utilisateur_email, cartp_wms) as
                      select id, created_at,'', name, 	description, 
                      CASE
                        WHEN lex_map_format_id=1 THEN 0
                        WHEN lex_map_format_id=2 THEN 1
                        WHEN lex_map_format_id=3 THEN 2
                      end, id, metadata_sheet_id, user_id, state::int, admin_id, user_email, public_metadata_wms 
                      from admin.map");
        $this->addSql("create view catalogue.stockage_carte
                      (pk_stockage_carte, ts, stkcard_id, stkcard_path, stk_server) as
                      select id, null, null, path, 1 from admin.map");

        $this->addSql("create view catalogue.fiche_metadonnees
                      (pk_fiche_metadonnees, ts,fmeta_id, fmeta_description, fmeta_fk_couche_donnees, statut, schema)  as 
                      select  	metadata_sheet.id, metadata_sheet.created_at,	public_metadata_id, null, 	layer.id, null, null from admin.metadata_sheet
                      left join admin.layer on metadata_sheet.id = layer.metadata_sheet_id ;");
        $this->addSql("create view catalogue.sous_domaine 
                       (pk_sous_domaine, ts, ssdom_id, ssdom_nom, ssdom_description, ssdom_fk_domaine, 
                       ssdom_admin_fk_groupe_profil, ssdom_createur_fk_groupe_profil, ssdom_editeur_fk_groupe_profil,
                       ssdom_service_wms, ssdom_service_wms_uuid) as 
                       select  	id, created_at, '', name, description, domain_id, 
                       profile_admin_id, profile_creator_id, profile_editor_id,
                       wms_service::int, wms_service_uuid from admin.subdomain");
        $this->addSql("create view catalogue.domaine 
                      (pk_domaine, ts, dom_id, dom_nom, dom_description, dom_rubric) as
                      select id, created_at, '',name, description ,rubric_id from admin.domain");
        $this->addSql("create view catalogue.rubric_param 
                      (rubric_id, rubric_name) as
                      select id, name from admin.rubric");
        $this->addSql("create view catalogue.ssdom_dispose_couche 
                      (pk_ssdom_dispose_couche, ts, ssdcouch_fk_couche_donnees, ssdcouch_fk_sous_domaine) as
                      select null, null,  	layer.id, subdomain_id from admin.layer 
                      inner join admin.metadata_sheet on metadata_sheet.id = layer.metadata_sheet_id
                      inner join admin.subdomain_metadata_sheet on metadata_sheet.id = subdomain_metadata_sheet.metadata_sheet_id  ");
        $this->addSql("create view catalogue.ssdom_dispose_metadata
                      (pk_ssdom_dispose_metadata, ts, uuid, ssdcouch_fk_sous_domaine) as
                      select null, null, metadata.uuid, subdomain_metadata_sheet.subdomain_id from admin.metadata_sheet 
                      inner join public.metadata on metadata.id = metadata_sheet.public_metadata_id
                      inner join admin.subdomain_metadata_sheet on metadata_sheet.id = subdomain_metadata_sheet.metadata_sheet_id ");
        $this->addSql("create view catalogue.ssdom_visualise_carte
                      (pk_ssdom_visualise_carte, ts, ssdcart_fk_carte_projet, ssdcart_fk_sous_domaine) as
                      select null, null,  	map.id, subdomain_id from admin.map 
                      inner join admin.metadata_sheet on metadata_sheet.id = map.metadata_sheet_id
                      inner join admin.subdomain_metadata_sheet on metadata_sheet.id = subdomain_metadata_sheet.metadata_sheet_id  ");
        $this->addSql("create view catalogue.utilisateur
                      (pk_utilisateur, ts, usr_id, usr_nom, usr_prenom, usr_email, usr_telephone, usr_telephone2, 
                      usr_service, usr_description, usr_password, usr_pwdexpire, usr_generic, usr_ldap, usr_signature,
                      date_expiration_compte) as  	
                      select id, created_at, login, name, first_name, email, phone_number, phone_number2,
                      service, 	description, '',  	password_expire, generic::int, 	0, '', 
                      account_expiration from admin.users");


        //RECREATE usefull views as before

        $this->addSql("CREATE VIEW catalogue.administrateurs_sous_domaine AS
                      SELECT domaine.pk_domaine,
                         domaine.dom_nom,
                         sous_domaine.pk_sous_domaine,
                         sous_domaine.ssdom_nom,
                         groupe_profil.pk_groupe_profil,
                         groupe_profil.grp_nom,
                         utilisateur.pk_utilisateur,
                         utilisateur.usr_id
                        FROM ((((catalogue.domaine
                          RIGHT JOIN catalogue.sous_domaine ON ((domaine.pk_domaine = sous_domaine.ssdom_fk_domaine)))
                          LEFT JOIN catalogue.groupe_profil ON ((sous_domaine.ssdom_admin_fk_groupe_profil = groupe_profil.pk_groupe_profil)))
                          LEFT JOIN catalogue.grp_regroupe_usr ON ((grp_regroupe_usr.grpusr_fk_groupe_profil = groupe_profil.pk_groupe_profil)))
                          LEFT JOIN catalogue.utilisateur ON ((grp_regroupe_usr.grpusr_fk_utilisateur = utilisateur.pk_utilisateur)));");

        $this->addSql("CREATE VIEW catalogue.cartes_sdom AS
                        SELECT carte_projet.cartp_format,
                            carte_projet.pk_carte_projet,
                            carte_projet.cartp_description,
                            carte_projet.cartp_nom,
                            sous_domaine.pk_sous_domaine,
                            sous_domaine.ssdom_nom,
                            stockage_carte.pk_stockage_carte,
                            stockage_carte.stkcard_path,
                            
                            fiche_metadonnees.pk_fiche_metadonnees,
                            fiche_metadonnees.fmeta_id,
                                CASE
                                    WHEN (operationallowed.operationid = 0) THEN 4
                                    ELSE 1
                                END AS statut,
                            domaine.pk_domaine,
                            domaine.dom_rubric AS dom_coherence,
                            domaine.dom_nom,
                            carte_projet.cartp_wms
                        FROM ((((((((catalogue.domaine
                            JOIN catalogue.sous_domaine ON ((domaine.pk_domaine = sous_domaine.ssdom_fk_domaine)))
                            JOIN catalogue.ssdom_visualise_carte ON ((sous_domaine.pk_sous_domaine = ssdom_visualise_carte.ssdcart_fk_sous_domaine)))
                            JOIN catalogue.carte_projet ON ((ssdom_visualise_carte.ssdcart_fk_carte_projet = carte_projet.pk_carte_projet)))
                            LEFT JOIN catalogue.fiche_metadonnees ON ((carte_projet.cartp_fk_fiche_metadonnees = fiche_metadonnees.pk_fiche_metadonnees)))
                            JOIN public.metadata ON (((fiche_metadonnees.fmeta_id)::bigint = metadata.id)))
                            LEFT JOIN public.operationallowed ON (((metadata.id = operationallowed.metadataid) AND (operationallowed.operationid = 0) AND (operationallowed.groupid = 1))))
                            JOIN catalogue.stockage_carte ON ((carte_projet.cartp_fk_stockage_carte = stockage_carte.pk_stockage_carte)))
                            );");


        $this->addSql("CREATE VIEW catalogue.cartes_sdom_pub AS
                        SELECT cartes_sdom.cartp_format,
                        cartes_sdom.pk_carte_projet,
                        cartes_sdom.cartp_description,
                        cartes_sdom.cartp_nom,
                        cartes_sdom.pk_sous_domaine,
                        cartes_sdom.ssdom_nom,
                        cartes_sdom.pk_stockage_carte,
                        cartes_sdom.stkcard_path,
                        cartes_sdom.pk_fiche_metadonnees,
                        cartes_sdom.fmeta_id,
                        cartes_sdom.statut,
                        cartes_sdom.pk_domaine,
                        cartes_sdom.dom_coherence,
                        cartes_sdom.dom_nom,
                        cartes_sdom.cartp_wms
                        FROM catalogue.cartes_sdom
                        WHERE (cartes_sdom.statut = 4);");

        $this->addSql("CREATE VIEW catalogue.couche_sdom AS
                        SELECT domaine.pk_domaine,
                        domaine.dom_rubric AS dom_coherence,
                        domaine.dom_nom,
                        sous_domaine.pk_sous_domaine,
                        sous_domaine.ssdom_nom,
                        fiche_metadonnees.pk_fiche_metadonnees,
                        fiche_metadonnees.fmeta_id,
                            CASE
                                WHEN (operationallowed.operationid = 0) THEN 4
                                ELSE 1
                            END AS statut,
                        couche_donnees.pk_couche_donnees,
                        couche_donnees.couchd_nom,
                        couche_donnees.couchd_description,
                        couche_donnees.couchd_type_stockage,
                        couche_donnees.couchd_emplacement_stockage,
                        couche_donnees.couchd_wms,
                        couche_donnees.couchd_wfs,
                        couche_donnees.couchd_download,
                        metadata.uuid,
                        couche_donnees.couchd_restriction,
                        couche_donnees.couchd_visualisable,
                        couche_donnees.changedate
                        FROM ((((((catalogue.domaine
                            JOIN catalogue.sous_domaine ON ((domaine.pk_domaine = sous_domaine.ssdom_fk_domaine)))
                            JOIN catalogue.ssdom_dispose_couche ON ((sous_domaine.pk_sous_domaine = ssdom_dispose_couche.ssdcouch_fk_sous_domaine)))
                            JOIN catalogue.fiche_metadonnees ON ((ssdom_dispose_couche.ssdcouch_fk_couche_donnees = fiche_metadonnees.fmeta_fk_couche_donnees)))
                            JOIN catalogue.couche_donnees ON ((couche_donnees.pk_couche_donnees = fiche_metadonnees.fmeta_fk_couche_donnees)))
                            JOIN public.metadata ON ((metadata.id = (fiche_metadonnees.fmeta_id)::bigint)))
                            LEFT JOIN public.operationallowed ON (((metadata.id = operationallowed.metadataid) AND (operationallowed.operationid = 0) AND (operationallowed.groupid = 1))));");

        $this->addSql("CREATE VIEW catalogue.dom_sdom AS
                        SELECT domaine.pk_domaine,
                        domaine.dom_rubric,
                        domaine.dom_nom,
                        sous_domaine.pk_sous_domaine,
                        sous_domaine.ssdom_nom
                        FROM (catalogue.domaine
                            LEFT JOIN catalogue.sous_domaine ON ((domaine.pk_domaine = sous_domaine.ssdom_fk_domaine)));");

        $this->addSql("CREATE VIEW catalogue.v_desc_metadata AS
                        SELECT m.id AS metadataid,
                            cd.couchd_nom AS couchenom,
                            cd.couchd_type_stockage AS couchetype,
                            cd.couchd_emplacement_stockage AS couchetable,
                            m.data AS metadataxml,
                            fm.schema
                        FROM (((public.metadata m
                            LEFT JOIN catalogue.fiche_metadonnees fm ON (((m.id)::text = (fm.fmeta_id)::text)))
                            JOIN catalogue.couche_donnees cd ON ((fm.fmeta_fk_couche_donnees = cd.pk_couche_donnees))));");

        $this->addSql("CREATE VIEW catalogue.v_desc_raster AS
                        SELECT m.id AS metadataid,
                        r.id AS layerid,
                        r.name,
                        r.srid,
                        r.ulx,
                        r.uly,
                        r.lrx,
                        r.lry,
                        r.bands,
                        r.cols,
                        r.rows,
                        r.resx,
                        r.resy,
                        r.format,
                        r.vrt_path,
                        r.compression,
                        r.color_interp
                        FROM ((public.metadata m
                            JOIN catalogue.fiche_metadonnees fm ON (((m.id)::text = (fm.fmeta_id)::text)))
                            JOIN catalogue.raster_info r ON ((fm.fmeta_fk_couche_donnees = r.id)));");

        $this->addSql("CREATE VIEW catalogue.statistique_liste_carte_publiees AS
                        SELECT carte_projet.pk_carte_projet,
                        carte_projet.cartp_nom AS \"nom de la carte\",
                        carte_projet.cartp_description AS \"résumé\",
                        carte_projet.cartp_format AS format,
                        stockage_carte.stkcard_path AS stockage,
                        metadata.changedate,
                        metadata.createdate,
                        metadata.id AS \"id metadata\",
                        metadata.uuid AS \"uuid metadata\"
                        FROM ((((catalogue.carte_projet
                            JOIN catalogue.stockage_carte ON ((carte_projet.cartp_fk_stockage_carte = stockage_carte.pk_stockage_carte)))
                            JOIN catalogue.fiche_metadonnees ON ((carte_projet.cartp_fk_fiche_metadonnees = fiche_metadonnees.pk_fiche_metadonnees)))
                            JOIN public.metadata ON (((fiche_metadonnees.fmeta_id)::bigint = metadata.id)))
                            JOIN public.operationallowed ON (((metadata.id = operationallowed.metadataid) AND (operationallowed.operationid = 0) AND (operationallowed.groupid = 1))));");

        $this->addSql("CREATE VIEW catalogue.statistique_liste_couche_publiees AS
                        SELECT couche_donnees.pk_couche_donnees,
                        couche_donnees.couchd_nom AS \"nom de la couche\",
                        couche_donnees.couchd_description AS \"résumé\",
                        couche_donnees.couchd_type_stockage AS \"couche vecteur ?\",
                        couche_donnees.couchd_emplacement_stockage AS \"emplacement de stockage\",
                        couche_donnees.couchd_wms AS \"Publiée en WMS ?\",
                        couche_donnees.couchd_wfs AS \"Publiée en WFS ?\",
                        metadata.changedate,
                        metadata.createdate,
                        metadata.id AS \"id metadata\",
                        metadata.uuid AS \"uuid metadata\"
                        FROM (((catalogue.couche_donnees
                            JOIN catalogue.fiche_metadonnees ON ((couche_donnees.pk_couche_donnees = fiche_metadonnees.fmeta_fk_couche_donnees)))
                            JOIN public.metadata ON (((fiche_metadonnees.fmeta_id)::bigint = metadata.id)))
                            JOIN public.operationallowed ON (((metadata.id = operationallowed.metadataid) AND (operationallowed.operationid = 0) AND (operationallowed.groupid = 1))));");

        $this->addSql("CREATE VIEW catalogue.statistique_dernieres_mises_a_jour AS
                        SELECT statistique_liste_couche_publiees.\"nom de la couche\" AS nom,
                        statistique_liste_couche_publiees.\"résumé\",
                        statistique_liste_couche_publiees.changedate AS date,
                        'donnée'::text AS type,
                        statistique_liste_couche_publiees.\"uuid metadata\" AS id,
                        statistique_liste_couche_publiees.\"couche vecteur ?\" AS data_type
                        FROM catalogue.statistique_liste_couche_publiees
                    UNION
                        SELECT statistique_liste_carte_publiees.\"nom de la carte\" AS nom,
                        statistique_liste_carte_publiees.\"résumé\",
                        statistique_liste_carte_publiees.changedate AS date,
                        'carte'::text AS type,
                        statistique_liste_carte_publiees.\"uuid metadata\" AS id,
                        statistique_liste_carte_publiees.format AS data_type
                        FROM catalogue.statistique_liste_carte_publiees
                        ORDER BY 3 DESC
                        LIMIT 30;");
        $this->addSql("CREATE VIEW catalogue.statistique_dernieres_nouveautes AS
                    SELECT statistique_liste_couche_publiees.\"nom de la couche\" AS nom,
                    statistique_liste_couche_publiees.\"résumé\",
                    statistique_liste_couche_publiees.changedate AS date,
                    statistique_liste_couche_publiees.createdate,
                    'donnée'::text AS type,
                    statistique_liste_couche_publiees.\"uuid metadata\" AS id,
                    statistique_liste_couche_publiees.\"couche vecteur ?\" AS data_type
                    FROM catalogue.statistique_liste_couche_publiees
                    UNION
                    SELECT statistique_liste_carte_publiees.\"nom de la carte\" AS nom,
                    statistique_liste_carte_publiees.\"résumé\",
                    statistique_liste_carte_publiees.changedate AS date,
                    statistique_liste_carte_publiees.createdate,
                    'carte'::text AS type,
                    statistique_liste_carte_publiees.\"uuid metadata\" AS id,
                    statistique_liste_carte_publiees.format AS data_type
                    FROM catalogue.statistique_liste_carte_publiees
                    ORDER BY 4 DESC
                    LIMIT 30;");
        $this->addSql("CREATE VIEW catalogue.statistique_liste_metadata_publiees AS
                    SELECT statistique_liste_couche_publiees.\"nom de la couche\" AS nom,
                    statistique_liste_couche_publiees.\"résumé\",
                    statistique_liste_couche_publiees.changedate AS date,
                    statistique_liste_couche_publiees.createdate,
                    'donnée'::text AS type,
                    statistique_liste_couche_publiees.\"id metadata\" AS id,
                    statistique_liste_couche_publiees.\"couche vecteur ?\" AS data_type
                    FROM catalogue.statistique_liste_couche_publiees
                    UNION
                    SELECT statistique_liste_carte_publiees.\"nom de la carte\" AS nom,
                    statistique_liste_carte_publiees.\"résumé\",
                    statistique_liste_carte_publiees.changedate AS date,
                    statistique_liste_carte_publiees.createdate,
                    'carte'::text AS type,
                    statistique_liste_carte_publiees.\"id metadata\" AS id,
                    statistique_liste_carte_publiees.format AS data_type
                    FROM catalogue.statistique_liste_carte_publiees;");
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        //NOT TO DOWN
        /*$this->addSql("drop view catalogue.carte_projet");
        $this->addSql("drop view catalogue.rubric_param");
        $this->addSql("drop view catalogue.sous_domaine");
        $this->addSql("drop view catalogue.domaine");
        $this->addSql("drop view catalogue.fiche_metadonnees");
        $this->addSql("drop view catalogue.couche_donnees");
        $this->addSql("drop view catalogue.prodige_help_group");
        $this->addSql("drop view catalogue.grp_regroupe_usr");
        $this->addSql("drop view catalogue.prodige_help");
        $this->addSql("drop view catalogue.ssdom_dispose_couche");
        $this->addSql("drop view catalogue.ssdom_dispose_metadata");
        $this->addSql("drop view catalogue.utilisateur");

        $this->addSql("alter table admin.layer drop column wfs_metadata_uuid");
        $this->addSql("alter table admin.layer drop column wms_sdom_id");
        $this->addSql("alter table admin.layer drop column cgu_display");
        $this->addSql("alter table admin.layer drop column cgu_message");
        $this->addSql("alter table admin.layer drop column synchronized_layer");*/

    }
}
