<?php

declare(strict_types=1);

namespace DoctrineMigrations\From44;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230223169999 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE view catalogue.v_utilisateur as  SELECT utilisateur.pk_utilisateur,
                       utilisateur.usr_id,
                       utilisateur.usr_nom,
                       utilisateur.usr_prenom,
                       utilisateur.usr_email
                       FROM catalogue.utilisateur
                       WHERE utilisateur.date_expiration_compte IS NULL OR utilisateur.date_expiration_compte IS NOT NULL AND utilisateur.date_expiration_compte > now();');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP view catalogue.v_utilisateur');
    }
}
