<?php

declare(strict_types=1);

namespace DoctrineMigrations\From44;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220309089999 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Migrate Data from catalogue';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql("insert into admin.users as a
                            select b.pk_utilisateur as id, b.usr_id as login, b.usr_nom as name, b.usr_prenom as first_name, b.usr_email as email, b.usr_telephone as phone_number, b.usr_telephone2 as phone_number2,
                                b.usr_service as service , b.usr_description as description, b.usr_password, b.usr_pwdexpire as password_expire , b.usr_generic::bool as generic ,
                                b.date_expiration_compte as account_expiration, now() as created_at
                            from catalogue.utilisateur b;");
        $this->addSql("select setval('admin.users_id_seq', (select MAX(pk_utilisateur) from catalogue.utilisateur))");

        $this->addSql("insert into admin.user_context as a
                            select b.id as id, b.fk_utilisateur as user_id, b.context as context, now() as created_at
                            from catalogue.utilisateur_contexte b;");
        $this->addSql("select setval('admin.user_context_id_seq', (select MAX(id) from catalogue.utilisateur_contexte))");

        $this->addSql("insert into admin.user_favorite_area as a
                            select b.id as id, b.fk_utilisateur as user_id, b.zone_favorite as zone_favorite, now() as created_at
                            from catalogue.utilisateur_zone_favorite b;");
        $this->addSql("select setval('admin.user_favorite_area_id_seq', (select MAX(id) from catalogue.utilisateur_zone_favorite))");

        $this->addSql("insert into admin.structure  as a
                            select b.pk_structure as id, b.structure_nom as name,  b.structure_sigle as sigle, b.structure_siren as siren, b.structure_siret as siret, b.ts as created_at
                            from catalogue.structure b;");
        $this->addSql("select setval('admin.structure_id_seq', (select MAX(pk_structure) from catalogue.structure))");

        $this->addSql('insert into admin.users_structures as a
                            select b.fk_utilisateur as user_id, b.fk_structure as structure_id
                            from catalogue.utilisateur_structure b');

        $this->addSql("insert into admin.metadata_sheet as a
                            select min(b.pk_fiche_metadonnees) as id, b.fmeta_id::integer as public_metadata_id , b.schema as schema, now() as created_at
                            from catalogue.fiche_metadonnees b
                            where exists (select 1 from public.metadata m where m.id = b.fmeta_id::integer)
                            group by public_metadata_id , schema, created_at
                            ;");
        $this->addSql("select setval('admin.metadata_sheet_id_seq', (select MAX(id) from admin.metadata_sheet))");

        $this->addSql('insert into admin.layer as a
                            select b.pk_couche_donnees as id, ms.id as metadata_sheet_id,
                            CASE
                                WHEN b.couchd_type_stockage=0 THEN 2
                                WHEN b.couchd_type_stockage=1 THEN 1
                                WHEN b.couchd_type_stockage=-3 THEN 3
                                WHEN b.couchd_type_stockage=-4 THEN 4
                            end as lex_layer_type_id, null , b.couchd_nom as name, substring(b.couchd_description from 1 for 2048)  as description, b.couchd_emplacement_stockage as storage_path, case when b.couchd_wms is null then false else b.couchd_wms::bool end as wms,
                            case when b.couchd_wfs is null then false else b.couchd_wfs::bool end as wfs, case when b.couchd_download is null then false else b.couchd_download::bool end as download, case when b.couchd_restriction is null then false else  b.couchd_restriction::bool end as restriction, b.couchd_restriction_exclusif::bool as exclusive_restriction,
                            b.couchd_restriction_buffer as buffer_restriction, case when b.couchd_visualisable is null then false else b.couchd_visualisable::bool end as viewable, case when b.couchd_extraction_attributaire is null then false else b.couchd_extraction_attributaire::bool end as assigned_extraction,
                            b.couchd_extraction_attributaire_champ as field_assigned_extraction, b.changedate as last_import, now() as created_at
                            from catalogue.couche_donnees b
                            inner join catalogue.fiche_metadonnees fm on fm.fmeta_fk_couche_donnees = b.pk_couche_donnees
                            inner join admin.metadata_sheet ms on fm.fmeta_id::integer = ms.public_metadata_id
                            where exists (select 1
                                      from admin.metadata_sheet ms
                                      where ms.id = fm.pk_fiche_metadonnees)
                                      and  b.couchd_type_stockage in (0,1,-3,-4);');
        $this->addSql('select setval(\'admin.layer_id_seq\', (select MAX(id) from admin.layer))');

        $this->addSql('insert into admin.zoning as a
                            select b.pk_zonage_id as id, b.zonage_fk_couche_donnees as layer_id, b.zonage_nom as name, b.zonage_minimal:: bool as minimal,
                                b.zonage_field_name as field_name, b.zonage_field_id as field_id
                            from catalogue.zonage b
                            where exists (select 1  from admin.layer l  where l.id = b.zonage_fk_couche_donnees);');
        $this->addSql('select setval(\'admin.zoning_id_seq\', (select MAX(id) from admin.zoning))');

        $this->addSql('insert into admin.area as a
                            select b.pk_perimetre_id as id, b.perimetre_zonage_id as zoning_id, b.perimetre_code as code, b.perimetre_nom as name, false, now() as created_at
                            from catalogue.perimetre b;');
        $this->addSql('select setval(\'admin.area_id_seq\', (select MAX(id) from admin.area))');

        $this->addSql('insert into admin.user_area_access as a
                            select distinct on (b.usrperim_fk_utilisateur, b.usrperim_fk_perimetre)
                            b.usrperim_fk_utilisateur as user_id, b.usrperim_fk_perimetre as area_id, b.ts as created_at
                            from catalogue.usr_accede_perimetre b');

        $this->addSql('insert into admin.user_area_alert  as a
                            select b.usralert_fk_perimetre as area_id, b.usralert_fk_couchedonnees as layer_id, b.usralert_fk_utilisateur as user_id, b.ts as created_at
                            from catalogue.usr_alerte_perimetre_edition b');

        $this->addSql('insert into admin.user_area_edition as a
                            select distinct on (b.usrperim_fk_utilisateur, b.usrperim_fk_perimetre)
                            b.usrperim_fk_perimetre as area_id, b.usrperim_fk_utilisateur as user_id, b.ts as created_at
                            from catalogue.usr_accede_perimetre_edition b
                            inner join admin.users u on u.id = b.usrperim_fk_utilisateur');

        $this->addSql('insert into admin.map as a
                            select b.pk_carte_projet as id, b.cartp_fk_fiche_metadonnees as metadata_sheet_id, b.cartp_utilisateur as user_id, b.cartp_administrateur as admin_id,
                                CASE
                                WHEN b.cartp_format=0 THEN 1
                                WHEN b.cartp_format=1 THEN 2
                                WHEN b.cartp_format=2 THEN 3
                            end as lex_map_format_id, sc.stkcard_path as path, b.cartp_nom as name, b.cartp_description as description, b.cartep_etat::bool as state,
                            b.cartp_utilisateur_email as user_email, m.id as public_metadata_wms, b.ts as created_at
                            from catalogue.carte_projet b
                            inner join catalogue.stockage_carte sc on sc.pk_stockage_carte = b.cartp_fk_stockage_carte
                            left join public.metadata m on m.id = b.cartp_wms
                            where b.cartp_format in (0,1,2) and b.cartp_fk_fiche_metadonnees in (select id from admin.metadata_sheet) ;');
        $this->addSql('select setval(\'admin.map_id_seq\', (select MAX(id) from admin.map));');

        $this->addSql('insert into admin.rubric  as a
                            select b.rubric_id  as id, b.rubric_name as name, now() as created_at
                            from catalogue.rubric_param b;');
        $this->addSql('select setval(\'admin.rubric_id_seq\', (select MAX(id) from admin.rubric));');

        $this->addSql('insert into admin.domain as a
                            select b.pk_domaine  as id, b.dom_rubric as rubric, b.dom_nom as name, b.dom_description as description, b.ts as created_at
                            from catalogue.domaine b;');
        $this->addSql('select setval(\'admin.domain_id_seq\', (select MAX(id) from admin.domain));');

        $this->addSql('insert into admin.help as a
                            select b.pk_prodige_help_id as id, b.prodige_help_title as title, b.prodige_help_desc as description,
                              now() as created_at,  null, b.prodige_help_url as url
                            from catalogue.prodige_help b;');
        $this->addSql('select setval(\'admin.help_id_seq\', (select MAX(id) from admin.help));');

        $this->addSql('insert into admin.profile as a
                            select b.pk_groupe_profil as id, b.grp_nom as name, b.grp_description as desciption, b.grp_is_default_installation::bool as grp_is_default_installation,
                                b.grp_template_mail as template_mail, b.ts as created_at
                            from catalogue.groupe_profil b;');
        $this->addSql('select setval(\'admin.profile_id_seq\', (select MAX(id) from admin.profile));');

        $this->addSql('insert into admin.profiles_helps as a
                            select b.hlpgrp_fk_groupe_profil as profile_id, b.hlpgrp_fk_help_id as help_id
                            from catalogue.prodige_help_group b ;');

        $this->addSql('insert into admin.profile_domain_access as a
                            select b.grpdom_fk_groupe_profil as profile_id, b.grpdom_fk_domaine as domain_id
                            from catalogue.grp_accede_dom b;');

        $this->addSql('insert into admin.subdomain as a
                            select b.pk_sous_domaine as id, creator.pk_groupe_profil as profile_creator_id, editor.pk_groupe_profil  as profile_editor_id,
                                admin.pk_groupe_profil as profile_admin_id, b.ssdom_fk_domaine as domain_id, b.ssdom_nom  as name, b.ssdom_description as description,
                                b.ssdom_service_wms::bool as wms_service, b.ssdom_service_wms_uuid as wms_service_uuid, b.ts as created_id
                            from catalogue.sous_domaine b
                            left join catalogue.groupe_profil creator on b.ssdom_createur_fk_groupe_profil  = creator.pk_groupe_profil     
                            left join catalogue.groupe_profil editor on b.ssdom_editeur_fk_groupe_profil = editor.pk_groupe_profil     
                            left join catalogue.groupe_profil admin on b.ssdom_admin_fk_groupe_profil  = admin.pk_groupe_profil 
                            ;');
        $this->addSql('select setval(\'admin.subdomain_id_seq\', (select MAX(id) from admin.subdomain));');

        $this->addSql('insert into admin.profile_subdomain_access as a
                            select b.grpss_fk_groupe_profil as profile_id, b.grpss_fk_sous_domaine as subdomain_id
                            from catalogue.grp_accede_ssdom b
                            inner join admin.subdomain d on d.id = b.grpss_fk_sous_domaine
                            inner join admin.profile p on p.id = b.grpss_fk_groupe_profil;');

        $this->addSql('insert into admin.subdomain_metadata_sheet as a
                            select ms.id as metadata_sheet_id, b.ssdcart_fk_sous_domaine as subdomain_id
                            from catalogue.ssdom_visualise_carte b
                            inner join catalogue.carte_projet cp on cp.pk_carte_projet = b.ssdcart_fk_carte_projet
                            inner join admin.map m on m.id = cp.pk_carte_projet
                            inner join admin.metadata_sheet ms on ms.id = m.metadata_sheet_id;');

        $this->addSql('insert into admin.skill
                            select b.pk_competence_id as id, b.competence_nom, now() as created_at
                            from catalogue.competence b ;');
        $this->addSql('select setval(\'admin.skill_id_seq\', (select MAX(id) from admin.skill));');

        $this->addSql('insert into admin.skills_layers
                            select l.id as layer_id, b.competencecouche_fk_competence_id as skill_id
                            from catalogue.competence_accede_couche b
                            inner join catalogue.couche_donnees cd on cd.pk_couche_donnees = b.competencecouche_fk_couche_donnees
                            inner join admin.layer l on cd.pk_couche_donnees = l.id;');

        $this->addSql('insert into admin.skills_maps
                            select m.id as map_id, b.competencecarte_fk_competence_id  as skill_id
                            from catalogue.competence_accede_carte b
                            inner join catalogue.carte_projet cp on cp.pk_carte_projet = b.competencecarte_fk_carte_projet
                            inner join admin.map m on cp.pk_carte_projet = m.id;');

        $this->addSql('insert into admin.profiles_skills
                            select b.fk_grp_id as profile_id, b.fk_competence_id as skill_id
                            from catalogue.grp_accede_competence b;');

        $this->addSql('insert into admin.profile_privilege
                            select b.grptrt_fk_groupe_profil as profile_id, b.grptrt_fk_traitement as lex_privilege_id
                            from catalogue.grp_autorise_trt b;');

        $this->addSql('insert into admin.profile_privilege_layer
                            select b.grprat_fk_groupe_profil as profile_id, b.grprat_fk_couche_donnees as layer_id, b.grprat_fk_trt_id as lex_privilege_id, 1 as lex_structure_type,
                            b.grprat_champ as field, now() as created_at
                            from catalogue.grp_restriction_attributaire b;');

        $this->addSql('insert into admin.profile_user
                            select b.grpusr_fk_groupe_profil as profile_id, b.grpusr_fk_utilisateur as user_id, b.grpusr_is_principal as is_principal,
                                b.ts as created_at
                            from catalogue.grp_regroupe_usr b;');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
