<?php

declare(strict_types=1);

namespace DoctrineMigrations\From44;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230713000000 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Add admincli user';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        //add missing migration
        $password = '$2y$13$ez8xoype7HQii/s9xaDQxuEOqqmI2U6Qg4T2OF3Ic.RVhcJMS5ECO';
        $this->addSql("insert into admin.users(login, name, first_name, email, description, password, password_expire, generic, 
                       created_at, is_expired, reference_gn) 
                       values ('admincli', 'admincli', 'admincli', 'no-reply@prodige-opensource.org', 
                       'utilisateur administrateur système', '".$password."', 
                       '2050-01-01', false, now(), false, 0);");
        $this->addSql("insert into admin.profile_user(profile_id, user_id, is_principal, created_at) 
                       select 200, id, 1, now() from admin.users where login='admincli'; ");

    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs

        $this->addSql("delete from admin.profile_user where user_id in (select id from admin.users where login='admincli');");
        $this->addSql("delete from admin.users where login='admincli';");

    }
}
