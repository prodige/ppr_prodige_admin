<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220605165769 extends AbstractMigration
{

    public function __construct(Connection $connection, LoggerInterface $logger,)
    {
        parent::__construct($connection, $logger);
    }

    public function getDescription(): string
    {
        return 'Add WxS settings';
    }

    public function up(Schema $schema): void
    {
        // SETTING WxS
        $this->addSql(
            "INSERT INTO admin.setting (lex_setting_type_id, lex_setting_category_id, module_id, name, title, description, value)
                    VALUES (1, 4, null, 'wms_server_title', 'Nom du serveur WMS', 'Nom du serveur WMS', 'WMS Demo Server'),
                        (1, 4, null, 'wms_server_abstract', 'Résumé WMS', 'Résumé WMS', 'abstract'),
                        (1, 5, null, 'wfs_server_title', 'Nom du serveur WFS', 'Nom du serveur WFS', 'WFS demo server'),
                        (1, 5, null, 'wfs_server_abstract', 'Résumé WFS', 'Résumé WFS', 'abstract')"
        );
    }

    public function down(Schema $schema): void
    {
        $this->addSql('truncate admin.setting');
    }
}
