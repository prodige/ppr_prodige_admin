<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230705102056 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Add constraint public_metadta_id unique';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        //add missing migration
        $this->addSql("alter table admin.metadata_sheet add constraint constraint_uuid unique(public_metadata_id)");
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs

        $this->addSql("alter table admin.metadata_sheet add constraint unique(public_metadata_id)");

    }
}
