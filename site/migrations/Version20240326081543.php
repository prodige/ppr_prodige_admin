<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240326081543 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Add api management for layers and uniqueness of storage path';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE admin.layer ADD api_layer BOOLEAN DEFAULT false NOT NULL');
        $this->addSql('ALTER TABLE admin.layer ADD api_fields JSON DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE admin.layer DROP api_layer');
        $this->addSql('ALTER TABLE admin.layer DROP api_fields');
    }
}
