<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231211160809 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Creation de la table datahub';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE admin.DatahubSetting (id SERIAL NOT NULL, lex_setting_type_id INT NOT NULL, name VARCHAR(128) NOT NULL, title VARCHAR(255) NOT NULL, value VARCHAR(255) NOT NULL, setting_order INT DEFAULT 0 NOT NULL, PRIMARY KEY(id), FOREIGN KEY (lex_setting_type_id) REFERENCES admin.lex_setting_type(id))');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE admin.DatahubSetting DROP CONSTRAINT FK_A71DFD47DCA6FBAC');
        $this->addSql('DROP TABLE admin.DatahubSetting');
    }
}
