<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20250123102130 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Add Url Referer in map';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE admin.map ADD url_referer character varying DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE admin.map DROP url_referer');
    }
}
