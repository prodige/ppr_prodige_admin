<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230320000000 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'migrate settings';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        //add missing migration
        
        $this->addSql("INSERT INTO admin.setting (lex_setting_type_id, lex_setting_category_id, 
                       module_id, name, title, description, value, setting_order) 
                       select  1, 2, NULL, 'PRO_DOWNLOAD_METADATA_UUID',
                       'Uuid de la métadonnée de service de téléchargment libre', 
                       'Uuid de la métadonnée de service de téléchargment libre', 
                       uuid, 8  from public.metadata where id = (select value from admin.setting where name='PRO_DOWNLOAD_METADATA_ID')::int");

        $this->addSql("INSERT INTO admin.setting (lex_setting_type_id, lex_setting_category_id, 
                       module_id, name, title, description, value, setting_order) 
                       select  1, 2, NULL, 'PRO_WMS_METADATA_UUID',
                       'Uuid de la métadonnée de service WMS', 
                       'Uuid de la métadonnée de service WMS', 
                       uuid, 9  from public.metadata where id = (select value from admin.setting where name='PRO_WMS_METADATA_ID')::int");

        $this->addSql("INSERT INTO admin.setting (lex_setting_type_id, lex_setting_category_id, 
                       module_id, name, title, description, value, setting_order) 
                       select  1, 2, NULL, 'PRO_WFS_METADATA_UUID',
                       'Uuid de la métadonnée de service WFS', 
                       'Uuid de la métadonnée de service WFS', 
                       uuid, 10  from public.metadata where id = (select value from admin.setting where name='PRO_WFS_METADATA_ID')::int");

        $this->addSql("INSERT INTO admin.setting (lex_setting_type_id, lex_setting_category_id, 
                       module_id, name, title, description, value, setting_order) 
                       select  1, 2, NULL, 'PRO_FEATURE_CATALOGUE_METADATA_UUID',
                       'Uuid de la métadonnée modèle de catalogue d''attribut', 
                       'Uuid de la métadonnée modèle de catalogue d''attribut', 
                       uuid, 11  from public.metadata  where isharvested='n' and istemplate='y' and schemaid='iso19110' order by id limit 1");


        
        
        
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql("delete from  admin.setting where name in ('PRO_DOWNLOAD_METADATA_UUID','PRO_WMS_METADATA_UUID','PRO_WFS_METADATA_UUID','PRO_FEATURE_CATALOGUE_METADATA_UUID')");

    }
}
