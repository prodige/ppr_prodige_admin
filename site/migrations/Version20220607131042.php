<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220607131042 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {

        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE admin.engine DROP isatom');
        $this->addSql('ALTER TABLE admin.engine ALTER join_id DROP NOT NULL');
        $this->addSql('UPDATE admin.engine SET join_id = NULL');
        $this->addSql('ALTER TABLE admin.engine ADD CONSTRAINT FK_FF7FF0EB9DD3F874 FOREIGN KEY (join_id) REFERENCES admin.engine (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_FF7FF0EB9DD3F874 ON admin.engine (join_id)');
        $this->addSql('ALTER TABLE admin.help DROP row_number');
        $this->addSql('ALTER TABLE admin.help ALTER title TYPE VARCHAR(255)');
        $this->addSql('ALTER TABLE admin.help ALTER title DROP DEFAULT');
        $this->addSql('SELECT setval(\'admin.lex_setting_category_id_seq\', (SELECT MAX(id) FROM admin.lex_setting_category))');
        $this->addSql('ALTER TABLE admin.lex_setting_category ALTER id SET DEFAULT nextval(\'admin.lex_setting_category_id_seq\')');
        $this->addSql('SELECT setval(\'admin.lex_setting_type_id_seq\', (SELECT MAX(id) FROM admin.lex_setting_type))');
        $this->addSql('ALTER TABLE admin.lex_setting_type ALTER id SET DEFAULT nextval(\'admin.lex_setting_type_id_seq\')');
        $this->addSql('ALTER TABLE admin.map DROP CONSTRAINT map_public_metadata_wms_fkey');
        $this->addSql('ALTER TABLE admin.metadata_sheet DROP CONSTRAINT metadata_sheet_public_metadata_id_fkey');
        $this->addSql('ALTER INDEX admin.idx_b3da4d59ccfa12b8 RENAME TO IDX_C2506D20CCFA12B8');
        $this->addSql('ALTER INDEX admin.idx_b3da4d59ea6efdcd RENAME TO IDX_C2506D20EA6EFDCD');
        $this->addSql('ALTER INDEX admin.idx_b3da4d59bd83edc0 RENAME TO IDX_C2506D20BD83EDC0');
        $this->addSql('ALTER INDEX admin.idx_b3da4d59aa4c67a1 RENAME TO IDX_C2506D20AA4C67A1');
        $this->addSql('ALTER TABLE admin.structure ALTER name SET NOT NULL');
        $this->addSql('ALTER TABLE admin.user_area_access DROP CONSTRAINT FK_14B9B144A76ED395');
        $this->addSql('ALTER TABLE admin.user_area_access DROP CONSTRAINT FK_14B9B144BD0F409C');
        $this->addSql('ALTER TABLE admin.user_area_access ADD CONSTRAINT FK_14B9B144A76ED395 FOREIGN KEY (user_id) REFERENCES admin.users (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE admin.user_area_access ADD CONSTRAINT FK_14B9B144BD0F409C FOREIGN KEY (area_id) REFERENCES admin.area (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE admin.module ADD activated BOOLEAN DEFAULT false NOT NULL');
        $this->addSql('ALTER TABLE admin.engine DROP CONSTRAINT FK_FF7FF0EB9DD3F874');
        $this->addSql('DROP INDEX IDX_FF7FF0EB9DD3F874');
        $this->addSql('ALTER TABLE admin.engine ADD isatom BOOLEAN NOT NULL');
        $this->addSql('ALTER TABLE admin.engine ALTER join_id SET NOT NULL');
        $this->addSql('ALTER TABLE admin.metadata_sheet ADD CONSTRAINT metadata_sheet_public_metadata_id_fkey FOREIGN KEY (public_metadata_id) REFERENCES metadata (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_9F01EE0EF076628 ON admin.metadata_sheet (public_metadata_id)');
        $this->addSql('ALTER TABLE admin.map ADD CONSTRAINT map_public_metadata_wms_fkey FOREIGN KEY (public_metadata_wms) REFERENCES metadata (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_36293E37E92E55A4 ON admin.map (public_metadata_wms)');
        $this->addSql('ALTER TABLE admin.help ADD row_number INT DEFAULT NULL');
        $this->addSql('ALTER TABLE admin.help ALTER title TYPE TEXT');
        $this->addSql('ALTER TABLE admin.help ALTER title DROP DEFAULT');
        $this->addSql('ALTER INDEX admin.idx_c2506d20ea6efdcd RENAME TO idx_b3da4d59ea6efdcd');
        $this->addSql('ALTER INDEX admin.idx_c2506d20ccfa12b8 RENAME TO idx_b3da4d59ccfa12b8');
        $this->addSql('ALTER INDEX admin.idx_c2506d20aa4c67a1 RENAME TO idx_b3da4d59aa4c67a1');
        $this->addSql('ALTER INDEX admin.idx_c2506d20bd83edc0 RENAME TO idx_b3da4d59bd83edc0');
        $this->addSql('ALTER TABLE admin.lex_setting_type ALTER id DROP DEFAULT');
        $this->addSql('ALTER TABLE admin.lex_setting_category ALTER id DROP DEFAULT');
        $this->addSql('ALTER TABLE admin.structure ALTER name DROP NOT NULL');
        $this->addSql('ALTER TABLE admin.user_area_access DROP CONSTRAINT fk_14b9b144bd0f409c');
        $this->addSql('ALTER TABLE admin.user_area_access DROP CONSTRAINT fk_14b9b144a76ed395');
        $this->addSql('ALTER TABLE admin.user_area_access ADD CONSTRAINT fk_14b9b144bd0f409c FOREIGN KEY (area_id) REFERENCES admin.area (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE admin.user_area_access ADD CONSTRAINT fk_14b9b144a76ed395 FOREIGN KEY (user_id) REFERENCES admin.users (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
    }
}
