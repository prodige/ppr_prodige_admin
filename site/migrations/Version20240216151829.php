<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240216151829 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Adding geojson tags to xml files';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $tabMetadata = $this->connection->fetchAllAssociative(
            "SELECT m.public_metadata_id, md.data,md.uuid
                    FROM admin.layer l
                    INNER JOIN admin.metadata_sheet m ON m.id = l.metadata_sheet_id   
                    INNER JOIN public.metadata md ON md.id=m.public_metadata_id
                    WHERE lex_layer_type_id IN (1,3,4) AND md.data NOT LIKE '%WWW:DOWNLOAD:application/vnd.geo+json%';"
        );

        foreach ($tabMetadata as $metadatum) {
            $addLink = $this->addGeojsonLink($metadatum['data'], $metadatum['public_metadata_id'], $metadatum['uuid']);
            $this->addSql(
                "update public.metadata SET data =:data where id=:id",
                ['id' => $metadatum['public_metadata_id'], 'data' => $addLink['data']]
            );
        }
    }

    protected function addGeojsonLink($data, $publicId, $uuid)
    {
        $version = "1.0";
        $encoding = "UTF-8";
        $metadata_doc = new \DOMDocument($version, $encoding);
        $entete = "<?xml version=\"" . $version . "\" encoding=\"" . $encoding . "\"?>\n";
        $metadata_data = $entete . $data;

        $metadata_doc->loadXML($metadata_data);
        $addStrXML =
            "<tag xmlns:gmd=\"http://www.isotc211.org/2005/gmd\" xmlns:gco=\"http://www.isotc211.org/2005/gco\">                                    
                        <gmd:onLine>
                            <gmd:CI_OnlineResource>
                                <gmd:linkage>
                                    <gmd:URL>" . htmlspecialchars(
                $_ENV['PRODIGE_URL_CATALOGUE']
                . "/api/data/" . $uuid . "?srs=EPSG:4326"
            ) . "</gmd:URL>
                                </gmd:linkage>
                            <gmd:protocol>
                                <gco:CharacterString>WWW:DOWNLOAD:application/vnd.geo+json</gco:CharacterString>
                            </gmd:protocol>
                            <gmd:name>
                               <gco:CharacterString>API Geojson</gco:CharacterString>
                            </gmd:name>
                             <gmd:description>
                        <gco:CharacterString>API au format geojson</gco:CharacterString>
                     </gmd:description>
                     <gmd:function>
                        <gmd:CI_OnLineFunctionCode codeList=\"http://standards.iso.org/iso/19139/resources/gmxCodelists.xml#CI_OnLineFunctionCode\"
                                                   codeListValue=\"download\"/>
                     </gmd:function>
                     </gmd:CI_OnlineResource>
                            </gmd:onLine>                                        
                    </tag>";


        $orgDoc = new \DOMDocument;
        $orgDoc->loadXML($addStrXML);

        $xpath = new \DOMXPath($metadata_doc);
        $transfertOption = $xpath->query(
            "/gmd:MD_Metadata/gmd:distributionInfo/gmd:MD_Distribution/gmd:transferOptions/gmd:MD_DigitalTransferOptions"
        );
        if ($transfertOption->length > 0) {
            $nodeToImport = $orgDoc->getElementsByTagName("onLine")->item(0);
            $newNodeDesc = $metadata_doc->importNode($nodeToImport, true);
            $transfertOption->item(0)->appendChild($newNodeDesc);
        }
        $new_metadata_data = $metadata_doc->saveXML();
        $new_metadata_data = str_replace($entete, "", $new_metadata_data);
        return ['data' => $new_metadata_data, 'public_id' => $publicId];
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
