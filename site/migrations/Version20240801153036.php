<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240801153036 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Fix default installation';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE admin.users ADD is_default_installation BOOLEAN NOT NULL DEFAULT FALSE');
        $this->addSql("UPDATE admin.users SET is_default_installation = TRUE WHERE login = 'admincli' OR login = 'vinternet' OR login = 'admin@prodige-opensource.org'");
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE admin.users DROP is_default_installation');
    }
}
