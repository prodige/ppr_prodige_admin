<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230720142425 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Add Dataviz';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql("CREATE TABLE admin.dataviz (id SERIAL NOT NULL, metadata_sheet_id INT DEFAULT NULL, layer_id INT DEFAULT NULL, user_id INT DEFAULT NULL, lex_dataviz_type_id INT DEFAULT NULL, title VARCHAR(255) NOT NULL, description TEXT DEFAULT NULL, param_traitement JSON DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))");
        $this->addSql("CREATE INDEX IDX_5EA0F2B738E60D5B ON admin.dataviz (metadata_sheet_id)");
        $this->addSql("ALTER TABLE admin.dataviz ADD param_chart JSON DEFAULT NULL;");
        $this->addSql("CREATE INDEX IDX_5EA0F2B7EA6EFDCD ON admin.dataviz (layer_id)");
        $this->addSql("CREATE INDEX IDX_5EA0F2B7A76ED395 ON admin.dataviz (user_id)");
        $this->addSql("CREATE INDEX IDX_5EA0F2B7C8FF05C ON admin.dataviz (lex_dataviz_type_id)");
        $this->addSql("CREATE TABLE admin.lex_dataviz_type (id SERIAL NOT NULL, name VARCHAR(128) NOT NULL, description TEXT DEFAULT NULL, PRIMARY KEY(id))");
        $this->addSql("CREATE UNIQUE INDEX UNIQ_EE28C2615E237E06 ON admin.lex_dataviz_type (name)");
        $this->addSql("ALTER TABLE admin.dataviz ADD CONSTRAINT FK_5EA0F2B738E60D5B FOREIGN KEY (metadata_sheet_id) REFERENCES admin.metadata_sheet (id) NOT DEFERRABLE INITIALLY IMMEDIATE");
        $this->addSql("ALTER TABLE admin.dataviz ADD CONSTRAINT FK_5EA0F2B7EA6EFDCD FOREIGN KEY (layer_id) REFERENCES admin.layer (id) NOT DEFERRABLE INITIALLY IMMEDIATE");
        $this->addSql("ALTER TABLE admin.dataviz ADD CONSTRAINT FK_5EA0F2B7A76ED395 FOREIGN KEY (user_id) REFERENCES admin.users (id) NOT DEFERRABLE INITIALLY IMMEDIATE");
        $this->addSql("ALTER TABLE admin.dataviz ADD CONSTRAINT FK_5EA0F2B7C8FF05C FOREIGN KEY (lex_dataviz_type_id) REFERENCES admin.lex_dataviz_type (id) NOT DEFERRABLE INITIALLY IMMEDIATE");
        $this->addSql("INSERT INTO admin.lex_dataviz_type(name, description) VALUES ('MULTI SET BAR', NULL), ('LINE',NULL), ('AREA',NULL),
                        ('MULTI SERIES PIE',NULL),('MULTI SERIES DOUGHNUT',NULL),('POLAR AREA',NULL),('RADAR',NULL), ('BUBBLE',NULL)");

    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql("CREATE SCHEMA public");
        $this->addSql("ALTER TABLE admin.dataviz DROP CONSTRAINT FK_5EA0F2B738E60D5B");
        $this->addSql("ALTER TABLE admin.dataviz DROP CONSTRAINT FK_5EA0F2B7EA6EFDCD");
        $this->addSql("ALTER TABLE admin.dataviz DROP CONSTRAINT FK_5EA0F2B7A76ED395");
        $this->addSql("ALTER TABLE admin.dataviz DROP CONSTRAINT FK_5EA0F2B7C8FF05C");
        $this->addSql("DROP TABLE admin.dataviz");
        $this->addSql("DROP TABLE admin.lex_dataviz_type");
    }
}
