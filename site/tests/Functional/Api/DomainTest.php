<?php

declare(strict_types=1);

namespace App\Tests\Functional\Api;

use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\ApiTestCase;
use App\Entity\Administration\Organisation\Domain;
use App\Tests\AbstractTest;
use App\Tests\RefreshDatabaseTrait;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class DomainTest extends AbstractTest
{
    // This trait provided by AliceBundle will take care of refreshing the database content to a known state before each test
//    use RefreshDatabaseTrait;

    private string $url = '/api/domains';
    private string $resource = 'Domain';
    private int $paginationLimit = 10;
    private string $token = "";

    public function __construct(?string $name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
//        $this->token = "eyJhbGciOiJub25lIiwib3JnLmFwZXJlby5jYXMuc2VydmljZXMuUmVnaXN0ZXJlZFNlcnZpY2UiOjE1MDIwLCJ0eXAiOiJKV1QifQ.eyJzdWIiOiJhZG1pbkBwcm9kaWdlLmZyIiwib2F1dGhDbGllbnRJZCI6ImFkbWluIiwicm9sZXMiOltdLCJpc3MiOiJodHRwczpcL1wvY2FzLnByb2RpZ2UuaW50ZXJuYWxcLyIsIm5vbmNlIjoiIiwiY2xpZW50X2lkIjoiYWRtaW4iLCJhdWQiOiJhZG1pbiIsImdyYW50X3R5cGUiOiJQQVNTV09SRCIsInBlcm1pc3Npb25zIjpbXSwic2NvcGUiOltdLCJzY29wZXMiOltdLCJzdGF0ZSI6IiIsImV4cCI6MTY1MDYzOTIyNSwiaWF0IjoxNjUwNjM4NjI1LCJqdGkiOiJBVC0yMS15RG5DMUxoZU9MTTEtWHFrbzlEY2VRQzMxbVRyZEhtbiJ9.";
//        $this->getToken();
    }

//    private function getToken(): void
//    {
//        dump($this->token);
//        $response = static::createClient()->request(
//            'POST',
//            '/api/login',
//            [
//                'headers' => [
//                    'Content-Type' => 'application/ld+json'
//                ],
//                'json' => [
//                    "login" => "admin@prodige-opensource.org",
//                    "password" => "test"
//                ]
//            ]
//        );
//        $this->token = json_decode($response->getContent())->access_token;
//        dump($this->token);
//        dump('==================');
//        sleep(1);
//    }

    public function testUn(): void
    {
        $this->assertCount(1, [1]);
        $this->assertCount(2, [1, 2]);
        $this->assertCount(3, [1, 1, 1]);
    }

    public function testDeux(): void
    {
        $this->assertCount(1, [1]);
        $this->assertCount(2, [1, 2]);
    }

    public function testGetCollection(): void
    {
        $token = $this->getToken();

//        $response = $this->createClientWithCredentials($token)->request('GET', '/users');

        // The client implements Symfony HttpClient's `HttpClientInterface`, and the response `ResponseInterface`
        $response = $this->createClientWithCredentials($token)->request(
            'GET',
            $this->url
        );

        $this->assertResponseIsSuccessful();
        // Asserts that the returned content type is JSON-LD (the default)
        $this->assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');

        // Asserts that the returned JSON is a superset of this one
        $this->assertJsonContains([
            '@context' => '/api/contexts/Domain',
            '@id' => $this->url,
            '@type' => 'hydra:Collection',
//            'hydra:totalItems' => 100,
//            'hydra:view' => [
//                '@id' => $this->url . '?page=1',
//                '@type' => 'hydra:PartialCollectionView',
//                'hydra:first' => $this->url . '?page=1',
////                'hydra:last' => $this->url . '?page=10',
//                'hydra:next' => $this->url . '?page=2',
//            ]
        ]);

        // Because test fixtures are automatically loaded between each test, you can assert on them
//        $this->assertCount($this->paginationLimit, $response->toArray()['hydra:member']);

        // Asserts that the returned JSON is validated by the JSON Schema generated for this resource by API Platform
        // This generated JSON Schema is also used in the OpenAPI spec!
        $this->assertMatchesResourceCollectionJsonSchema(Domain::class);
        sleep(1);
    }

//    public function testGetDomain(): void
//    {
//        // The client implements Symfony HttpClient's `HttpClientInterface`, and the response `ResponseInterface`
//        $response = static::createClient()->request(
//            'GET',
//            $this->url . '/48',
//            ['headers' => ['Authorization' => 'Bearer ' . $this->token]]
//        );
//
//        $this->assertResponseIsSuccessful();
//        // Asserts that the returned content type is JSON-LD (the default)
//        $this->assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');
//
//        // Asserts that the returned JSON is a superset of this one
//        $this->assertJsonContains([
//            '@context' => '/api/contexts/Domain',
////            '@id' => $this->url . '/1',
//            '@type' => 'Domain',
////            'id' => 1,
////            'name' => 'aName',
////            'rubric' => [
////                '@id' => '/api/rubrics/1'
////            ]
//        ]);
//        sleep(1);
//        // Asserts that the returned JSON is validated by the JSON Schema generated for this resource by API Platform
//        // This generated JSON Schema is also used in the OpenAPI spec!
//        $this->assertMatchesResourceItemJsonSchema(Domain::class);
//    }

//    public function testGetDomainNotFound(): void
//    {
//        // The client implements Symfony HttpClient's `HttpClientInterface`, and the response `ResponseInterface`
//        $response = static::createClient()->request('GET', $this->url . '/99999');
//
//        $this->assertResponseStatusCodeSame(404);
//        $this->assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');
//
//        $this->assertJsonContains([
//            '@context' => '/api/contexts/Error',
//            '@type' => 'hydra:Error',
//            'hydra:title' => 'An error occurred',
//            'hydra:description' => 'Not Found'
//        ]);
//    }
//
//    public function testCreateDomain(): void
//    {
//        $response = static::createClient()->request('POST', $this->url, [
//            'headers' => ['content-type' => 'application/ld+json'],
//            'json' => [
//                'name' => 'aDomain',
//                'description' => 'a random description !',
//                'rubric' => '/api/rubrics/1'
//            ]
//        ]);
//
//        $this->assertResponseStatusCodeSame(201);
//        $this->assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');
//        $this->assertJsonContains([
//            '@context' => '/api/contexts/Domain',
//            '@type' => $this->resource,
//            'name' => 'aDomain',
//            'description' => 'a random description !',
//            'rubric' => ['@id' => '/api/rubrics/1']
//        ]);
//        $this->assertMatchesRegularExpression('~^/api/domains/\d+$~', $response->toArray()['@id']);
//        $this->assertMatchesResourceItemJsonSchema(Domain::class);
//    }
//
//    public function testCreateDomainWithoutName(): void
//    {
//        $response = static::createClient()->request('POST', $this->url, [
//            'headers' => [
//                'content-type' => 'application/ld+json'
//            ],
//            'json' => [
//                'description' => 'a random description !',
//                'rubric' => '/api/rubrics/1'
//            ]
//        ]);
//
//        $this->assertResponseStatusCodeSame(422);
//        $this->assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');
//
//        $this->assertJsonContains([
//            '@context' => '/api/contexts/ConstraintViolationList',
//            '@type' => 'ConstraintViolationList',
//            'hydra:title' => 'An error occurred',
//            'hydra:description' => 'name: This value should not be blank.'
//        ]);
//    }
//
//    public function testCreateDomainWithRubricNotFound(): void
//    {
//        static::createClient()->request('POST', $this->url, [
//            'headers' => [
//                'content-type' => 'application/ld+json'
//            ],
//            'json' => [
//                'name' => 'aDomain',
//                'description' => 'a random description !',
//                'rubric' => '/api/rubrics/999999'
//            ]
//        ]);
//
//        $this->assertResponseStatusCodeSame(400);
//        $this->assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');
//
//        $this->assertJsonContains([
//            '@context' => '/api/contexts/Error',
//            '@type' => 'hydra:Error',
//            'hydra:title' => 'An error occurred',
//            'hydra:description' => 'Item not found for "/api/rubrics/999999".'
//        ]);
//    }
//
//    public function testUpdateDomain(): void
//    {
//        $client = static::createClient();
//        // findIriBy allows to retrieve the IRI of an item by searching for some of its properties.
//        $iri = $this->findIriBy(Domain::class, ['name' => 'aName']);
//
//        $client->request('PUT', $iri, [
//            'headers' => [
//                'content-type' => 'application/ld+json'
//            ],
//            'json' => [
//                'name' => 'updated name',
//            ]
//        ]);
//
//        $this->assertResponseIsSuccessful();
//        $this->assertJsonContains([
//            '@id' => $iri,
//            'name' => 'updated name',
//        ]);
//    }
//
//    public function testDeleteBook(): void
//    {
//        $client = static::createClient();
//        $iri = $this->findIriBy(Domain::class, ['name' => 'aName']);
//        $client->request(
//            'DELETE', $iri,
//            [
//                'headers' => ['content-type' => 'application/ld+json']
//            ]
//        );
//
//        $this->assertResponseStatusCodeSame(204);
//        $this->assertNull(
//        // Through the container, you can access all your services from the tests, including the ORM, the mailer, remote API clients...
//            static::$container->get('doctrine')->getRepository(Domain::class)->findOneBy(['name' => 'aName'])
//        );
//    }

//    public function testLogin(): void
//    {
//        $response = static::createClient()->request('POST', '/login', [
//            'json' => [
//                'email' => 'admin@example.com',
//                'password' => 'admin',
//            ]
//        ]);
//
//        $this->assertResponseIsSuccessful();
//    }
}