<?php

namespace App\Tests;

use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\ApiTestCase;
use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\Client;
use Hautelook\AliceBundle\PhpUnit\RefreshDatabaseTrait;

abstract class AbstractTest extends ApiTestCase
{
    private ?string $token = null;
    private Client $clientWithCredentials;

//    use RefreshDatabaseTrait;

    public function setUp(): void
    {
        self::bootKernel();
    }

    protected function createClientWithCredentials($token = null): Client
    {
        $token = $token ?: $this->getToken();

        return static::createClient([], [
            'headers' => [
                'authorization' => 'Bearer ' . $token,
                'Accept' => 'application/ld+json'
            ]
        ]);
    }

    /**
     * Use other credentials if needed.
     */
    protected function getToken($body = []): string
    {
        if (!is_null($this->token)) {
            return $this->token;
        }

        $response = static::createClient()->request('POST', '/api/login', [
            'headers' => [
                'Content-Type' => 'application/ld+json'
            ],
            'json' => $body ?: [
                'login' => 'admin@prodige-opensource.org',
                'password' => 'test',
            ]
        ]);

        $this->assertResponseIsSuccessful();
        $data = json_decode($response->getContent());
        $this->token = $data->access_token;

        return $data->access_token;
    }
}