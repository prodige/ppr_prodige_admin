<?php

/*
 * This file is part of the Hautelook\AliceBundle package.
 *
 * (c) Baldur Rensch <brensch@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace App\Tests;

use Hautelook\AliceBundle\PhpUnit\BaseDatabaseTrait;
use Symfony\Component\HttpKernel\KernelInterface;

/**
 * Purges and loads the fixtures before the first test and wraps all test in a transaction that will be roll backed when
 * it has finished.
 *
 * @author Kévin Dunglas <dunglas@gmail.com>
 */
trait RefreshDatabaseTrait
{
    use BaseDatabaseTrait;

    protected static bool $dbPopulated = false;

    protected static function bootKernel(array $options = []): KernelInterface
    {
        static::ensureKernelTestCase();
        $kernel = parent::bootKernel($options);
        $container = static::$kernel->getContainer();

        if (!static::$dbPopulated) {
            $conn = $container->get("doctrine")->getConnection($connection_name);
            $list = $conn->executeQuery(
                "select relname
                    FROM pg_class AS c
                    JOIN pg_namespace AS ns ON c.relnamespace = ns.oid
                    where nspname = 'admin' and relkind='S'
                    order by relname;"
            )->fetchAll();

            if ($list == true) {
                $delete = "";
                foreach ($list as $sequence) {
                    $delete .= "ALTER SEQUENCE admin." . $sequence["relname"] . " RESTART WITH 1;";
                }
                $conn->exec($delete);
            }

            static::populateDatabase();
            static::$dbPopulated = true;
        }


        $container->get('doctrine')->getConnection(static::$connection)->beginTransaction();

        return $kernel;
    }

    protected static function ensureKernelShutdown(): void
    {
        $container = null;
        if (null === $container && null !== static::$kernel) {
            $container = static::$kernel->getContainer();
        }

        if (null !== $container) {
            $connection = $container->get('doctrine')->getConnection(static::$connection);
            if ($connection->isTransactionActive()) {
                $connection->rollback();
            }
        }

        parent::ensureKernelShutdown();
    }
}
